# Exposés

## Préambule

Les exposés permettent de mettre en oeuvre les capacités :
- **Rechercher** de l'information, **apprendre à utiliser des sources** de qualité,
- __Apporter__ à vos camarades -- qui auront travaillé un autre thème -- les connaissances, les exemples nécessaires à la __compréhension du thème__ que vous aurez abordé,
- __Coopérer__ au sein d’une équipe.

## Consignes

- Par __binôme__, vous aurez à produire un exposé d'une durée de __6-8 minutes maximum__, cet exposé sera à choisir parmi les différentes thématiques en lien avec le programme. Il sera __présenté à l'oral__ devant vos camarades de classe,
- À l'issue de l'oral, 5-10 minutes seront consacrées à un __échange sur la thématique__,
- Vous devez produite un __support numérique pour l'oral__ : diaporama Powerpoint, Impress, petit site web...

## Liste de sujets

Je vous propose la liste de sujets suivants. Vous pouvez aussi me proposer un autre sujet en lien avec l'informatique.

| Id | Thème | Intitulé | Problématiques possibles à aborder |
| :--: | :-----: | :-------- | :---------------------------------- |
| 1 | Données structurées | Stockage  des  données | - Qu'est-ce que le big data ?<br />- À quoi servent nos données ?<br />- Quels sont les impacts énergétiques et environnementaux des data centers ? |
| 2 | Internet |  Les réseaux pair-à-pair | - Présentation et fonctionnement <br />- Les usages licites et illicites du pair-à-pair.<br />- Qu’est-ce qu’Hadopi  ?<br />- Les bénéfices des usages légaux du pair-à-pair |
| 3 | Internet | La neutralité du Net | - Comment définir le principe de la neutralité du net ?<br />- Quels sont ces bienfaits ?<br />- Pourquoi et comment est-il remis en cause ? |
| 4 | Géolocalisation | Sécurité ou danger ? | - Quels sont les usages de la géolocalisation ?<br/>- Avantages et dérives possibles du partage de la géolocalisation ? |
| 5 | Informatique embarquée | L'Internet des objets | - Quel est son but ?<br />- Quelques exemples d'utilisation<br />- Quelles problématiques soulève-t-il ? |
| 6 | Web | Web et droit | - Dans quelle mesure puis-je utiliser et/ou modifier ce que je trouve sur le Web ?<br />- Comment sont appliqués les droits d'auteur ?<br />- Qu'est-ce que les logiciels et licences libres ? |
| 7 | Web | Les cookies | - Qu'est-ce qu'un cookie ?<br />- À quoi sert-il ?<br />- Quelles problématiques soulève les cookies tierce partie ? |
| 8 | Web | La désinformation sur le Web | - Qu'est-ce qu'une désinformation ?<br />- Quelle.s forme.s peut-elle prendre ?<br/>- Comment s'en prémunir ? |
| 9 | Réseaux sociaux | La cyberviolence | - Quelles sont les formes de cyberviolence sur les réseaux sociaux ?<br />- Quelles sont les conséquences pour les victimes et agresseurs ?<br />- Que faire face à une situation de cyberviolence ? Comment les éviter ? |
| 10 | Réseaux sociaux | Les Phénomènes viraux : Ice Bucket Challenge / Mannequin Challenge / #MeToo ... | - En quoi consistent-ils ?<br />- Comment deviennent-ils viraux ?<br />- Quels sont leurs impacts positifs / négatifs sur la société ? |
| 11 | Réseaux sociaux | Le modèle économique des réseaux sociaux | - Comment se financent les différents réseaux sociaux ?<br />- Quels sont les objectifs attendus des réseaux sociaux ? |
| 12 | Société | L'informatique : un domaine genré ? | - Historique des personnages de l'informatique<br />- La représentation homme / femme est-elle égalitaire ? Causes<br />- Quelques exemples de décision pour l'égalité ? |
| 13 | Société | Le vote électronique | - Quels sont les avantages ? inconvénients ?<br />- Quelques exemples  de mise en place de ce système ? |
| 14 | Société | Le Mème | - Qu'est-ce qu'un mème ?<br />- Quelles sont ses origines ?<br/>- Quelle est son utilité ?<br/>- Le mème, un objet libre de droit ? |
| 15 | Société | Intelligence Artificielle : Généralités | - Comment se définit l'IA ?<br />- Historique de ce domaine<br/>- Quelques exemples d'applications |
| 16 | Société | Intelligence Artificielle : Les biais | - Qu'est-ce qu'un biais ?<br />- Quels sont-ils concernant l'IA ?<br />- Quels impacts ont les biais ? |
| 17 | Société | Film : Imitation Game | - Quel est le protagoniste de ce film ? <br />- Que relate le film ? <br/>- Quels ont été les apports du protagoniste à l'informatique ? à la société ? |
| 18 | Société | Le streaming : Que de la vidéo en live ? | - Comment devient-on streameur ?<br />- Quel est le modèle économique du streaming ?<br />- Quels sont les impacts du streaming sur la santé ? l'environnement ? société ? |

## Évaluation

- Le __contenu__, la __présentation__ orale, l'__échange__ questions-réponses seront autant d'éléments d'évaluation de votre travail. 
- Le barème d'évaluation est disponible [ici](./bareme.pdf), la note est __individuelle__ sur 10.
- Les sources doivent être __explicitées et variées__.

## Recommandations

- __Premier réflexe__ pour les exposés : aller sur wikipédia chercher les définitions des termes cités.
- Sur internet, on trouve beaucoup de résultats… beaucoup trop ! Il faut absolument éviter les sites conspirationistes. __Vos sources doivent être citées__ (url, date, auteur).
- __Multiplier les sources__ : magazine, article sur internet, vidéo, livre, journal papier... Il est important de multiplier les supports et sources d'informations pour obtenir une vision globale du sujet,
- Sur des sujets __clivants__ (c’est à dire pouvant donner lieu à des avis opposés), il est important de __présenter les deux côtés du débat__. Si l’on présente un résumé d’une page ou d’un article donnant un avis fort, il est important de préciser qu’il peut exister des opinions opposées, et si possible de les présenter aussi.

## Ressources

- [Site interstices](https://interstices.info/), Interstices est une revue de culture scientifique en ligne, créée par des scientifiques pour vous inviter à explorer les sciences du numérique.
- [Site le blob](https://leblob.fr/), Le blob propose une nouvelle vidéo à la une chaque jour, avec un fil d’actualité scientifique quotidien et des enquêtes mensuelles sur les grands enjeux contemporains.
- [Site Sciences et Avenir](https://www.sciencesetavenir.fr/high-tech/informatique/), Site du magazine mensuel français de vulgarisation scientifique.