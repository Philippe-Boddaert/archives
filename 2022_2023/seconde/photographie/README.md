---
title : La photographie numérique
author : Philippe Boddaert
license: CC-BY-NC-SA 
---
# La photographie numérique

![](./assets/1200.png)

C’est le nombre de photos réalisées sur notre planète en 2017. 

Elles ont été réalisées à __85%__ avec des __smartphones__, 5% avec des tablettes, et seulement 10% avec des appareils photos (selon le site quechoisir, 05/11/2017).

## 1. Contexte

Jusqu’à l’arrivée de la photo numérique, la photo n’existait que sous sa __forme physique__ : négatifs, positifs, tirages papier, archives vouées à la sauvegarde de la mémoire, à l’information ou à l’art.

| | |
| :--: | :--: |
| ![](./assets/camera.jpg)<br/>__Camera obscura__ de Nièpce | ![](./assets/daguerre.jpg) <br/>Utilisation d'un __daguerréotype__ |
| ![](./assets/polaroid.jpg)<br/>Appareil __Polaroid__ | ![](./assets/pellicule.jpg)<br/>__Pellicule__ photo |
| ![](./assets/reflex.png)<br/>Appareil photo __numérique__ | ![](./assets/smartphone.png)<br/>__Smartphone__ |

La photographie numérique et sa généralisation ont considérablement changé nos pratiques et notre approche de la photo. La gratuité, l’immédiateté, la réplique facile des images ainsi que l’usage des smartphones ont formalisé et accru nos usages. On prend en photo sa place de parking, son assiette au restaurant, le tableau du prof...

## 2. Programme

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce qu'une image numérique ? Quelles sont ses caractéristiques ? [Activité 1](./activite_1.pdf)
- Comment née une image en couleur ?
- Comment stocker une image dans un ordinateur ?
- Quels traitements est-il possible d'effectuer sur une image numérique ?
- Une image numérique contient-elle seulement que des données visuelles ?