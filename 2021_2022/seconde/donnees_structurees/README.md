---
title : Les données structurées
author : M.BODDAERT
license: CC-BY-NC-SA 
---
# Les données structurées

## Présentation

Le nombre de places dans une voiture, l'emplacement de la Place Masséna à Nice, votre nom, les paroles de la Marseillaise, la température à la surface du soleil...Ces éléments, disparates en apparence, ont en commun d'être des __données__.

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce qu'une __donnée structurée__ ?
- Comment les __traiter__ numériquement ?

## Progression

```mermaid
flowchart LR;
    A[1. Définition]-->B[2. Activité - Fruits & Castor];
    B-->C[3. Activité - Musique & Castor];
    B-->D[4. Activité - Don du sang];
    
    click A "./donnees_structurees/definition" "Lien vers 1. Définition"
    click B "./donnees_structurees/fruits" "Lien vers 2. Activité - Fruits & Castor"
    click D "./donnees_structurees/don" "Lien vers 4. Activité - Don du sang"
    click C "./donnees_structurees/musique" "Lien vers 3. Activité - Musique & Castor"
```

## Attendus du Programme

| Contenus                            | Capacités attendues                                          |
| ----------------------------------- | ------------------------------------------------------------ |
| Données                             | Définir une donnée personnelle.<br />Identifier les principaux formats et représentations de données. |
| Données structurées                 | Identifier les différents descripteurs d’un objet.<br />Distinguer la valeur d’une donnée de son descripteur.<br/>Utiliser un site de données ouvertes, pour sélectionner et récupérer des données. |
| Traitement de données structurées   | Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables. |
| Métadonnées                         | Retrouver les métadonnées d’un fichier personnel.            |
| Données dans le nuage (cloud) | Utiliser un support de stockage dans le nuage.<br />Partager des fichiers, paramétrer des modes de synchronisation.<br/>Identifier les principales causes de la consommation énergétique des centres de données ainsi que leur ordre de grandeur. |

