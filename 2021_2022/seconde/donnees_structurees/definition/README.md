---
title : Les données en machine
author : M. BODDAERT
license : CC-BY-NC-SA
---
# Les données en machine

## 1. Contexte

**Enregistrer des données**, pour s'en souvenir, prévoir, faire des calculs est une activité très ancienne. Tous les supports d'écriture (tablettes d'argile, parchemins, livres, cartes perforées, mémoire d'ordinateurs) ont été utilisés successivement pour noter des tableaux de données.

## 2. Historique

- **Vers 2400 av-JC** : Les Sumériens utilisent des tablettes d'argile pour graver leurs documents de comptabilité (vente de terrain, partage d'héritage...)

![](./assets/tablette.jpeg)

_Source : pngwing_

- **1454** : Invention de l'imprimerie par Gutenberg, qui permet de conserver les écrits sur du papier, le copier et le diffuser rapidement.

![](./assets/imprimerie.jpeg)

- **Vers 1930**: Les **cartes perforées** inventées à la fin du XIX e siècle deviennent le principal support pour stocker des données. Elles sont utilisées jusque dans les années 1980 pour entrer des programmes et des données dans les ordinateurs. Il fallait des cartons entiers de cartes pour écrire certains programmes.

![](./assets/carte_perfo.jpeg)

- **1956**: Création par IBM du premier **disque dur magnétique**. Il pèse 1 tonne et a une capacité de stockage de 5 Mo (1Mo= $`10^6`$ soit 1 million d'octets). Aujourd'hui, un disque dur classique sur un ordinateur personnel a une capacité de 1 Teraoctet...(1To=$`10^{12}`$ soit mille milliards d'octets).

![](./assets/disque_dur.png)

_Source : IBM_

- **1984** : Mise sur le marché de la **mémoire flash** par Toshiba. Ce type de mémoire rapide et sans pièce mécanique est utilisé dans les clés USB, les appareils photos et remplace progressivement les disques durs.

![](./assets/flash.jpeg)

_Source : lebigdata.fr_

- __1999__ : création du premier service de stockage en ligne via un data center (par Salesforce.com)

__Question : Comment est stockée une information dans une machine ?__

## 3. Principe de fonctionnement du stockage dans une machine

### 3.1. Système binaire

Si les supports sont multiples et divers dans une machine, le principe de gestion de la mémoire reste le même.

Une machine est __binaire__ et Il est possible de voir sa mémoire comme un ruban de grande taille divisé en cellules contenant chacune une valeur (soit 0, soit 1).

![](./assets/bit.png)

On parle alors de __bit__, _binary digit_, unité de stockage la plus simple, ne pouvant prendre que 2 valeurs possibles.

### 3.2. Unité de mesure et comparatif des supports de stockage

#### 3.2.1 Octet

Un __octet__ (_byte_ en anglais) est un regroupement de 8 bits. Il s'agit de l'unité de base généralement utilisée par les machines pour lire et écrire les données.

![](./assets/octet.png)

Il est très courant en informatique de mesurer la capacité mémoire d'un disque dur, de la RAM d'un ordinateur ou d'un débit de données Internet avec une unité de mesure exprimée comme un multiple d'octets. Ces multiples sont traditionnellement des puissances de 10 et on utilise les préfixes "kilo", " méga", etc. pour les nommer. 

Le tableau ci-dessous donne les principaux multiples utilisés dans la vie courante.

| Nom              | Symbole            | Valeur |
| :--------------- |:---------------:   | -----:|
| Kilooctet  |   Ko                    | 1000 = 10<sup>3</sup> octets |
| Mégaoctet  |   Mo                     | 1000 Ko = 10<sup>3</sup> Ko |
| Gigaoctet  |   Go                     | 1000 Mo = 10<sup>3</sup> Mo |
| Teraoctet  |   To                     | 1000 Go = 10<sup>3</sup> Go |


#### 3.2.2. À Faire

1. À combien de bits correspond 4 octets ?
2. À combien d'octets correspond 6,7 Ko ?
3. À combien d'octets correspond 1 Go ?

#### 3.2.3. Comparatif

![](./assets/stockage.jpeg)

_Source : futura-sciences_

__Question : À quoi correspond la suite bits `0000011001`? `00100011` ?__

### 3.3. Encodage

La réponse dépend de l'__encodage__ utilisé.

> __Encodage__ : Transcription de données vers un format ou un protocole donné.

*J'ai créé un document via **Word**, j'ai utilisé l'**encodage docx**.*

*J’ai pris une photo via une **application de mon téléphone**, elle a utilisé l’**encodage** png.*

### 3.4. Exemple : Les chaines de caractères

L'**ASCII** (*American Standard Code for Information Interchange*) est une norme d'encodage des caractères apparue dans les années 1960, toujours utilisé (mais qui a tendance à disparaitre au profit d'autres normes).

Il s'agit d'une table de 128 entrées, numérotées de 0 à 127 en binaire, (0000000 à 1111111).

Les caractères sont codés sur 7 bits, un _septulet_ de bits, appelé __point de code__.

![](./assets/ascii.png)

Exemples : 

- Le point de code `1000000` correspond au caractère @
- Le caractère `+` est encodé par le point de code `0101011`en ASCII

#### 3.4.1. Principe

Pour obtenir un caractère, en fonction d'un septuplet `s`, on récupère le caractère au point de code `s`.

#### 3.4.2. À Faire

1. Quel est le point de code en ASCII du caractère `#`? `A` ? `8`?
2. Quels sont les mots suivants, encodés en ASCII ?
   1. `1001111 1001011`,
   2. `1010011 1001110 1010100`,
   3. `0111011 0101101 0101001`,

3. Quelle est la représentation en binaire de votre prénom (sans les accents éventuels) ? Combien de bits sont nécessaires pour coder votre prénom ?

## 4. Bilan

Un ...................(1), une ...................(2), un ...................(3) sont des supports de stockage de données.

Sur un support, les données sont stockées sous forme de ...................(4), ne pouvant prendre que 2 valeurs possibles (soit 0, Soit 1).

Un ...................(5) est un ensemble de 8 bits consécutifs.

L'...................(6) est la transcription d'une donnée dans un format particulier. Un même octet correspond à des données différentes selon l'encodage utilisé.

En ASCII, les caractères sont encodés sur ...................(7) bits.
Pour obtenir le caractère encodé en ASCII, il faut ................... (8).

