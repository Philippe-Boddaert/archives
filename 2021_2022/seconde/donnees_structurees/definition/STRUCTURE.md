---
title : Les données structurées
author : M. BODDAERT, d'après les travaux de Chrsitophe Mieszczak et Luc Bournonville
license : CC-BY-NC-SA
---
# Les données structurées

![](./assets/big.jpg)

## 1. Définition

Pour décrire un objet (individu, voiture, entreprise, état etc...) on peut utiliser des mots (chaines de caractères) ou des nombres: on les appelle des __données__

> __Donnée__ : Une valeur décrivant un objet

Exemple : 

- __0493627700__, Donnée correspondant au numéro de téléphone du Lycée Masséna,
- __yeux bleus__, Donnée relative à la couleur des yeux d'un individu.

Aujourd'hui, les données sont partout :

- ce sont elles qui permettent à la plupart des moteurs de recherche de répondre au mieux à vos requêtes,
- ce sont elles qui sont au coeur des modèles économiques des réseaux sociaux (entre autres), 
- aux différents IA d'apprendre et de prendre des décisions.

Cependant, une donnée brute ne sert pas à grand chose. Pour pouvoir l'exploiter et la traitée, il est impératif de les structurer.

## 2. À Faire

Répondre aux que/stions suivantes :

1. ✏ __Qu'est-ce qu'une donnée dans le contexte de la vidéo ?__ 
2. ✏ __À quelle condition peut-on comparer des données ?__ 
3. ✏ __Pourquoi structurer les données ?__ 
4. ✏ __Qu'est-ce qu'une table de données ?__ 
5. ✏ __Où peut-on trouver les informations pour remplir une table ?__ 
6. ✏ __Quelles sont les opérations que l'on peut faire sur une table ?__ 
7. ✏ __Pourquoi créer plusieurs tables ?__ 
8. ✏ __Comment un chercheur a-t-il montré qu'il était possible d'accéder à des données médicales individuelles ?__ 
9. ✏ __Quel est l'enjeu du RGPD cité dans la vidéo ?__ 

Pour cela, visionner la vidéo (plusieurs visionnages sont nécessaires) :

[![Données structurées](https://img.youtube.com/vi/IJJgcZ2DEs0/0.jpg)](https://www.youtube.com/watch?v=IJJgcZ2DEs0)

## 3. Typologie de données

On identifie 2 typologie de données :

|     Type de données     |                       Caractéristiques                       |                      Métaphore associée                      |
| :---------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| Données structurées | Organisation logique, format identique, appréhendable par un ordinateur. |            Couverts rangés dans un tiroir précis             |
| Données non structurées | Entassement d'objets divers sans logique, formats différents, nécéssite un traitement humain | des choses entassés avec d'autres babioles dans une caisse dans une brocante |

## 4. Structuration de données

Regrouper des données de manière organisée permet d'en __faciliter l'exploitation__ et permet de produire de __nouvelles informations__.

Pour reprendre l'exemple de la vidéo d'introduction, un morceau de musique a les caractéristiques suivantes : durée, titre, style, date, et interprète.

Ces caractéristiques sont appelées __descripteurs__.  Les données __qualitatives__ se présentent sous forme de chaines de caractères, les données __quantitatives__, sous forme de nombres.

| Durée | Titre | Style | Date | Interprète |
| :--: | :--: | :--: | :--: | :--: |
| 04:34 | Roux Cool | Reggae | 1993 | Bob |
| 04:34 | Love Bird | Jazz | 1956 | Elsa |
| 03:25 | Re Vole Love | Rap | 2017 | Mc |
| 08:45 | Blue Pigeon | Rock | 1987 | Pixees |
| 02:23 | Flying Grass | Jazz | 1979 | Little Pink Dots |
| 03:21 | Stay Strong | Reggae | 1997 | Bob |

Le tableau ci-dessus utilise 5 __descripteurs__ pour décrire des morceaux de musique.

Ces morceaux partageant les mêmes __descripteurs__ constituent une __collection__.

Chaque ligne du tableau correspond à un objet, l'intersection d'une ligne (__objet__) et d'une colonne (__descripteur__) est une donnée.

Une __base de données__ regroupe plusieurs __collections__ et permet de faire des opérations entre ces __collections__ (tris, recoupements ...)

### 4.1. À Faire

Ci-dessous une collection d'interprètes :

| Nom  | Pays     | Naissance  |
| :----: | :--------: | :----------: |
| Bob  | Jamaique | 03/04/1970 |
| Pixees | Angleterre | 09/11/1972 |
| Little Pink Dots |  Angleterre | 23/03/1956 |
| Mc | France | 11/09/1983 |
| Elsa | Etats-Unis | 19/07/1940 |
| Grant Alapo | Etats-Unis | 04/04/1990 |

1. ✏ Quels sont les descripteurs ? Quels sont ceux qualitifs ? quantitatifs ?
2. ✏ Quel descripteur permet de relier cette collection à celle des morceaux de musique ?
3. ✏ Quelle opération permet d'obtenir les morceaux de musiques dont l'interprète est anglais ? Quels sont ces morceaux de musiques ?

## 5. Métadonnées

>  __Métadonnées__ : Informations associées aux données (structurées ou non). Données qui décrivent d’autres données.
>
> Par exemple : date de sauvegarde, auteur du fichier ...

Les métadonnées sont accessibles sans même ouvrir le fichier !

Télécharger le fichier [otter.jpg](./assets/otter.jpg). 

- Quel est l'auteur de la photo ? 
- A quelle date la photo a-été prise ? 
- Quelles sont les dimensions de la photo ? 
- Quelle est la marque de l'appareil photo ?

Pour répondre à ces questions, il faut accéder aux métadonnées.

A partir du Bureau Windows:

- Cliquer sur `Mes données`
- Cliquer Sur `Téléchargements`
- Faire un clic droit sur le fichier
- Cliquer sur l'onglet `Détails`

### À Faire

1. Télécharger le fichier [mystere.jpg](./assets/mystere.jpg). Sans ouvrir le fichier avec un logiciel d'affichage d'image, Quand a-t-elle été prise ? Sur quel appareil ? Où a-t-elle été prise ? (Le site https://www.coordonnees-gps.fr/ peut vous aider si vous connaissez la latitude et longitude d'une coordonnée)
2. Télécharger le fichier [production.mp4](./assets/production.mp4). Sans ouvrir le fichier avec un logiciel de visualisation de vidéo, quelles informations pouvez-vous obtenir ? Dimensions ? Durée ?


## 6. Stockage des données

### 6.1. Historique

- 1930 : utilisation des cartes perforées, premier support de stockage de données,
- 1956 : invention du disque dur permettant de stocker de plus grandes quantités de données, avec un accès de plus en plus rapide,
- 1970 : invention du modèle relationnel (E. L. Codd) pour la structuration et l’indexation des bases de données,
- 1999 : Invention de la clé USB,
- 1999 : création du premier service de stockage en ligne via un data center (par Salesforce.com)

### 6.2. Le cloud

Tout le monde a déjà attendu parlé du "cloud", mais qu'est ce que c'est exactement ?

#### À Faire

Après avoir visionné la vidéo ci-dessous, répondez aux questions suivantes :

1. ✏ Qu'est-ce que le cloud ?
2. ✏ D'où vient le terme cloud ?
3. ✏ Quelles sont les évolutions techniques qui ont permis le développement le cloud ?
4. ✏ Quelles sont les 2 problématiques liées à la gestion du cloud ?
5. ✏ Quels sont les avantages du cloud ? 
6. ✏ Quels sont les désavantages du cloud ?

[![Données structurées](https://img.youtube.com/vi/5YawCCUxa_E/0.jpg)](https://www.youtube.com/watch?v=5YawCCUxa_E)

### 6.3. Impacts

- Selon [actu-environnement](https://www.actu-environnement.com/ae/news/data-centers-efficience-energetique-36248.php4), les **data center consomment en moyenne en France 5,15 MWh/m2/an**. En somme, un data center de 10 000 m2 consomme en moyenne autant qu’une ville de 50 000 habitants,
- 40% de cette consommation électrique est utilisée uniquement pour les refroidir.
- Tous les 2 jours, [la population mondiale produit autant d’information](https://www.franceculture.fr/emissions/la-methode-scientifique/big-data-des-chiffres-et-des-chiffres) qu’elle n’en a générée depuis l’aube de son existence jusqu’en 2003

![](./assets/pollution.jpg)

_Source : Grizzlead_

#### À Faire

✏ Après avoir visionné la vidéo ci-dessous, faites un inventaire des principales causes de la consommation énergétique des centres de données

[![Données structurées](https://img.youtube.com/vi/iiHxCX76bYU/0.jpg)](https://www.youtube.com/watch?v=iiHxCX76bYU)
