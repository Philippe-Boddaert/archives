---
title : Pokemon
author : M. BODDAERT, d'après les travaux de Christophe Mieszczak
license : CC-BY-NC-SA
---
# Pokemon

## 1. Contexte

Le format __CSV__ est un format texte utilisé pour sauvegarder des données en utilisant une structure simple.

Ouvrez, __avec Notepad++__, le fichier [pokemon_stats_1.csv](./assets/pokemon_stats_1.csv)

Voici un extrait des données de ce fichier :

```plaintext
name;classification;attack;defense
Swablu;Cotton Bird Pokémon;40;60
Budew;Bud Pokémon;30;35
Minun;Cheering Pokémon;40;50
Metapod;Cocoon Pokémon;20;55
Chikorita;Leaf Pokémon;49;65
Pinsir;Stagbeetle Pokémon;155;120
Cinccino;Scarf Pokémon;95;60
Elgyem;Cerebral Pokémon;55;55
Foongus;Mushroom Pokémon;55;45
Ariados;Long Leg Pokémon;90;70
Aipom;Long Tail Pokémon;70;55
Accelgor;Shell Out Pokémon;70;40
Dialga;Temporal Pokémon;120;120
Starly;Starling Pokémon;55;30
Ledyba;Five Star Pokémon;20;30
Politoed;Frog Pokémon;75;75
Dodrio;Triple Bird Pokémon;110;70
Aggron;Iron Armor Pokémon;140;230
Mankey;Pig Monkey Pokémon;80;35
```

Vous l'aviez deviné, il s'agit d'une base de données au sujet des Pokémons !

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/269px-International_Pok%C3%A9mon_logo.svg.png)

_Source : Wikipedia_

La base nous donne des informations sur les Pokemon selon plusieurs critères appelés `attributs`, ou `champs` présents dans la première ligne et séparés par des `;`

- `name ` désigne bien sûr le nom des Pokemon.
- `classification` nous précise la classe du Pokémon.
- `attack` sa force d'attaque. Plus elle est élevée, plus le Pokémon est dangereux.
- `defense` son niveau de défense. Plus il est élevé, plus le Pokémon est résistant.

Sous les `attributs` , on trouve leurs `valeurs` elles aussi séparées par un `;`.

__En observant ces données__, pourriez-vous me dire :

1. Combien y-a-t-il de Pokémons ?
2. Quelles sont les caractéristiques de *Comfey* ?
3. Qui a la meilleure défense ?
4. Combien de Pokémons ont une attaque supérieure à 70 ?
5. Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ?
6. Existe-t-il des Pokémons dont l'attaque et la défense son supérieur à 100 ? Si oui, combien ?

Pas facile ... Heureusement, il y a d'autres moyens de visualiser et de traiter ces données.

## 2. Manipulations avec un tableur

## 2.1. Statistiques de combats

Ouvrez le fichier *pokemon_stats_1.csv* avec un __tableur__ (celui de la suite *libreOffice* par exemple, pour utiliser un logiciel libre).

Ce dernier affiche alors les données sous la forme d'un tableau : c'est déjà plus facile de s'y retrouver.

Un __tableur__ est un logiciel permettant de manipuler des données structurées, notamment des filtres, tris et calcul sur ces données.

N.B : Les réponses aux questions suivantes doivent être saisies dans le fichier reponses.csv devant être remis sur Pronote à la fin de la séance.

__Question 1__. Utilisez la barre de défilement pour déterminer l'effectif des Pokemons. __Notez votre réponse et la valeur de la colonne E du dernier Pokemon dans le fichier reponses.csv.__

__Question 2__. On peut effectuer diverses opérations sur nos colonnes. Pour chercher le Pokemon nommé *Comfey*, le plus simple est de réaliser un tri sur l'attribut `name`, c'est à dire sur la colonne A, puis de chercher où est *Comfey*. Maintenant que la colonne A est dans l'ordre alphabétique, retrouvez *Comfey* et ses caractéristiques. __Notez la classification du pokemon et la valeur de la colonne E  dans le fichier reponses.csv.__

__Question 3__. Pour trouvez qui a la meilleure defense, il faut trier par ordre décroissant avec la clé de tri `defense`. __Notez le nom du Pokemon et la valeur de la colonne E dans le fichier reponses.csv.__

__Question 4.__ Combien y a-t-il de Pokemon dont la valeur d'attaque est supérieure à 140. __Notez ce nombre, ainsi que le mot formé dans la colonne E dans le fichier reponses.csv.__

__Question 5.__ Pour savoir, parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ? __Notez les noms de Pokemon et le mot formé dans la colonne E dans le fichier reponses.csv.__

__Question 6.__ Pour cette question, vous savez déjà tout ce qu'il faut savoir pour vérifier l'existence de Pokémons dont l'attaque est supérieure à 100 et la défense supérieure ou égale à 184 et trouver leur nombre. N'oubliez pas de trier les pokemon par ordre alphabétique. __Notez le mot formé dans la colonne E dans le fichier reponses.csv.__

## 2.2. Statistiques morphologiques

Ouvrez cette fois le fichier [pokemon_stats_2.csv](./assets/pokemon_stats_2.csv)

__Question 7.__ Déterminer les 5 plus gros Pokémons, en les triant par poids par ordre décroissant puis par ordre alphabétique sur le nom. __Notez le nomdu 7ème Pokemon et la valeur de la lettre dans la colonne F dans le fichier reponses.csv.__

__Question 8.__ Quel est le poids moyen d'un Pokémons ? On pourra utiliser la fonction `moyenne` sur la colonne relative au poids. __Notez votre réponse dans le fichier reponses.csv.__

__Question 9.__ Combien y-a-t-il de types de Pokémons différents ? Pour répondre à cette question, on pourrait utiliser un tri... __Notez votre réponse dans le fichier reponses.csv.__

## 2.3. Joindre les 2 bases

On se pose maintenant quelques questions :

- Les Pokémons les plus gros sont-ils les plus forts ?
- Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ?

Le problème est que les données nécessaires sont réparties dans deux bases différentes : on a besoin de les joindre pour répondre à nos questions !

__Question 10.__ Considérons la première base : Existe-t-il un attribut capable d'identifier de manière unique une ligne de notre base de donnée ? Un tel attribut est appelé __identifiant__, ou __clé primaire__.

### 2.3.1. Indications

Pour joindre nos deux bases, il faudrait qu'elles aient un attribut commun qui soit un identifiant dans au moins une des deux bases. Y-en-a-t-il un ?

Vous allez commencer par ouvrir les deux fichiers csv sur lesquels on travaille puis copier les deux tableaux côte à côte.

Il faut maintenant faire correspondre les attributs. Pour cela, triez séparément, les deux tableaux selon l'attribut name : les colonnes correspondent dorénavant !

Supprimez les colonnes inutiles pour plus de lisibilité.

Répondez maintenant aux questions ci-dessous en utilisant les tris adéquats.

__Question 11.__ Les Pokémons les plus gros sont-ils les plus forts ? __Notez votre réponse dans le fichier reponses.csv.__

__Question 12.__ Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ? __Notez votre réponse dans le fichier reponses.csv.__