---
title : Activité - Don du sang
author : M. BODDAERT, d'après les travaux de Christophe Mieszczak
license : CC-BY-NC-SA
---
# Activité - Don du sang

## 1. Contexte

Fin janvier 2022, un groupe d'amis a décidé de faire un don du sang. 

![don du sang - source wikipédia](https://upload.wikimedia.org/wikipedia/fr/thumb/2/25/Logo_don_du_Sang_b%C3%A9n%C3%A9vol_en_France.JPG/220px-Logo_don_du_Sang_b%C3%A9n%C3%A9vol_en_France.JPG)

Voici un tableau regroupant les données personnelles que les médecins leur ont demandé avant leur prise de sang :


| Nom| Date de Naiss| CP Ville|
| ------------------- | :------: | :---: |
| Roux Emmanuelle     | 13/01/06 | 06230 |
| Masson Edeline      | 13/03/07 | 06000 |
| Astier Cécile       | 01/03/06 | 06000 |
| Boulot Céline       | 29/06/06 | 06240 |
| Campagne David      | 16/05/06 | 06230 |
| Bernard Jean-Pierre | 12/05/06 | 06000 |
| Lefebvre Claire     | 21/02/05 | 06000 |
| Mayeux Eric         | 13/06/05 | 06240 |
| Garcia Michel       | 20/06/06 | 06240 |
| Naîdji Antoine      | 13/04/06 | 06800 |
| Van Helsen Baptiste | 03/03/05 | 06230 |

## 2. À Faire

### 2.1. Exercice

On appelle __descripteur__, ou attribut, ou champ, le nom associé aux données d'une colonne. Un attribut est un identifiant lorsqu'il possède une valeur différente sur chaque ligne.

1. ✏ Quels sont les descripteurs utilisés ici ?
2. ✏ Certains de ces descripteurs sont-ils un identifiant pour ces données ? Si oui, lesquels ?

### 2.2. Exercice

Après leur passage, un médecin a récapitulé les résultats des analyses dans un tableau en les anonymant. 

L'un des amis a réussi à en prendre une photographie avec son téléphone.

| Age  | Sexe |  Ville   | Groupe sanguin |
| :--: | :--: | :------: | :------------: |
|  16  |  F   | Beausoleil |       A        |
|  16  |  G   |  Villefranche-sur-Mer  |       A        |
|  17  |  F   |  Nice  |       A        |
|  16  |  F   |  Villefranche-sur-Mer  |       AB       |
|  16  |  G   | Cagnes-sur-Mer |       AB       |
|  16  |  G   | Beausoleil |       B        |
|  17  |  G   | Beausoleil |       B        |
|  17  |  G   |  Villefranche-sur-Mer  |       B        |
|  15  |  F   |  Nice  |       B        |
|  16  |  G   |  Nice  |       O        |
|  16  |  F   |  Nice  |       O        |



1. ✏ Quels sont les descripteurs utilisés ici ?
2. ✏ Certains attributs sont-ils des identifiants ?

Voici une petite liste de codes postaux des alentours de Nice.

|  Ville   | Code postal |
| :------: | :---------: |
|  Villefranche-sur-Mer  |    06230    |
| Saint-Laurent-du-Var |    06700    |
|   Nice   |    06000    |
| Beausoleil |    06240    |
|  Beaulieu-sur-Mer  |    06310    |
| Cagnes-sur-Mer |    06800    |
| Èze |    06360    |

### 2.3. Exercice - Vraiment anonyme ?

1. ✏ Télécharger le fichier [don.ods](./don.ods)
2. ✏ En croisant les données, est-il possible de retrouver le groupe sanguin de chaque individu ?
3. ✏ Pour cela, nous allons compléter la feuille 3 du fichier de données :

|Age|Année Naiss.|Sexe|Ville     |Code postal| Nom- Prénom |groupe sanguin |
|:-:| :--------: |:-: |:--------:|:--------: |:---:|:-------------:|
|16 |            | F  | Beausoleil |           |     | A        |
|16 |            | G  |  Villefranche-sur-Mer  |           |     | A        |
|17 |            | F  |  Nice  |           |     | A        |
|16 |            | F  |  Villefranche-sur-Mer  |           |     | AB       |
|16 |            | G  | Cagnes-sur-Mer |           |     | AB       |
|16 |            | G  | Beausoleil |           |     | B        |
|17 |            | G  | Beausoleil |           |     |B        |
|17 |            | G  |  Villefranche-sur-Mer  |           |     |B        |
|15 |            | F  |  Nice  |           |     |B        |
|16 |            | G  |  Nice  |           |     |O        |
|16 |            | F  |  Nice  |           |     |O        |

## 3. Analyse

### 3.1. RGPD

Le **R**èglement **G**énéral de la **P**rotection des **D**onnées encadre le traitement des données personnelles sur le territoire de l’Union européenne.

En lisant l'article https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on, répondez aux questions suivantes :

1. Quelle est la définition d'une __donnée personnelle__ ?

1. En fonction de la définition d'une donnée personnelle, indiquez quelles sont parmi les données suivantes celles qui peuvent être considérées comme personnelles ou non personnelle.

   | Donnée | Personnelle / Non personnelle |
   | :-----------: | :-----------: |
   | Nom | ? |
   | Prénom | ? |
   | N° de téléphone | ? |
   | Photo d'identité | ? |
   | Voix | ? |
   | Adresse complète | ? |
   | Age | ? |
   | Établissement scolaire | ? |
   | Code Postal | ? |
   | Poids | ? |

2. La CNIL a réalisé une fiche à destination des organisations souhaitant effectuer des traitements de données personnelles [Les 6 bons reflexes](https://www.cnil.fr/fr/adopter-les-six-bons-reflexes). Indiquez à quelles obligations sont soumis les organismes collectant nos données :

   | Obligations | Vrai / Faux |
   | :-----------: | :-----------: |
   | Préciser quelles données sont sauvegardées dans leur base |     ?        |
   | Garder vos données pour une période de 10 ans | ? |
   | Indiquer les objectifs de la conservation de ces données | ? |
   | Envoyer une copie des données à votre domicile | ? |
   | Transférer vos données à la CNIL | ? |
   | Modifier les données à votre demande | ? |
   | Vendre vos données | ? |
   | Garantir la sécurité de nos données | ? |

Si les organismes sont soumis aux obligations du RGPD, les précisions sont souvent difficiles d'accès : [Voici par exemple la charte de Doctissimo](https://www.doctissimo.fr/equipe/charte/charte-donnees-personnelles-cookies).

### 3.2. Données personnelles et Anonymat

Lorsqu'un organisme collecte des données personnelles, même si elles sont anonymées, il est souvent possible, en croisant des données, de les désanonymer. Une grande partie des individus d'une population est identifiable.

### À Faire

Répondez aux questions suivantes après visionnage de la vidéo :
   [![Resolution de nom](https://img.youtube.com/vi/6t9Lds5zxZk/0.jpg)](https://www.youtube.com/watch?v=6t9Lds5zxZk&ab_channel=CashInvestigation)

   1. ✏ Quel est le principe de l'anonymisation des données ? 
   2. ✏ Quel est le bénéfice attendu de ce principe ? 
   3. ✏ Quel outil les chercheurs ont créé ?
   4. ✏ Le chercheur dispose d'une base de données de combien de personnes ? 
   5. ✏ De combien d'informations basiques et faciles à acquérir dispose le chercheur pour retrouver une personne ? 
   6. ✏ Avec les données sur la région et la date de naissance, combien obtient-il de résultats ?
   7. ✏ Quelles opérations effectuent-ils pour obtenir ce résultat ?

### À Faire

En lisant l'article https://usbeketrica.com/fr/article/donnees-personnelles-etude-enterre-anonymat, répondez aux questions suivantes :

1. ✏ Qu'ont développé les chercheurs ?
2. ✏ Quels sont les 3 descripteurs permettant de ré-identifier 83% des américains ?
3. ✏ Avec combien de données, l'étude montre que 99% des américains peuvent être ré-identifiés ?
4. ✏ Que propose l'article comme solution alternative à l'anonymisation des données ?

### 3.3. Bilan

Lors de la création d'une base de données confidentielles et sensibles, comme celle des dossiers médicaux, il faut donc choisir avec un grand soin les descripteurs si l'on souhaite qu'il soit impossible, ou du moins très difficile, de désanonymer ces données, prendre très au sérieux la sécurité de la base et garantir que ces données ne pourront pas être détournée pour un _mauvais_ usage.

## 4. Synthèse

__Complétez le texte avec les mots manquants, en vous aidant du support de l'activité.__

Une .................... (1) est « toute information se rapportant à une personne physique identifiée ou identifiable » de manière directe ou indirecte. Par exemple : ....................(2), ....................(3), ....................(4).

Le .................... (5) est un dispositif de l'Union Européenne pour encadrer le traitement des données personnelles.

L'anonymisation des données est une technique consistant à .....................(6)

La .................... (7) consiste à retrouver une personne à partir de ses données personnelles anonymisées.

Selon certaines études, par un système de .................... (8) et de ....................(9), il est possible de retrouver 83% des personnes vivant aux états-unis à partir de 3 descripteurs : ....................(10), ....................(11) et ....................(12).
