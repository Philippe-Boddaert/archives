---
title : Activité - Fruits & Castor
author : M. BODDAERT
license: CC-BY-NC-SA
---
# Activité - Fruits & Castor

![](./assets/vegetables.png) ![](./assets/beaver.png)

_Source : Pixabay_

## 1. Contexte

Cette activité a pour objectif de manipuler des données structurées, notamment les mécanismes de filtre et tri.

Pour cela, nous allons utiliser la plateforme Castor-informatique.

## 2. À Faire

1. Ouvrir un navigateur Web,

2. Rendez-vous à l'adresse [https://concours.castor-informatique.fr/](https://concours.castor-informatique.fr/),

3. Saisisez le code de l'activité fourni en début de séance. 

   __N.B : Au commencement de l'activité, un code personnel vous sera communiqué. Il est impératif de retenir ce code (sur papier ou photo via téléphone) pour revenir sur votre activité en cas de problème technique.__

4. Effectuez les séquences dans l'ordre, sauf la séquence 2
	
__N.B : Avancez à votre rythme en commençant par la version une étoile de chaque séquence__

## 3. Synthèse

Complétez le texte avec les mots qui conviennent.

A. lignes
B. colonnes
C. copie
D. stockage
E. cases
F. filtre
G. sélection
H. variables
I. stocker
J. descripteurs
K. filtrer
L. condition
M. valeur
N. trier
O. ordre
P. aspect
Q. modifier
R. aléatoire
S. sens

Un filtre est une opération de ............ (1) de données.

Un ............. (2) permet de garder seulement les ............. (3) du tableau qui nous intéressent.

La première ligne du tableau contient les noms de colonne, qu'on appelle ............. (4).

Dans notre exemple, chaque liste déroulante à gauche du tableau correspond à un descripteur. Ces listes déroulantes permettent de choisir la colonne retenue pour ............. (5) les données.

On choisit une ............. (6) dans la liste déroulante choisie.

............. (7) les données, c'est alors ne garder que celles qui ont la valeur souhaitée pour le descripteur retenu.

Trier les données d'une table, c'est modifier l' ............. (8) des lignes pour que les données soient rangées d'une certaine manière.

Pour trier les données d'une table, il faut choisir le ou les ............. (9) à considérer pour le tri.

Usuellement, on choisit aussi un ............. (10) pour le tri : croissant ou ............. (11).
