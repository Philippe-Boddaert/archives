---
title : Activité - Musique & Castor
author : M. BODDAERT
license: CC-BY-NC-SA
---
# Activité - Musique & Castor

![](./assets/birds.jpg) ![](./assets/beaver.png)

_Source : Pixabay_

## 1. Contexte

Cette activité a pour objectif de manipuler des données structurées, notamment les mécanismes de filtre et tri, à travers l'utilisation du __tableur LibreOffice Calc__.

> Un **tableur** est un logiciel capable de manipuler des données de toute nature, sous la forme de tables (__feuille de calcul__).

LibreOffice Calc est un __logiciel libre__, comme son nom le suggère.

## 2. Logiciel Libre / Propriétaire

La notion de logiciel libre est apparue dans les années 1980 sous l'impulsion du programmeur et militant __Richard Stallman__.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Richard_Stallman_at_LibrePlanet_2019.jpg/185px-Richard_Stallman_at_LibrePlanet_2019.jpg)

_Source : Wikipedia_

Aujourd'hui, un logiciel est considéré comme **libre**, au sens de la *Free Software Foundation*, s'il confère à son utilisateur quatre libertés :

1. la liberté d'**exécuter** le programme, pour tous les usages,
2. la liberté d'**étudier le fonctionnement du programme et de l'adapter à ses besoins**,
3. la liberté de **redistribuer** des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies),
4. la liberté d'**améliorer** le programme et de **distribuer ces améliorations** au public, pour en faire profiter toute la communauté.

Tout logiciel ne répondant pas à ces critères et dit **"propriétaire"**.

Au vu de la définition d'un logiciel **libre**, est-ce que les logiciels suivant le sont ?

1. Excel ?
2. Google Sheets ?
3. Windows 10 ?
4. Android ?
5. IOS ?
6. Linux ?
7. Mozilla Firefox ?
8. Google Chrome ?

## 3. À Faire

1. Ouvrir le logiciel LibreOffice Calc (Menu Démarrer > Toutes les Applications > LibreOffice 7.2 > LibreOffice Calc),
2. Télécharger le fichier [musique.csv](./assets/musiques.csv),
3. Ouvrir le fichier _musique.csv_ en faisant attention d'avoir exactement cette configuration à l'ouverture :
![](./assets/configuration.png)
4. Ouvrir un navigateur Web,
5. Rendez-vous à l'adresse [https://concours.castor-informatique.fr/](https://concours.castor-informatique.fr/),
6. Saisisez le code de l'activité fourni en début de séance. 

   __N.B : Au commencement de l'activité, un code personnel vous sera communiqué. Il est impératif de retenir ce code (sur papier ou photo via téléphone) pour revenir sur votre activité en cas de problème technique.__
7. Effectuez les séquences dans l'ordre à partir du `3 - Musique 1`
	
	__N.B : Avancez à votre rythme en commençant par la version une étoile de chaque séquence__
## 4. Synthèse

__Complétez le texte avec les mots de la liste qui conviennent.__

Un .............(1) est un logiciel qui permet de .............(2), .............(3), .............(4) des données.

Dans un tableur, les données sont représentées sous la forme de tables, constituées de .............(5) et .............(6).

Chaque ligne décrit un .............(7), chaque colonne une propriété ou .............(8) d'un élément.

Le tableur a un vocabulaire spécifique :

- Un .............(9), qui saura enregistrer sous la forme d'un fichier,
- Une .............(10), qui porte un nom, comporte des lignes et colonnes pour stocker les données. Une .............(11) se trouve à l'intersection des 2 notions précédentes qui contient une donnée élémentaire.
- Elle contient une valeur et une valeur de mise forme. La valeur peut être le résultat d'un calcul issu d'une .............(12) de calcul.

Un fichier .............(13) (comma separated value) permet de stocker des données sous forme de table.

La première ligne contient le noms des .............(14) et chaque ligne contient un objet, composé de données séparées par un .............(15).

_Liste de mots_ : 

__ardoise ----- point d'interrogation ----- traitement de texte ----- classeur ----- diaporama ----- point d'exclamation ----- feuille de calcul ----- tableur ----- colonnes ----- VSC ----- objet ----- cahier ----- formule__

__méthode ----- représenter ----- point virgule ----- traiter ----- interroger ----- lignes ----- élément ----- CSV ----- descripteur ----- en-tête ----- XLSX ----- titre ----- cartable ----- noyau ----- cellule__
