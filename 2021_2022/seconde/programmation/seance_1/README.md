---
title : Séance 1 - Découverte langage Python
author : S. COLOMBAN et J. CHEVILLOT
license : CC-BY-NC-SA
---

# Séance 1 - Définition et instructions élémentaires

## 1. Définition

> Un __algorithme__ est un ensemble d’instructions qui s’enchaînent dans un ordre logique et bien déterminé.
>
> Un algorithme peut s’écrire :
> 
> - en langage naturel
> - dans un langage de programmation (Python, JavaScript, PHP, C++, . . . )

## 2. Exemple
Pour ce trimestre, Alice a obtenu trois notes en mathématiques, désignées par les variables _a, b_ et _c_.
Voici l’algorithme suivant afin de calculer et afficher la moyenne d’Alice et un commentaire associé.

```python
demander trois entiers a, b et c (les valeurs des trois notes sur 20)
moyenne ← (a + b + c)/3 												← signifie "prend la valeur"
afficher(moyenne)
si moyenne < 10 alors :
	afficher("Ta moyenne est inférieure à 10")
sinon :
	afficher("Ta moyenne est satisfaisante")
```
Alice lance cet algorithme avec les notes _a, b_ et _c_ suivantes : 16, 11 et 9.

### 2.1. À Faire

1. Quelle sera la valeur finale de la variable moyenne ?
2. Que va-t-il précisément s’afficher ?

## 3. Vocabulaire

L’instruction d’__affectation__ permet d’attribuer une __valeur__ à une __variable__. 

Lorsque l’on veut donner une valeur à la variable _x_, on écrit alors _x_ ← ... (on lit « _x_ prend la valeur . . . »)

Les instructions dans un algorithme font intervenir des quantités, qui sont appelées __variables__. 

Elles sont repérées par un __nom__ et possèdent une __valeur__ qui peut varier. 

On distinguera cette année quatre types de variables : 

| Type | Nom courant en programmation | Description |
| :--: | :--: | :-- |
| __entier__ | int, comme integer en anglais | correspondant aux nombres entiers |
| __flottant__ | float | correspondant aux nombres décimaux |
| __booléen__ | bool, d’après le mathématicien George Boole | correspondant aux deux états Vrai ou Faux |
| __chaîne de caractères__ | string | correspondant aux suites de caractères (texte), par exemple "bonjour") |

### 3.1. À Faire

#### Exercice 1

Alice a écrit un algorithme. Pour chaque ligne, __donner le type__ de la variable utilisée

|Instructions | variable|  type |
| :-- | :--: | :--: |
| _a_ ← 10 | _a_ | entier (int) |
| _b_ ← 3.1425 | _b_ | |
| _c_ ← "bonjour" | _c_ | |
| _d_ ← Faux | _d_ | |
| _e_ ← _a_ + _b_ | _e_ | |

#### Exercice 2

Bob a écrit l’algorithme suivant. Compléter le tableau des variables afin de suivre l’évolution des valeurs de _a, b_ et _c_.

|Instructions | _a_ |  _b_ | _c_ |
| :-- | :--: | :--: | :--: |
| _a_ ← 10 |10 | — | — |
| _b_ ← 5 + _a_ | | | |
| _c_ ← 2 * _b_ | | | |
| _a_ ← _a_ + _b_ | | | |
| _c_ ← _a_ − 5 | | | |
| afficher(_c_) | | | |

#### Exercice 3

Charlie a écrit l’algorithme suivant. Compléter le tableau des variables afin de suivre l’évolution des valeurs de _a, b_ et _c_.

|Instructions | _a_ |  _b_ | _c_ |
| :-- | :--: | :--: | :--: |
| _b_ ← 10 | | | |
| _a_ ← 2 + _b_ | | | |
| si _a_ < 10 alors | — | — | — |
| _b_ ← _b_ + 1 | | | |
| _c_ ← _b_ + 20 | | | |
| sinon : | — | — | — |
| _b_ ← _b_ - 1 | | | |
| _c_ ← _b_ + 10 | | | |
| afficher(_c_) | | | |

#### Exercice 4

Dave souhaite écrire un algorithme qui demande de saisir l’âge d’un élève et écrive suivant le cas : "tu es majeur(e)" ou "tu es encore mineur(e)" Compléter son algorithme ci dessous :

```txt
demander(age)
...
...
...
...
...
...
...
...
...
```

## 4. Première découverte PYTHON

Remarque importante : Le langage Python est un langage libre et multiplateformes qui sera utilisé au lycée en mathématiques, en sciences physiques et, bien sûr, en SNT. 

Nous utiliserons l'interface suivante pour programmer en Python : https://console.basthon.fr

### 4.1. Exemple

#### À Faire

Copier/Coller ce code dans l'interface Basthon

```python
prenom = input("donnez-moi votre prénom")

print("Bonjour", prenom)
```

Exécutez-le...que constatez-vous ?

1. Sur ce même modèle, ajoutez la possibilité de demander le `nom` et de l'ajouter au message de bienvenue
2. Ajoutez la possibilité de demander votre `année de naissance` et d'afficher votre âge dans le message de bienvenue.

#### À Faire

Voici l'implantation en Python de l'algorithme de l'exemple (Cf 2. Exemple) :

Copier/Coller ce code dans l'interface Basthon

```python
a = float(input("donne-moi la note a : "))
b = float(input("donne-moi la note b : "))
c = float(input("donne-moi la note c : "))

moyenne = (a + b + c) / 3

if moyenne < 10:
    print("Insuffisant")
else:
    print("Satisfaisant")
```

Exécutez le avec les valeurs et exécutez le avec les valeurs 16, 11 et 9 pour les différentes notes _a, b_ et _c_.

### 4.2. À Faire

#### Exercice 5

On considère l’algorithme suivant : 

```txt
demander(a) 				avec a un decimal 
b ← a + 3 
b ← b * 2 
b ← b - 5
afficher(b)
```

1. Compléter le tableau des variables suivant :

| Instructions | a |  b |
| :-- | :--: | :--: |
| demander(_a_) avec _a_ un décimal | | — |
| _b_ ← _a_ + 3 | | |
| _b_ ← _b_ * 2 | | |
| _b_ ← _b_ − 5 | | |
| afficher(_b_) | | |

2. . La traduction de cet algorithme en langage Python est la suivante :
```python
a = float(input("donne-moi un décimal a : "))
b = a + 3
b = b * 2
b = b - 5
print(b)
```

On pourrait écrire ce programme de manière beaucoup plus courte.
Un élève a commencé à le faire mais il s’est trompé à la ligne 2. 
__Corriger son travail.__

```python
a = float(input("donne-moi un décimal a : "))
b = (a + 2) * 3 - 5
print(b)
```

Ma correction :
```python
a = float(input("donne-moi un décimal a : "))
b = .....................................
print(b)
```

## 5. Synthèse

__À RETENIR : LANGAGE NATUREL ET TRADUCTION EN PYTHON__

![](./assets/synthese.png)

__Source : S. COLOMBAN et J. CHEVILLOT_

## 6. Pour aller plus loin

### Exercice 6

On donne ci-dessous un programme incomplet écrit en langage Python.

```python
longueur = int(input("longueur =")) 
largeur = int(input("largeur ="))

aire = ...... 
perimetre = ......

print("L’aire de ce rectangle vaut ",aire)
print("Le périmètre de ce rectangle vaut ", perimetre)
```

1. Compléter ce programme pour qu’il calcule et affiche l’aire et le périmètre d’un rectangle après avoir demandé sa longueur et sa largeur.
2. Pour __longueur = 15__ et __largeur = 8__, qu’obtient-on en sortie ?

### Exercice 7

Le droit d’entrée journalier dans un parc aquatique est de 37 € pour un adulte et de 28 € pour un enfant.

L’algorithme ci-dessous permet de calculer le prix payé par un groupe comprenant des adultes et des enfants.

Les variables _a_ et _e_ représentent le nombre d’adultes et le nombre d’enfants du groupe et la variable  _prix_ représente le prix payé par le groupe.

```txt
demander(a)
demander(e)

prix ← ......

afficher(.......)
```

1. Compléter cet algorithme pour indiquer le prix à payer en sortie.
2. Traduire cet algorithme en langage Python.

### Exercice 8

Un distributeur de boissons n’accepte que les pièces de 20 cts, 50 cts et 1 euro.

Le prix d’une boisson est de 1,90 €.

Compléter le programme ci-dessous, qui demande combien de pièces de chaque valeur vous insérez dans la machine et qui vous affiche la monnaie à rendre.

```python
n1 = int(input("combien de pièces de 20 cts ?"))
n2 = int(input("combien de pièces de 50 cts ?")) 
n3 = int(input("combien de pièces de 1 euro ?"))

somme = ................................................... 
monnaie = ...................................................

print("Vous avez inséré la somme de : ", somme)
print("la monnaie à rendre est de ", monnaie)
```

