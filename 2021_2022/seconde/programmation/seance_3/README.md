---
title : Séance 3 - Boucle bornées
author : P. BODDAERT
license : CC-BY-NC-SA
---
# Séance 3 - Les boucles bornées

## 1. Découverte des boucles bornées

On considère l'algorithme ci-dessous :

```txt
n ← 10
Pour tout i allant de 0 à n exclu
	afficher(i)
```

Sa traduction en Python :

```python
n = 10
for i in range(0, n):
	print(i)
```

1. Que signifie __for__ en Python ?
2. Sans saisir ce programme, donner la liste de toutes les valeurs affichées par celui-ci
3. Saisir ce programme sur Basthon et vérifier la réponse à la question précédente

## 2. Définition

__À RETENIR : SYNTAXE D’UNE BOUCLE EN PYTHON__

![](./assets/synthese_for.png)

N.B : À la différence de la boucle conditionnée, dont on ne connait pas le nombre de tours de boucles (i.e itération), dans une boucle bornée, le nombre de tours est connu et à définir.

## 3. À Faire

### Exercice 1

On considère le programme Python suivant :

```python
a = 1
b = 5
for i in range(3):
	a = 2 * a
	b = b + a
print(b)
```

On va commencer par analyser le code pour le comprendre.

1. Quel est le bloc d'instructions répété dans la boucle `for` ? Combien de fois est-il répété ?
2. Expliquez pourquoi il n'y a qu'une seule valeur qui s'affiche dans la console si on exécute le code ?
3. Recopiez et complétez le tableau ci-dessous avec les valeurs des variables _i, a_ et _b_ à chaque tour de boucle.

| _i_ | _a_  |  _b_  |
| :--: | :--: | :---: |
| — |  1   |   5   |
|  0   |   |  |
|   |   |    |
|   |   |    |

4. Quelle est la valeur finale de la variable _b_ ?
5. Exécutez ensuite le code et vérifiez ce qu'il produit.

### Exercice 2

L'article 2.2 Les punitions scolaires d'une circulaire ministérielle du 13 Juillet 2000 rappelle qu' « Il convient également de distinguer soigneusement les punitions relatives au comportement des élèves de l'évaluation de leur travail personnel. Ainsi n'est-il pas permis de baisser la note d'un devoir en raison du comportement d'un élève ou d'une absence injustifiée. Les _lignes et les zéros doivent également être proscrits._»

Votre professeur était élève bien avant cette circulaire et a souvent été puni avec pour sanction le fait de devoir copier 100 fois sur une feuille la phrase suivante : "Je ne discuterai plus avec mon camarade de classe pendant les cours."

Écrire un programme Python qui permet de facilement transcire la punition.

### Exercice 3

Soit l'algorithme suivant :

```txt
n ← 10
somme ← 0

Pour tout i allant de 0 à n exclu
	somme ← somme + i
afficher("La somme des", n, "premiers entiers est :", somme)
```

1. Compléter les pointillés afin de traduire l’algorithme suivant en Python
```python
n = 10
...
...
...
...
...
...
...
...
```
2. Un élève a créé le programme précédent l'a exécuté. Parmi les propositions suivantes, laquelle s’affiche à l’écran ?

- [ ] A. 10
- [ ] B. 45
- [ ] C. 55
- [ ] D. 66

### Exercice 4

Vous disposez de 10€ et économisez 3€ par jour pendant 31 jours.

1. Compléter les pointillés du code Python suivant :

```python
somme  = 10

for jour in range(.....):
	somme = ...

print("Somme économisée:",somme)
```

2. De quelle somme disposerez-vous au bout des 31 jours ?

### Exercice 5

L'algorithme suivant permet de compter les occurrences d'une lettre _l_ dans une chaine de caractères _c_.

```txt
c ← "anticonstitutionnellement"
l ← "n"
occurrence ← 0

Pour tout i allant de 0 à la taille de la chaine c exclue
	si la lettre à la position i est égale à l alors
		occurrence ← occurrence + 1
afficher("la lettre",l, "apparrait", occurrence, "fois dans la chaine de caractères")
```

1. Compléter les pointillés du code Python suivant :
```python
c = "anticonstitutionnellement"
l = "n"
occurrence = 0

for i in range(0, len(c)):  # len(c) permet d'obtenir la taille de c
	if ..... == c[i]:					# c[i] permet de récupérer la lettre à la ième position dans c
	    .....
print(.....)
```
2. Exécuter le code pour les valeurs de _c_ et _l_  suivantes et compléter le tableau des résultats obtenus

| _c_ | _l_ | _occurrence_ |
| :-----: | :-----: | :--------------: |
| anticonstitutionnellement |   l    |                |
| anticonstitutionnellement |   n    |                |
| Vrouuuuuuum | u | |
| Il y avait, pour finir, paraphant, trois traits horizontaux (dont l'un au moins paraissait plus court) qu'un gribouillis confus barrait. | e | |

### Exercice 6

Une feuille de papier mesure 0,1 mm d’épaisseur.
La distance Terre-Lune est de 384 400 km soit 384 400 000 000 mm.
Vous pliez une feuille en deux, puis encore en deux, puis encore en deux, etc...

1. Écrire, ci dessous, un algorithme qui calcule et affiche l’épaisseur obtenue après _n_ plis.

```txt
n ← demander("entrez un nombre de plis à effectuer")
epaisseur ← 0.1

Pour plis allant de 0 à n exclu
	epaisseur ← ........

afficher(.......)
```

2. Coder cet algorithme en Python et exécuter le.
3. Remplir le tableau de résultats obtenus pour les valeurs de _n_ suivantes :
| _n_ | _epaisseur_ |
| :--: | :--: |
| 1 | |
| 5 | |
| 10 | |
| 30 | |
| 40 | |
| 50 | |

4. Combien de plis sont nécessaires pour que l'épaisseur obtenue dépasse la distance Terre-Lune ?

## 4. Synthèse

La structure boucle bornée est codé en Python par l'instruction ........(1). 

Le bloc d'instruction est exécuté sur un ........(2), allant de la valeur ........(3) à la valeur ........(4) ........(5).

