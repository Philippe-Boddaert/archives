# La Tortue

## 1. Contexte

Cette activité a pour but de reproduire des dessins sur une grille.

![](./assets/origine.png)

Pour cela, vous programmez une tortue grâce au langage Python.

## 2. À Faire

Pour cela, nous allons utiliser la plateforme Castor-informatique.

1. Ouvrir un navigateur Web,

2. Rendez-vous à l'adresse [https://concours.castor-informatique.fr/](https://concours.castor-informatique.fr/),

3. Saisisez le code de l'activité fourni en début de séance. 

   __N.B : Au commencement de l'activité, un code personnel vous sera communiqué. Il est impératif de retenir ce code (sur papier ou photo via téléphone) pour revenir sur votre activité en cas de problème technique.__

4. Effectuez les séquences dans l'ordre. 

   __N.B : Avancez à votre rythme en commençant par la version une étoile de chaque séquence__

## 3. Programmation de la tortue

La tortue sait effectuer 4 actions : 

- __forward(pas)__ : la tortue avance du nombre de __pas__ indiqué en paramètre. 

  <table style="text-align: center;">
    <tr><td><img src="./assets/origine.png"/></td><td>Exemple : forward(50), la tortue avance de 50 pas.</td><td><img src="./assets/forward.png"/></td></tr>
    <tr><td>Situation de départ</td><td></td><td>Situation après appel de la fonction</td></tr>
  </table>

- __backward(pas)__ : la tortue recule du nombre de __pas__ indiqué en paramètre.

  <table style="text-align: center;">
    <tr><td><img src="./assets/origine.png"/></td><td>Exemple : backward(50), la tortue recule de 50 pas.</td><td><img src="./assets/backward.png"/></td></tr>
    <tr><td>Situation de départ</td><td></td><td>Situation après appel de la fonction</td></tr>
  </table>
  
- __left(angle)__ : la tortue pivote sur elle-même de l'__angle__ indiqué en paramètre.

  <table style="text-align: center;">
    <tr><td><img src="./assets/origine.png"/></td><td>Exemple : left(90), la tortue pivote d'un angle de 90° vers la gauche.</td><td><img src="./assets/left.png"/></td></tr>
    <tr><td>Situation de départ</td><td></td><td>Situation après appel de la fonction</td></tr>
  </table>
  
- __right(angle)__ : la tortue pivote sur elle-même de l'__angle__ indiqué en paramètre. 

	<table style="text-align: center;">
    <tr><td><img src="./assets/origine.png"/></td><td>Exemple : right(90), la tortue pivote d'un angle de 90° vers la droite.</td><td><img src="./assets/right.png"/></td></tr>
    <tr><td>Situation de départ</td><td></td><td>Situation après appel de la fonction</td></tr>
  </table>