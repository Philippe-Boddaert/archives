---
title : Séance 2 - Instructions conditionnelles et boucles
author : S. COLOMBAN et J. CHEVILLOT
license : CC-BY-NC-SA
---

# Séance 2 - Les instructions et boucles conditionnelles

## 1. Découverte du test

On considère le programme ci-dessous qui permet de calculer le prix (en euros) d’affranchissement d’une lettre en fonction de sa masse (en grammes).

```python
masse = float(input("donne-moi un décimal (masse en g) : "))

if masse < 20:  #"if" signifie "si"
    prix = 0.95
else:           #"else" signifie "sinon"
    prix = 1.60

print(prix)
```

__Quel sera le prix d’affranchissement d’une lettre pesant 30g ?__

## 2. Définition

__À RETENIR : SYNTAXE D’UN « SI... SINON... » EN PYTHON__

![](./assets/synthese_if.png)

_Source : S. COLOMBAN_

La _condition_ est souvent une comparaison entre deux valeurs, elle se code ainsi :

| Syntaxe Python | Description |
| :--------------: | :----------- |
|  $`x < y`$              | x inférieur à y |
|  $`x == y`$              | x égal à y |
|  $`x <= y`$              | x inférieur ou égal à y |
|  $`x > y`$              | x supérieur à y |
|  $`x\, != y`$              | x différent de y |
|  $`x >= y`$              | x supérieur ou égal à y |

## 3. À Faire

### Exercice 1

Compléter ci-dessous un programme en Python qui demande de saisir un entier $`n`$ et affiche suivant le cas : « __l’entier est positif ou nul__ » ou « __l’entier est négatif__ »

```python
n = int(input("entrez un entier n:")) 

if ........................... :
    ...................
else :
    ...................
```

### Exercice 2

Les tarifs d’entrée au théâtre sont :

- jusqu’à 10 ans : 5 €
- de 11 à 17 ans :13 €
- adultes : 25 €

Compléter le programme en Python qui demande de saisir un entier $`age`$ et affiche suivant le cas le prix à payer.

```python
age = int(input("entrez votre âge : "))

if .............. :
    prix = .......... 
else :
    if ................ : 
        prix = ..........
    else :
        prix = ..........
    
print("prix à payer =", prix)
```

## 4. Découverte de la boucle

On considère l’algorithme ci-dessous :

```txt
entier ← 5
tant que (entier <= 1000)
	afficher(entier)
	entier ← entier * 10
afficher("fin")
```

Ainsi que sa traduction en Python : 

```python
entier = 5
while entier <= 1000:
    print(entier)
    entier = entier * 10
print("fin")
```

1. Que signifie __while__ en Python ?
2. Sans saisir ce programme, donner la liste de toutes les valeurs affichées par celui-ci
3. Saisir ce programme sur Basthon et vérifier la réponse à la question précédente

## 5. Définition

__À RETENIR : SYNTAXE D’UNE BOUCLE EN PYTHON__

![](./assets/synthese_while.png)

## 6. À Faire

### Exercice 3

Soit l'algorithme suivant : 

```txt
afficher("bonjour")
a ← 10
b ← 30
tant que (a < 18)
	a ← a + 5
	b ← b + 10
afficher(b)
```

1. Compléter les pointillés afin de traduire l’algorithme suivant en Python

```python
print("bonjour")
a = 10
...
...
...
...
...
...
...
```

2. Un élève a créé le programme précédent l'a exécuté. Parmi les propositions suivantes, laquelle s’affiche à l’écran ?

![](./assets/exercice_3.png)

### Exercice 4

Le programme suivant permet de déterminer le plus petit entier $`n`$ dont le triple dépasse 10000.

```python
n = 0
while (n * 3 <= 10000):
    n = n + 1
print("n=", n)
```

1. Modifier ci-dessous la ligne 2 du programme précédent afin de déterminer le plus petit entier $`n`$ dont le double dépasse 50000.
2. Exécute le programme. Quelle valeur de $`n`$ trouvez-vous ?

### Exercice 5

Une feuille de papier mesure 0,1 mm d’épaisseur.
La distance Terre-Lune est de 384 400 km soit 384 400 000 000 mm.
Vous pliez une feuille en deux, puis encore en deux, puis encore en deux, etc...

1. Écrire, ci dessous, un algorithme qui calcule et affiche au bout de combien de plis, l’épaisseur obtenue dépassera la distance Terre-Lune.

```txt
epaisseur ← 0.1
plis ← 0

tant que ............. faire
	epaisseur ← ........
	plis ← ........

afficher(.......)
```

2. Coder cet algorithme en Python et exécuter le. Combien de plis sont nécessaires pour que l'épaisseur obtenue dépasse la distance Terre-Lune ?

```python
epaisseur = 0.1
plis = 0

while ............ :
    epaisseur = .........
    plis = .........

print(plis)
```
## 7. Synthèse

La structure if permet d'exécuter un bloc d'instructions si une ........(1) est vraie. 

Dans le cas contraire, le bloc d'instructions exécuté est celui défini après l'instruction ........(2).

La structure boucle conditionnelle est codée en Python par l'instruction ........(3). 

Le bloc d'instruction est exécuté tant que la condition est .........(4). Autrement dit, la boucle s'arrête dès que la condition est ..........(5).

## 8. Pour aller plus loin

### Exercice 6

Vous disposez de 10€ et économisez 3€ par jour.
Au bout de quelle durée (nombre de jours) pourrez-vous acheter une tablette à 100€ ?

Compléter le code Python suivant :

```python
duree = 0
somme = 10

while .......... :
	duree = .....
	somme = .....

print("Durée nécessaire :", .....)
```

### Exercice 7

![](./assets/exercice_7.png)

1. Compléter le tableau suivant :

| étape | rajout | nombre (triangulaire) |
| :-----: | :------: | :------------------: |
|    1   |   1     |         1           |
| 2 | 2 | 3 |
| 3 | 3 | 6 |
| 4 | | |
| 5 | | |
| 6 | | |

2. D'après ce tableau, quand on passe d'une ligne à la suivante, que deviennent les variables suivantes :

```txt
etape ← ......
rajout ← ......
nombre ← ......
```

3. Compléter le programme Python suivant :
```python
etape = 1
rajout = 1
nombre = 1

while etape <= ... :
	etape = ....
	rajout = ....
	nombre = ....

print("Le 30ème nombre triangulaire est :", nombre)
```

4. D’après ce programme, quel est le 30ème nombre triangulaire ? 
