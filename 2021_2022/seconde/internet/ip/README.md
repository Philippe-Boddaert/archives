---
title : Le protocole IP
author : M. BODDAERT, M.MATHIEU
license : CC-BY-NC-SA
---
# Le protocole IP

> Le protocole Internet Protocol (IP) est  responsable de l’__adressage__ et de la __fragmentation des paquets__ de données dans les réseaux numériques.
> Il assure que toute donnée comporte une information sur l'émetteur (source) et le récepteur (destination).

## 1. L'adressage

Pour comprendre ce que sont les adresses IP et les masques de sous-réseau, nous allons utiliser une comparaison avec les numéros de téléphone fixe.

### 1.1. Adresse IP

__Exemples__ :

- En téléphonie fixe, le numéro 04.97.13.20.00 est attribué à la mairie de Nice.
- Sur Internet, l'adresse IP 91.198.174.192 est attribué au serveur de Wikipedia.

#### 1.1.1. Définition

> L'adresse `IP` est une adresse numérique permettant d'identifier les  hôtes connectés à un réseau. 

Une adresse IP (dans sa version 4) est constituée de 4 nombres compris entre 0 et 255, séparés par des points.

__Exemple__ : 

```mermaid
flowchart
	a["88.45.124.201"]
```

Un rapide calcul : $`256^4 = 4\,294\,967\,296`$, nous donne le nombre d'adresses IPv4 possibles. 

Compte-tenu du développement d'internet, ce nombre est aujourd'hui trop petit (au passage, il y a aujourd'hui plus d'objets connectés à internet que d'humain).

Afin d'augmenter le nombre d'adresses possibles, une  nouvelle version, IPv6, est entrée en service en 2010 et cohabite avec la norme IPv4 : Les adresses IP ne sont plus codées sur 4 mais 16 nombres. Le nombre d'adresses disponibles théoriquement est donc colossal.

__Questions__ : 

1. Combien d'adresses sont disponibles en IPv6 ?
2. Soit l'adresse IPv4 `145.65.345.1`. Est-ce une adresse IP valide ? Si non, Pourquoi ?

### 1.1.2. À Faire : Connaitre son adresse IP

1. Cliquez sur le bouton `Windows` ![](./assets/1.png) 
2. Dans la barre de recherche, cliquez sur `Toutes les applications`

![](./assets/2.png)

3. Descendez dans la liste pour ouvrir le sous-menu `Windows Powershell`

![](./assets/3.png)

4. Lancez l'application Powershell (`Windows PowerShell ISE`)

![](./assets/4.png)

5. Dans la fenêtre noire, tapez `ipconfig` puis pressez la touche entrée.
	1. Notez votre adresse IP et les autres informations données
	2. Comparez-la avec celle de votre voisin (indiquez les différences et les points communs)
	3. À votre avis, pourquoi dans la salle, le début des adresses IP commencent par les mêmes nombres ?

## 2. Masque de sous réseau

## 2.1. Adresses dans un sous-réseau

En france, les numéros de téléphone sont répartis géographiquement selon les indicatifs.

![](http://www.frameip.com/wp-content/uploads/plan-numerotation-telephonique-zone-geographique-decoupage-departemental.jpg)
Source : https://www.frameip.com/plan-numerotation-telephonique/

Tout numéro de téléphone fixe commençant par 04 indique que le correspondant se situe dans le Sud-Est de la France.

Tout numéro de téléphone fixe commençant par 04.97, 04.92 ou 04.93 indique que le correspondant se situe dans le département Alpes Maritimes.

__Exemple__ :

- le numéro de la mairie de Nice : 04.97.13.20.00
- le numéro du lycée Masséna : 04.93.62.77.00
- le numéro du complexe sportif Jean Médecin : 04.93.86.24.01

__Dans un réseau local, l'adresse IP de chaque hôte commencera toujours par les mêmes nombres.__

## 2.2. Masque de réseau

Dans le cas des numéros de téléphones, les 2 premiers nombres sont liés au département français.

__Exemple__ : Le numéro du lycée Masséna __04.93__.62.77.00

```mermaid
flowchart LR
	subgraph "Numéro de téléphone"
		a["04"]
		b["93"]
		c["#"]
		d["#"]
		e["#"]
	end
	style a fill:#f00,color:#fff
	style b fill:#f00,color:#fff
	style c fill:#00f,color:#fff
	style d fill:#00f,color:#fff
	style e fill:#00f,color:#fff
```

La partie rouge indique que dans le département, tous les numéros de téléphone fixes commenceront obligatoirement par 04.93.

La partie bleue sera libre afin d'être affecté à chaque ligne de téléphone du département.

En informatique, c'est un peu la même chose : 

- les premiers nombres de l'adresse IP des ordinateurs sont liés au réseau local où ils se trouvent,
- les nombres suivants identifient l'hôte dans le réseau.

__Exemple__ : 192.168.0.12

```mermaid
flowchart LR
	subgraph "Numéro de téléphone"
		a["192"]
		b["168"]
		c["0"]
		d["#"]
	end
	style a fill:#f00,color:#fff
	style b fill:#f00,color:#fff
	style c fill:#f00,color:#fff
	style d fill:#00f,color:#fff
```

La partie rouge indique l'ID de réseau, c'est le __numéro du réseau__

La partie bleue indique l'ID d'hôte, c'est le __numéro de l’hôte__

La taille des numéros est variable est dépend du __masque de sous-réseau__

On écrit le masque de sous-réseau de la façon suivante :

| | | | | | | | |
| :--: | :--: | :--: | :--: | :--: | :--: | :--: | :-- |
| 192 | . | 168 |. |0| . | 12 | ← Exemple d'adresse IP |
| 255 | . | 255 |. | 255 | . | 0 | ← Masque de sous-réseau |

- Le 255 signifie que le nombre correspond à la partie réseau et ne peux pas être changé,
- Le 0 signifie que le nombre correspond à la partie hôte et peut être changé pour nommer chaque machine.

### 2.2.1. Plage d'adresse IP

Deux adresses IP différentes qui ont la même partie réseau sont dites dans la __même plage__.

__Exemples__ :

- Avec le masque __255.255.255__.0, __192.168.0__.1 et __192.168.0__.2 sont dans la même plage.
- Avec le masque __255.255.255__.0, __192.168.0__.1 et __192.168.1__.1 ne sont pas dans la même plage.
- Avec le masque __255.255__.0.0, __192.168__.0.1 et __192.168__.1.1 sont dans la même plage.

__Pour que deux ordinateurs d'un même réseau local puissent communiquer, ils doivent être dans la même plage d'adresse IP__

__Questions__ :

1. Avec le masque de sous-réseau `255.255.255.0`, combien peut-on avoir d'hôtes au maximum identifiés dans notre réseau local ?
2. Avec le masque de sous-réseau `255.255.0.0`, combien peut-on avoir de machines au maximum identifiées dans notre réseau local ?
3. Soit le réseau suivant, constitué de 4 hôtes, dont 3 ordinateurs.
	1. Est-ce que les hôtes P1 et P2 peuvent communiquer ensemble ? Justifier votre réponse.
	2. Est-ce que les hôtes P1 et P3 peuvent communiquer ensemble ? Justifier votre réponse.
	3. Comment faire pour que les 3 autres hôtes puissent communiquer entre eux ? Justifier votre réponse.

![](./assets/filius_1.png)

### 2.2.2. À Faire : Tester la connexion

1. Dans la barre de recherche du menu Windows de votre ordinateur, tapez "Powershell" et lancez l'application.
2. Dans la fenêtre noire, tapez `ping [Adresse IP]` où `[Adresse IP]`est l'adresse IP de votre voisin, puis pressez la touche entrée.
3. En cherchant sur le web l'utilité de  la commande `ping` interprétez le résultat obtenu.

## 3. Synthèse 

Faire une synthèse de la séance en __recopiant et complétant__ le texte suivant :

Dans un réseau, chaque hôte possède une .....................(1)  qui permet de l'identifier de manière unique. 

Pour que deux hôtes puissent communiquer directement, ils doivent être dans le même réseau, c'es-à-dire avoir le même  ................(2) de réseau. 

L'échange de données entre deux hôtes nécessitent qu'ils adoptent un même langage. On parle de ................(3) de communication. Celui utilisait pour identifier les hôtes est le ................(4)

La commande ................(5) permet d'afficher les paramètres réseau de l'hôte.

La commande ................(6) permet de tester la connexion d'un hôte à un autre.

## 4. Pour aller plus loin

On ne rentre pas dans le détail du fonctionnement du masque de sous-réseau, mais sachez qu'il est possible d'avoir par exemple un masque de la forme `255.255.240.0`

Ce masque est entre `255.255.255.0` et  `255.255.0.0` mais il faut alors travailler avec l'écriture binaire des adresses IP pour en comprendre le fonctionnement.

Si vous voulez plus de renseignements, je vous invite à chercher par vous même car cela n'est pas au programme de notre travail...Une ressource pour vous aider : [une vidéo d'Atika EL ORCH](https://www.youtube.com/watch?v=cF6o2Uq_gWg)

