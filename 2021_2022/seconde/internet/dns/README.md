---
title : DNS, l'annuaire d'Internet
author : M. BODDAERT, M.MATHIEU
license: CC-BY-NC-SA
---
# DNS, l'annuaire d'Internet

## Avant-Propos

Le 05 Octobre 2021, les applications Facebook, Instagram, Messenger et Whatsapp ont été bloqués pendant plus de 6 heures, affectant des milliards d'utilisateurs dans le monde (Source : [Franceinfo](https://www.francetvinfo.fr/internet/reseaux-sociaux/facebook/panne-de-facebook-instagram-messenger-et-whatsapp-trois-questions-apres-l-incident-geant-qui-a-affecte-les-reseaux-sociaux-lundi_4795571.html))

![Source : Pixabay](./assets/interrogation.jpg)

__Que s'est-il passé ?__

## 0. Retour sur l'adresse IP

> Connaissez vous le super moteur de recherche 216.58.214.163 ? Non ? Pourtant vous l'utilisez très souvent !

> De la même manière que votre amie Alice poste quotidiennement ses pensées sur un morceau d'eurodance sur le fameux 95.100.86.78 !

> Et que dire de Bob, qui fait f5 toutes les 10 min sur 46.33.191.5 ?

Cela vous parait idiot ? Pourtant les adresses IP citées au dessus sont bel et bien utilisées quotidiennement par des milliers de personnes. Néanmoins, vous les connaissez probablement mieux sous leur petit nom *__google__*, *__tiktok__* et *__pronote__* !

Comme nous l'avons vu avec l'adresse IP, chaque machine possède un identifiant sur le réseau. Tout comme chaque personne possède une adresse physique.

## 1. Retour sur 40 ans d'histoire

Voici un tableau de l'évolution du nombre d'hôtes connectés en réseau mondial (Source : ISC) :

| Année | Nombre d'ordinateurs hôtes | Événement |
| :--: | :--: | :-- |
| 1969 | 4 | Création du réseau Arpanet |
| 1971 | 19 | |
| 1981 | 213 | |
| 1983 | 562 | Création du réseau Internet |
| 1984 | 1 024 | |
| 1985 | 1 961 | |
| 1986 | 5 089 | |
| 1987 | 28 174 | |
| 1988 | 56 000 | |
| 1989 | 159 000 | Création du Web |
| 1990 | 313 000 | Création de la première page Web |
| 1991 | 617 000 | |
| 1992 | 1 136 000 | |
| 1993 | 2 056 000 | |
| 1994 | 3 864 000 | |
| 1995 | 15 000 000 | |
| 2001 | 100 000 000 | |
| 2006 | 350 000 000 | |
| 2020 | Plusieurs milliards... | |

## 2. La problématique

À la création d'Internet en 1983, on stockait les adresses des hôtes dans un fichier HOST.TXT, que __chaque hôte devait posséder pour connaitre l'ensemble des adresses IP joignables__.

![](./assets/fichier_txt.png)

Ce système fonctionne pour quelques centaines d'hôtes, mais atteint ses limites face à la croissance exponentielle du nombre d'hôtes connectés.

Très rapidement, cette solution devint ingérable.

En 1983, le protocole __DNS__ (**D**omain **N**ame **S**ystem) voit le jour. 

Ce système montra son utilité : quatre années plus tard, on enregistrait plus de 20 000 enregistrements de nom de domaine.

Pour nous autres, êtres humains, il est complexe de retenir toutes les adresses IP des hôtes que l'on souhaite contacter. Et à moins de tout noter dans un répertoire, jamais vous ne vous rappellerez de tous ces nombres. __C'est là que le DNS intervient__.

## 3. La solution : Le DNS

### 3.1. Adresse symbolique

Le protocole DNS permet d'associer une ___adresse IP___ à une *__adresse symbolique__* aussi appelée __*URL*__: celle ci est simple à retenir, et très souvent en rapport avec son contenu.

Par exemple, le site `www.learnthenet.com` est le site qui permet d'en apprendre sur l'internet et son adresse IP est : `206.80.4.157`.

En tant qu'utilisateur, il est plus simple d'utiliser l'adresse symbolique `www.learnthenet.com` que l'adresse physique `206.80.4.157`.

Tout comme, il est plus aisé d'utiliser l'adresse `www.google.com`que `216.58.214.163`.

### 3.2. Principe du DNS

Le protocole DNS peut être installé sur tout hôte d'un réseau avec un logiciel approprié (BIND, AnswerX, Knot DNS...), l'hôte devient alors __serveur DNS__.

Tout hôte $`A`$ qui souhaite contacter un autre hôte $`B`$ à partir de son adresse symbolique, contacte le serveur DNS qui communique à $`A`$ l'adresse IP de $`B`$ avec laquelle il peut le contacter. 

```mermaid
flowchart TB
	A[Hôte A] -->|1. demande l'adresse IP de example.com|C[Serveur DNS];
	C -->|2. indique l'adresse IP de B à partir de son url|A;
	A -->|3. contacte via l'adresse IP de B|B[Hôte B : www.example.com];
```

On dit alors que le serveur DNS opère une __résolution de nom de domaine__, i.e obtenir l'adresse IP à partir de l'URL.

__Illustration avec l'adresse www.learnthenet.com  :__ 

![dns.gif](assets/dns.gif)

_Source : http://www.learnthenet.com graphics licensed from ReadyGo_

### 3.2. Décomposition d'une URL

Une __URL__ (**U**niform **R**essource **L**ocator), est composée de 5 parties : 

1. Le protocole, 
2. Le sous-domaine, 
3. Le nom de domaine principal, 
4. Le domaine de deuxième niveau,
5. Le répertoire.

Par exemple, dans cette adresse, que peut on retrouver ? 

![url_ada.png](assets/url_ada.png)

| Partie                                                  | Type                       | Définition                                                   |
| ------------------------------------------------------- | -------------------------- | ------------------------------------------------------------ |
| HTTPS                                                   | Protocole                  | Indique au navigateur le type de ressource souhaitée         |
| WWW                                                     | Sous-domaine               | Séparé du domaine par un point, il permet de diviser un site en plusieurs parties.Par convention, on le nomme www s'il s'agit d'une page web. |
| franceculture                                           | Domaine Principal          | Généralement, indique le nom de votre site web et permet immédiatement de savoir ce que l'on va y trouver. |
| fr                                                      | Domaine de deuxième niveau | Précise le type d’entité sous laquelle est enregistrée votre organisation sur internet (ici, la France, mais on trouve org, net, com...) |
| numerique/ada-lovelace-la-premiere-codeuse-de-lhistoire | Repertoire                 | Indique la ressource à laquelle vous êtes en train d'accéder (page web, vidéo, fichier texte, page html, dossier...) |

### 3.3. À Faire

1. Répondez à la question "__Si une partie de l'URL est incorrecte, que se passe -il ?__" Pour y répondre, regardez la vidéo suivante :

[![principe_url](https://img.youtube.com/vi/WLZM2SoSArc/0.jpg)](https://www.youtube.com/watch?v=WLZM2SoSArc)

2. Donnez les différentes parties de l'url https://fr.wikipedia.org/wiki/Ada_Lovelace en remplissant le tableau correspondant :

| Partie | Type                       | Définition                                                                                                                                    |
|--------|----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| ...    | Protocole                  | Indique au navigateur le type de ressource souhaitée                                                                                          |
| ...    | Sous-domaine               | Séparé du domaine par un point, il permet de diviser un site en plusieurs parties.Par convention, on le nomme www s'il s'agit d'une page web. |
| ...    | Domaine Principal          | Généralement, indique le nom de votre site web et permet immédiatement de savoir ce que l'on va y trouver.                                    |
| ...    | Domaine de deuxième niveau | Précise le type d’entité sous laquelle est enregistrée votre organisation sur internet (ici, la France, mais on trouve org, net, com...)      |
| ...    | Repertoire                 | Indique la ressource à laquelle vous êtes en train d'accéder (page web, vidéo, fichier texte, page html, dossier...)                          |

### 3.4 À Faire

la commande `nslookup [url]` permet d'obtenir l'IP associée à une URL. 

Exemple : ![](./assets/nslookup.png)

L'adresse associée à www.google.com est `216.239.38.120`

1. Avec le logiciel Powershell, tapez la command `nslookup [url]` pour les urls suivantes et notez les adresses IP obtenues :
	1. www.example.com
	2. nintendo.co.jp
	3. www.lfpasteur.com.br 
2. Saisir les adresses obtenues à la question 3 dans un navigateur. Quels sont les résultats obtenus ?

## 4. Organisation de l'annuaire

Il n'existe pas __un__ annuaire centralisé contenant l'ensemble des adresse symboliques.

L'annuaire DNS est dit __décentralisé__ : chaque serveur DNS connait une partie de l'annuaire, L'ensemble des serveurs DNS sont structurés entre eux de manière hiérarchique et constitue au global l'annuaire entier.


```mermaid

graph TB;
subgraph racine
    a["."]
    end
subgraph Domaines du plus haut niveau
	a-->com;
    a-->org;
    a-->net;
    a-->gov;
    a-->...;
    end
subgraph Sous-domaines
    com-->google;
    org-->wikipedia;
    net-->etc...;
    end
subgraph Sous-domaines de google
    google-->www;
    end
subgraph Sous-domaines de wikipedia
    wikipedia-->fr;
    wikipedia-->en;
    end
    
```

### 4.1 Localisation des serveurs DNS racine

![Carte des serveurs DNS](https://upload.wikimedia.org/wikipedia/commons/e/ee/Root-current.svg)

_Carte de localisation des 123 serveurs DNS ___racine___ en 2006, Source : Wikipédia_

### 4.2. Requête de résolution de nom

Lors d'une requête pour obtenir l'adresse IP, les serveurs DNS sont amenés à être appelés successivement jusqu'à celui qui a l'adresse symbolique / adresse IP dans son annuaire.

__Illustration en vidéo__ :

[![Resolution de nom](https://img.youtube.com/vi/av0zX-dr8o8/0.jpg)](https://www.youtube.com/watch?v=av0zX-dr8o8)


## 5. Synthèse

**Faire une synthèse en copiant et complétant le texte suivant** :

Tout hôte du réseau Internet a besoin de connaitre ................(1) de l'hôte qu'il souhaite contacter. 

Face à la croissance du nombre d'hôtes, ................(2) a été créé en 1983. Un serveur implantant ce mécanisme permet d'associer une ................(3) à une ................(4) d'un hôte.

L'adresse symbolique est appelée ................(5) et se décompose en 5 parties dont le protocole, le ................(6), le ................(7), le ................(8) et le répertoire.

L'annuaire DNS est dit ................(9), car il n'existe pas un seul serveur DNS connaissant l'ensemble des entrées de l'annuaire mais il est réparti sur la globalité des serveurs.

Lorsqu'un client contacte le serveur DNS, on dit qu'il opère une ................(10).

La commande ................(11) permet d'obtenir le résultat de cette opération.

