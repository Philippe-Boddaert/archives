---
title : Enjeux éthique et sociétaux d’internet
author : M. Pablo Sanchez
---
#  Devoir Maison - Enjeux éthique et sociétaux d’internet

## Présentation

Ce devoir à la maison consiste à étudier des documents divers (vidéo, graphique, texte...) autour de la notion de __la neutralité du net__.

Vous devez : 

- Effectuer les activités, décrites dans la partie __1. Activités à réaliser__
- Pour cela, vous vous aiderez des documents mis à disposition dans la partie __2. Documents à disposition__,
- Respecter les consignes de rendus, décrites dans la partie __3. Consignes pour le rendu__

## 1. Activités à réaliser

### 1. Questions

Répondre aux questions suivantes, en justifiant le plus possible :

1. Comment définir le principe de la neutralité du net ?
2. Comment garantir la liberté de communication sur internet ?
3. Comment réguler le trafic vidéo, qui consomme plus des deux tiers du trafic total d'Internet ?

### 2. Commentaire argumenté

Réaliser à l’aide des __documents présentés__ et de __vos recherches personnelles__, un commentaire argumenté sur __le sujet de votre choix__ parmi les 2 sujets suivants :

| Sujet 1 | Sujet 2 |
| :-- | :-- |
| Développer un argumentaire à propos des __garanties que peut apporter la neutralité du Net__ dans notre société. | Développer un argumentaire à propos des __inconvénients liés à l’absence de la neutralité du Net__ dans notre société. |

<div style="page-break-after: always;"></div>

## 2. Documents à disposition

## Document 1. Vidéos 

1. Vidéo "___Neutralité, j'écris ton nom___" : https://www.youtube.com/watch?v=hZnq3xg-PRM

2. Vidéo "___Qu’est-ce que la neutralité du Net ?___" : https://www.youtube.com/watch?v=A3dElmSY8q4

_Quelques réflexions à avoir en regardant les vidéos_ :

- Que permet la neutralité du net ?
- Quel est son principe ?
- A quoi ressemblerait le net sans la neutralité ?
- Pourquoi les institutions (politiques, économiques) sont divisées à propos du principe de neutralité du net ?
- Noter des cas où les opérateurs ont transgressés la principe de neutralité.

## Document 2. Définition

La __neutralité du Net__ :

La __neutralité d’internet__ est un principe qui garantit l’égalité de traitement de tous les flux de données sur Internet. Selon Sébastien Soriano, Président de l’Autorité de Régulation des Communications Electronique et des Postes (ARCEP) :

> « C’est la liberté de circulation dans le monde numérique. Sur internet, c’est la liberté d’innover, de poster des contenus, de consulter tout ce que l’on veut, sur tous les sites, les applications que l’on veut sans avoir des biais qui soient introduits par des intermédiaires. La neutralité du net, c’est avoir accès au vrai internet

Concrètement, cela concerne les __FAI (Fournisseur d’Accès Internet)__ qui se doivent de transmettre des données sans en examiner le contenu ou l’altérer, sans prise en compte de la source ou de la destination des données et sans privilégier un protocole de communication.

## Document 3. Trafic Internet mondial

<img src="./assets/trafic.png" style="zoom:50%;" />

_Source : 1H2018 Global Internet Phenomena_

Le monde est de plus en plus connecté : en 2019, 51% de la population mondiale a accès à internet. Le débit des connexions va augmenter. 

Le débit moyen fixe va ainsi doubler en 5 ans pour s’établir à 42,5 mégabits par seconde. La majorité du trafic se fera par des mobiles (70%), transitera par du WI-FI (53%).

Enfin l’usage de la vidéo sera le principal facteur de croissance (80% du trafic d’ici 5 ans)

| 1992 | 1997 | 2002 | 2013 | 2018 |
| :--: | :--: | :--: | :--: | :--: |
| 100go par jour | 100go par heure | 100go par seconde | 28 875go par seconde | 50 000go par seconde |


## Document 4. Principe de la neutralité du Net

<img src="./assets/afp.png" style="zoom: 50%;" />

_Source : afp_

<div style="page-break-after: always;"></div>

## Document 5. Fournisseurs d'Accès Internet

![](./assets/fai.png)

_Source : tribu_

<div style="page-break-after: always;"></div>

## 3. Consignes pour le rendu

Vous répondrez aux questions sur un __document numérique fait sur un traitement de texte de votre choix__. Si vous avez un problème d’accès à un matériel informatique, le rendu sur feuille papier est __toléré__.

### 3.1. Consignes

- Page A4, police __Arial 12__,
- Nom, Prénom et Classe en __en-tête__
- Export de votre document en format __PDF__
- Le fichier doit se nommer : __NOM_Prénom-enjeux.pdf__

Une fois votre devoir finalisé, vous l’importerez dans pronote.

Devoir à rendre avant : __Lundi 8 Novembre 2021 23:59__

_Tout devoir remis en retard ne sera pas corrigé._

### 3.2. Barème

Le devoir sera noté sur __10 points__ :

| Intitulé | Nombre de points | 
| :-- | :--: |
| Questions | __3 points__ |
| Sujet 1 ou 2 au choix | __4 points__ |
| Respect des consignes | __3 points__ |

