---
title : Réseau, Internet, Définitions
author : M. BODDAERT, M.MATHIEU
license : CC-BY-NC-SA
---
# Réseau, Internet : Définitions

## 1. Première définition

Selon l'INSEE (Institut national de la statistique et des études économiques) :

> __Internet__ : Ensemble de __réseaux__ mondiaux __interconnectés__ qui permet à des ordinateurs et à des serveurs de communiquer efficacement au moyen d'un __protocole de communication commun__ (IP). Ses principaux __services__ sont le __Web__, le FTP, la messagerie et les groupes de discussion.

Détaillons chacun des termes de cette définition.

## 2. Qu'est-ce qu'un réseau ?

### 2.1. Définition

> On appelle __réseau__ (___network___)  un ensemble d'__équipements__ reliés entre eux pour échanger des informations.

__Question__ : Quels types de réseaux connaissez-vous ? Quelles informations échangent-ils ?

| Réseau | Informations échangées |
| :--: | :--: |
| | | 
| | |
| | |
| | |

Nous allons ici parler des __équipements__ qui échangent des données dans un __réseau informatique__.

__Question__ : Citer au moins 5 équipements pouvant faire partie d'un réseau informatique.

| Équipements d'un réseau informatique |
| :--: |
| |
| |
| |
| |
| |
| |

### 2.2. Les constituants d'un réseau

Voici le schéma d'une partie du réseau du lycée.

![](./assets/filius_1.png)

#### 2.2.1. Les hôtes : clients et serveurs

- Tout d'abord il y a les **hôtes** (ou host), c'est à dire toutes les machines échangeant des données sur le réseau :
	- Ordinateurs
	- Smartphone
	- Tablettes
	- Imprimantes.
	- Télévision
	- Console vidéo
	- Appareils connectés (thermostats, réfrigérateur, four, volets roulants ...)
- Parmi ces hôtes, il y a :
	- Les __clients__ : par extension, ce sont les machines qui utilisent un logiciel, appelé client, qui envoie des requêtes l'autre type d'hôte, c'est à dire les serveurs.
	- Les __serveurs__ : par extension, ce sont les machines qui utilisent un logiciel, appelé serveur, qui attend les demandes des clients et y répond, s'il le peut.

#### 2.2.2. Interconnexions

Les interconnexions permettent au données de transité d'un équipement à un autre. 

- La carte réseau qui permet de traduire les données en un format transportable sur les médias cités juste en dessous.

![carte reseau - wikipedia - GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Network_card.jpg/220px-Network_card.jpg)

- Des câbles, comme le rj45 ci-dessous, sont un média habituel des réseaux.

![câble rj45 - wikipedia CC BY SA [auteur](https://commons.wikimedia.org/wiki/User:David.Monniaux)](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Ethernet_RJ45_connector_p1160054.jpg/200px-Ethernet_RJ45_connector_p1160054.jpg)

- les médias sans sans fil (Wifi - 3G - 4G - 5G ...) sont de plus en plus présents.

#### 2.2.3. Les périphériques réseau

##### 2.2.3.1. Commutateur (switch)

Pour relier plusieurs machines entre elles, on utilise un `commutateur` , appelé `switch` en anglais.

![image switch - wikipédia - CC BY SA auteur [geek2003](https://commons.wikimedia.org/w/index.php?title=User:Geek2003&action=edit&redlink=1)](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/5600-Stack-Front-Avaya_HiRes.jpg/220px-5600-Stack-Front-Avaya_HiRes.jpg)

En reliant plusieurs machines entre eux à l'aide d'un switch, on crée un `LAN`. 

Pour communiquer, les appareils doivent pouvoir être identifiés. Un peu  comme chaque maison à son adresse, les appareils connectés s'identifient à l'aide d'une adresse appelée `adresse IP`.

##### 2.2.3.2. Routeur

Un `routeur` (router en anglais)  assure la circulation des informations entre les **différents** réseaux. Avec les switchs, les routeurs sont responsable de la _livraison_ des données entre les hôtes.

![routeur - wikipedia - CC BY SA auteur[Hellisp](https://commons.wikimedia.org/wiki/User:Hellisp)](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Dlink_wireless_router.jpg/300px-Dlink_wireless_router.jpg)

##### 2.2.3.3. Remarque sur la box Internet

Une box internet joue le rôle de switch de routeur et assure la liaison entre le réseau local et le **F**ournisseur d'**A**ccès à  **I**nternet (FAI).   

### 2.3.. Type de réseaux

On distingue différents types de réseaux:

- le réseau local  appelé `LAN` pour **L**ocal **A**rea Network : formé des machines réunies dans  une même pièce ou bâtiment,
- le réseau urbain `MAN` pour **M**etropolitan Local  **A**rea **N**etwork : réseau à l'échelle d'une ville ou d'une agglomération,
- le réseau étendu `WAN` pour **W**ide Local  **A**rea **N**etwork : réseau reliant plusieurs sites ou ordinateurs du monde entier.
- `Internet`, le réseau des réseaux : Il est constitué  d'une multitude de réseaux _locaux_ qui se sont petit à petit connectés entre eux pour former le plus vaste des réseaux.

![](./assets/filius_2.png)

### 2.4. Synthèse

__Recopier et compléter le texte à trou suivant__ :

Un réseau est un ensemble .............(1) reliés entre eux afin de permettre la .............(2) de données.

Les machines (ordinateurs, tablettes, imprimantes) connectés à un réseau sont appelées .............(3).

On appelle .............(4), une machine qui envoie une requête sur le réseau et .............(5), la machine qui répond à cette requête.

Pour se connecter à un réseau, une machine a besoin d'une .............(6) réseau, de .............(7) ou de .............(8).

Dans un même réseau, les machines sont connectées via un .............(9).

Un .............(10) permet de connecter différents réseaux entre eux. On peut dire qu'.............(11) est le réseau des réseaux.

### 2.5. À Faire

Sur une __feuille__, dessiner le réseau local de chez vous avec l'ensemble des matériels, périphériques et interconnexions le constituant.

## 3. Internet 

Pour fonctionner, Internet a besoin d'une quantité de câbles hallucinante. Une partie d'entre eux sont fabriqués... à Calais, en France.

Regardez un peu [ici](https://www.submarinecablemap.com/) pour en avoir un aperçu des câbles sous-marins.

Pour communiquer, les ordinateurs (ou tous autres appareils connectés entre eux) utilisent des règles appelées `protocoles`. Ils existent de nombreux protocoles, chacun d’entre eux réalisant une tâche bien précise. 

### 3.1 À Faire

__Lire le document__ [Protocole](./PROTOCOLE.md) et __copier en complétant__ le texte à trou :

Un protocole est un ensemble de .............(1) permettant aux ordinateurs de communiquer entre eux.

Pour que les réseaux soient compatibles entre eux, il faut que les protocoles gèrent les contraintes :

- .............(2) du matériel,
- .............(3) des hôtes,
- .............(4) de données, notamment l'administration des pertes
- .............(5) des données sous forme binaire

Ainsi, le modèle .............(6) répond à ces contraintes via 4 couches de protocoles :

- la couche  .............(7) choisit le mode de transmission,
- la couche  .............(8) choisit le mode de transport. selon la fiabilité nécessaire,
- la couche  .............(9) permet d'interconnecter les réseaux,
- la couche  .............(10) matérialise le transport des données

## 4. Internet $`=`$ ou $`\neq`$ Web ?

__Question__ : Est-ce-que Internet et le Web c'est la même chose ?

- __Internet__ est un réseau d'ordinateurs qui communiquent entre eux à l'aide de protocoles. C'est comparable au réseaux routiers, où le code de la route en est un _protocole_,
- __Le Web__ est l'une des applications qui utilise __internet__. Il y en a d'autres : courrier électronique, transfert de fichiers, voip pour la téléphonie etc...C'est comparable aux voitures sur le réseau routier, où chacune emprunte la route, en respectant le code.

