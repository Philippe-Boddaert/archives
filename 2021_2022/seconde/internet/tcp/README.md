---
title : Le protocole TCP
author : M. BODDAERT, M.MATHIEU
license : CC-BY-NC-SA
---

# Le protocole TCP

> Le protocole __TCP__, ou *Transmission Control Protocol* est toujours associé à IP, sous la dénomination __TCP/IP__. 
>
> Il s'agit d'un membre essentiel d'Internet : il assure l'__échange de données__, en __garantissant l'arrivée à destination__ de paquets.

## 1. Livraison de données

### 1.1. Définition 

Imaginons que vous souhaitiez acheter quelque chose sur votre site de e-commerce préféré : lorsque vous passez votre commande, vous devez __indiquer votre adresse postale__, afin d'__assurer la livraison__ jusqu'à votre domicile.

Dans le réseau Internet, c'est la même chose, grâce à l'adresse __IP__ : chaque machine a la sienne, et vos données peuvent traverser tout le réseau mondial jusqu'à parvenir au bon destinataire : on parle alors de __routage__.

### 1.2. Problématique

Prenons une personne nommée _Arobase_ passant une commande de 15 articles auprès d'une célèbre chaine de magasin de meuble _IKEB_, via son site web.

![diagramme.svg](./assets/ikeb.svg)

Hélas, la commande est volumineuse pour être livré en une fois, en effet, le poids maximum d'un colis est de 1,5 kg.

Le commerçant IKEB est soumis aux problématiques suivantes :

- Comment découper la commande pour faire parvenir l'ensemble des éléments ?
- Comment s'assurer que tous les éléments arrivent bien à Arobase ?
- Quelle.s solution.s le commerçant va t-il choisir pour expédier les différents colis ?

__En informatique__

![](./assets/tcp.jpg)

Cela fonctionne de la même manière : un hôte client souhaite récupérer un fichier volumineux depuis un hôte serveur. 

Or, __sur Internet, la taille maximale d'un paquet est de 1,5Ko__ (1 Kilo octet = 8000 bits).

Le serveur, comme le commerçant IKEB est soumis aux mêmes problématiques : 

- ___Comment découper le fichier ?___
- ___Comment s'assurer que tous les "paquets" de fichiers parviennent à destination ?___
- ___Quelle.s solution.s le serveur peut-il mettre en place pour acheminer l'ensemble des paquets ?___

$`\Rightarrow`$ Le protocole TCP offre 2 solutions.

## 2. Solutions de livraison

Le commerçant IKEB a le choix entre deux méthodes de livraison :

- Soit l'ensemble des paquets sera livrée en trois jours, avec une numérotation précise de chaque colis et un accusé de réception de la part d'Arobase,
- Soit la commande arrivera dès le lendemain, mais sans aucune garantie de fiabilité.

On peut rapprocher ces deux méthodes à deux protocoles de transmission de données pour un hôte serveur sur Internet :

- [x] TCP
- [x] UDP

### 2.1. TCP : méthode fiable

TCP assure une __qualité de service__, c'est à dire qu'il assure le découpage du fichier en plus petits paquets, en permettant le routage des données par quelques chemins que cela soit tout en promettant une reconstitution des fichiers demandés.

#### 2.1.1. Illustration

Prenons l'exemple d'un hôte client souhaitait récupérer un fichier se trouvant sur un hôte serveur.

![](./assets/tcp.jpg)

__Étape 1__ : 

- le client envoie une requête au serveur ("Envoie moi le fichier avec la méthode TCP"),
- le serveur découpe le fichier en 3 paquets (matérialisés par des ronds de couleur)

![](./assets/tcp_1.jpg)

__Étape 2__ : 

- le serveur envoie le premier paquet (rouge) au client et lui demande un accusé réception pour ce paquet.

![](./assets/tcp_2.jpg)

__Étape 3__ : 

- le client reçoit le premier paquet (rouge),
- il envoie un accusé de réception au serveur.

![](./assets/tcp_3.jpg)

__Étape 5__ : 

- le serveur reçoit l'accusé de réception du premier paquet (rouge),
- il envoie le deuxième paquet (vert) au client et lui demande un accusé réception pour ce paquet.

![](./assets/tcp_4.jpg)

__Étape 6__ : 

- Au bout d'un certain temps, le serveur identifie qu'il n'a pas reçu d'accusé réception du deuxième paquert (vert) du client.

![](./assets/tcp_5.jpg)

__Étape 7__ : 

- Le serveur renvoie le deuxième paquet (vert) au client et lui demande un accusé réception pour ce paquet.

![](./assets/tcp_4.jpg)

__Étape 8__ : 

- le client reçoit le deuxième paquet (vert),
- Il envoie un accusé de réception au serveur.

![](./assets/tcp_6.jpg)

__Étape 9__ : 

- le serveur reçoit l'accusé de réception du deuxième paquet (vert),
- il envoie le troisième paquet (bleu) au client et lui demande un accusé réception pour ce paquet.

![](./assets/tcp_7.jpg)

__Étape 10__ : 

- le client reçoit le troisième paquet (bleu),
- Il envoie un accusé de réception au serveur.

![](./assets/tcp_8.jpg)

__Fin__ :

- le serveur n'a plus de paquets à envoyer,
- le client a reçu l'ensemble des paquets et peut reconstituer le fichier.

#### 2.1.2. À Faire

__Répondre aux questions suivantes__ : 

1. Qu'implique la méthode TCP sur le fichier transmis par le serveur ?
2. Comment le client parvient-il à reconstituer le fichier ?
3. Qu'implique la méthode TCP sur la durée totale d'acheminement d'un fichier ?
4. Que faire si un paquet se perd ou arrive en double avec la méthode TCP ?

### 2.2. UDP : méthode non fiable

Contrairement au protocole TCP, UDP s'affranchit de la numérotation des paquets et n'assure en aucun cas la fiabilité des données transmises.

#### 2.2.1. Illustration

Prenons l'exemple d'un hôte client souhaitait récupérer un fichier se trouvant sur un hôte serveur.

![](./assets/tcp.jpg)

__Étape 1__ : 

- le client envoie une requête au serveur ("Envoie moi le fichier avec la méthode UDP"),
- le serveur découpe le fichier en 3 paquets (matérialisés par des ronds de couleur).

![](./assets/tcp_1.jpg)

__Étape 2__ : 

- le serveur envoie le premier paquet (rouge) au client __sans__ demander un accusé réception pour ce paquet.

![](./assets/udp_2.jpg)

__Étape 3__ : 

- le client reçoit le premier paquet (rouge),
- le serveur envoie le deuxième paquet (vert) au client __sans__ demander un accusé réception pour ce paquet.

![](./assets/udp_3.jpg)

__Étape 4__ : 

- le client __ne reçoit pas__ le deuxième paquet (vert),
- le serveur envoie le troisième paquet (bleu) au client __sans__ demander un accusé réception pour ce paquet.

![](./assets/udp_4.jpg)

__Étape 5__ : 

- le client reçoit le troisième paquet (bleu)

![](./assets/udp_5.jpg)

__Fin__ :

- le serveur n'a plus de paquets à envoyer,
- le client n'a pas reçu l'ensemble des paquets et ne peut pas reconstituer le fichier.

#### 2.2.2. À Faire

__Répondre aux questions suivantes__ : 

1. Qu'implique la méthode UDP sur le fichier transmis par le serveur ?
2. Comment le client parvient-il à reconstituer le fichier ?
3. Qu'implique la méthode UDP sur la durée totale d'acheminement d'un fichier ?

### 2.3 À Faire

Voici une liste d'usages d'Internet :

| Usages | TCP (fiable)  | UDP (non fiable) |
| :------ | :----: | :----: |
| 1. Arobase souhaite envoyer des fichiers personnels sur son cloud Bobble Drive  |     |     |
| 2. Arobase regarde la retransmission d'une partie de son jeu favori sur une plateforme de streaming | | |
| 3. Arobase envoie un e-mail souhaitant un bon anniversaire à sa mamie | | | 
| 4. Arobase télécharge une application sur son téléphone portable | | | 
| 5. Arobase écoute le dernier morceau de son artiste préféré sur Spotifaille | | |
| 6. Arobase suit son cours de SNT via la plateforme du CNED "Ma classe à la maison" | | |

__Répondre à la questions suivante__ :

Indiquer pour chaque usage s'il nécessite l'usage de la méthode TCP ou UDP (en faisant une croix dans la bonne colonne)

## 3. Synthèse 

Faire une synthèse de la séance en __recopiant__ et __complétant__ le texte suivant :

TCP et UDP sont des .....................(1) de routage,

Sur le réseau Internet, les données sont découpées en .....................(2), dont la taille maximale est de .....................(3)

TCP permet d'assurer un échange .....................(4), c'est-à-dire que les données transmises sont bien arrivées à destination. Pour cela, la méthode TCP nécessite la .....................(5) des paquets et un .....................(6) de la part de l'hôte client.

UDP offre une méthode différente de TCP. Elle ne garantit pas le bon acheminement des paquets mais garantit un échange plus .....................(7) que TCP.

.....................(8) est un exemple de service nécessitant la méthode TCP.

.....................(9) est un exemple de service nécessitant la méthode UDP.
