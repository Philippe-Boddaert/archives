---
title : Le Web
author : M.BODDAERT, M. Stéphan VAN ZUIJLEN
license: CC-BY-NC-SA 
---

# Le Web

![](./assets/web.jpg)

Source : Pixabay

## 1. Définition

### 1.1. À Faire

Répondre aux questions suivantes :

1. ✏  __Sur quel concept des années 1965 le web repose-t-il ?__ 
3. ✏  __Pourquoi Tim Bernes-Lee a-t-il créé le Web en 1989 ?__ 
5. ✏  __Quel usage a permi de démocratiser le Web ?__ 
6. ✏  __Combien de sites Web existent en 2014 ? en 2017 ?__ 
7. ✏  __Comment expliquer la prolifération des sites web ? Sont-ils tous actifs ?__ 

Pour cela, visionner la vidéo (plusieurs visionnages sont nécessaires) :

[![principe_url](https://img.youtube.com/vi/xo7iR2ipY1Y/0.jpg)](https://www.youtube.com/watch?v=xo7iR2ipY1Y&t=25)

### 1.2. Généralités

> Le "__World Wide Web__" (littéralement la « toile (d'araignée) à l'échelle mondiale ») , plus communément appelé "__Web__" a été développé au CERN (Conseil Européen pour la Recherche Nucléaire) par le Britannique Sir Timothy John Berners-Lee et le Belge Robert Cailliau au début des années 90.

À cette époque les principaux centres de recherche mondiaux étaient déjà connectés les uns aux autres, mais pour faciliter les échanges d'informations Tim Berners-Lee met au point le __système hypertexte__.

> __Le système hypertexte permet, à partir d'un document, de consulter d'autres documents en cliquant sur des mots clés.__
>
> Ces mots "cliquables" sont appelés __hyperliens__ et sont souvent soulignés et en bleu.

La première page web publiée est toujours disponible [http://info.cern.ch/hypertext/WWW/TheProject.html](http://info.cern.ch/hypertext/WWW/TheProject.html)

## 2. Principe de fonctionnement

Lorsque je renseigne l'url dans mon navigateur, il affiche une page.

Si ce mécanisme est familier, il soulève plusieurs questions...

![](./assets/iceberg.png)

Et si l'affichage d'une page web n'était que la face immergée de l'iceberg.

### 2.1. À la surface du navigateur Web...

#### 2.1.1. À Faire

1. ✏  Ouvrir un navigateur web et aller à l'url : http://info.cern.ch/hypertext/WWW/TheProject.html
2. ✏  Faire un clic droit et cliquer sur "Afficher la source"
3. ✏  Comparer la page web avec le code source affiché.
   1. Retrouvez-vous l'ensemble des éléments affichés dans le source ?
   2. Trouvez le titre de la page. Comment est-il défini ?
   3. Comment est déclaré l'accès à la page "History" ? Le titre "World Wide Web" a une taille plus grande que le reste du texte, comment expliquez-vous ce fait ?
4. ✏  Sauvegardez la page ( Pour cela, dans le navigateur Web aller à "Fichier > Enregistrer la page sous" ). Dans l'explorateur de fichier,  à l'emplacement où le fichier est sauvegardé :
   1. Quelle est l'extension du fichier ?
   2. Double cliquez sur le fichier. que se passe-t-il ?

#### 2.1.2. Analyse

> Une __page web__ est un __fichier HTML__, contenant du texte lisible par un humain. 
>
> Il est écrit en langage HTML(HyperText Markup Language) qui décrit le contenu et la structure de la page, avec l'utilisation de __balises__.
>
> Il est interprété par le navigateur. Autrement dit : suivant la balise utilisée, le navigateur va savoir quelle est la nature du contenu.

On va utiliser des balises pour définir un paragraphe ou un titre par exemple, ou encore pour insérer une image ou une vidéo dans un document. La balise `p`, par exemple sert à définir un paragraphe. 

Nous venons de donner des éléments de réponses aux questions Qu'est-ce qu'une page Web ? Que fait le navigateur ?

Reste la question : Comment le navigateur récupère la page web ? 

### 2.2. ...Plongée dans la navigation web

#### 2.2.1. À Faire

Ouvrir la console web et observer le trafic réseau :

1. ✏  Quelle est la taille, type et la durée de récupération de la page web ?
2. ✏  Quelle est l'adresse du serveur de la page web ?
3. ✏  Indiquer le détail de la connexion entre le navigateur et le serveur :
   1. Temps d'envoi de la requête
   2. Temps d'attente de la réponse
   3. Temps de réception de la réponse

### 2.2.2. Analyse

> __http__ : est l'abréviation de **H**yper**T**ext **T**ransfer **P**rotocol. (est-ce plus clair ?)
>
> http est donc le protocole qui permet à une machine de demander et de recevoir une ressource d'un serveur

Lorsque l'on se connecte à un serveur pour lui demander une page cela se fait sous forme de __requête http__.

Notre navigateur va envoyer une requête au serveur, pour lui demander une page et celui-ci enverra une réponse, après la lecture de la requête.

## 3. Synthèse

✏  **Recopier** et **compléter** le texte à trou suivant :

Le protocole ..............(1) permet à un hôte de demander et recevoir une ressource d'un serveur. 

Le navigateur envoie une ..............(2) au serveur pour lui demander une ressource. Le serveur envoie une ..............(3), qui contient différentes informations, dont le contenu de la ressource.

Dans le cas d'une page web, une requête HTTP renvoie ..............(4)

Une page web contient des ..............(5), qui permettent de la relier à d'autres ressources comme des ..............(6), des ..............(7) ou des ..............(8).

L'ensemble des pages web constitue ..............(9)

## 4. Web$`\neq`$ Internet

Techniquement __le web se base sur trois choses__ : 

- le protocole **HTTP** (HyperText Transfert Protocol), 
- les **URL** (Uniform Resource Locator) et 
- le langage de description **HTML** (HyperText Markup Language).

Il ne faut pas confondre le web et internet.

**Internet** est un réseau qui se base sur le protocole **TCP/IP**, chargé de la communication entre ordinateurs.

**Le web** s'appuie sur internet et se base sur __HTTP, URL et HTML__.

On trouve également d'autres choses que le "web" sur internet, par exemple, les e mails avec le protocole SMTP (Simple Mail Transfert Protocol) et les transferts de fichiers avec le protocole FTP (File Transfert Protocol).
