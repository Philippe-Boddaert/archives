---
title : Sécuriser sa navigation web
author : M.BODDAERT, M. Stéphan VAN ZUIJLEN
license: CC-BY-NC-SA 
---
# Sécuriser sa navigation web

## 1. Contexte

Marcher dans la rue, prendre la voiture, manger un repas ou boire de l'eau sont autant de «risques» que nous prenons au quotidien. __Le «risque zéro» n'existe pas__....Il en est de même pour la navigation sur le web.

L'objectif de cette séance est d'étudier les __risques liés au Web__ et de s'en prémunir.

![](./assets/high_wire.jpg)

_Source : Pixabay_

## 2. Les cookies : un gâteau qui laisse des miettes

![](./assets/cookie.png)

_Source : Pixabay_

### 2.1. Préambule

Lorsque l'on consulte des sites comme https://www.google.com/, https://www.lemonde.fr/, https://yokoshop.com/, https://www.allocine.fr/ ou https://www.jeuxvideo.com/, il n'est pas rare de voir apparaitre des panneaux similaires à ceux-ci :

![](./assets/google.png)

![](./assets/lemonde.png)

![](./assets/allocine.png)

__À quoi ces panneaux d'information sont-ils relatifs ? Pourquoi apparaissent-ils ? Que faire dans ce cas ?__ 

### 2.2. Définition

✏ __Répondre__ aux questions suivantes en vous aidant la vidéo : https://www.youtube.com/watch?v=HfiJ3ME8Tvs

1. Qu'est-ce qu'un cookie ?
2. À quoi sert un cookie pour un site web ?
3. Quelle est la problématique des cookies tierce partie ?
4. Quelles informations stockent ces cookies ?
5. À qui sert les cookies tierce partie ?

### 2.3. Analyse

Depuis 2016, le **RGPD** (**R**èglement **G**énéral sur la **P**rotection des **D**onnées) oblige les éditeurs de sites web à recueillir ___un consentement préalable de l'utilisateur avant le stockage d'informations sur son terminal ou l'accès à des informations déjà stockées sur celui-ci.___

Le site de la [**CNIL**](https://www.cnil.fr/fr/cookies-et-traceurs-que-dit-la-loi) (**C**ommission **N**ationale de l'**I**nformatique et des **L**ibertés) rappelle les conditions d'un recueil du consentement valable :

1. Le consentement doit être __préalable__ au dépôt et/ou à la lecture de cookies,
2. Le consentement est une manifestation de volonté, __libre, spécifique, univoque et éclairée__,
3. Le consentement n'est valide que si la personne exerce un __choix réel__,
4. Le consentement doit pouvoir être __retiré simplement et à tout moment__ par l'utilisateur.

#### 2.3.1. À Faire

✏ Pour chaque panneau de recueil de consentement, indiquez s'ils vous semblent respecter les 4 conditions d'un recueil de consentement valable.

### 2.4. Bonne pratique

1. Privilégiez la __"Navigation Privée"__, qui n'enregistre pas les cookies et les données des sites visitées
2. En vous aidant des informations du site de la[ CNIL](https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser) paramétrez votre navigateur pour limiter vos traces sur le web.
2. Acceptez de __manière limitée__ l'utilisation de cookies, privilégiez l'option __"Continuer sans accepter"__ lorsque un écran de paramétrage apparait.

## 3. Une tranche de Fake

### 3.1. À Faire

1. ✏  __Consultez__ les sites suivants :
	- [JO de Tokyo](https://www.legorafi.fr/2021/08/06/jo-suite-a-une-panne-de-frein-un-cycliste-bloque-plus-de-24-heures-sur-la-piste-du-velodrome-de-tokyo/)
	- [Vaccin anti-Covid 19](https://www.actualitesdujour.fr/601aa950f1610/un-vaccin-pour-contrecarrer-le-vaccin-de-la-covid-19.html)
	- [Candidature aux présidentielles](https://nordpresse.be/didier-raoult-annonce-sa-candidature-a-lelection-presidentielle/)
	- [Un aspirateur fou s'échappe durant 24 Heures](https://www.presse-citron.net/le-robot-aspirateur-dun-hotel-sechappe-la-fuite-dure-24-heures/)
2. ✏  __Répondez__ aux questions suivantes :
	1. Que vous inspirent ces articles ?
	2. Quelles indications permettent d'identifier si les informations sont vraies ?

### 3.2. Analyse

> La __désinformation__ est un ensemble de techniques de communication visant à tromper des personnes ou l'opinion publique pour protéger des intérêts (privés ou non) ou influencer l'opinion publique.
> 
> Le terme __Fake news__ a pris une importance particulière à l'ère du Web mais apparait dès ... le 19ème siècle !
>
> Cette [vidéo](https://www.youtube.com/watch?v=DAFcB6hONcA) (à regarder chez vous) sur l'aspect historique des fake news

![](./assets/fake_news.jpg)

_A man with "fake news" rushing to the printing press, 7 Mars 1894, Source : Wikipedia_

### 3.3. À Faire

1. ✏ Rendez-vous sur le site des décodeurs du monde à cette adresse : [https://www.lemonde.fr/verification/](https://www.lemonde.fr/verification/)
2. ✏ Quel est l'avis du site des décodeurs lorsque vous lui soumettez les urls suivantes :
   1.  https://www.legorafi.fr/2021/08/06/jo-suite-a-une-panne-de-frein-un-cycliste-bloque-plus-de-24-heures-sur-la-piste-du-velodrome-de-tokyo/
   2. https://www.actualitesdujour.fr/601aa950f1610/un-vaccin-pour-contrecarrer-le-vaccin-de-la-covid-19.html
   3. https://www.france24.com/fr/vid%C3%A9o/20211206-sealand-la-principaut%C3%A9-qui-r%C3%A9siste-encore-et-toujours-%C3%A0-l-envahisseur-et-%C3%A0-la-pand%C3%A9mie
   3. https://www.presse-citron.net/le-robot-aspirateur-dun-hotel-sechappe-la-fuite-dure-24-heures/
   4. https://nordpresse.be/didier-raoult-annonce-sa-candidature-a-lelection-presidentielle/

### 3.4. Vérifier une image

1. Cette photo représente une pomme. Me croyez-vous ?

![](./assets/pomme.jpeg)

2. Cette tortue est nait le jour de la rentrée des classes, soit le 2 Septembre 2021. Me croyez-vous ?

![](./assets/tortue.jpeg)

3. La photo (et le tweet associé) est-elle issue du compte de l'Élysée ?

![](./assets/twitter.png)

Il est souvent difficile de vérifier une image, Heureusement, il existe des outils efficaces pour retrouver l'origine d'une photo !

#### 3.4.1. À Faire

1. Télécharger les 3 photos [Pomme](./assets/pomme.jpeg), [Tortue](./assets/tortue.jpeg), [Twitter](./assets/twitter.png)
2. Tenter de retrouver l'origine des photos en utilisant les sites [Google Images](https://www.google.fr/imghp?hl=fr) et [TinEye](https://tineye.com/)
3. En utilisant la même technique, déterminez quelles informations sont vraies ou fausses...[parmi ces 10 infos](./exercices)

### 3.5. Bonne pratique

1. __S'interroger__ sur la source d'information ([Guide des questions à se poser](https://www.gouvernement.fr/fausses-nouvelles-guide-des-questions-a-se-poser-face-a-une-information)) : Qui me donne cette information ? Quelles sont ses intérêts ? Comment puis-je recouper cette information ? 
2. Utiliser des __outils de vérification__ mis en place par des journaux (ex : [Les décodeurs du monde](https://www.lemonde.fr/verification/)) ou de recherche inversé d'images ([Google Images](https://www.google.fr/imghp?hl=fr) ou [TinEye](https://tineye.com/))


## 4. HTTPS, STP C OK

### 4.1. Définition

Le but du protocole **HTTP** (**H**yper**T**ext **T**ransfer **P**rotocol) est de permettre un transfert de fichiers (essentiellement au format HTML) localisés grâce à une chaîne de caractères appelée URL entre un navigateur (le client) et un serveur Web.

- Le navigateur effectue une __requête HTTP__
- Le serveur traite la requête puis envoie une __réponse HTTP__, contenant la page HTML
- Le navigateur affiche le contenu de la page HTML.

![](https://img-19.ccm2.net/q1_lqTcNUtYc3W0d3SYPwb-Lrk8=/377x/bfbd92d7cd9443e38170cde13721391b/ccm-encyclopedia/internet-images-comm.gif)

_Source : commentcamarche.net_

La plupart du temps, on ne fait que recevoir des ressources d'un serveur, et ces informations sont __transmises en clair__...

Imaginons une communication avec sa banque en http...

![](./assets/http_1.png)

_Source : culture-informatique.net_

Si la connexion se fait en __http__, alors tous les échanges entre son ordinateur et sa banque se font __en texte, non crypté et donc lisible__ par n'importe quelle personne qui espionne le réseau. (comme un pirate par exemple).

Pour __pallier à ce problème de sécurité__, il a fallu trouver une parade : c'est le __https__ !

Le protocole https est composé de 2 protocoles :

- le protocole http
- le protocole ssl(Secure Socket Layer) : c'est lui qui donne le **S** au protocole http**S** (S pour Secure)

Le serveur et le client vont établir une __connexion chiffrée__ dont eux seuls pourront lire le contenu car seul le client et le serveur en possession de la clé de décryptage pourront déchiffrer les données reçues.

![](./assets/http_2.png)

_Source : culture-informatique.net_

### 4.2. À Faire

1. ✏  Allez sur le [site du lycée](http://www.lycee-massena.fr/)
2. ✏  Que constatez-vous au niveau de la barre de saisie de l'url ? Qu'en conclure ?
3. ✏  Allez sur le [site des impôts](https://www.impots.gouv.fr/portail/)
4. ✏  Que constatez-vous au niveau de la barre de saisie de l'url ? Qu'en conclure ?

### 4.3. Bonne pratique

1. Vérifier l'url des sites visités. Elle doit débuter par __https://__ et non __http://__.
2. Vérifier la présence d'un cadenas ![](./assets/lock.png) dans la barre de saisie de l'url. Il indique que le navigateur utilise bien le protocole https pour le site visité.

## 5. Synthèse

__Répondre__ à ce quizz en guise de synthèse de cette activité.

1. __Face à une information délivrée sur un le Web, il est préférable de__ :

- [ ] A. Partager l'information avec son réseau pour demander si elle est vraie
- [ ] B. Chercher si d'autres sources d'information infirment ou confirment l'information
- [ ] C. Utiliser des outils de vérification (les décodeurs du monde, Google Image...)
- [ ] D. Relayer à l'oral à ses connaissances cette information

2. __Les Cookies sont-ils dangereux ?__

- [ ] A. Oui
- [ ] B. Non
- [ ] C. Ça dépend

3. __Je reçois via un SMS un message me demandant de cliquer sur le lien https://tinyurl.com/5n6rbfjf, que faire ?__

- [ ] A. Cliquer
- [ ] B. Ne pas cliquer 

4. __En vous rendant sur un site de e-commerce pour acheter une nouvelle paire de chaussures, vous constatez que le protocole de l'url est `https` mais l'icône du cadenas n'est pas présente. Que faire__ ?

- [ ] A. Continuer l'achat, notamment donner le numéro de carte bancaire de vos parents
- [ ] B. Ne pas continuer l'achat
- [ ] C. Aller sur un autre site

5. __En vous rendant sur un site de e-commerce, le panneau suivant apparait. Que faites-vous ?__
![](./assets/cdiscount.png)

- [ ] A. Cliquer sur `Accepter`
- [ ] B. Cliquer sur `Continuer sans accepter`
- [ ] C. Cliquer sur `Paramétrer les cookies`
