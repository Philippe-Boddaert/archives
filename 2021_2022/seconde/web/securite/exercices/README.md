# Exercices - Fake News

## 1. Contexte

Dans cette activité, je vous propose 10 images ou commentaires issus du Web.

À vous de déterminer lesquelles sont authentiques ? lesquelles sont fausses ? Et surtout pourquoi ?

Pour répondre à cette question, il convient de :

- Trouver dans un premier temps la source de l'image ou commentaire, en cherchant les __mots clés__ sur un moteur de recherche, ou utiliser [Google image](https://www.google.fr/imghp?hl=fr) ou [TinEye](https://tineye.com/) pour trouver la source d'une image, 
- Déterminer si cette information est vraisemblable ou non (i.e le site a-t-il une bonne réputation en matière d'information ? d'autres sources citent, valident ou infirment l'information ?)

## 2. Vrai ou Faux

| N° | Image | Description |
| :--: | :-----: | :-----------: |
| 1 | ![](./assets/navire.jpeg) | Le navire de croisière Imperials a été déposé sur une colline à la suite d’un tsunami en Corée du Sud en 2007. Son propriétaire prévoit de creuser un canal pour le ramener. |
| 2 | ![](./assets/police.png) |  Cette photo prouve que dans certains quartiers la police française est menacée par des gangs qui ont pris le pouvoir. |
| 3 | ![](./assets/jetman.jpeg) | Jetman est un homme qui vole avec des moteurs sur le dos. |
| 4 | ![](./assets/aeroport.jpeg) | Pour atterrir sur la piste de l’aéroport international de l’île de Saint-Martin, les avions passent à moins de 3 mètres au-dessus de la plage. Certains baigneurs ont même été blessés à cause du souffle des réacteurs. |
| 5 | ![](./assets/volcan.png) | Le Teranowai, en Polynésie, est un volcan unique au monde. De ses entrailles sort une lave transparente, semblable à du verre liquide. Cette propriété étonnante est due à la silice, un minéral cristallin, qui compose une grande partie du sous-sol de la ceinture de feu, un alignement de volcans qui bordent |
| 6 | ![](./assets/charlie.png) | Dans la C3 abandonnée par les tueurs de Charlie Hebdo, en 2015, la carte d’identité |
| 7 | ![](./assets/camion.png) | En 2013, l’acteur belge Jean-Claude Van Damme (alors âgé de 53 ans) a tourné une pub sans trucage où il effectue un grand écart les pieds posés sur les rétroviseurs de 2 camions roulant en marche arrière. |
| 8 | ![](./assets/chocolatine.png) | À Toulouse, en 2013, un touriste parisien a été abattu par une boulangère pour lui avoir demandé un “pain au chocolat” au lieu d’une “chocolatine”, comme on dit dans la ville. |
| 9 | ![](./assets/exotropie.jpeg) | L'exotropie, ou avoir les deux yeux tournés vers l'extérieur, est parfois appelée "yeux de coq" ou "yeux de mur". |
| 10 | ![](./assets/toilette.jpeg) | Des semaines de troubles et de violences en Ukraine ont été suivies d'un bref coup d'Etat, le dirigeant ukrainien, Viktor Ianoukovitch, ayant fui son pays le 22 février 2014. Les photos de son palais somptueux ont commencé à apparaître dans les réseaux sociaux. Malheureusement, les médias ont du mal à contrôler les images trouvées sur Twitter et Facebook.|

## 3. Synthèse

1. Que pouvez-vous faire si vous êtes confronté à une fake news ?
2. Que se passe-t-il si vous la partagez ?
3. Dans quel but, ces genres de texte sont-ils publiés ?