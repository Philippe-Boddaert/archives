---
title : Le Web
author : M.BODDAERT
license: CC-BY-NC-SA 
---
# Le web

## Présentation

Le Web (toile ou réseau) désigne un système donnant accès à un ensemble de données (page, image, son, vidéo) accessibles sur le réseau internet.

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce que le __Web__ ?
- Comment __fonctionne__-t-il ?

## Progression

```mermaid
flowchart LR;
    A[1. Définition]-->B[2. Première page HTML];
    B-->C[3. Création d'un site];
    C-->D[4. Moteur de recherche];
    D-->E[5. Sécuriser sa navigation];
    
    click A "./web/definition" "Lien vers 1. Définition"
    click B "./web/html" "Lien vers 2. Première page HTML"
    click C "./web/html/SITE.md" "Lien vers 3. Création d'un site"
    click D "./web/moteur" "Lien vers 4. Moteur de recherche"
    click E "./web/securite" "Lien vers 5. Sécuriser sa navigation"
```

## Attendus du Programme

| Contenus                                                     | Capacités attendues                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Repères historiques | Connaître les étapes du développement du Web. |
| Notions juridiques | Connaître certaines notions juridiques (licence, droit d’auteur, droit d’usage, valeur d’un bien). |
| Hypertexte | Maîtriser les renvois d’un texte à différents contenus. |
| Langages HTML et CSS | Distinguer ce qui relève du contenu d’une page et de son style de présentation. Étudier et modifier une page HTML simple. |
| URL | Décomposer l’URL d’une page. Reconnaître les pages sécurisées. |
| Requête HTTP | Décomposer le contenu d’une requête HTTP et identifier les paramètres passés. |
| Modèle client/serveur | Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur. |
| Moteurs de recherche : principes et usages | Mener une analyse critique des résultats fournis par un moteur de recherche. Comprendre les enjeux de la publication d’informations. |
| Paramètres de sécurité d’un navigateur | Maîtriser les réglages les plus importants concernant la gestion des cookies, la sécurité et la confidentialité d’un navigateur. Sécuriser sa navigation en ligne et analyser les pages et fichiers. |

