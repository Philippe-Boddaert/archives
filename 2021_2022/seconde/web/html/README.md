---
title : Ma première page Web
author : M.BODDAERT d'après les travaux de M. Stéphan VAN ZUIJLEN
license: CC-BY-NC-SA 
---

# Ma première page Web

## 1. Contexte

L'objectif de cette activité est d'appréhender le langage HTML, à travers la création de votre __portrait chinois__ sous la forme d'une page web.

> Dans le domaine littéraire, un __portrait chinois__ est un jeu permettant de déceler certains aspects de la personnalité d'un individu ou d'identifier ses goûts ou ses préférences personnelles, au travers d'un questionnaire.

Quelques exemples de question d'un __portait chinois__ :

1. Si j'étais une saison, je serais...
2. Si j'étais un animal, je serais...
3. Si j'étais un pays, je serais...
4. Si j'étais un site web, je serais...
5. Si j'étais une recette, je serais...
6. Si j'étais une série, je serais...

Exemple : [Portrait chinois de votre professeur](./assets/portrait_chinois.pdf)

## 2. La structure de base

Pour créer une page web, il faut un éditeur de texte (Notepad++)

1. ✏ Ouvrir Notepad++
2. ✏ Copier ces quelques lignes de code dans l'éditeur choisi.

```html
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Mon portrait chinois</title>
 </head>
 <body>

 </body>
</html>
```

3. ✏ Enregistrer la page sous le nom : `index.html`
4. ✏ Ouvrir le fichier avec un navigateur web. Que constatez-vous ?

### 2.1 Analyse

> Le ___HyperText___ ___Markup___ ___Language___ (HTML) est un langage de **balisage** conçu pour représenter les pages ___Web___.
> 
> On parle de langage de balisage. 
> 
> Le `HTML` permet de décrire la structure d'une page web.

Un document HTML est composé de 4 parties obligatoires :

- l'instruction du langage (`!DOCTYPE html>`), à destination du navigateur afin qu'il interprète correctement le document,
- une structure, entre les balises `<html>`et `</html>`,
- une en-tête, entre les balises `<head>` et `</head>`,
- un corps, entre les balises `<body>`et `</body>`.

### 2.2. Syntaxe des balises

Les balises HTML respectent une syntaxe simple et stricte :

- Un **chevron ouvrant (<)**
- Le **type de la balise**
- Des **attributs** (optionnels). Un espace, suivi du nom de l'attribut, d'un signe égal (=) et d'une valeur entre doubles quotes ("").
- Un **chevron fermant (>)**

_Anatomie d'un élément_ :

```math
\overbrace{\color{green}{<}\underbrace{\color{green}{\text{body}}}_{type}\color{green}{>}}^{Balise \; ouvrante}\text{...}\overbrace{\color{green}{\text{</body>}}}^{Balise \; fermante}
```

_Anatomie d'un élément avec attribut_ :

```math
\overbrace{\color{green}{<}\underbrace{\color{green}{\text{meta}}}_{type}\quad \underbrace{\color{blue}{\text{charset}}=\color{red}{\text{'utf-8'}}}_{attributs}\color{green}{>}}^{Balise \; ouvrante}\dots\overbrace{\color{green}{\text{</meta>}}}^{Balise \; fermante}
```

Pour l'instant, le navigateur affiche une page blanche...C'est normal, car il n'y a rien à afficher dans le __corps__ de la page, à part "Mon portrait chinois" dans l'onglet de navigation.

## 3. Ajouter un titre

✏ Ajoutez un __titre__ dans le corps de la page : l'élément `h1` permet d'écrire avec une grande taille le texte qu'il contient.

```html
 <h1>Mon portrait chinois</h1>
```

✏ Ajoutez un autre __titre__ dans le corps de la page, mais plus petit : l'élément `h2` permet d'écrire avec une  taille le texte qu'il contient, plus petit que l'élément `h1`.

```html
 <h2>Si j'étais une saison, je serais...</h2>
```

__N.B__ : Les éléments `h1`, `h2`, `h3` donnent des __textes de tailles différentes__...testez les différents éléments

## 4. Ajouter un paragraphe

✏ Ajoutez un __paragraphe__ pour notre page : l'élément `p` permet d'écrire un texte sous forme d'un paragraphe de taille normale.

```html
 <p>???</p>
```

✏ Remplacez les ??? du paragraphe avec votre réponse à la question `Si j'étais une saison, je serais...`

## 5. Ajouter du "style"

Ajoutons un peu de _style_ à notre page !

### 5.1. Alignement de texte

Le code suivant permet de spécifier l'__alignement__ d'un texte :

```html
<p style="text-align:center;">???</p>
```

La structure style="..." est un __attribut__ de l'élément `p`.

Ici, elle contient valeur `text-align:center` qui indique que le texte du paragraphe doit être centré.

✏ Testez les différentes valeurs de l'attribut `text-align` (les valeurs peuvent être `right, left, center` ou `justify`)

### 5.2. Couleur de texte

Ce serait mieux si ce titre était en rouge...C'est l'attribut `color` de l'attribut `style` qui permet cela :

```html
<p style="text-align:center;color:red;">???</p>
```

Pour les couleurs, on utilise les couleurs définies par leurs noms, voir sur ce [site web](https://html-color-codes.info/color-names/) ou par leurs valeurs hexadécimales (FF0000 pour le rouge)...mais nous verrons ce mécanisme lors d'un prochain cours.

__N.B__ : On constate que l'attribut `style` peut prendre différentes valeurs. Pour cela, il est nécessaire de les séparer par `;`

## À vous de jouer

Maintenant que vous savez ajouter des titres, paragraphes et y mettre du style, c'est à vous de compléter votre portrait chinois !!!

- ✏ Ouvrir le [portrait chinois de votre professeur commenté](./assets/portrait_chinois_commentaire.pdf)
- ✏ En vous inspirant de celui de votre professeur. Compléter votre portrait chinois (votre page web) en ajoutant, dans l'ordre, les rubriques :
  1. Si j'étais un animal, je serais...
  2. Si j'étais un pays, je serais...
  3. Si j'étais un site web, je serais...
  4. Si j'étais une recette, je serais...
  4. Si j'étais une série, je serais...
  4. ...Et tout autre rubrique de votre imagination (ce [site](https://oser-ecrire.fr/ecrire-portrait-chinois) peut vous aider à trouver l'inspiration)
  
- La suite de ce cours vous indique comment ajouter les différents éléments d'une page web.

## 6. Ajouter une image

Ajouter une image, dans le corps d'une page se fait, avec l'élément `img`. 

Pour cela, il faut disposer de son adresse sur le web (son URL) et la spécifier dans l'attribut `src`.

⚠ Le web comme tout support est soumis à la législation, notamment sur le respect de la propriété intellectuelle. Ainsi, il est __interdit d'ajouter et/ou de diffuser une image__ dans une page web __sans en détenir les droits nécessaires__.

⚠ On utilisera les images du site [Pixabay](https://pixabay.com) ou [Wikipédia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal), qui garantissent que les images sont utilisables au regard de la législation.

✏ Essayez ceci :

```html
<img src="https://cdn.pixabay.com/photo/2019/09/28/10/34/gandalf-4510395_960_720.jpg" />
```

__N.B__ : On constate que la balise `img` contient juste une balise ouvrante. Le `/>` à la fin de la balise permet d'indiquer sa fermeture.

## 7. Ajouter un lien

Pour ajouter un lien vers une autre page web, on utilise l'élément `a`, pour lequel on spécifie son URL dans l'attribut `href`

✏ Essayez ceci :

```html
<a href="URL de la ressource" > texte à afficher qui indique le lien </a>
```

__N.B__ : On constate que le navigateur affiche un __lien cliquable__ correspondant au texte, entre les balises ouvrantes et fermantes.

__N.B__ : Un clic sur le lien ouvre la page web à la place de la page en cours. Pour ouvrir la page dans un nouvel onglet, il faut spécifier la valeur de l'attribut `target`à `_blank` , comme dans l'exemple ci-dessous :

```html
<a href="URL de la ressource" target="_blank"> texte à afficher qui indique le lien </a>
```

## 8. Ajouter des listes

### 8.1. Liste non ordonnée

Pour ajouter une liste, dans le corps d'une page, il faut utiliser la structure d'éléments `ul` et `li` (`ul` pour unordered list et `li` pour list item).

✏ Essayez ceci  :

```html
<ul>
  <li>élément 1</li>
  <li>élément 2</li>
  <li>élément 3</li>
</ul>
```

__N.B__ : Le navigateur ajoute automatiquement une marque au début de chaque élément `li`.

### 8.2. Liste ordonnée

Pour ajouter une liste ordonnée, i.e numérotée, dans le corps d'une page, il faut utiliser la structure d'éléments `ol` et `li`  (`ol` pour ordered list et `li` pour list item)

✏ Essayez ceci  :

```html
<ol>
  <li>élément 1</li>
  <li>élément 2</li>
  <li>élément 3</li>
</ol>
```

__N.B__ : Le navigateur ajoute automatiquement un numéro au début de chaque élément `li`. Les numéros suivent l'ordre de déclaration des éléments `li`.

## 9. Ajouter un tableau

Les tableaux sont très utiles pour des affichages structurés d'informations.

✏ Essayez ceci  :

```html
<table>
  <tr>
    <th>Prénom</th>
    <th>Nom</th>
  </tr>
  <tr>
    <td>Jean</td>
    <td>Dupont</td>
  </tr>
  <tr>
    <td>Marion</td>
    <td>Duval</td>
  </tr>
   <tr>
    <td>Martin</td>
    <td>Durand</td>
  </tr>
</table>
```

Les balises :

- `table` définit la structure entière de la table,
- `tr` définit une ligne de la `table`,
- `th` définit une colonne d'en-tête d'une table,
- `td` définit une colonne d'une ligne (i.e `tr`).

__N.B__ : Sur ce [site](https://www.tablesgenerator.com/html_tables), il est possible de générer le code html via une interface (Attention : cocher la case `Do not generate CSS`)

## 10. Éléments spéciaux

### 10.1. Les emojis

Il est possible d'ajouter des emojis dans une page web. Pour cela, il est nécessaire de connaitre sa __référence Unicode__, i.e un nombre écrit entre `&#`et `;`

✏ Essayez ceci  :

```html
<p>&#129409;</p>
```

Quelques emoji :

| emoji | Référence Unicode |
| :-----: | :-----------------: |
|  &#128077;     |    128077               |
| &#128078; | 128078 |
| &#128512; | 128512 |
| &#128514; | 128514 |
| &#128533; | 128533 |
| &#128007; | 128007 |
| &#127800; | 127800 |
| &#127757; | 127757 |
| &#128156; | 128156 |

Liste __plus complète mais non exhaustive__ des emoji : [Site w3schools.com](https://www.w3schools.com/charsets/ref_emoji.asp)

### 10.2. Les commentaires

Ajouter des commentaires dans une page web peut être nécessaire pour garder en mémoire certains éléments de développement que l'on ne souhaite pas afficher à l'utilisateur.

✏ Essayez ceci  :

```html
<!-- Ceci est un commentaire. Il ne sera pas affiché dans le navigateur.-->
```

### 10.3. Les sauts de ligne

Vous pouvez ajouter autant de retours à la ligne dans votre code, ceux-ci ne seront jamais affichés visuellement dans votre navigateur.

La balise `br` est à utiliser pour créer un saut de ligne dans le texte.

✏ Essayez ceci  :

```html
<p>Une ligne< br />Une autre ligne</p>
```

### 10.4. L'espace insécable

Comme les sauts de ligne, les espaces dans le code HTML ne seront jamais affichés visuellement dans votre navigateur.

L'entité `&nbsp;` est à utiliser pour créer un espace insécable, i.e qui ne peut être coupé.

✏ Essayez ceci  :

```html
<p>Un&nbsp;espace</p>
```

## 11. Synthèse

✏ __Recopier__ et __compléter__ le texte à trou suivant :

Le langage HTML est un langage de ............(1).

| Type de balise  | Utilité               | Commentaire                                                  |
| :---------------: | --------------------- | ------------------------------------------------------------ |
| `h1`            | ............(1) |                                                              |
| ............(2) | Ajouter un paragraphe | Le texte affiché est celui défini entre la balise ............(3) et ............(4) |
| ............(5) | Ajouter une image | Nécessite de spécifier son url dans l'attribut ............(6) |
| ............(7) | Ajouter un lien vers une autre page web | Nécessite de spécifier son url dans l'attribut ............(8) |
| `ul` | ............(9) | Nécessite d'ajouter chaque élément dans une balise ............(10) |
| ............(11) | Ajouter une liste ordonnée | idem que pour la balise `ul` |
