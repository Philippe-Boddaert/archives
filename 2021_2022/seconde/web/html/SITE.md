---
title : Site collaboratif
author : M.BODDAERT
license: CC-BY-NC-SA 
---

# Site collaboratif

## 1. Contexte

L'objectif est de créer un __site collaboratif__ autour de quelques __personnages clés__ du domaine de l'informatique.

La page d'accueil existe et est accessible à cette adresse [https://philippe-boddaert.gitlab.io/histoire/index.html](https://philippe-boddaert.gitlab.io/histoire/index.html)

Elle contient les liens vers les autres pages du site mais...les pages n'existent pas. C'est vous qui allez les créer !!

## 2. Consignes

1. Par groupe de 2, vous allez recevoir un papier contenant un numéro et le nom d'un personnage,
2. Vous devez créer une page html comportant une biographie de celui/celle-ci,
3. Le fichier html de la page doit avoir pour nom `page_N.html` où `N` est le numéro qui vous a été attribué,
4. Votre page doit comporter :
   1. Un __titre__ (balise h1), correspondant au nom du personnage clé,
   2. Un __sous-titre__ (balise h2) avec la mention __"Page crée le JJ/MM/AAAA par X.X et Y.Y"__ où JJ/MM/AAAA est la date du jour et X.X et Y.Y vos initiales respectives.
   3. Une __image__ (balise img) de la personnalité et/ou d'une de ses inventions 
   4. Des rubriques : 
      1. avec pour titre __Biographie__ (balise h1) et des paragraphes (balise p) contenant date et lieu de naissance, études, ....
      3. avec pour titre __Contributions__ (balise h1) et des paragraphes (balise p) contenant la description des inventions, créations, ..., 
      4. avec pour titre __Ressources__ (balise h1). La rubrique Ressources doit contenir un __tableau__ (balise table) comportant les oeuvres (film, livre, site) faisant référence à la personnalité.
   5. __3 liens__ (balise a) : 
      1. un lien vers la __page précédente__ (c'est-à-dire vers le numéro inférieur),
      2. un lien vers la __page d'accueil__ (index.html),
      3. un lien vers la __page suivante__ (c'est-à-dire vers le numéro supérieur).
5. À la fin de la séance, vous devez rendre votre __page html__ dans __pronote__.

Compléments : 
- Un exemple de page sur [Steve Jobs](./assets/page_steve_jobs.pdf)
- Vous êtes libres de définir le __style__ que vous désirez (couleur, taille des textes...). Tous les éléments de style doivent se trouver dans un fichier `page_N.css` où `N` est le numéro qui vous a été attribué

N.B : (Pour rappel, le lien vers le [cours sur le CSS](./CSS.md))

## 3. Liens utiles

- Site sur l'histoire de l'informatique : http://histoire.info.online.fr/
- Encyclopédie : https://fr.wikipedia.org/
- Article "Ces femmes qui ont révolutionné l’informatique moderne" : https://usbeketrica.com/fr/article/elles-ont-revolutionne-l-informatique-moderne
- Revue de culture scientifique en ligne https://interstices.info/
- Plateforme vidéo : https://www.youtube.com
- Emission de podcast : https://www.franceculture.fr/

## 4. Barème

Le devoir est noté sur **10 points** :

| Intitulé              | Nombre de points |
| --------------------- | :--------------: |
| Pertinence des informations (Recherche d'information, source des informations utilisées... ) | **3 points** |
| Présence de tous les éléments | **3 points** |
| Syntaxe correcte | **3 points** |
| Personnalisation de la page (Présence d'un style) | **1 points** |
