# Les Graphes

## 1. Contexte

Les stations de tramway de Nice sont voisines entre elles :

- Centre Universitaire Méditerranéen et Alsace-Lorraine,
- Jean Médecin et Durandy,
- Acropolis et Garibaldi,
- Garibaldi et Cathédrale,
- Gare Thiers et Jean Médecin,
- Masséna et Opéra,
- Garibaldi et Port Lympia,
- Garibaldi et Durandy,
- Cathédrale et Opéra,
- Libération et Gare Thiers
- Alsace-Lorraine et Jean Médecin,
- Jean Médecin et Masséna

__Si je vous demande combien et à quelles stations intermédiaires le tramway va s'arrêter si je monte à Centre Universitaire Méditerranéen et descend à Garibaldi ?__ ....Il est difficile de répondre.

Répondre à la question est plus aisée si on __modélise (représente)__ la situation de la manière suivante :

- décrire les stations sous la forme d'un cercle,
- de relier les stations voisines par un trait entre 2 cercles.

__À Faire__ : 

1. Représenter les stations et le fait qu'elles soient voisines par la modélisation décrite ci-dessus.
2. Combien et à quelles stations intermédiaires le tramway va s'arrêter si je monte à Centre Universitaire Méditerranéen et descend à Garibaldi ?

## 2. Définition

Nous venons d'introduire un objet abstrait permettant de modéliser une situation concrète : le __Graphe__ !

Un **graphe** est un **modèle abstrait** constitué par :

- des **sommets** (aussi appelés **nœuds** ou **points**). Dans notre exemple : __Les stations de tramway__
- des **arêtes** (ou **liens**) reliant ces sommets. Dans notre exemple : __Les rails__ qui relient les stations

Un graphe est utile pour __modéliser des relations__ entre des entités (station de métro, personnes, villes, ...)

```mermaid
flowchart LR;
 A["Lanval Hopital"]---B["Magnan"]
 B---C["Centre Universitaire Méditerranéen"];
```

## 3. À Faire

1. Ouvrir un navigateur Web,

2. Rendez-vous à l'adresse https://concours.castor-informatique.fr/,

3. Saisissez le code de l'activité fourni en début de séance,

   **N.B : Au commencement de l'activité, un code personnel vous sera communiqué. Il est impératif de retenir ce code (sur papier ou photo via téléphone) pour revenir sur votre activité en cas de problème technique.**

4. Effectuez les séquences dans l'ordre : 1, 2, 3, 4, 5, 6, 7.

**N.B : Avancez à votre rythme en commençant par la version avec le moins d'étoiles de chaque séquence**

## 4. Synthèse

__Complétez le texte avec les mots de la liste suivante qui conviennent.__



Un **............** (1) est constitué de **sommets** (ou noeuds) et d'**arêtes**.

Une **............** (2) est représentée par un trait entre deux sommets

Une arête symbolise la **............** (3) entre deux personnes ou deux objets.

Le **............** d'un sommet est le **............** (4) d'arêtes dont il est l'extrémité.

Dans un graphe, deux sommets sont **............** (5) s'ils sont reliés par une arête.

Dans un graphe, une **............** (6) est une suite de sommets reliés consécutivement par des arêtes.

La **............** (7) de la chaîne est égale au nombre de ses arêtes.

La **distance** entre deux sommets est le nombre **............** (8) d'arêtes qu'il faut parcourir pour aller de l'un à l'autre.

Le **............** (9) d'un graphe est la distance maximale possible entre deux de ses sommets.



_Liste de mots_ :

__branche --- relation --- proches --- division --- graphe --- programme --- nombre --- modélisées --- surveillées --- voisins chaîne --- arête --- degré --- pourcentage --- largeur --- longueur --- hauteur --- adjacents --- maximum --- minimum --- moyen --- rayon ---noyau --- diamètre --- centre__
