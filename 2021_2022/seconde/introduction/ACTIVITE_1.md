---
title : Introduction à l'informatique
author : M. BODDAERT, M.MATHIEU
license: CC-BY-NC-SA
---
# Introduction

## L'informatique c'est quoi ?

L'informatique est structurée par quatre concepts:

- L'__algorithme__ : méthode opérationnelle permettant de résoudre un problème.
- La __machine__ : système physique doté de fonctionnalités permettant d'effectuer des calculs.
- Le __langage__ : moyen de communication entre l'informaticien et la machine.
- Les __données__ : éléments symboliques susceptibles d'être traitées par une machine.

## Définition

On peut dire que l'informatique est : __La science du traitement automatisé de l'information__.

- __Science__ : donc un aspect théorique (domaine des mathématiques)
- __Traitement automatisé__ : c'est l'ordinateur (domaine des technologies)
- __Information__ : Tout ce qui est numérisable (texte,musique, voix,image, films, ADN,...)

L'informatique regroupe un grand nombre de domaines : langage et programmation, architecture et système, théorie des graphes, réseau, sécurité, base de données, intelligence artificielle, etc.

Comme toute connaissance scientifique et technique, les concepts de l'informatique ont une __histoire__ plus ancienne qu'il n'y parait et ont été forgés par des femmes et des hommes...sur des siècles !

## Une brève histoire

| | Qui | Date | Événement |
| :--:| :--: | :--: | :-- |
| | - | 1er siècle avant J.-C. | Machine d’Anticythère, le plus vieux __mécanisme à engrenages connu__. ![Anticythère](./assets/anticythere.jpeg) C'est un mécanisme de bronze comprenant des dizaines de roues dentées, solidaires et disposées sur plusieurs plans. Il permettait de calculer des positions astronomiques.  |
| ⚠ | __Charles Babbage__ | 1834 | Commence à construire sa __machine analytique__, première machine mécanique programmable.<br/>La machine comprend les différentes parties que l'on retrouve dans un ordinateur d'aujourd'hui : périphérique d'entrée des programmes et données (clavier ou mémoire de masse), unité de commande (microprocesseur), unité de calcul (une partie du microprocesseur), mémoire (disque dur), et périphérique de sortie (imprimante).<br />Charles Babbage ne __réalisera jamais sa machine__, mais il passera le reste de sa vie à la concevoir dans les moindres détails. |
| | __John P. Eckert__ et __John W. Mauchly__ | 1943 | __ENIAC__, machine servant au calcul des trajectoires balistiques, est créée. Son poids est de 30 tonnes pour des dimensions de 2,4 x 0,9 x 30,5 mètres occupant une surface de 67 mètres carrés.![ENIAC](./assets/Eniac.jpeg) |
| | __John Von Neumann__ | 1944 | A donné son nom à « l’architecture de __von Neumann__ » utilisée dans la quasi-totalité des ordinateurs modernes. |
| | __DARPA__ | 1969| Début d'__Arpanet__, le prédécesseur d'Internet par le ministère de la défense américaine |
| | __Intel__ | 1971| Le microprocesseur 4004 d’Intel est de la __taille d’un timbre__, il développe des performances équivalents à celle de l’ENIAC (1946), qui occupait toute une pièce.![Intel 4004](./assets/Intel_C4004.jpeg) |
| | __Jimmy Wales__ | 2001 | Création de __Wikipédia__ |

### À Faire

- Cette histoire est très incomplète, vous allez contribuer à la développer.
- Via votre téléphone, tablette ou ordinateur, cherchez les dates / protagonistes des événements suivants :

| | Qui | Date | Événement |
| :--: | :--: | :--: | :-- |
| | | | Premier réseau social sur Internet |
| | | | Sortie du 1er Iphone |
| ⚠ | | | Premier site web |
| ⚠ | | | Création du premier ordinateur |
| | | | Sortie du premier micro-ordinateur |
| | | | Sortie du premier ordinateur portable |
| | | | Création du langage Python |
| ⚠ | | | Création d'Internet |
| | | | Création de la souris |
| | | | Création du mot Bug |
| | | | Création de Snapchat | 
| ⚠ | | | Premier programme informatique |
| ⚠ | | | Première machine à calculer |
| | | | Création du CD-ROM |
| ⚠ | | | Première langage de programmation |
| | | | Création du mot ordinateur |
| | | | Création du mot informatique |
| | | | Création du bluetooth |

- Pour vous aider, il est possible de consulter le site de Serge Rossi : [http://histoire.info.online.fr/](http://histoire.info.online.fr/)

___N.B : Les événements marqués par ⚠ sont à connaitre___

