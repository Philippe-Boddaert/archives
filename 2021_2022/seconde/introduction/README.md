---
title : Introduction
author : M. BODDAERT
license : CC-BY-NC-SA
---
# Introduction à la SNT

Ce dossier contient :

- La présentation effectuée lors de la première séance sous la forme d'un PDF ([la présentation](./PRESENTATION.pdf)),
- L'[activité 1](./ACTIVITE_1.md) sur une définition et brève histoire de l'informatique.
- L'[activité 2](./ACTIVITE_2.md) sur une analyse de l'activité 1, du point de vue de la place des femmes dans le numérique.

