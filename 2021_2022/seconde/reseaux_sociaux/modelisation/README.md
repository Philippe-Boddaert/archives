---
title : Modélisation des réseaux sociaux
author : M.BODDAERT
license: CC-BY-NC-SA 
---
# Modélisation des réseaux sociaux

![](https://cdn.pixabay.com/photo/2018/12/01/09/53/network-3849202_960_720.jpg)

_Source : Pixabay_

## 1. Contexte

Alice, Batoul, Clara, Dilan, Éloïse, Fatah, Grégory et Hamza sont dans la même classe.

En dehors des cours, ils se voient régulièrement aux activités suivantes : 

- Alice, Batoul, Clara, Dilan et Hamza font du judo ensemble ;
- Clara, Dilan et Éloïse se voient au club robotique ; 
- Grégory et Hamza font partie du même groupe de musique ; 
- Grégory et Fatah se croisent souvent à la bibliothèque.

### 1.1. À Faire

Répondre aux questions suivantes sur votre cahier. 

1. Placer les noms des huit élèves (à peu près en cercle), et relier par un trait chaque couple d’élèves qui se voit hors des cours

```mermaid
flowchart LR;

Alice---Batoul;
Alice---Clara;
A["..."]---Alice
```

Cette représentation est appelée « graphe ». Répondre aux questions suivante en regardant uniquement le graphe.

2. Quels élèves voient beaucoup d’autres élèves hors des cours ? Lesquels en voient peu ? 
3. Pendant les vacances (donc hors du temps scolaire), je veux faire passer une information le plus rapidement possible à ces huit élèves. À quel élève dois-je m’adresser pour qu’il fasse passer cette information le plus vite possible ?

## 2. Formalisation

> Un __graphe__  est un ensemble de __sommets__ pouvant être reliés entre eux par des __arêtes__.

Exemple : Pour représenter un réseau routier, les sommets sont les villes, et les arêtes sont les routes entre ces villes.

```mermaid
flowchart BT
	Grenoble --- Lyon;
	Marseille --- Grenoble;
	Marseille --- Lyon
	Montpellier --- Marseille
	Nice --- Marseille;
	Toulouse --- Montpellier
```
Définitions :

- Un __chemin__ entre 2 sommets A et B représente l'ensemble des d'arêtes à traverser pour relier A à B.
- La __distance d'un chemin__ est le nombre d'arêtes du chemin.
- L’__écartement d’un sommet__ est la distance la plus grande qui existe entre lui et n’importe quel autre sommet du graphe.
- Le __diamètre d’un graphe__ est le plus grand écartement de ses sommets.
- Le __centre d’un graphe__ est le sommet qui a le plus petit écartement.
- Le __rayon d’un graphe__ est l’écartement de son centre.

Le tableau suivant contient la distance entre chaque élève, i.e le nombre minimal d'arête entre 2 élèves, du graphe de la partie  1.

### 2.1. À Faire

1. Complétez le tableau

|         | Alice | Batoul | Clara | Dilan | Eloise | Fatah | Grégory | Hamza |
| :-------: | :----: | :------: | :-----: | :-----: | :------: | :-----: | :-------: | :-----: |
| Alice   | - | 1 |  |  |  |  |  |  |
| Batoul  | 1 | - |  |  |  |  |  |  |
| Clara   |  |  | - |  |  |  |  |  |
| Dilan   |  |       |      |   -   |       |      |  |  |
| Eloise  |  |  |  |  | - |  |  |  |
| Fatah   |  |  |  |  |  | - |  |  |
| Grégory |  |  |  |  |  |  | - |  |
| Hamza   |  |  |  |  |  |  |  | - |

2. Calculer l'écartement de chacun des sommets

| Sommet | Écartement |
| :--: | :--: |
| Alice |  |
| Batoul  |  |
| Clara |  |
| Dilan |  |
| Eloise |  |
| Fatah |  |
| Grégory |  |
| Hamza |  |

2. Déterminer le diamètre, le centre et le rayon du graphe.

| Caractéristique | Valeur |
| :------: | :------: |
| Diamètre |  |
| Centre |  |
| Rayon |  |

## 3. Théorie du "Petit monde"

## 3.1. À Faire

Répondre aux questions suivantes après visionnage de la vidéo suivante :

[![principe_url](https://img.youtube.com/vi/gOiIQ0qGiCc/0.jpg)](https://www.youtube.com/watch?v=gOiIQ0qGiCc&t=6s)

1. Quelle est le principe de la théorie des 6 degrés de séparation ? 
1. Qui a théorise la notion de "petit monde" ? _
1. Quelle est l'expérience menée par Duncan Watts en 2002 ? 
1. Quel est le degré de séparation sur Facebook ? 
1. Établir une comparaison entre le résultat de cette théorie et la notion de diamètre dans un graphe

## 3.2. Compléments

D’après la Revue de l’OFCE / Débats et politiques publiée en 2012 :

- En __2006__ à partir du système de messagerie instantanée de Microsoft (Microsoft Messenger), sur 30 milliards de conversations entre 240 millions d’individus, Leskovec et Horvitz ont pu former un graphe de 180 millions de sommets et 1,3 milliards de liens. Le nombre moyen de degrés de séparation entre deux individus est de __6,6__.
- En __2010__ sur Twitter la distance moyenne calculée sur la base de 5,2 milliards d’échanges valait __4,67__.
- En __2011__ sur une sélection aléatoire de 1 500 utilisateurs de Twitter la distance moyenne valait __3,44__.

## 4. Exercices

### 4.1. Diamètre, centre et rayon d'un graphe

A partir du graphe suivant, compléter le tableau suivant et identifier le centre du graphe, son diamètre et son rayon.

```mermaid
flowchart LR;
A---B;
A---C;
A---D;
A---E;
B---C;
B---E;
C---D;
```

| Distance de | A    | B    | C    | D    | E    | Distance la plus grande par ligne |
| :-----------: | :----: | :----: | :----: | :----: | :----: | :---------------------------------: |
| A           | -    |  |  |  |  |  |
| B           |  | -    |  |  |  |  |
| C           |  |  | -    |  |  |  |
| D           |  |  |  | -    |  |  |
| E           |  |  |  |  | -    |  |

- Diamètre : ?

- Centre : ?

- Rayon : ?

### 4.2. Représentation d'un réseau social

1 - Représenter sous forme d'un graphe, un réseau social constitué de 6 personnes A, B, C, D, E, et F où :

- A est ami avec B et D,
- B est ami avec A, C et E,
- C est ami avec B et E,
- D est ami avec A et E,
- E est ami avec F.

2 - Compléter le tableau des distances suivant :

| Distance de | A    | B    | C    | D    | E   | F | Distance la plus grande par ligne |
| :-----------: | :----: | :----: | :----: | :----: | :----: | :---: | :---------------------------------: |
| A           | -    |  |  |  |  |  ||
| B           |  | -    |  |  |  |  ||
| C           |  |  | -    |  |  |  ||
| D           |  |  |  | -    |  |  ||
| E           |  |  |  |  | -    |  ||
| F           |  |  |  |  |     | - ||

3 - Identifier le centre du graphe, son diamètre et son rayon.

- Diamètre : ?

- Centre : ?

- Rayon : ?
