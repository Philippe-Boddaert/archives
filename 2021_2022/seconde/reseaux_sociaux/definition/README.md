---
title : Les réseaux sociaux
author : M.BODDAERT
license: CC-BY-NC-SA 
---

# Les réseaux sociaux

## 1. Contexte

### 1.1. À Faire

1. Quel est le premier réseau social auquel vous avez participé ? Quel était votre but ?

2. Voici une liste de réseaux sociaux. Pensez-vous que tous ces réseaux sociaux ont les mêmes fonctionnalités ? Indiquer leurs fonctionnalités :

| Réseau | Fonctionnalités |
| :--: | :--: |
| Facebook | |
| Twitter |  |
| Linkedin |  |
| Instagram | |
| Snapchat | |
| WhatsApp | |
| Printerest | |

## 2. Définition

> Un __réseau social__, c’est d’abord des relations entre des personnes : une entreprise, une association, une famille, etc. constituent des réseaux sociaux.
>
> Sur le Web, un réseau social désigne une application qui :
>
> - met en relation des utilisateurs (anciens camarades de classe, employés et entreprises, rencontres de personnes pour des sorties, etc.)
> - propose des fonctionnalités :
>   - partage de documents (texte, images, vidéo etc.)
>   - le contenu peut être d’accès public ou privé, en direct, éphémère ou persistant
>   - on peut généralement réagir aux publications d’autres utilisateurs (like, ou autre)

## 3. Cartographie

Il est possible de cartographier les réseaux sociaux selon 6 fonctionnalités :

![](http://www.monlyceenumerique.fr/snt_seconde/reseaux_sociaux/img/social-media-landscape-2020-1.jpg)

_Source : fredcavazza.net_

### 3.1. À Faire

En groupes, choisir un réseau social et utiliser la grille d'analyse suivante. vous présenterez les résultats de vos recherches sous la forme d'un mini exposé.

|                        Thème                         | Votre analyse |
| :--------------------------------------------------: | :-----------: |
|                         Nom                          |               |
|       Implantation d'origine (pays d'origine)        |               |
|                         URL                          |               |
|                   Date de création                   |               |
|                 Objectifs principaux                 |               |
|                     Public ciblé                     |               |
| Services proposés (audio, notification, vidéos, ...) |               |
|                   Nombre d'abonnés                   |               |
|        Modèle économique, sources de revenus         |               |
|        Paramétrage et protection des données         |               |
|           Informations visibles et cachées           |               |
|      A qui appartiennent les données déposées ?      |               |
|                Avantages de ce réseau                |               |
|             Points faibles de ce réseau              |               |
|         Vous pouvez indiquer d'autres thèmes         |               |

## 4. Modèle économique

Tous les réseaux sociaux ont un coût de fonctionnement. De plus, les réseaux sociaux les plus connus sont gérés par des entreprises commerciales, avec des employés, des charges, et un modèle économique visant à produire des bénéfices.

Les coûts de fonctionnement sont :

- les serveurs
- un accès à internet pour les connecter aux utilisateurs et utilisatrices
- l'électricité pour les faire tourner
- un loyer pour les entreposer
- des employés
  - techniciens pour faire fonctionner les serveurs
  - informaticiens pour programmer le réseau social
  - designers pour l'aspect du réseau social
  - commerciaux pour vendre le produit
  - etc.
- des impôts
- des bureaux pour tous ces travailleurs et travailleuses
- etc.

Il existe différentes possibilités :

- l’accès peut être payant
- le site peut dépendre de la générosité de ses utilisateurs (appel aux dons)
- la publicité sur le site peut financer l’entreprise
- l’entreprise peut revendre à des entreprises clientes les données personnelles des utilisateurs laissées sur le réseau social, ou permettre des publicités ciblées (plus efficaces donc plus chères) grâce aux analyses des données personnelles
- le freemium: les fonctionnalités de base sont gratuites pour tous et il est possible de payer pour avoir des fonctionnalités supplémentaires.
- Autres…

