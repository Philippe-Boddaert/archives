---
title : Les réseaux sociaux
author : M.BODDAERT
license: CC-BY-NC-SA 
---
# Les réseaux sociaux

## 1. Présentation

Le premier réseau social s’appelait __Classmates.com__. Il a été créé en 1995 par Randy Conrads. L’objectif de ce site était de remettre en contact des anciens camarades de classe.

![](./assets/classmates.jpeg)

De multiples applications de réseautage social sont apparues depuis (Facebook, Instagram, ...).

Il est possible de s'interroger sur :

- Qu'est-ce qu'un réseau social ?
- Quelle est la spécificité d'un réseau par rapport à un autre ?
- Quels impacts "sociaux" engendre ces réseaux ?

## 2. Progression

```mermaid
flowchart LR;
    A[1. Définition]-->B[2. Modélisation];
    B-->C[3. Impacts];
    
    click A "./reseaux_sociaux/definition" "Lien vers 1. Définition"
    click B "./reseaux_sociaux/cartographie" "Lien vers 2. Modélisation"
    click C "./reseaux_sociaux/impacts" "Lien vers 3. Impacts"
```

## 3. Attendus du Programme

| Contenus | Capacités attendues |
| -------- | ------------------- |
| Identité numérique, e-réputation, identification, authentification | Connaître les principaux concepts liés à l’usage des réseaux sociaux. |
| Réseaux sociaux existants | Distinguer plusieurs réseaux sociaux selon leurs caractéristiques, y compris un ordre de grandeur de leurs nombres d’abonnés.<br/>Paramétrer des abonnements pour assurer la confidentialité de données personnelles. |
| Modèle économique des réseaux sociaux | Identifier les sources de revenus des entreprises de réseautage social. |
| Rayon, diamètre et centre d’un graphe | Déterminer ces caractéristiques sur des graphes simples. |
| Notion de « petit monde » Expérience de Milgram | Décrire comment l’information présentée par les réseaux sociaux est conditionnée par le choix préalable de ses amis. |
| Cyberviolence | Connaître les dispositions de l’article 222-33-2-2 du code pénal. Connaître les différentes formes de cyberviolence (harcèlement, discrimination, sexting...) et les ressources disponibles pour lutter contre la cyberviolence. |

