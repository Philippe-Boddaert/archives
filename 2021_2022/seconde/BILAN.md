# Bilan

## Exercice 1

Alice, Batoul, Clara, Dilan, Éloïse, Fatah, Grégory et Hamza sont dans la même classe.

En dehors des cours, ils se voient régulièrement aux activités suivantes :

- Alice, Batoul, Clara, Dilan et Hamza font du judo ensemble ;
- Clara, Dilan et Éloïse se voient au club robotique ;
- Grégory et Hamza font partie du même groupe de musique ;
- Grégory et Fatah se croisent souvent à la bibliothèque.

__À Faire sur une feuille de papier__ :

1. Modélisez les relations décrites ci-dessus sous la forme d'un graphe.
2. Quels élèves voient beaucoup d’autres élèves hors des cours ? Lesquels en voient peu ? 
3. Pendant les vacances (donc hors du temps scolaire), je veux faire passer une information le plus rapidement possible à ces huit élèves. À quel élève dois-je m’adresser pour qu’il fasse passer cette information le plus vite possible ?

## Exercice 2

![](./assets/mots_croises.png)

__À Faire : Complétez la liste de mots croisés avec les mots correspondants aux descriptions suivantes__

N.B : Tous les mots font référence à des éléments vus en cours, n'hésitez pas à parcourir les ressources du site https://gitlab.com/Philippe-Boddaert/seconde

| N° | DESCRIPTION DES MOTS À L'HORIZONTALE |
| :--: | :-- |
|2 | Élément reliant deux sommets dans un graphe (qui se retrouve aussi dans un poisson)|
|4 | Encodage des caractères (Et moyen de transport préféré des gens qui en ont)|
|7 | Nous lui devons la découverte du premier BUG (ça a fait Bzz Bzzz dans les circuits) !!|
|9| Commande qui permet d'obtenir l'adresse IP associée à une URL (et ça rime avec MAKEUP)|
|11| Élément de base (LA base quoi...) d'une page HTML |
|13| En-tête (qui casse la tête) de colonnes d'un fichier de données |
|15| Opération de sélection d'un sous-ensemble de jeu de données (...ça permet aussi de faire le café) |

| N° | DESCRIPTION DES MOTS À LA VERTICALE |
| :--: | :-- |
| 1 |  Sans elle pas de Wifi (Vous avez capté ?) |
| 3 | Opération qui permet à une variable de prendre une valeur (Il n'y a qu'à suivre la flèche) |
| 5 | Créatrice du premier algorithme (Son prénom est un palindrome) |
| 6 | Structure qui permet de répéter une suite d'instructions (... et les cheveux) |
| 8 | (Animal totem du maison d'Harry Potter...) et langage préféré des élèves de SNT |
| 10 | HTTP---TCP---IP---DNS... |
| 12 | Réseau reliant des ressources sur Internet par un système de lien hypertexte (...ça tisse une toile) |
| 14 | Unité élémentaire dans un ordinateur (c'est le 8ème d'un octet) |

<div style="page-break-after: always; break-after: page;"></div>

## Exercice 3

### Activité 1

Voici l'image d'un poisson :

![](./assets/poisson.png)

Dans un ordinateur, l'image est stockée numériquement par un exemple de « pixels » (les petits carrés qui constituent une image numérique).

L'image est codée numériquement de la manière suivante :

- L'image est codée ligne par ligne en partant du haut,
- Chaque ligne est codée de gauche à droite,
- Un pixel noir est codé par un 1,
- Un pixel blanc est codé par un 0.

__À Faire : Indiquez le codage numérique de l'image du poisson, sachant qu'un pixel ne peut prendre qu'une seule valeur (1 ou 0).__

<center>
<table style="width:auto;border-collapse:collapse;">
  <tr><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td><td style="height:40px; width:40px;border: 1px solid black;"></td></tr>
</table>
</center>
<div style="page-break-after: always; break-after: page;"></div>

### Activité 2

Voici le codage d'une image suivant le principe indiqué dans l'activité 1 :

<center>
<table style="width:auto;border-collapse:collapse;">
  <tr><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td></tr>
  <tr><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">0</td><td style="height:40px; width:40px;border: 1px solid black;">1</td><td style="height:40px; width:40px;border: 1px solid black;">1</td></tr>
</table>
</center>

__À Faire : Quelle image est codée ? Pour cela, noircissez les cases représentant un pixel égal à 1.__

### Activité 3

Sur une __feuille de papier__, coder numériquement (avec de 1 ou 0) une image de votre choix, de taille 8x8.

__À Faire : Échangez votre feuille avec celle de votre voisin et décodez son image.__

## Exercice 4

Un message est codé, en utilisant la séquence de cours dont le mot 4 de l'exercice 2 fait référence.

67 69 32 78 39 69 83 84 32 80 65 83 32 76 69 32 80 76 85 83 32 70 79 82 84 32 78 73 32 76 69 32 80 76 85 83 32 82 65 80 73 68 69 32 81 85 73 32 65 85 82 65 32 68 85 32 83 85 67 67 69 83 44 32 77 65 73 83 32 67 69 76 85 73 32 81 85 73 32 78 39 65 66 65 78 68 79 78 78 69 32 80 65 83 46 

__À Faire : Décodez le message__

## Exercice 5

On peut associer un mois de l'année à un numéro de la manière suivante :

| N°   | Mois     |
| ---- | -------- |
| 1    | Janvier  |
| 2    | Février  |
| ...  |          |
| 12   | Décembre |

Le code suivant permet de demander un numéro de mois à un élève et affiche un message sur la période scolaire correspondante.

Cependant les lignes de codes sont dans le désordre.

__À Faire : Mettre les lignes de code dans l'ordre et tester le code avec les mois de Janvier, Mai, Juin et Septembre.__

```python
mois = int(input("Entrez le numéro du mois"))

# Les instructions suivantes sont dans le désordre. Remettez de l'ordre dans tout cela !
print("C'est les vacances")
elif mois >= 6:
if mois > 12:
else:
print("C'est le premier trimestre scolaire")
elif mois > 8:
print("C'est la longue période de travail")
print("C'est un mois d'un calendrier non reconnu")
```

