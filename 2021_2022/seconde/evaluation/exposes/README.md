# Exposés

## Préambule

- Permet de mettre en oeuvre les capacités :
	- **Rechercher** de l'information, **apprendre à utiliser des sources** de qualité,
	- **Présenter** un problème ou sa solution, **développer une argumentation** dans le cadre d’un débat.
- Vous serez évalués en utilisant la [grille du grand oral](./grille-evaluation.ods) (lycée général, lycée technologique). L'évaluation sera reportée sur une grille de [notation individuelle](./grille-individuelle.ods).

## Consignes

- Exposé __individuel__ de 5 à 10 minutes. 
- Avec un __support numérique__ (Powerpoint, pdf ou libreoffice) de préférence, 5 à 10 diapositives (une par minute).
- Suivie d'une __séance de questions / réponses__ de 5 à 10 minutes par rapport au sujet et à la présentation.

## Recommandations

- __Premier réflexe__ pour les exposés : aller sur wikipédia chercher les définitions des termes cités.
- Sur internet, on trouve beaucoup de résultats… beaucoup trop ! Il faut absolument éviter les sites conspirationistes. __Vos sources doivent être citées__ (url, date, auteur).
- __Multiplier les sources__ : magazine, article sur internet, vidéo, livre, journal papier... Il est important de multiplier les supports et sources d'informations pour obtenir une vision globale du sujet,
- Consulter le __CDI__ (**C**entre de **D**ocumentation et d'**I**nformation) : Mme HUYET et Mme POUGAULT-POINAT, Professeurs documentalistes, seront ravies de vous aider,
- Sur des sujets __clivants__ (c’est à dire pouvant donner lieu à des avis opposés), il est important de __présenter les deux côtés du débat__. Si l’on présente un résumé d’une page ou d’un article donnant un avis fort, il est important de préciser qu’il peut exister des opinions opposées, et si possible de les présenter aussi.

## Liste de sujets

Je vous propose la liste de sujets suivants. Vous pouvez aussi me proposer un autre sujet en lien avec l'informatique.

### Informations d'actualités

Vous pouvez présenter une actualité en lien avec l'informatique :

- cyberattaques,
- vote de nouvelles lois,
- découvertes et inventions,
- scandales, procès,
- etc.

### Personnages historiques

Qui est-ce ? À quelle époque a-t-il vécu ? Quelle est son histoire ? Pourquoi est-ce une personnalité importante de l'histoire de l'informatique ? Quelles ont été ses contributions ?

- Ada Lovelace
- Alan Turing
- Grace Hopper
- Linus Torvald
- Richard Stallman

### Culture libre

- ***Logiciel libre*** : Qu'est-ce que le logiciel libre / le logiciel propriétaire ? Quels sont les avantages du logiciel libre / du logiciel propriétaire ? Quels sont des exemples de logiciel libre / propriétaire que vous utilisez tous les jours ? Pourquoi des bénévoles ou des entreprisent choisissent-elles de développer un logiciel libre plutôt que propriétaire ?
- ***Licences Creative commons*** : Que sont les licences *Creative commons* ? Que permettent-elles que le droit d'auteur « par défaut » ne permet pas ? Pourquoi des artistes utilisent-ils ces licences ?
- ***Linux*** : Qu'est-ce que Linux ? Qui l'utilise ? Quelles sont ses différences avec Windows et MacOS ?
- ***Wikipédia*** : Qu'est-ce que Wikipédia ? Qui écrit les articles de Wikipédia ? Ses informations sont-elles fiables ? En quoi est-elle différente des encyclopédies classiques ? Peut-on utiliser Wikipédia pour faire un exposé au lycée ?

### Présentation de films / Livres

Vous pouvez faire une critique cinématographique du film, et présenter les choses que vous avez apprises en le regardant.

- ***Les Figures de l'ombre*** (Theodore Melfi, 2016) Récit de calculatrices (femmes faisant des calculs) afro-américaines à la NASA lors de la conquête spaciale.
- ***Imitation game*** (Morten Tyldum, 2014) Biographie romancée d'Alan Turing, mathématicien, informaticien, et cryptologue britannique, qui a réussi à « casser » le code Enigma utilisé par les Allemands durant la Seconde Guerre mondiale, dont les travaux en informatique sont toujours utilisés, et qui était homosexuel à une époque où c'était illégal.
- D'autres films / livres en lien avec l'informatique…

### Technique et Politique

- ***Bitcoin*** : Qu'est-ce que le bitcoin ? Comment fonctionne-t-il ? Quelles questions pose-t-il ?
- ***Chiffrement*** : Comment fonctionne le chiffrement ? Qui utilise le chiffrement ? Le chiffrement est-il sûr ?
- ***Photos sur Facebook (ou Instagram, etc.)*** : À qui appartiennent les photos publiées sur Facebook ? Sont-elles privées ? Quel droit à Facebook sur ces photos ? A-t-on le droit de publier n'importe quelles photos sur Facebook (*L'Origine du monde*, des photos d'amis, des photos d'œuvres d'art, etc.) ?
- ***Peer-to-peer*** : Qu'est-ce que le peer-to-peer ? Pourquoi est-il utilisé ? Quelle est la différence par rapport au téléchargement direct ? Est-il légal ? Qu'est-ce que la HADOPI ? Que sanctionne-t-elle ?
- ***Darknet, Tor*** : Qu'est-ce que le « darknet » ? Qu'est-ce que Tor ? Comment fonctionne-t-il ? Qui utilise Tor et le « darknet » ?
- ***Guerre 2.0*** :  Les attaques entre états, notion de cyberguerre, espionnage des sociétés commerciales, drones et armes autonomes ?
- ***Réseaux sociaux*** : Modération et choix par les réseaux sociaux de ce qui est acceptable ou non ? Dans quelle mesure une plateforme filtre-t-elle son contenu ? Quel est le cadre juridique des actions de modérations ?
- ***Tous fichés*** : Vidéo-protection, QR code à scanner. Quelles sont les technologies de surveillance et de contrôle ? La "contrôlocratie" ou "crédit social" un nouvel attribut du citoyen ?
- ***Vote électronique*** : Liker c'est voter ? Quels sont les avantages, inconvénients de ce système de vote ? A-t-il déjà été utilisé ? 

## Culture Populaire

- ***Is Internet English ?*** : Streamer, cloud, cookie, e-mail, geek, chill, HashTag...nombreux sont les mots anglais liés au web et à internet. Pourquoi cet état de fait ? Internet a-t-il une langue ? Faut-il parler anglais pour comprendre et utiliser Internet ? La langue a-t-elle un impact sur nos comportements ? 
- ***Le Mème*** : Qu'est-ce qu'un mème ? Quelles sont ses origines ? Quelle est son utilité ? Le mème, un objet libre de droit ?
- ___Phénomènes viraux___ : Ice Bucket Challenge / Mannequin Challenge / #MeToo ... Les phénomènes viraux sur le Web sont nombreux. En quoi consistent-ils ? Comment deviennent-ils viraux ? Quels sont leurs impacts positifs / négatifs sur la société ?

## Métiers

- ***Streamer.euse / Youtubeur.se : un métier ?*** : Qu'est-ce qu'être un streameur.se ? Quel parcours ? Quel est le modèle économique ? Être streameur.se / youtubeur.se est-ce être libre ? Quels sont les impacts du streaming sur la santé ? l'environnement ? 
- ***Business Analyst*** : Que fait un *business analyst* ? Dans quels secteurs travaillent les *business analyst* ? Quelles sont les compétences nécessaires pour occuper ce poste ?
- ***Développeur/Développeuse Web*** : Dans quels secteurs travaillent-ils ? Quelles sont les compétences nécessaires pour occuper ce poste ? Quelles sont les études pour y parvenir ?
- ***Community Manager*** : Dans quels secteurs travaillent-ils ? Quelles sont les compétences nécessaires pour occuper ce poste ? Quelles sont les études pour y parvenir ?

## Ressources

- [Site interstices](https://interstices.info/), Interstices est une revue de culture scientifique en ligne, créée par des scientifiques pour vous inviter à explorer les sciences du numérique.
- [Site le blob](https://leblob.fr/), Le blob propose une nouvelle vidéo à la une chaque jour, avec un fil d’actualité scientifique quotidien et des enquêtes mensuelles sur les grands enjeux contemporains.
- [Site Sciences et Avenir](https://www.sciencesetavenir.fr/high-tech/informatique/), Site du magazine mensuel français de vulgarisation scientifique.
- [Page Numérique du Site France Culture](https://www.franceculture.fr/numerique), Station de radio culturelle qui propose des chroniques, podcast, courtes vidéos sur différentes thématiques dont le Numérique
