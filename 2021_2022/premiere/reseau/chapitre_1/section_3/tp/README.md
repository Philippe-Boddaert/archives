# Citateur : Générateur de citations

![](./assets/exemple.png)

Un [motto](https://fr.wiktionary.org/wiki/motto) est une devise, une citation, une phrase qui a la capacité d'inspirer nos pensées et guider nos actes.

L'objectif est de créer une page Web qui affiche une nouvelle citation sur une action de l'utilisateur.

Une démonstration est disponible à cette adresse : [https://philippe-boddaert.gitlab.io/citations/](https://philippe-boddaert.gitlab.io/citations/)

## 1. Contexte

Récupérez les fichiers [index.html](./assets/index.html), [style.css](./assets/style.css) et [citateur.js](./assets/citateur.js) et placez dans un même répertoire.

Ouvrir le fichier `index.html` dans un navigateur : une citation s'affiche, un bouton `rafraichir` est disponible mais rien ne se passe....

## 2. Attendu

Votre travail consiste à ajouter du code Javascript dans le fichier `citateur.js` pour obtenir le __citateur__, un générateur de citations.

Le fichier `citateur.js` contient une variable `citations`, de type Tableau, qui contient l'ensemble des citations sous forme d'une chaine de caractère. (libre à vous de modifier, d'ajouter des citations dans ce tableau...).

Vous allez construire le citateur de manière itérative, en ajoutant à chaque version une nouvelle fonctionnalité ou l'enrichissement d'une fonctionnalité existante.

Les versions sont à faire dans l'ordre.

## 3. Implémentation du citateur

### Version 1 : Affichage aléatoire d'une citation (\*)

L'objectif est d'afficher une citation, choisie aléatoirement, à chaque clic sur le bouton `rafraichir`.

Sur un clic sur le bouton `rafraichir` :

- On récupère une valeur aléatoire entre 0 et la taille du tableau `citations`, notée `i`
- On modifie le paragraphe `citation` avec la valeur de la citation à l'indice `i`.

#### Aide

L'instruction suivante permet d'obtenir un nombre aléatoire entre 0 et 10, non inclus :

```js
Math.floor(Math.random() * 10);
```

### Version 2 : Affichage aléatoire d'une image de fond (\*)

Sur le même principe que la gestion des citations, afficher une image de fond choisie aléatoirement parmi un tableau prédéfini.

#### Aide

Soit la variable `corps`, correspondant à l'élément `body`de la page HTML. Il est possible de modifier la propriété de l'image de fond via l'instruction JavaScript suivante :

```js
let urlImage = "https://cdn.pixabay.com/photo/2016/11/29/04/19/ocean-1867285_960_720.jpg";
corps.style.backgroundImage = "url(" + urlImage + ")";
```

### Version 3 : Affichage aléatoire sur un autre événement (\*)

Jusqu'ici, une citation et une images choisies aléatoirement sont affichées sur un clic sur le bouton `rafraichir`, dans cette version, on souhaite avoir le même comportement lorsque l'utilisateur presse la touche `espace` du clavier ! 

#### Aide

Le bloc de code suivant permet d'afficher un message lorsque l'utilisateur clique sur la touche `b` du clavier.

```js
document.addEventListener('keydown', function(event){
    if (event.keyCode == 66){
      alert("Vous avez appuyé sur la touche b.");
    }
});
```

Le site [https://keycode.info/](https://keycode.info/) peut vous aider à obtenir les codes associées au touche d'un clavier.

### Version 4 : Génération aléatoire d'une citation (\***)

Les citations sont définies dans une et une seule chaine de caractères.

L'idée est de générer des citations aléatoires à partir de parties de phrases.

Pour cela, on définit 3 groupes : `sujet`, `verbe` et `complement` qui contiennent chacuns une partie d'une phrase. Une citation aléatoire est la concaténation d'une sélection aléatoire des éléments des 3 groupes.

#### Aide

Exemples avec les groupes suivants :

| Groupe     | Éléments du groupe                                           |
| ---------- | ------------------------------------------------------------ |
| sujet      | 'La voiture électrique', 'Un verre de lait', 'Demain matin'  |
| verbe      | 'soigne tes blessures', 'ne veut pas dire grand chose', 'est une arme redoutable', 'te transformera en princesse' |
| complement | 'c'est bon ça', 'surtout le dimanche', 'ça n'est que mon opinion', 'passe le message' |

Imaginons que les 3 nombres suivants soient choisis aléatoirement : `0, 2, 1`, la citation générée et affichée serait alors : `La voiture électrique est une arme redoutable surtout le dimanche`.

### Version 5 : Mode roulette (\****)

L'idée est d'afficher 5 citations / images aléatoires toutes les 200 millisecondes puis une citation / image définitive lorsque l'utilisateur presse la touche `a`.

#### Aide

##### setInterval

La fonction `setInterval` permet d'exécuter une fonction à un intervalle de temps donné.

La signature de `setInterval`est : `setInterval(fonction, intervalle)` où :

- `fonction` est la fonction à exécuter à chaque intervalle de temps,
- `intervalle` est un entier correspondant à un nombre de millisecondes.

Le code suivant change toute les secondes (1000 millisecondes) la taille de la citation, d'une valeur entre 0 et 80 pixels.
```javascript
let citation = document.querySelector("#citation");
setInterval(function(){ 
  citation.style.fontSize = Math.floor(Math.random() * 80) + "px"; 
}, 1000);
```

Plus d'informations sur la fonction `setInterval`: https://www.w3schools.com/jsref/met_win_setinterval.asp

##### clearInterval

La fonction `clearInterval` permet de stopper l'exécution d'un intervalle.

Un exemple d'utilisation : https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_clearinterval

### Version 6 : Un peu de couleur (\**)

L'idée est de changer la couleur du texte de la citation lorsque l'utilisateur presse certaines touches du clavier.

Exemple :

| Touche | Comportement                  |
| ------ | ----------------------------- |
| `r`    | Le texte est affiché en rouge |
| `g`    | Le texte est affiché en vert  |
| `b`    | Le texte est affiché en bleu  |

### Version 7 : Image aléatoire au survol (\*)

L'idée est lorsque l'utilisateur survole la citation courante une image aléatoire est affichée parmi une liste prédéfinie (Par exemple celle définie dans la version 2).

### Version 8 : Citation par mot clé (\****)

L'idée est de pouvoir afficher une citation selon un mot choisi.

Imaginons un nouveau bouton disponible sur l'interface (`Rechercher`), lorsque l'utilisateur clique sur ce bouton :

```txt
Une boite de dialogue apparait pour saisir un mot, noté m
Si aucune citation ne comporte m alors
	le message "Aucune citation ne comporte" + m est affiché dans le paragraphe d'id citation
Sinon si une seule citation comporte m alors
	la citation est affichée dans le paragraphe d'id citation
Sinon
	une citation comportant m est choisie au hasard et est affichée dans le paragraphe d'id citation
FinSi
```

#### Aide

La fonction `prompt` permet d'afficher une boite de dialogue, à travers laquelle l'utilisateur peut renseigner une valeur.

La fonction `prompt` prend en paramètre une chaine de caractères correspondant au message affiché dans la boite de dialogue.

Le retour de la fonction `prompt` est la valeur saisie par l'utilisateur.

```javascript
let age = prompt("Veuillez saisir votre âge");
// la variable age contient la valeur saisie par l'utilisateur
```