let citations = [
  "À la fin d'un livre, il faut s'arrêter de lire...sous peine de lire le code barre.",
  "L'homme nu ne craint pas les pickpockets.",
  "Les céréales avant le lait ? Le lait avant les céréales ?<br/>L'essentiel est d'avoir un bol.",
  "Si pour s'endormir il faut compter les moutons...<br/>Que comptent les moutons pour s'endormir ?"
];

// Objectif de la Version 1

// Sur un clic sur le bouton rafraichir :
//    On récupère une valeur aléatoire entre 0 et la taille du tableau citations, notée i
//    On modifie le paragraphe citation avec la valeur de la citation à l'indice i.