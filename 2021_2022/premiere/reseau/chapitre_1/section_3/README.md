# Le langage JavaScript

## 1. Contexte

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/240px-Unofficial_JavaScript_logo_2.svg.png)

_Logo non officiel, Source : GitHub.com_

Le JavaScript a été créé en 1995 par Brendan Eich, il est utilisé pour programmer le navigateur côté client.

Attention à ne pas confondre le JavaScript et le Java, ce sont des langages différents.

## 2. Syntaxe du JavaScript

Basthon permet d'exécuter du code Javascript. Pour cela, il faut utiliser l'adresse suivante : [https://notebook.basthon.fr/js](https://notebook.basthon.fr/js/#)

__À Faire : Consulter ce [petit manuel de Javascript](./manuel.ipynb) avec Basthon, afin d'appréhender la syntaxe de ce langage.__

## 3. Intégration de code JavaScript

Le code javascript peut être intégré à un document `html` grâce à la balise `<script>`.

Les fichiers JavaScript utilisent l'extension `.js` que l'on importe en précisant son URI grâce à l'attribut `src`.

Copier ce code dans un fichier `script.js`:

```js
alert('Hello world !');
console.log('Script Hello world exécuté.');
```


Copier ce code dans un fichier `index.html`:

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>Un peu de javascript</title>
  </head>
  <body>
    <h1>Titre principal de mon document</h1>
    <p>
      Lorsque cette page s'affiche...un <a href="https://www.larousse.fr/dictionnaires/francais/pop-up/188019">pop-up</a> apparait !!
    </p>
     <script src='script.js'></script>
  </body>
</html>
```

## 4. Débogage du JavaScript

Pour déboguer du code javascript, on utilise le plus souvent la console du navigateur(Ctrl + Maj + I). Puis on y affiche des valeurs de variables depuis le programme javascript avec la fonction `console.log()`.

## 5. Manipulation des éléments d'une page web : le DOM

### 5.1. Définition

> le DOM est l'abréviation de **Document Object Model**. Le DOM est une **interface de programmation** normalisée par le W3C, qui permet à des scripts JavaScript d'examiner et de modifier le contenu du navigateur web. 

Par le DOM, la composition d'un document HTML est représentée sous forme d'une structure en arbre. 

On associe un arbre DOM au code **HTML** d'une page .html. Prenons l'exemple de la page ci-dessous :

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> arbre du DOM  </title>
    </head>
    
    <body>
        <h1> Titre principal </h1>
        <h2> titre de la section 1 </h2>
        <p id="important">
        Paragraphe avec un lien <a href="https://www.google.com">Google</a>  
        </p>
      <div>
        <p class="autre">Lorem Ipsum</p>
        <p class="autre">Sed ut perspiciatis</p>
      </div>
      <h1 id="vide"></h1>
      <script src="script.js"></script>
    </body>
</html>
```

L'arbre associé à cette page est le suivant :

```mermaid
flowchart TD
	html --> head
	html --> body
	head --> meta
	head --> title
	body --> h1
	body --> h2
	body --> p
	p --> a
	body --> div
	div --> p1["p"]
	div --> p2["p"]
	body --> ha["h1"]
```

### 5.2. Manipulation du DOM via JavaScript : les sélecteurs

À l'aide du DOM, un script peut modifier le document présent dans le navigateur en ajoutant, modifiant, supprimant des nœuds de l'arbre.

#### 5.2.1. À Faire

__Ajouter et exécuter successivement les codes dans le script `script.js`__

```javascript
document.querySelector('#vide').innerHTML = "Un titre rempli";
```

```javascript
document.querySelector('#important').style.color = '#AA00AA';
```

```javascript
let paragraphes = document.querySelectorAll('p');
for (let i = 0; i < paragraphes.length; i++){
  paragraphes[i].style.backgroundColor = 'black';
}
```

#### 5.2.2. Bilan

On a donc introduit 2 nouvelles fonctions, dont la syntaxe permet de faire appel aux sélecteurs CSS. On applique en général ces deux méthodes à partir de la racine **document**.

- `querySelector()` : renvoie le premier élément trouvé satisfaisant au sélecteur (type de retour : Element), ou null si aucun objet correspondant n'est trouvé.

- `querySelectorAll()` : renvoie tous les éléments satisfaisant au sélecteur, dans l'ordre dans lequel ils apparaissent dans l'arbre du document (type de retour : NodeList), ou un tableau NodeList vide si rien n'est trouvé.

### 5.3. Exercices

Écrire les instructions JavaScript permettant de :

1. Modifier le texte du premier `h1` par `Un meilleur Titre`
2. Souligner le texte du paragraphe `important`
3. Mettre le fond du `div` en couleur `magenta`
4. Centrer le texte de tous les titres `h1`

## 6. Interaction au sein d'une page web : les événements

### 6.1. À Faire

Copier le code suivant dans le script `script.js` et rafraichir la page Web.

```javascript
document.querySelector('#important').addEventListener('click', function(){
  alert('Ce message est très important !');
});
```

### 6.2. Définition

#### 6.2.1. Paradigme

JavaScript est un langage multi-paradigme.

> Un __paradigme de programmation__ est une façon d'approcher la programmation informatique.

> Tout comme Python, JavaScript suit le __paradigme de programmation impérative__ : décrit les opérations comme une suite d'instructions exécutées par l'ordinateur pour modifier l'état du programme.
>
> Javascript implémente le __paradigme de programmation événementielle__. Le programme est principalement défini par ses réactions aux différents événements qui peuvent se produire.

#### 6.2.2. Événement

Dans l'exemple précédent, le programme __réagit__ à l'événement consécutif au click sur le paragraphe `important`.

Un __événement__ est une action qui se produit et qui possède deux caractéristiques essentielles :

- C'est une action qu'on peut « écouter », c'est-à-dire une action qu'on peut détecter car le système va nous informer qu'elle se produit ;
- C'est une action à laquelle on peut « répondre », c'est-à-dire qu'on va pouvoir attacher un code à cette action qui va s'exécuter dès qu'elle va se produire.

#### 6.2.3. Gestionnaire d'événements

Pour écouter et répondre à un événement, nous allons définir ce qu'on appelle des **gestionnaires d’événements**.

```javascript
document.querySelector('#important').addEventListener('click', function(){
  alert('Ce message est très important !');
});
```

La fonction `addEventListener` est une manière d'implanter un gestionnaire d'événements en javascript.

Elle s'applique sur un élément HTML et prend 2 paramètres :

1. Le __type de l'événement__ à écouter (dans l'exemple `click`), sous la forme d'une chaine de caractères,
2. Le __code à exécuter__ en cas de déclenchement de cet événement, sous la forme d'une fonction.

Quelques types d'événements les plus utilisés (Une liste plus complète [ici](https://developer.mozilla.org/fr/docs/Web/Events#listing_des_%C3%A9v%C3%A9nements)) :

| `Type événement` | Description                 |
| -------------- | -------------------------------------- |
| `click`      | clic de la souris sur l'élément        |
| `dblclick`   | double-clic de la souris sur l'élément |
| `mouseover`     | déplacement de la souris sur l'élément                   |
| `mouseout`     | déplacement de la souris hors de l'élément                   |
| `keydown` | appuie sur une touche du clavier       |
| `keyup`      | touche de clavier relâchée             |

### 6.3. Exercices

1. Modifier l'exemple du 6.1. pour afficher la pop-up d'alerte sur un survol du paragraphe `important`,
2. Modifier le texte du h1 `vide` pour que quand l'utilisateur survole celui-ci, le titre soit "Ah ah !" et lorsque celui-ci n'est plus survolé "Au revoir !".
3. En vous référant l'exemple ci-dessous, écrire le code javascript permettant de mettre le texte des paragraphes `autre` en vert lorsque l'utilisateur appuie sur la touche `v`, en rouge lorsque l'utilisateur appuie sur la touche `r`.

```javascript
document.addEventListener('keydown', function(event){
  let titre = document.querySelector('#vide');
  if (event.keyCode == 32){
    titre.innerHTML = "Touche Espace pressée";
  } else if (event.keyCode == 27){
    titre.innerHTML = "Touche Esc pressée";
  }
});
```

N.B : Le site https://keycode.info/ permet d'obtenir la valeur du `keyCode` associée à la touche d'un clavier.
