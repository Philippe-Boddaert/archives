# Configurer le style d'une page Web

## 1. Introduction

✏ Télécharger le fichier [style_1.css](./assets/style_1.css) et déplacer le dans le même répertoire que votre fichier HTML relatif à l'activité précédente.

✏ Reprendre le fichier html et ajouter dans l'en-tête, i.e dans le corps de la balise `<head>...</head>`, la ligne suivante :

```html
<link rel="stylesheet" href="style_1.css">
```

__Que constatez-vous ?__

✏ Télécharger le fichier [style_2.css](./assets/style_2.css) et déplacer le dans le même répertoire que votre fichier HTML relatif à l'activité précédente.

✏ Reprendre le fichier html et ajouter dans l'en-tête, i.e dans le corps de la balise `<head>...</head>`, la ligne suivante :

```html
<link rel="stylesheet" href="style_2.css">
```

__Que constatez-vous ?__

## 2. Définition

> Un fichier `.css` est une __feuille de style CSS__ (**C**ascading **S**tyle **S**heets).
>
> Elle permet l'élaboration de la mise en forme hors des documents HTML.

Il est par exemple possible de ne décrire que la __structure__ d'un document en __HTML__, et de décrire toute la __présentation__ dans une __feuille de style CSS séparée__.

| Bénéfices importants                                         | Avantages des feuilles de style                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| - Un changement de présentation grandement facilité<br />- Une réduction notable de la complexité de l'architecture d'un document. | - La structure du document et la présentation sont gérés dans des fichiers séparés.<br />- La conception d'un document se fait dans un premier temps sans se soucier de la présentation, ce qui permet d'etre plus efficace. <br />- Le code HTML est considérablement __réduit en taille et en complexité__, puisqu'il ne contient plus de balises ni d'attributs de présentation. <br />- La modification du style d'un élément, dans une feuille de style CSS, se __répercute automatiquement__ à toutes les pages HTML qui y sont rattachées. |

## 3. Syntaxe

On associe à chaque type de balise html un bloc css.

La structure d'un __bloc css__ est :

```txt
type_de_l'élément_html {
	propriété : valeur;
	propriété2 : valeur2;
	...
}
```

__Illustration __:

```css
body {
  background-color: purple;
  color : yellow;
}
```

__Explication__ :

1. Le bloc css __s'applique__ à l'élément `body`de la page,
2. La couleur du fond du bloc est `purple`
3. La couleur de tout texte du bloc est `yellow`

### 3.1. Propriétés de style

Quelques exemples de propriétés CSS

| Propriété | Description |
| :--: | :-- |
| __color__ | Change la couleur du texte |
| __background-color__ | Change la couleur de fond |
| __line-height__ | Distance entre les lignes |
| __text-align__ | Alignement du texte : center, left, right ou justify |
| __text_decoration__ | Décoration du texte : underline, overline, line-through | 
| __font-size__ | Taille de la police du texte : 15px, 150 % |
| __font-family__ | Police d’écriture : serif, sans-serif, monospace, cursive,fantasy, system-ui |
| __letter-spacing__ | Espace entre les caractères |
| __border-width__ | L’épaisseur de la bordure : 2px |
| __border-color__ | Couleur de bordure : red, #ff0000, rgb(25, 0, 0) |
| __border-style__ | Type de bordure : solid, dashed, double |
| __border-radius__ | Arrondi de bordure : 25%, 50% |
| __list-style-type__ | Choix du type de puces pour les li |

Pour obtenir une liste plus exhaustive, consultez le site [css-faciles.com](http://www.css-faciles.com/proprietes-css-liste-alphabetique.php).

### 3.2. À Faire

1. ✏ Quelle est la signification du bloc css suivant ?
```css
h1 {
	text-align : center;
	color : green;
}
```

2. ✏ Modifier la feuille de style pour que :

   1. Le texte d'un titre `h2`soit aligné à droite,
   2. La couleur de fond des paragraphes soient en gris clair (`lightgray`)
   3. Les textes des paragraphes soient en gras (`bold`)

3. ✏ Modifier la feuille de style_1 pour que la présentation du programme d'Algo Martin soit la plus proche de [cette présentation](./assets/programme_style_1_complet.pdf)


### 3.3. Bilan

Les blocs de styles CSS s'appliquent à __TOUS__ les éléments html du type défini.

```css
p {
  color : blue;
}
```

Les textes de __TOUS__ les paragraphes du document HTML auront la couleur du texte bleue.

✏ Que se passe-t-il si je souhaite appliquer un style pour un paragraphe ou un élément particulier ?

## 4. Les sélecteurs

Il est possible de spécifier un sélecteur d'un élément html.

### 4.1. Sélecteur unique : id

Dans le cas où l'élément HTML qu'on souhaite spécifier est unique, i.e dans le document html, un seul élément est à traiter, on utilise l'attribut `id`

L'attribut `id` prend pour valeur, une chaine de caractères.

Dans le document HTML, on définit l'attribut `id` pour l'élément que l'on souhaite différencier.

```html
<p id="auteur">M. Algo Martin</p>
```

Dans la feuille de style CSS, on fait précéder la valeur précédée d'un dièse :

```css
#auteur {
 color : red;
 font-size : 36px;
}
```

__N.B : Il ne doit avoir qu'un et un seul élément HTML avec la valeur `id` spécifiée.__ Autrement dit, pour l'exemple ci-dessus, il n'y a qu'un seul élément avec l'attribut `id="auteur"`

### 4.1. Sélecteur multiple : class

L'attribut `class` prend une chaine de caractères.

Dans le document HTML, on définit l'attribut `class` pour les éléments que l'on souhaite différencier.

```html
<p class="important">Ce texte est très important !!</p>
```

Dans la feuille de style CSS, on fait précéder la valeur précédée d'un point :

```css
.important {
 color : orange;
}
```

__N.B : Le style s'applique à tous les éléments HTML dont les attributs `class` ont la même valeur.__ Autrement dit, pour l'exemple ci-dessus, tous les paragraphes dont l'attribut `class="important"`

### 4.3. Exercices

✏ Télécharger le fichier [poeme.html](./assets/poeme.html) et [style_poeme.css](./assets/style_poeme.css)

#### 4.3.1 À Faire

✏ Modifier la feuille de style `style_poeme.css` pour que :

1. Le titre `h1` ait le texte centré,
2. Tous les paragraphes aient la couleur du texte en noir,
3. Les paragraphes dont la classe est `impair` aient la couleur du fond en `lightblue` ,
4. Le paragraphe de l'auteur soit en italique, la couleur du texte en blanc et aligné à droite.

#### 4.3.2. À Faire

✏ Ajouter le poème suivant ayant pour titre `Nice` et auteur `Vette de Fonclare`.

```txt
Est-elle provençale ou ne l’est-elle pas ?
Toujours est-il qu’elle est une fille du soleil
Dont elle est goulûment l’aficionada,
Et son ciel est si pur que son bleu émerveille !

Sa plage de galets longe une baie arquée
Où se dissout le soir l’aura de milliers d’Anges.
Ville aux mille couleurs, ses rues sont colorées
Par l’éclat du Levant aux rayons rouge-orange

Enluminant le linge étendu aux croisées
En milliers de drapeaux célébrant le Midi.
Trois Anglais en goguette y flânent éméchés,
La casquette à l’envers et l’air fort réjoui,

Zigzaguant en tanguant vers le Marché aux Fleurs.
Sur le cours Saleya des monceaux de bouquets
Attendent le chaland ravi et appâté
Par des odeurs fleuries vibrant dans la chaleur.

Est-elle provençale, notre Nice embaumée ?
Si elle ne l’est pas, moi, je vais l’adopter…
```

__N.B : Chaque paragraphe du poème doit être dans un élément paragraphe html distinct.__

✏ Modifier le fichier html et la feuille de style avec un style de votre choix (couleur du texte, du fond, style du texte...) mais différent de celui du poème de l'albatros.

## 5. Compléments

Les styles __s'appliquent en cascade__ (d'où le nom **C**SS). 

Le style final d'un élément est l'application successive des styles définis dans la feuille, respectant leur ordre d'apparition.

__Illustration__ :

Exemple dans un document HTML :

```html
<p id="auteur">M. Algo Martin</p>
```

Dans la feuille de style CSS :

```css
p {
  color : blue;
}

#auteur {
 font-size : 36px;
}
```

__Explication__ :

Le paragraphe aura le texte en bleu ET une taille de 36 pixels, car l'élément HTML est de type `p`et a pour sélecteur `id="auteur"`

