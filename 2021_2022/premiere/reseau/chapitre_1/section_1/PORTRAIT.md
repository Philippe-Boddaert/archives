---
title : Ma première page Web
author : M.BODDAERT d'après les travaux de M. Stéphan VAN ZUIJLEN
license: CC-BY-NC-SA 
---

# Mon portrait chinois

## 1. Contexte

L'objectif de cette activité est d'appréhender le langage HTML, à travers la création de votre __portrait chinois__ sous la forme d'une page web.

> Dans le domaine littéraire, un __portrait chinois__ est un jeu permettant de déceler certains aspects de la personnalité d'un individu ou d'identifier ses goûts ou ses préférences personnelles, au travers d'un questionnaire.

Quelques exemples de question d'un __portait chinois__ :

1. Si j'étais une saison, je serais...
2. Si j'étais un animal, je serais...
3. Si j'étais un pays, je serais...
4. Si j'étais un site web, je serais...
5. Si j'étais une recette, je serais...
6. Si j'étais une série, je serais...

Exemple : [Portrait chinois de votre professeur](./assets/portrait_chinois.pdf)

## 2. La structure de base

Pour créer une page web, il faut un éditeur de texte (Notepad++)

1. ✏ Ouvrir Notepad++
2. ✏ Copier ces quelques lignes de code dans l'éditeur choisi.

```html
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Mon portrait chinois</title>
 </head>
 <body>

 </body>
</html>
```

3. ✏ Enregistrer la page sous le nom : `index.html`
4. ✏ Ouvrir le fichier avec un navigateur web. Que constatez-vous ?

### 2.1 Analyse

> Le ___HyperText___ ___Markup___ ___Language___ (HTML) est un langage de **balisage** conçu pour représenter les pages ___Web___.
> 
> On parle de langage de balisage. 
> 
> Le `HTML` permet de décrire la structure d'une page web.

Un document HTML est composé de 4 parties obligatoires :

- l'instruction du langage (`!DOCTYPE html>`), à destination du navigateur afin qu'il interprète correctement le document,
- une structure, entre les balises `<html>`et `</html>`,
- une en-tête, entre les balises `<head>` et `</head>`,
- un corps, entre les balises `<body>`et `</body>`.

### 2.2. Syntaxe des balises

Les balises HTML respectent une syntaxe simple et stricte :

- Un **chevron ouvrant (<)**
- Le **type de la balise**
- Des **attributs** (optionnels). Un espace, suivi du nom de l'attribut, d'un signe égal (=) et d'une valeur entre doubles quotes ("").
- Un **chevron fermant (>)**

_Anatomie d'un élément_ :

```math
\overbrace{\color{green}{<}\underbrace{\color{green}{\text{body}}}_{type}\color{green}{>}}^{Balise \; ouvrante}\text{...}\overbrace{\color{green}{\text{</body>}}}^{Balise \; fermante}
```

_Anatomie d'un élément avec attribut_ :

```math
\overbrace{\color{green}{<}\underbrace{\color{green}{\text{meta}}}_{type}\quad \underbrace{\color{blue}{\text{charset}}=\color{red}{\text{'utf-8'}}}_{attributs}\color{green}{>}}^{Balise \; ouvrante}\dots\overbrace{\color{green}{\text{</meta>}}}^{Balise \; fermante}
```

Pour l'instant, le navigateur affiche une page blanche...C'est normal, car il n'y a rien à afficher dans le __corps__ de la page, à part "Mon portrait chinois" dans l'onglet de navigation.

## 3. Ajouter un titre

✏ Ajoutez un __titre__ dans le corps de la page : l'élément `h1` permet d'écrire avec une grande taille le texte qu'il contient.

```html
 <h1>Mon portrait chinois</h1>
```

✏ Ajoutez un autre __titre__ dans le corps de la page, mais plus petit : l'élément `h2` permet d'écrire avec une  taille le texte qu'il contient, plus petit que l'élément `h1`.

```html
 <h2>Si j'étais une saison, je serais...</h2>
```

__N.B__ : Les éléments `h1`, `h2`, `h3` donnent des __textes de tailles différentes__...testez les différents éléments

## 4. Ajouter un paragraphe

✏ Ajoutez un __paragraphe__ pour notre page : l'élément `p` permet d'écrire un texte sous forme d'un paragraphe de taille normale.

```html
 <p>???</p>
```

✏ Remplacez les ??? du paragraphe avec votre réponse à la question `Si j'étais une saison, je serais...`

## 5. À vous de jouer

Maintenant que vous savez ajouter des titres et paragraphes, c'est à vous de compléter votre portrait chinois !!!

- ✏ Ouvrir le [portrait chinois de votre professeur commenté](./assets/portrait_chinois_commentaire.pdf)
- ✏ En vous inspirant de celui de votre professeur. Compléter votre portrait chinois (votre page web) en ajoutant, dans l'ordre, les rubriques :
  1. Si j'étais un animal, je serais...
  2. Si j'étais un pays, je serais...
  3. Si j'étais un site web, je serais...
  4. Si j'étais une recette, je serais...
  4. Si j'étais une série, je serais...
  4. ...Et tout autre rubrique de votre imagination (ce [site](https://oser-ecrire.fr/ecrire-portrait-chinois) peut vous aider à trouver l'inspiration)
  
- Le [cours](./README.md) vous indique comment ajouter les différents éléments d'une page web.