# Réseau et Web

Lors de la navigation sur le Web, les internautes interagissent avec leur machine par le biais de pages Web.

Ces interfaces reposent sur la gestion d’événements associés à des éléments graphiques entrainant une communication d'information entre le navigateur et un serveur distant.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Modèle-client-serveur.svg/476px-Modèle-client-serveur.svg.png)

_Source : wikimedia_

Cette séquence se décompose en 2 chapitres :

- [Chapitre 1 : Le Web](./chapitre_1/)
- [Chapitre 2 : Le Réseau](./chapitre_2/)