# Méthode UDP

## 1. Principe

Contrairement au protocole TCP, __UDP__ (_User Datagram Protocol_) s'affranchit de la numérotation des paquets et n'assure en __aucun cas la fiabilité__ des données transmises.

## 2. Illustration

Prenons l'exemple d'un hôte client souhaitait récupérer un fichier se trouvant sur un hôte serveur.

![](./tcp.jpg)

__Étape 1__ : 

- le client envoie une requête au serveur ("Envoie moi le fichier avec la méthode UDP"),
- le serveur découpe le fichier en 3 paquets (matérialisés par des ronds de couleur).

![](./tcp_1.jpg)

__Étape 2__ : 

- le serveur envoie le premier paquet (rouge) au client __sans__ demander un accusé réception pour ce paquet.

![](./udp_2.jpg)

__Étape 3__ : 

- le client reçoit le premier paquet (rouge),
- le serveur envoie le deuxième paquet (vert) au client __sans__ demander un accusé réception pour ce paquet.

![](./udp_3.jpg)

__Étape 4__ : 

- le client __ne reçoit pas__ le deuxième paquet (vert),
- le serveur envoie le troisième paquet (bleu) au client __sans__ demander un accusé réception pour ce paquet.

![](./udp_4.jpg)

__Étape 5__ : 

- le client reçoit le troisième paquet (bleu)

![](./udp_5.jpg)

__Fin__ :

- le serveur n'a plus de paquets à envoyer,
- le client n'a pas reçu l'ensemble des paquets et ne peut pas reconstituer le fichier.
