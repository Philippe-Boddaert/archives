# Méthode TCP

## 1. Principe

La méthode __TCP__ du protocole TCP assure une qualité de service, c'est à dire qu'il assure le découpage du fichier en plus petits paquets, en permettant le routage des données par quelques chemins que cela soit tout en promettant une __reconstitution des fichiers demandés__.

## 2. Illustration

Prenons l'exemple d'un hôte client souhaitait récupérer un fichier se trouvant sur un hôte serveur.

![](./tcp.jpg)

__Étape 1__ : 

- le client envoie une requête au serveur ("Envoie moi le fichier avec la méthode TCP"),
- le serveur découpe le fichier en 3 paquets (matérialisés par des ronds de couleur)

![](./tcp_1.jpg)

__Étape 2__ : 

- le serveur envoie le premier paquet (rouge) au client et lui demande un accusé réception pour ce paquet.

![](./tcp_2.jpg)

__Étape 3__ : 

- le client reçoit le premier paquet (rouge),
- il envoie un accusé de réception au serveur.

![](./tcp_3.jpg)

__Étape 5__ : 

- le serveur reçoit l'accusé de réception du premier paquet (rouge),
- il envoie le deuxième paquet (vert) au client et lui demande un accusé réception pour ce paquet.

![](./tcp_4.jpg)

__Étape 6__ : 

- Au bout d'un certain temps, le serveur identifie qu'il n'a pas reçu d'accusé réception du deuxième paquert (vert) du client.

![](./tcp_5.jpg)

__Étape 7__ : 

- Le serveur renvoie le deuxième paquet (vert) au client et lui demande un accusé réception pour ce paquet.

![](./tcp_4.jpg)

__Étape 8__ : 

- le client reçoit le deuxième paquet (vert),
- Il envoie un accusé de réception au serveur.

![](./tcp_6.jpg)

__Étape 9__ : 

- le serveur reçoit l'accusé de réception du deuxième paquet (vert),
- il envoie le troisième paquet (bleu) au client et lui demande un accusé réception pour ce paquet.

![](./tcp_7.jpg)

__Étape 10__ : 

- le client reçoit le troisième paquet (bleu),
- Il envoie un accusé de réception au serveur.

![](./tcp_8.jpg)

__Fin__ :

- le serveur n'a plus de paquets à envoyer,
- le client a reçu l'ensemble des paquets et peut reconstituer le fichier.