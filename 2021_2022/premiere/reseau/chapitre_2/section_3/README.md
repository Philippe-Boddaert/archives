---
title : Protocole du Bit alterné
author : David Roche
license : CC-BY-NC-SA
---

# Protocole du Bit alterné

## 1. Contexte

Nous avons vu que le protocole TCP propose un mécanisme d'accusé de réception afin de s'assurer qu'un paquet est bien arrivé à destination. On parle plus généralement de __processus d'acquittement__. 

Ces processus d'acquittement permettent de détecter les pertes de paquets au sein d'un réseau, l'idée étant qu'en cas de perte, l'émetteur du paquet renvoie le paquet perdu au destinataire. 

Nous allons ici étudier un protocole simple de récupération de perte de paquet : le __protocole de bit alterné__.

## 2. Principe

Le protocole de bit alterné est implémenté au niveau de la couche Physique du modèle TCP/IP.

![Le modèle TCP/IP](https://isn-icn-ljm.pagesperso-orange.fr/SNT/Internet/res/image-comm2.png)

_Source : Van Zuijlen Stéphan_

Le principe de ce protocole est simple, considérons 2 ordinateurs en réseau : 

- un ordinateur A qui sera l'émetteur des trames et 
- un ordinateur B qui sera le destinataire des trames. 

Au moment d'émettre une trame :

1. A va ajouter à cette trame un bit (1 ou 0) appelé __drapeau__ (_flag_ en anglais). 
2. B va envoyer un __accusé de réception__ (_acknowledge_ en anglais souvent noté ACK) à destination de A dès qu'il a reçu une trame en provenance de A. 
3. À cet accusé de réception, on associe aussi un bit drapeau (1 ou 0).

## 3. Illustration

La règle est relativement simple : 

- la première trame envoyée par A aura pour drapeau 0, 
- dès cette trame reçue par B, ce dernier va envoyer un accusé de réception avec le drapeau 1 (ce 1 signifie "la prochaine trame que A va m'envoyer devra avoir son drapeau à 1"). 
- Dès que A reçoit l'accusé de réception avec le drapeau à 1, il envoie la 2e trame avec un drapeau à 1, et ainsi de suite...

![img](https://pixees.fr/informatiquelycee/n_site/img/nsi_prem_res_bitAlt_1.png)

Le système de drapeau est complété avec un système d'horloge côté émetteur. Un "chronomètre" est déclenché à chaque envoi de trame, si au bout d'un certain temps, l'émetteur n'a pas reçu un acquittement correct (avec le bon drapeau), la trame précédemment envoyée par l'émetteur est considérée comme perdue et est de nouveau envoyée.

Examinons quelques cas :

### 3.1. Trame perdue

![img](https://pixees.fr/informatiquelycee/n_site/img/nsi_prem_res_bitAlt_2.png)

Au bout d'un certain temps ("TIME OUT") A n'a pas reçu d'accusé de réception, la trame est considérée comme perdue, elle est donc renvoyée.

### 3.2. Accusé de réception perdu

![img](https://pixees.fr/informatiquelycee/n_site/img/nsi_prem_res_bitAlt_3.png)

A ne reçoit pas d'accusé de réception avec le drapeau à 1, il renvoie donc la trame 1 avec le drapeau 0. 

B reçoit donc cette trame avec un drapeau à 0 alors qu'il attend une trame avec un drapeau à 1 (puisqu'il a envoyé un accusé de réception avec un drapeau 1), il "en déduit" que l'accusé de réception précédent n'est pas arrivé à destination : il ne tient pas compte de la trame reçue et renvoie l'accusé de réception avec le drapeau à 1. 

Ensuite, le processus peut se poursuivre normalement.

## 4. Bilan

Dans certaines situations, le protocole de bit alterné ne permet pas de récupérer les trames perdues, c'est pour cela que ce protocole est aujourd'hui remplacé par des protocoles plus efficaces, mais aussi plus complexes.