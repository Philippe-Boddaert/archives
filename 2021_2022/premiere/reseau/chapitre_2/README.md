# Réseau - Rappel de Seconde

## 1. Définition

> On appelle __réseau__ (___network___)  un ensemble d'__équipements__ reliés entre eux pour échanger des informations.

### 1.1. Hôte

![](./assets/filius_1.png)

Un réseau est constitué d'hôtes, c'est à dire toutes les machines échangeant des données sur le réseau (Ordinateurs, Smartphone, Tablette, Imprimante, Console de Jeux vidéo...)

Parmi ces hôtes, il y a :

- Les __clients__ : par extension, ce sont les machines qui utilisent un logiciel, appelé client, qui envoie des requêtes l'autre type d'hôte, c'est à dire les serveurs.
- Les __serveurs__ : par extension, ce sont les machines qui utilisent un logiciel, appelé serveur, qui attend les demandes des clients et y répond, s'il le peut.

### 1.2. Connexion

Pour relier plusieurs machines entre elles, on utilise un `commutateur` , appelé `switch` en anglais.

Un `routeur` (router en anglais)  assure la circulation des informations entre les **différents** réseaux.

### 1.3. Type de réseau

On distingue différents types de réseaux:

- le réseau local  appelé `LAN` pour **L**ocal **A**rea Network : formé des machines réunies dans  une même pièce ou bâtiment,
- le réseau urbain `MAN` pour **M**etropolitan Local  **A**rea **N**etwork : réseau à l'échelle d'une ville ou d'une agglomération,
- le réseau étendu `WAN` pour **W**ide Local  **A**rea **N**etwork : réseau reliant plusieurs sites ou ordinateurs du monde entier.
- `Internet`, le réseau des réseaux : Il est constitué  d'une multitude de réseaux _locaux_ qui se sont petit à petit connectés entre eux pour former le plus vaste des réseaux.

![](./assets/filius_2.png)

## 2. Protocoles de communication

Pour communiquer, les hôtes d'un réseau utilisent des règles appelées `protocoles`. 

> Un __protocole__ est un ensemble de règles permettant aux ordinateurs de communiquer entre eux.

Ils existent de nombreux protocoles, chacun d’entre eux réalisant une tâche bien précise. 

![Le modèle TCP/IP](https://isn-icn-ljm.pagesperso-orange.fr/SNT/Internet/res/image-comm2.png)

Le modèle TCP/IP est décomposé en 4 couches de protocoles :

- la couche **Application** choisit le mode de transmission (http, https, ftp...),
- la couche **Transport** choisit le mode de transport. selon la fiabilité nécessaire (TCP, UDP) ,
- la couche **Internet** permet d'interconnecter les réseaux (IP),
- la couche **Physique** matérialise le transport des données (câbles, WIFI, bluetooth...)

### 2.1. Protocole IP

> Le protocole Internet Protocol (IP) est  responsable de l’__adressage__ et de la __fragmentation des paquets__ de données dans les réseaux numériques.

> Il assure que toute donnée comporte une information sur l'émetteur (source) et le récepteur (destination).

#### 2.1.1. Adresse IP

L'adresse `IP` est une adresse numérique permettant d'identifier les hôtes connectés à un réseau. 

Dans la version IPv4, l'adresse est constituée de 4 nombres compris entre 0 et 255, séparés par des points.

```mermaid
flowchart
	a["192.168.0.11"]
```

Dans un même réseau local, l'adresse IP de chaque hôte commencera toujours par les mêmes nombres.

#### 2.1.2. Masque de sous-réseau

Le masque de sous-réseau, associée à l'adresse IP, indique dans quel réseau l'hôte se trouve.

On écrit le masque de sous-réseau de la façon suivante :

|      |      |      |      |      |      |      |                             |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | --------------------------- |
| 192  | .    | 168  | .    | 0    | .    | 11   | ← Exemple d'adresse IP      |
| 255  | .    | 255  | .    | 255  | .    | 0    | ← __Masque de sous-réseau__ |

- Le 255 signifie que le nombre correspond à la partie réseau et ne peux pas être changé,
- Le 0 signifie que le nombre correspond à la partie hôte et peut être changé pour nommer chaque machine.

#### 2.1.3. Illustration

![](./assets/filius_3.png)

- Avec le masque __255.255.255__.0, __192.168.0__.10 et __192.168.0__.11 sont dans la même plage et peuvent communiquer entre eux,
- Avec le masque **255.255.255**.0, **192.168.0**.10 et **192.168.1**.20 ne sont pas dans la même plage et ne peuvent pas communiquer entre eux.
- Avec le masque **255.255**.0.0, **192.168**.0.1 et **192.168**.1.1 sont dans la même plage et peuvent communiquer entre eux.

__Pour que deux ordinateurs d'un même réseau local puissent communiquer, ils doivent être dans la même plage d'adresse IP.__

### 2.2. Protocole de Transport

> Le protocole __TCP__, ou *Transmission Control Protocol* est toujours associé à IP, sous la dénomination __TCP/IP__. 

> Il s'agit d'un membre essentiel d'Internet : il assure l'__échange de données__, en __garantissant l'arrivée à destination__ de paquets.

Il existe 2 protocoles de transport :

- __TCP__ : méthode fiable (Cf. [Plus d'infos](./assets/TCP.md))
- __UDP__ : méthode non fiable (Cf. [Plus d'infos](./assets/UDP.md))


## 3. Attendus de Première

| Contenus | Capacités attendues |
| -------- | ------------------- |
|  Interaction clientserveur.<br />Requêtes HTTP, réponses du serveur        | Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre.<br />Distinguer ce qui est mémorisé dans le client et retransmis au serveur.<br />Reconnaître quand et pourquoi la transmission est chiffrée.                    |
| Transmission de données dans un réseau<br />Protocoles de communication<br />Architecture d’un réseau | Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation.<br />Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné).<br />Simuler ou mettre en œuvre un réseau. |
