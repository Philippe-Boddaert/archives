# Bienvenue sur le serveur Web

## 1. Présentation

### 1.1. Méthodes du protocole HTTP

Comme vu dans la [précédente section](../section_1/), le __protocole HTTP__ est celui utilisé dans un modèle client / serveur pour obtenir une ressource sur le Web.

2 __méthodes__ du protocole HTTP sont les plus usitées et dont les mécanismes sont à connaitre :

- `GET` : qui permet d'obtenir une ressource
- `POST` : qui permet de transmettre (i.e soumettre, créer) une ressource

### 1.2. Serveur Web

Un **serveur web** est un logiciel qui répond à des requêtes du web en utilisant principalement le **protocole HTTP**.

Le serveur expose des ressources qu'il fournit lorsqu'une **requête** via un navigateur est effectuée.

Le serveur définit des **routes** auquel il associe un traitement qui renvoie une **réponse** sous la forme de page html.

## 2. Premier serveur Web

### 2.1. À Faire

1. Copier les instructions suivantes dans un script python `serveur.py` :

```python
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def acceuil():
  return "<html><head></head><body><h1>Bonjour le monde !!</h1></body></html>"

if __name__ == '__main__':
    app.run()
```

2. Exécuter le script
3. Ouvrir l'URL http://localhost:5000/ avec un navigateur

#### Question : Qu'obtenez-vous ?

### 2.2. Bilan

Ce qu'il faut noter :

- Grâce au décorateur `@app.route`, Flask nous permet d'associer l'url http://localhost:5000/ à la fonction `accueil`.
- Lorsque la requête HTTP est créée par le navigateur, la fonction `accueil` est appelée,
- Le corps de la réponse HTTP, envoyée au navigateur, prend la valeur de retour de la fonction.

## 3. Exercices

### À Faire 1 - Hello World personnalisé

1.  Remplacer le code de la fonction `accueil` par le suivant :
```python
@app.route('/message')
def acceuil():
  prenom = request.values.get("prenom")
  return "<html><head></head><body><h1>Bonjour {} !!</h1></body></html>".format(prenom)
```
2.  Ouvrir l'URL http://localhost:5000/message?prenom=Martin avec un navigateur.

#### Question : Qu'obtenez-vous ?

#### Bilan

- Il est possible de paramètrer une URL en suivant le format suivant : `http://mon.serveur.com/page.html?cle1=valeur1&cle2=valeur2&cle3=valeur3`,
- Le caractère `?` symbolise le passage de paramètres dans l'url,
  - Chaque paramètre est un couple de clé, valeur séparé par le caractère `=`,
  - Le caractère `&` sépare chaque paramètre de l'url.
- La bibliothèque `Flask` met à disposition les paramètres passés dans l'url via l'objet `request.values` et la méthode `get` qui prend le nom du paramètre et renvoie la valeur sous la forme d'une chaine de caractères.

### À Faire 2 - Un peu plus d'informations

1. Reprendre l'exercice 1
2.  Ajouter un paramètre `nom` et `annee` (qui correspond à l'année de naissance de l'utilisateur),
3.  Modifier le retour de la fonction `accueil` pour qu'à l'appel de l'url http://localhost:5000/message?prenom=Martin&nom=Dupont&annee=1986, le message `"Bonjour Martin Dupont, déjà 36 ans !!"` apparaisse dans la page Web.

N.B Pour la gestion des dates en Python, consulter https://www.w3schools.com/python/python_datetime.asp

### À Faire 3 - Notion de Formulaire HTML

Nous avons vu qu'il est possible de passer des paramètres à une URL. Indiquer une liste de paramètres avec leurs valeurs s'avèrent très fastidieux et source d'erreur.

Heureusement, il est possible de simplifier et de configurer ces paramètres grâce au `Formulaire HTML`.

#### Tâches à réaliser

1.  Récupérer le fichier [compte.html](./assets/compte.html)
2.  Ouvrir ce fichier avec un navigateur Web
3.  Renseigner les champs
4.  Cliquer sur le bouton `Envoyer`

#### Question : Qu'obtenez-vous ?

#### Bilan

- Un formulaire est un élément HTML (`<form></form>`) contenant des champs d'entrées (zone de saisie),
- Un champ d'entrée est un élément HTML (`<input name="" type="text">`) où l'attribut name correspond au nom du paramètre dans l'URL,
- Un champ d'entrée de type `submit` est un bouton qui, sur un clic, soumet le formulaire,
- Lorsque le formulaire est soumis, une requête HTTP est envoyée avec l'ensemble des paramètres correspondant aux champs d'entrées,
- L'URL appelée est celle configurée dans l'attribut `action` du formulaire

### À Faire 4 - Compléments sur les formulaires HTML

1. Ajouter ce bloc d'instructions HTML dans l'élément `<form>` de la page `compte.html`

```html
<label for="mdp">Votre mot de passe : </label>
<input type="password" name="mdp" id="mdp" /><br><br>
<p>Aimez-vous le chocolat ?</p>
<input type="radio" name="choixChocolat" value="1" id="choix1">
<label for="choix1">Non</label>
<input type="radio" name="choixChocolat" value="2" id="choix2">
<label for="choix2">Oui</label><br><br>
<label for="moyen_transport">Choisissez votre moyen de transport pour venir au lycée :</label>
<select name="transport" id="moyen_transport">
  <option value="bus">Bus</option>
  <option value="voiture">Voiture</option>
  <option value="velo">Vélo</option>
  <option value="autre">Autre</option>
</select>
```

2. Rafraichir la page dans le navigateur
3. Saisir des valeurs pour les nouveaux champs d'entrée
4. Modifier le script `serveur.py` pour récupérer les valeurs de ces champs et personnaliser le message d'accueil selon les règles suivantes :

```txt
Si l'utilisateur aimme le chocolat alors
 S'il utilise le Vélo alors
 	Afficher "Votre péché mignon n'a aucune incidence !"
 Sinon
  Afficher "Aïe Aîe Aïe, il va falloir faire du sport"
 Finsi
Sinon
 S'il utilise la voiture ou le bus alors
 	Afficher "C'est bien, un peu d'exercice serait encore meilleur"
 Sinon s'il utilise le vélo alors
  Afficher "On ne peut pas faire mieux"
 Sinon
  Afficher "J'espère que c'est un moyen de transport écologique ?"
 FinSi
FinSi
```

#### Bilan

- Un formulaire HTML permet de définir différents champs d'entrée
- La syntaxe des différents types de champs sont :

| Champ d'entrée | exemple code HTML |
| :--------------: | :--------- |
| Texte | `<input type="text" name="prenom"/>` |
| Mot de passe | `<input type="password" name="prenom"/>` |
| Bouton radio | `<input type="radio" name="prenom"/>` |
| Case à cocher | `<input type="checkbox" name="prenom"/>` |
| Liste déroulante | `<select name="couleur">`<br/>  ` <option value="rouge">Rouge</option>`<br />  `<option value="bleu">Bleu</option>`<br/>`</select>` |

- La valeur d'un champ `password` est affichée à l'utilisateur par une suite de `*` cependant, cette valeur est donnée en clair dans la requête HTTP. 
- Une personne interceptant les requêtes HTTP a accès à cette valeur. Il n'y a aucun dispositif de sécurité et confidentialité des données avec le protocole HTTP et la méthode `GET`.

### À Faire 5 - La méthode POST

La méthode `GET` comme son nom l'indique permet d'__obtenir__ une ressource localisée sur un serveur web avec des paramètres de personnalisation.

Cependant, soumettre un formulaire consiste souvent à créer une ressource. Utiliser la méthode GET n'est pas recommandée. La méthode `POST` a été conçue à cet effet.

#### Tâches à réaliser

1.  Dans le fichier `compte.html` modifier l'attribut `method` du formulaire HTML avec la valeur POST
2.  Dans le script `serveur.py` changer le décorateur `@app.route('/message')` par `@app.route('/message', methods = ['POST'])`
3.  Relancer le serveur et rafraichir la page compte.html
4.  Saisir des valeurs et cliquer sur envoyer
5.  Dans l'onglet `Réseau`de l'outil de développement du navigateur, que constatez-vous ?
6.  Appeler l'URL suivante : http://localhost:5000/message?prenom=Martin&nom=Dupont&annee=1986. Que constatez-vous ?

### Bilan

- La méthode `POST` du protocole HTTP n'implique pas un passage de paramètres dans l'URL,
- Les valeurs des champs sont en claires, même avec la méthode POST.
- Cette méthode n'assure donc aucune confidentialité des données,
- Le protocole https est responsable du chiffrement et de la sécurisation des échanges entre le client et le serveur.

### 3. Synthèse

Ce qu'il faut savoir concernant les méthodes `GET`et `POST` du protocole HTTP :

|      | GET  | POST |
| :---- | :---- | :---- |
|  __Utilisation__ | Obtenir une ressource répondant à des critères | Soumettre des données (remplir un formulaire) |
|  __Visibilité__   | Visible pour l’utilisateur dans l'URL | Invisible pour l’utilisateur |
| __Longueur des données__ | Limitée - longueur maximale de l’URL à 2 048 caractères. | Illimitée. |
| __Sécurité__* | Les données sont __visibles__, accessibles en clair dans l'URL | Les données sont __invisibles dans l'URL__ mais __visibles__, accessibles en clair __dans le corps__ de la requête. |

\* : Seul un protocole comme HTTPS garantit un chiffrement et une sécurisation dans la communication des données. La méthode POST ne garantit pas un chiffrement des données.
