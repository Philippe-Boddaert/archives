# Interface Pokédex

## 1. Contexte

Lors d'une précédente activité, nous avions manipulé une base de données de Pokemon.

![](https://upload.wikimedia.org/wikipedia/commons/9/98/International_Pok%C3%A9mon_logo.svg)

Les données des Pokemon étaient stockées dans un fichier CSV et grâce aux méthodes `sort` et `filter` de Python, il était possible de trier et filtrer ces données pour répondre à certaines questions :

1. Qui a la meilleure défense ?
2. Combien de Pokémons ont une attaque supérieure à 70 ?
3. Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ?
4. ...

## 2. Attendu

L'objectif de cette activité est de fournir une interface Web qui permette d'interroger la base de données des Pokemon de manière plus intuitive que l'interface console de Python.

Une page [index.html](./assets/index.html) contient un formulaire avec :

- un champ permettant de saisir le nom du Pokemon
- un bouton "Rechercher".

Sur un clic sur le bouton, le formulaire est soumis au serveur web (route : `/recherche`)

Le [serveur web](./assets/serveur.py) renvoie la liste des Pokemon avec leurs caractéristiques sous forme d'un tableau, dont le nom contient celui saisi par l'utilisateur.

## 3. Matériel fourni

Afin d'initier l'activité, diverses ressources sont fournies :

- [Base de données](./assets/pokemon_stats_1.csv) : Fichier csv contenant les données des Pokemon
- [Serveur Web](./assets/serveur.py) : Structure intiale du serveur Web
- [Page de recherche](./assets/index.html) : Page de recherche du Pokédex

## 4. À Faire

### Version 1 - Recherche par le nom

__Objectif__ : Fournir des résultats de recherche, i.e un tableau de Pokemon

1. Écrire la fonction `convertir`, qui prend un Pokemon sous la forme d'un dictionnaire, et renvoie un Pokemon sous la forme d'une ligne d'un tableau HTML.

```python
def convertir(pokemon):
  '''
  Converti les données d'un Pokemon sous la forme d'une ligne d'un tableau HTML
  :param pokemon: (dict) Un Pokemon
  :resultat: (str) Les données d'un Pokemon sous la forme d'une ligne d'un tableau HTML
  :Exemple:
  	>>> pokemon = {"name" : "Metapod", "classification":"Coccon Pokémon", "attack" : 20, "defense" : 55}
		>>> convertir(pokemon)
		"<tr><td>Metapod</td><td>Coccon Pokémon</td><td>20</td><td>55</td></tr>"
	'''
	pass
```

2. Modifier la fonction `recherche` du serveur pour qu'elle renvoie un tableau HTML (i.e `<table>`) contenant les Pokemon dont les noms contiennent celui du paramètre `nom`de la requête.

### Version 2 - Recherche par l'attaque

__Objectif__ : Ajouter la gestion d'un critère de recherche sur l'attaque du Pokemon.

Le résultat de recherche contient les Pokemon dont le niveau d'attaque est supérieur ou égal à la valeur du critère saisie par l'utilisateur

### Version 3 - Recherche par la défense

__Objectif__ : Ajouter la gestion d'un critère de recherche sur la défense du Pokemon.

Le résultat de recherche contient les Pokemon dont le niveau de défense est supérieur ou égal à la valeur du critère saisie par l'utilisateur

### Version 4 - Tri des résultats

__Objectif__ : Ajouter la gestion d'un critère de tri, qui indique si les résultats doivent être triés par ordre croissant ou décroissant du nom

Dans le formulaire de la page index.html, le critère peut être implémenté par un champ de type `radio`.

### Version 5 - Tri avancé des résultats

__Objectif__ : Enrichir la gestion de tri en spécifiant sur quel critère le tri doit être effectué : nom, classification, attaque ou défense.

Dans le formulaire de la page index.html, le critère peut être implémenté par un champ de type `select`.

### Version 6 - Ajout d'un Pokemon

__Objectif__ : Enrichir la base de données des Pokemon avec un pokemon dont les caractéristiques sont saisies par l'utilisateur à travers un formulaire d'ajout.

Les caractéristiques à saisir sont : nom, classe (matérialisée par une liste déroulante), attaque et défense.

L'effet attendu est que le fichier CSV soit enrichi avec le nouveau Pokémon.

N.B : La méthode à employer dans ce cas est `POST`.
