from flask import Flask, request

app = Flask(__name__)

@app.route('/recherche')
def recherche():
    nom = request.values.get("nom")
    return "<html><head></head><body><h1>Pokemon dont le nom contient : {} !!</h1></body></html>".format(nom)

if __name__ == '__main__':
    app.run()
