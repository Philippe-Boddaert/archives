# Dialogue client-serveur sur le Web

## 1. Contexte

Sur Internet, on distingue deux catégories d’ordinateurs :

- les **clients** : les ordinateurs des internautes comme vous.
- les **serveurs** (Web): ce sont des ordinateurs puissants qui stockent (on dit aussi *hébergent*) et délivrent des sites Web aux internautes, c’est-à-dire aux clients.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Modèle-client-serveur.svg/476px-Modèle-client-serveur.svg.png)

Le World Wide Web utilise le **modèle client-serveur**. Il s’agit d’un mode d’échange de données dans lequel :

- les *clients* envoie des *requêtes*
- le *serveur* attend les requêtes des clients et y répond

Dans le cas du Web, c’est le __protocole HTTP__ qui permet cet échange entre client et serveur, ou sa version sécurisée HTTPS.

## 1. Définition

Le protocole **HTTP** (*HyperText Transfer Protocol*, soit “protocole de transfert hypertexte”) est un protocole de type client-serveur qui définit les messages envoyés entre le navigateur (client) et le serveur Web (serveur).

Le protocole HTTPS est la combinaison du HTTP avec une couche de chiffrement comme SSL ou TLS. Le mécanisme est le même que le protocole HTTP. L'aspect chiffrement fera l'objet d'un autre cours.

Dans ce cours, on se limitera à l'étude du protocole HTTP.

En seconde, vous avez vu le protocole TCP/IP, qui a en charge de router les paquets dans un réseau et d'en assurer le bon acheminement 

Le protocole HTTP est un protocole de plus haut niveau (Couche Application du modèle TCP/IP)

![Le modèle TCP/IP](https://isn-icn-ljm.pagesperso-orange.fr/SNT/Internet/res/image-comm2.png)

## 2. Contexte

Le but du protocole HTTP est de permettre un transfert de fichiers (essentiellement au format HTML) localisés grâce à une chaîne de caractères appelée URL entre un navigateur (le client) et un serveur Web.

![](https://img-19.ccm2.net/q1_lqTcNUtYc3W0d3SYPwb-Lrk8=/377x/bfbd92d7cd9443e38170cde13721391b/ccm-encyclopedia/internet-images-comm.gif)

_Source : commentcamarche.net_

- Le navigateur effectue une __requête HTTP__
- Le serveur traite la requête puis envoie une __réponse HTTP__

## 3. Déroulé d'une interaction client-serveur

### 3.1. À Faire

1. Ouvrir un navigateur web
2. Ouvrir l'outil de développement (sous Chrome : Plus d'outils > Outils de développement )
3. Afficher l'onglet `Réseau`
4. Copier `https://philippe-boddaert.gitlab.io/citations/index.html` dans le champ url du navigateur et appuyer sur `Entrée`

__Une multitude d'informations apparaissent que nous allons décrire et analyser.__

## 4. Description

### 4.1. Requête

Une requête HTTP est un ensemble de lignes envoyé au serveur par le navigateur. Elle comprend :

- __Une ligne de requête__ : c'est une ligne précisant le type de document demandé, la méthode qui doit être appliquée, et la version du protocole utilisée. La ligne comprend trois éléments devant être séparés par un espace :
	- La méthode
	- L'URL
	- La version du protocole utilisé par le client (généralement HTTP/1.0)
- __Les champs d'en-tête de la requête__ : il s'agit d'un ensemble de lignes facultatives permettant de donner des informations supplémentaires sur la requête et/ou le client (Navigateur, système d'exploitation, ...). Chacune de ces lignes est composée d'un nom qualifiant le type d'en-tête, suivi de deux points (:) et de la valeur de l'en-tête
- __Le corps de la requête__ : c'est un ensemble de lignes optionnelles devant être séparées des lignes précédentes par une ligne vide et permettant par exemple un envoi de données par une commande POST lors de l'envoi de données au serveur par un formulaire

_Exemple de requête HTTP_ :

```http
GET  https://philippe-boddaert.gitlab.io/citations/index.html HTTP/1.0    
Accept : text/html    
User-Agent : Mozilla/4.0 (compatible; MSIE 5.0; Windows 95)
```

### 4.2. Réponse

Une réponse HTTP est un ensemble de lignes envoyées au navigateur par le serveur. Elle comprend :

- __Une ligne de statut__ : c'est une ligne précisant la version du protocole utilisé et l'état du traitement de la requête à l'aide d'un code et d'un texte explicatif. La ligne comprend trois éléments devant être séparés par un espace :
	- La version du protocole utilisé
	- Le code de statut
	- La signification du code
- __Les champs d'en-tête de la réponse__ : il s'agit d'un ensemble de lignes facultatives permettant de donner des informations supplémentaires sur la réponse et/ou le serveur. Chacune de ces lignes est composée d'un nom qualifiant le type d'en-tête, suivi de deux points (:) et de la valeur de l'en-tête
- __Le corps de la réponse__ : il contient le document demandé

Les navigateurs modernes permettent de visualiser les requêtes et réponses HTTP qu'il génère.

_Exemple de réponse HTTP_ :

```http
HTTP/1.0 200 OK    
Date : Tue, 01 Mar 2022 08:25:03 GMT  
Content-Type : text/HTML    
Content-Length : 344    
Last-Modified : Sun, 16 Jan 2022 13:52:06 GMT
```

### 4.3. À Faire

1. Ouvrir l'inspecteur Web de votre navigateur internet, et sélectionner l'onglet `Réseau`.
2. Saisir l'url de cette page `https://philippe-boddaert.gitlab.io/citations/inconnu.html`
3. Identifier les éléments de la requête et réponse HTTP et classer les selon la définition ci-dessus
4. Quelles différences constatez-vous dans la réponse HTTP avec celle du 3.1 ?

Les requêtes et réponses HTTP sont générées et interprétées par le navigateur mais il est tout à fait possible de générer celles-ci en dehors d'un navigateur...notamment en python !!

## 5. Compléments

### 5.1. Méthodes de Requête

Le protocole HTTP est un protocole de haut niveau et permet d'intéragir avec des ressources en spécifiant au serveur l'action que l'on souhaite réaliser.

Pour une même URL, Il est possible de spécifier la `méthode`, i.e l'action à réaliser : 

| Méthode | Description |
| :--: | :--: |
| GET | Requête de récupération de la ressource |
| POST | Requête d'envoi de données |
| PUT | Requête d'envoi de données de mise à jour |
| DELETE | Suppression de la ressource |

### 5.2. Code de réponses

Le serveur WEB indique par le code de réponse l'état du traitement de la ressource.
Le code de réponse est un nombre à 3 chiffres : le premier indiquant la classe de statut et les suivants la nature du statut.

| Code | Ètat de traitement de la ressource | Description |
| :--: | :--: | :-- |
| 10x | Message d'information | Non utilisé dans la version 1.0 du protocole HTTP |
| 20x | Réussite | Ces codes indiquent le bon déroulement de la requête |
| 200	| OK	| La requête a été accomplie correctement |
| 201	| CREATED	| Elle suit une commande POST, elle indique la réussite, le corps du reste du document est sensé indiquer l'URL à laquelle le document nouvellement créé devrait se trouver. |
| 202	| ACCEPTED | La requête a été acceptée, mais la procédure qui suit n'a pas été accomplie |
| 203	| PARTIAL INFORMATION	| Lorsque ce code est reçu en réponse à une commande GET, cela indique que la réponse n'est pas complète. |
| 204	| NO RESPONSE	| Le serveur a reçu la requête mais il n'y a pas d'information à renvoyer |
| 205	| RESET CONTENT	| Le serveur indique au navigateur de supprimer le contenu des champs d'un formulaire |
| 30x	| Redirection	| Ces codes indiquent que la ressource n'est plus à l'emplacement indiqué |
| 301	| MOVED	| Les données demandées ont été transférées à une nouvelle adresse |
| 302	| FOUND	| Les données demandées sont à une nouvelle URL, mais ont cependant peut-être été déplacées depuis... |
| 303	| METHOD | Cela implique que le client doit essayer une nouvelle adresse, en essayant de préférence une autre méthode que GET |
| 40x	| Erreur due au client |	Ces codes indiquent que la requête est incorrecte |
| 400	| BAD REQUEST	La syntaxe de la requête est mal formulée ou est impossible à satisfaire |
| 401	| UNAUTHORIZED	| Le paramètre du message donne les spécifications des formes d'autorisation acceptables. Le client doit reformuler sa requête avec les bonnes données d'autorisation |
| 403	| FORBIDDEN	| L'accès à la ressource est tout simplement interdit |
| 404	| NOT FOUND	| Le serveur n'a rien trouvé à l'adresse spécifiée. |
| 50x	| Erreur due au serveur	| Ces codes indiquent qu'il y a eu une erreur interne du serveur |
| 500	| INTERNAL ERROR	| Le serveur a rencontré une condition inattendue qui l'a empêché de donner suite à la demande |
| 501	| NOT IMPLEMENTED	| Le serveur ne supporte pas le service demandé |
| 503	| SERVICE UNAVAILABLE	| Le serveur ne peut pas vous répondre à l'instant présent, car le trafic est trop dense |
| 504	| GATEWAY TIMEOUT	| La réponse du serveur a été trop longue vis-à-vis du temps pendant lequel la passerelle était préparée à l'attendre |

## 6. Ressources

[1] __RFC 2616 - spécification du protocole HTTP/1.1__, _Internet Engineering Task Force_, [https://www.ietf.org/rfc/rfc2616.txt](https://www.ietf.org/rfc/rfc2616.txt)

[2] __man page de Curl__, _Daniel Stenberg_, [https://curl.se/docs/manpage.html](https://curl.se/docs/manpage.html)