---
title : Historique
author : M. BODDAERT, d'après les travaux de David Landry, Germain Becker et Christophe Mieszczak
license : CC-BY-NC-SA
---
# Historique

## 1. Contexte

### 1.1. Définition

> __Machine (_Selon le Larousse_)__ : Appareil ou ensemble d'appareils __capable d'effectuer un certain travail__ ou de remplir une certaine fonction, soit sous la conduite d'un opérateur, soit d'une manière autonome.
>
> __Calculer (_Selon le CNRTL_)__ : __Évaluer d'avance__ et aussi exactement que possible les conditions probables ou nécessaires pour qu'une chose se produise ou produise ses effets

### 1.2. Problématique

__Quelles ont été les motivations et évolutions des machines ? __

__Quelles ères les machines ont-elles tranversées pour aboutir à l'ordinateur d'aujourd'hui ?__

Cette séance consiste en une revue historique des machines et ordinateurs.

## 2. Ère du Big Bang : Un monde sans machine

Le premier procédé opératoire connu est le calcul sur les __dix doigts de la main__, probablement à l’origine de l’utilisation du système décimal.

![](./assets/main.jpg)

Seulement, ce système ne permet pas de manipuler de grands nombres. __Il est difficile de stocker et manipuler des grands nombres avec 2 mains.__

L’homme a donc développé parallèlement :

- le __calcul écrit__, résultat de l’invention des chiffres, du système de numération de position et de la pratique de l’écriture.
- un __système de comptabilité__ par manipulation d’objets (cailloux, du latin calculus, calculi, jetons, etc.)

## 3. Ère des premiers outils : les abaques

> Un __abaque__ est, selon Wikipédia, "le nom donné à tout instrument mécanique plan facilitant le calcul".

Des supports multiples et variés sont apparus à partir du IVème siècle avant J-C.

### 3.1. Les tables de calcul

Les tables de calcul furent développées probablement en Mésopotamie et n'étaient à l'origine que des lignes tracées dans le sable. On pouvait utiliser les colonnes ainsi formées pour donner différentes valeurs aux cailloux selon leur position. Les supports physiques de ces tables se diversifièrent: de la pierre à la terre cuite au bois ou au marbre. 

![img](https://www.physique.usherbrooke.ca/~afaribau/essai/salamineTable.gif)

Le plus ancien (voir photo ci-dessus) date du VIème siècle avant J.C : il est en marbre et a été découvert en Grèce dans l'île de Salamine.

### 3.2. Les bouliers

✏ __À Faire__ : Regarder la vidéo suivante jusqu'à 2'02 pour comprendre le principe de fonctionnement d'un boulier.

[![Les bouliers](https://img.youtube.com/vi/GnMgHsos7cY/0.jpg)](https://youtu.be/GnMgHsos7cY?t=39)

### 3.3. Règles à calcul

En 1617, l'Anglais John Napier (1550- 1617), également connu sous le nom de Neper, met au point des bâtons mobiles qui permettent de réaliser rapidement des multiplications grâce à un codage astucieux des tables de Pythagore. 

![](./assets/batons.jpg)

Faciles à fabriquer et peu coûteux, les bâtons de Neper furent populaires dans toute l’Europe pendant plus de 200 ans ! 

### 3.4. Bilan

- Ces premiers outils, permettent de stocker, manipuler, effectuer des opérations.
- __Des actions manuelles sont nécessaires__ pour __procéder aux calculs__ et __interpréter__ le calcul et ses résultats. 

## 4. Ère de la machine mécanique

### 4.1. 1642, La Pascaline

![](./assets/Pascaline.jpeg)

Inventée par __Blaise Pascal__ en 1642, la Pascaline est ___capable___ d'effectuer addition et soustraction.

✏ __À Faire__ : Regarder la vidéo suivante pour comprendre le principe de fonctionnement de la Pascaline.

[![La Pascaline](https://img.youtube.com/vi/hSl2WFfCTD8/0.jpg)](https://youtu.be/hSl2WFfCTD8)

_Source : Chaine Youtube d'Yves Serra_

### 4.2. 1673, la machine de Leibniz

![](./assets/Leibniz.jpeg)

✏ __À Faire__ : Regarder la vidéo suivante pour voir le calcul d'une multiplication grâce à la machine de Leibniz.

[![La Pascaline](https://img.youtube.com/vi/_CpQGv3jdL8/0.jpg)](https://youtu.be/_CpQGv3jdL8)

_Source : Chaine Youtube d'Yves Serra_

### 4.3. Bilan

- Les machines mécaniques ont permis d'__automatiser__ la procédure de calcul,
- L'intervention humaine n'est requise que pour déterminer les entrées de la machine et interpréter la sortie,
- Une **machine** = Un __procédé de calcul figé__, i.e les machines ne peuvent réaliser que les opérations liées à leurs mécanismes, 
- La question d'une machine capable de __tout calculer__ (c'est-à-dire sur tout problème de calcul sur les nombres et autres) est posée par __Leibniz__.

## 5. Ère de la machine programmable

### 5.1. 1834, La machine analytique de Babbage

![](./assets/Charles_Babbage.jpeg)

__Charles Babbage__ conçoit les plans d’une __machine analytique__. Elle ne fut jamais réalisée de son vivant mais elle comportait une mémoire, une unité de calcul et une unité de contrôle, comme dans les ordinateurs modernes, ainsi que des périphériques de sortie (clavier et imprimante).

### 5.2. 1843, Premier programme

![](./assets/Ada_Lovelace.jpeg)

__Ada Lovelace__ compose les premiers programmes pour la __machine analytique__, elle a compris qu’une telle machine est universelle et peut exécuter n’importe quel __programme__ de calcul.

### 5.3. Bilan

- La machine analytique est la première machine programmable, i.e les opérations réalisables ne sont pas liées à son mécanisme.
- Elle ne fut jamais construite.

## 6. Ère de la machine universelle

### 6.1. 1936, Machine de Turing

Dans un article fondateur de 1936 "_On computable numbers, with an application to the entscheidungsproblem_", __Alan Turing__, définit précisément la notion de __calcul__ et la relie à l’__exécution d’un algorithme__ par une machine __imaginaire__ qui servira de modèle aux ordinateurs modernes.

![](./assets/Alan_Turing.jpeg)

ll faut garder à l'esprit que la __machine de Turing__ est un __modèle universel de calcul__ et qu'elle peut calculer tout ce que n'importe quel  ordinateur physique peut calculer (aussi puissant soit-il).  Inversement, ce qu'elle ne peut pas calculer ne peut l'être non plus  par un ordinateur. 

### 6.2. 1937, Thèse de Church-Turing

![](https://upload.wikimedia.org/wikipedia/en/a/a6/Alonzo_Church.jpg)

Les travaux de __Church et Turing__ montrent :

1. L'équivalence des __langages de programmation__ et les __modèles théoriques__ qu'ils ont inventé (lambda-calcul et Machine de Turing),
2. L'existence des __problèmes indécidables__ : Un problème de décision est dit __décidable__ s'il existe un algorithme, une procédure mécanique qui se termine en un nombre fini d'étapes, qui le décide, c'est-à-dire qui réponde par oui ou par non à la question posée par le problème.

Concrètement, les problèmes suivants sont __indécidables__ :

- décider si un programme est conforme à une spécification,
- décider si deux programmes sont égaux, i.e ont les mêmes comportements pour les mêmes entrées,
- corriger automatiquement des projets d'élèves,
- ... et bien d'autres !

### 6.3. Bilan

- En montrant l'indécidabilité de certains problèmes sur leurs modèles théoriques, Church et Turing ont montré que les problèmes indécidables ne peuvent être programmés sur un ordinateur, quelqu'il soit.

- Elle résume donc de manière saisissante le concept d'ordinateur et constitue un support idéal pour raisonner autour de la notion d'algorithme de calcul ou de démonstration.  En terminale, nous étudierons plus en détail le concept de __calculabilité__.

## 7. Ère de la machine électronique

### 7.1. 1940, Machines à programmes externes

La seconde guerre mondiale accélère la réalisation de machines à calculs pour calculer des trajectoires balistiques ou déchiffrer des codes secrets.

- En Allemagne, Konrad Zuse réalise en 1941, le __Z1__, première machine entièrement automatique lisant son programme sur une __carte perforée__. 
- Aux États-Unis, Howard Aiken conçoit le __Mark I__. Ces premières machines électromécaniques sont colossales, et occupent des pièces entières.
- En 1945, Mauchly et Eckert conçoivent avec l’__ENIAC__ une première machine utilisant des __tubes à vide__.

![](./assets/Eniac.jpeg)

### 7.2. 1945, l'ordinateur selon Von Neumann

Les premiers ordinateurs apparaissent aux États-Unis et en Angleterre, juste après-guerre, ils sont réalisés selon l’architecture décrite par __John Von Neumann__ dans son rapport sur la construction de l’__EDVAC__.

> Un __ordinateur__ est une machine programmable, capable d’exécuter tous les programmes calculables sur une machine de Turing et dont les programmes et les données sont enregistrés dans la __même mémoire__.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/185px-JohnvonNeumann-LosAlamos.jpg)

### 7.3. Bilan

- L'ordinateur d'aujourd'hui est bâti sur les principes éditées par Von Neumann,
- Les ordinateurs étaient volumineux, fragiles et réservés aux scientifiques.

## 8. Ère de la machine miniature et démocratisée

### 8.1. 1947,  Le transistor

Un transistor est un dispositif semi-conducteur à trois électrodes actives, qui permet de contrôler un courant ou une tension sur l'électrode de sortie grâce à une électrode d'entrée.

![](./assets/transistor.jpeg)

En d'autres termes, c'est un interrupteur contrôlé électroniquement, sans partie mécanique.

Il remplace progressivement les tubes à vide car plus tolérant aux pannes et plus petit.

### 8.2. 1958, Le circuit intégré

Le circuit intégré (CI), aussi appelé puce électronique, est un composant électronique, basé sur plusieurs composants de base (transistors).

À partir de la **fin des années 1960**, la densité de transistors par unité de surface des plaques de silicium constituant les circuits intégrés, double environ tous les 18 mois, selon la feuille de route des industriels établie en loi empirique sous le nom de **loi de Moore**, du nom du fondateur d’Intel. La miniaturisation accompagne la progression des capacités de calcul et la démocratisation des ordinateurs.

### 8.3. 1971, Le microprocesseur

![](./assets/Intel_C4004.jpeg)

L'apparition du **microprocesseur Intel 4004**, marque les **débuts de la micro-informatique**.

### 8.4. Bilan

- Les ordinateurs actuels sont basés sur le modèle de von Neumann, dans lequel la mémoire stocke à la fois les données et les programmes.
- Avec l’essor du réseau Internet et de ses applications comme le Web et l’explosion des télécommunications mobiles, les objets se transforment en ordinateurs : smartphones, **objets connectés**,...

## 9. Ère de l'ordinateur de demain ?

D'autres architectures apparaissent également, même si elles ne sont pas encore tout à fait au point, du moins pour le grand public :

- l'ordinateur quantique que les scientifiques étudient depuis les années 1990 et qui fait des progrès impressionnant depuis quelques années.
- moins connu, l'ordinateur ADN, un ordinateur non électronique actuellement exploré pour résoudre des problèmes combinatoires.

✏ __À Faire__ : Regarder la vidéo suivante jusqu'à 2'08 pour comprendre le principe de l'ordinateur quantique.

[![La Pascaline](https://img.youtube.com/vi/9VLB9AoqYxI/0.jpg)](https://youtu.be/9VLB9AoqYxI?t=16)

_Source : Chaine Youtube d'Antoine vs Science_
