---
title : Interface Humain-Machine
author: M. BODDAERT, d'après les travaux de Christophe Mieszczak
license : CC-BY-NC-SA
---
# Interface Humain-Machine

## 1. Attendus

| Contenus | Capacités attendues |
| :-- | :-- |
| Périphériques d’entrée et de sortie<br />Interface Homme-Machine (IHM)| Identifier le rôle des capteurs et actionneurs.<br />Réaliser par programmation une IHM répondant à un cahier des charges donné. |

## 2. Définition

### 2.1. Interface

> Une  __IHM (Interface Homme (Humain) - Machine)__ permet à un utilisateur d'interagir avec une _machine_, un système ou un appareil. 
>
> La __souris__ et  le __clavier__ sont des exemples d'IHM, les __écrans tactiles__, et les __manettes de jeux__ également et il en existe beaucoup d'autres !

### 2.2. Machine

Par __machine__, on entend bien évidemment ordinateur, mais pas seulement. En effet, les machines peuvent aussi être des __systèmes embarqués__ ou des __objets connectés__.

### 2.2.1. Système embarqué

> Un __système embarqué__ est un systèmes électronique et informatique __autonome__, souvent temps réel, spécialisé dans une tâche bien précise capable d’acquérir une donnée, la traiter et la communiquer. 

| Date | Description |
| :--: | :-- |
| 1967<br/>![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Dsky.jpg/330px-Dsky.jpg) | Premier système embarqué de guidage lors de la mission lunaire Apollo |
| 1984<br/>![](https://upload.wikimedia.org/wikipedia/commons/8/88/Cockpit_of_Airbus_A320-211_Air_France_%28F-GFKH%29.jpg) | Sortie de l’Airbus 320, premier avion équipé de commandes électriques informatisée |
|1998<br/>![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Ligne-14-Chatelet-1.jpg/320px-Ligne-14-Chatelet-1.jpg) | Mise en service du métro informatisé sans conducteur Météor (ligne 14 à Paris)  |
|2007<br/>![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/2010-365-34_12%25_Read_%284328619273%29.jpg/363px-2010-365-34_12%25_Read_%284328619273%29.jpg) | Arrivée du smartphone |

_Source : Wikipedia_

### 2.2.2 Objet connecté

> Un __objet connecté__ est un système informatique embarqué __disposant d’une connexion__ à un réseau local ou à L’Internet.

Remarque : On abuse souvent de l'appellation __objet connecté__ pour beaucoup d'objets qui n'en sont pas car ils ne sont que rarement branchés sur un réseau et n'inter-agisse que peu, ou pas du tout, avec lui.

Par exemple, la plupart des __montres connectées__ font plutôt parties des systèmes embarqués car elles ne nécessitent pas une connexion permanente au réseau. Seules celles qui doivent être connectées au smartphone pour pleinement fonctionner sont réellement connectées.

### 2.3. À Faire

Citez des exemples d'objets connectés et des systèmes embarqués.

## 3. Constituants d'une IHM

Les constituants essentiels des ces objet sont les __capteurs__ et le __actionneurs__.

### 3.1. Capteur

> Un __capteur__ permet d’acquérir une grandeur physique pour la transformer en grandeur « utilisable », exploitable par un système.
> Cette information logique ou analogique acquise sera portée par un signal analogique ou numérique.

| Information analogique | Information logique |
| :--: | :--: |
| ![](https://arduinogetstarted.com/images/tutorial/how-it-works-light-sensor.gif) | ![](https://i.pinimg.com/originals/9e/bb/47/9ebb47db63b7603d97f457c0ba5cd988.gif) |
| ![](https://eduno.fr/web/image/1592/1-1.png) | ![](https://eduno.fr/web/image/1593/33.png) |

- Exemples de capteurs :
  - clavier, souris,
  - écran tactile,
  - capteur de lumière,
  - accéléromètre,
  - thermomètre,
  - détecteur de mouvement,
  - ...

### 3.2. Actionneur

> Un actionneur permet de transformer l’énergie reçue en un phénomène physique (déplacement, dégagement de chaleur, émission de lumière …) pour répondre à un besoin donné.

Exemples d'actionneurs :

- haut-parleur,
- roues,
- bras motorisé,
- ...

### 3.3. Bilan

```mermaid
flowchart TD;
	subgraph Système
	A[Capteurs]-->|Envoie des données|B((Programme));
	B-->|Envoie des requêtes|C[Actionneurs];
	end
```

Le thermostat régulant la température d'une maison fonctionne suivant des programmes précis.

![](./assets/thermostat.jpeg)



Voici un algorithme, intégré au logiciel de gestion du processeur du thermostat, qui permet de gérer la température d'une pièce :

```
Tant que vrai :
	lire et stocker la température extérieure
	lire et stocker la température intérieure
	si la température intérieure est inférieure à 18 °C :
		mettre en chauffe les radiateurs
	sinon :
		mettre ou laisser les radiateurs en pause
```

## 4. Exercices

### Exercice 1

Voici une liste de différentes définitions. Chacune correspond à un des mots suivants : __actionneur, capteur, logiciel, mémoire, processeur__.

Associer à chaque définition suivante, le mot lui correspondant :

| Mot | Définition |
| :--: | :-- |
| | objet qui recueille des données provenant de ce que qui l'environne. |
| | objet qui stocke une information binaire composée de 0 et de 1. |
| | programme permettant de savoir comment traiter une information pour un objectif précis. |
| | objet qui traite des données numériques de manière automatique. |
| | objet qui en modifiant son état peut agir sur le monde physique qui l'evironne. |

### Exercice 2

Voici une liste d'objets, certains sont des capteurs, d'autres des actionneurs. 

Déterminer le type de chaque objet (capteur ou actionneur) :

| Objet | Type |
| :--: | :--: |
| Gyrophare | | 
| Sonde d’humidité |  |
| Détecteur de présence | |
| Télécommande | | 
| Bouton poussoir | | 
| Sirène | |  
| Moteur électrique | |
| Détecteur de lumière | |
