---
title : Utiliser la communication radio
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Utiliser la communication radio

## 1. Principe de base

Le module radio permet d'envoyer ou de recevoir des message vers ou depuis une ou plusieurs cartes microbit.

La communication se fait sans fil sur une fréquence entre 2,4GHz et 2,5Ghz selon le **canal** (numéroté entre 0 et 83) choisi.

Les messages ont une taille maxi de 251 caractères. Par défaut, la taille est fixée à 32 caractères.

Pour utiliser la radio, vous devez avoir 2 cartes microbit. La communication radio est très simple à mettre en oeuvre comme vous pourrez le voir. Attention cependant, les communications ne sont pas chiffrées, évitez de transmettre des informations sensibles ! !

## 2. Initialiser la communication

Commencer par importer le module radio

```
import radio
```

puis activez la radio qui par défaut est désactivée pour des raisons d'économie d'énergie

```
radio.on()
```

La transmission peut alors s'établir entre plusieurs cartes

## 3. Envoyer et recevoir

Pour envoyer un message sous forme d'une chaîne de caractères, utiliser

```
radio.send(message)
```

pour recevoir, utiliser

```
radio.receive()
```

Difficile de faire plus simple ! !

### 3.1. "Hello World"

Sur la carte émettrice, tapez le programme suivant :

```python
from microbit import *
import radio

radio.on()
while True:
    if button_a.was_pressed():
        radio.send("Hello World !!")
```

Sur la carte réceptrice, tapez le message suivant :

```python
from microbit import *
import radio

radio.on()
while True:
    incoming = radio.receive()
    if incoming:
        display.scroll(incoming)
```

Appuyez sur le bouton A de la carte émettrice et le message défile sur l'autre carte.

### 3.2. Envoyer un couple de 2 entiers

Imaginons que l'on souhaite faire un jeu de bataille navale en réseau, nous allons avoir besoin de transmettre un couple de coordonnées x et y par radio à l'autre carte. Cela se fait selon le même principe que celui décrit ci-dessus. On ajoute seulement une partie de code pour ecapsuler et désencapsuler les informations qui nous intéressent dans une chaîne de caractère.

Dans l'exemple qui suit, on fait bouger un pixel sur une carte en utilisant les boutons et on observe sur l'autre carte le pixel bouger de la même manière.

### 3.3. Bouger un pixel sur 2 écrans

Sur la carte émettrice :

```python
from microbit import *
import radio

x, y= 0, 0
radio.on()

def envoi():
    display.clear()
    display.set_pixel(x,y,9)
    message = "{},{}".format(x, y)
    radio.send(message)

envoi()

while True:
    if button_b.was_pressed():
        x = (x+1)%5
        envoi()
    if button_a.was_pressed():
        y = (y+1)%5
        envoi()
```

Sur la carte réceptrice :

```python
from microbit import *
import radio

radio.on()

while True:
    incoming = radio.receive()
    if incoming:
        try:
            target_x, target_y = incoming.split(',')
        except:
            continue
        x, y = int(target_x), int(target_y)
        display.clear()
        display.set_pixel(x, y, 9)
```

## 4. Configuration avancée de la radio

Tout ce que l'on vient de voir a l'avantage de marcher immédiatement mais si on fait cela dans une même salle de classe, gare aux interférences ! Il va falloir que chaque groupe d'élèves configure sa radio de manière unique pour éviter les problèmes.

### 4.1. Configuration de la fréquence

```
radio.config(channel=XX)
```

où XX est un numéro entre 0 et 83. Ce paramètre détermine la fréquence d'émission.

### 4.2. Configuration de l'adresse

```
radio.config(address=0x75626974)
```

L'adresse, codée sur 4 octet, permet de filtrer les messages qui nous sont destinés. Seuls ceux provenant d'une carte présentant la même adresse seront pris en compte. L'adresse donnée en exemple peut être modifiée, c'est l'adresse par défaut utilisée.

### 4.3. Configuration du groupe

Au sein d'une même adresse, 256 groupes numérotés de 0 à 255 peuvent cohabiter.

```
radio.config(group=7)
```

Seules les cartes possédant la même adresse et le même numéro de groupe communiqueront.

Comme vous le voyez, il est tout à fait possible de faire communiquer plusieurs groupes de personnes sans risquer d'interférences.

### 4.4. Autres paramètres configurables

- `radio.config(queue=3)` : nombre de messages dans la file d'attente. Au delà de 3 messages en attente, ils seront supprimés.
- `radio.config(length=32)` : longueur maximum du message. Celle-ci peut aller jusqu'à 251.
- `radio.config(power=6)` : puissance d'émission du signal (allant de 0 à 7)
- `radio.config(data_rate=radio.RATE_1MBIT)` : vitesse de transmission. Les vitesses admissibles sont **RATE_250KBIT**, **RATE_1MBIT** ou **RATE_2MBIT**

### 4.5. Réinitialiser les paramètres

radio.reset() permet de revenir aux réglages par défaut.