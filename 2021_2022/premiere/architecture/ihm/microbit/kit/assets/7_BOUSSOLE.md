---
title : Utiliser la boussole
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---
# Utiliser la boussole

## 1. Principe de base

La carte microbit est équipée d'un magnétomètre pouvant servir de boussole. Son utilisation comme pour le reste de ses capteurs est très simple : on calibre le compas

Par **compass.calibrate()** puis on interroge le compas par **compass.heading()**. 

Pour savoir si le compas est déjà calibré, utilisez la méthode **compass.is_calibrated()**.

## 2. Réalisation d'une boussole

```python
from microbit import *

if not compass.is_calibrated():
    compass.calibrate()

while True:
    needle = ((15 - compass.heading()) // 30) % 12
    display.show(Image.ALL_CLOCKS[needle])
```

## 3. Autres fonctions du magnétomètre

- La méthode **compass.get_field_strength()** renvoie la force du champ magnétique. Cela permet de détecter la présence d'un aimant.
- Cette force peut être décomposée selon les axes x, y et z : **compass.get_x()** **compass.get_y()** et **compass.get_z()**.
