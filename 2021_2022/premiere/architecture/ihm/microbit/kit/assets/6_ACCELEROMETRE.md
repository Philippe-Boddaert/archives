---
title : Utiliser l'accéléromètre
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Utiliser l'accéléromètre

## 1. Principe de base

La carte microbit est livrée avec un accéléromètre. Comme sur les smartphones, elle est capable de détecter son orientation ce qui peut donner naissance à des projets originaux.

La carte dispose de trois accéléromètres suivants les axes Ox, Oy et Oz.

![](./accelerometre.png)

Trois méthodes permettent de récupérer les valeur de ces accéléromètres en milli-g :

- `accelerometer.get_x()` : renvoie un nombre (entre -1000 et 1000 si la cartes est immobile) correspondant à l'inclinaison de l'axe des x , la valeur 0 correspond à un axe des x horizontal.
- `accelerometer.get_y()` : renvoie un nombre (entre -1000 et 1000 si la cartes est immobile) correspondant à l'inclinaison de l'axe des y , la valeur 0 correspond à un axe des y horizontal.
- `accelerometer.get_z()` : renvoie un nombre (entre -1000 et 1000 si la cartes est immobile) correspondant à l'inclinaison de l'axe des z , la valeur 0 correspond à un axe des z horizontal.

Observez le code ci-dessous :

```python
from microbit import *

while True:
    accX = accelerometer.get_x()
    if accX > 40:
        display.show(">")
    elif accX < -40:
        display.show("<")
    else:
        display.show("-")
```

## 2. Détecter des gestes

L'accéléromètre sait aussi interpréter les données d'accélération en gestes prédéfinis. Observez le code ci-dessous :

```python
from microbit import *

while True:
    gesture = accelerometer.current_gesture()
    if gesture == "face up":
        display.show(Image.HAPPY)
    else:
        display.show(Image.ANGRY)
```

## 3. Gestes prédéfinis

Les gestes reconnus sont : `up, down, left, right, face up, face down, freefall, 3g, 6g, 8g, shake`

Comme pour les boutons :

- les gestes sont accumulés dans une pile que l'on peut interroger par `accelerometer.get_gestures()`
- on peut détecter si un geste (par exemple une secousse) a eu lieu par `accelerometer.was_gesture("shake")`
- on peut détecter si un geste est en cours par `accelerometer.is_gesture("up")`