---
title : Utiliser le capteur de température
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Utiliser le capteur de température

## 1. Principe de base

La carte micro:bit intègre un capteur de température. Il n'est pas très précis dans la mesure ou il est soudé sur la carte elle-même, à proximité du processeur ; il est donc perturbé par l'échauffement provoqué par le fonctionnement de la carte. Le phénomène est encore amplifié lorsque la carte est insérée dans un étui de protection.

## 2. Lire la température

Pour lire la température - en degrés Celsius - il suffit d'appeler la fonction temperature() :

```python
from microbit import *

while True:
  display.scroll(temperature())
  sleep(5000)
```

