---
title : Gérer le son
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Gérer le son

L'ajout de bruitages rend souvent les applications plus attractives - sauf peut-être en salle de classe ! En connectant un haut-parleur entre les broches 0 et GND de la carte microbit, il est possible de jouer des sons.

![](./pin0-gnd.png)

## 1. Jouer une musique préenregistrée

La carte intègre un certain nombre de musiques réenregistrées pour différentes ambiances. Essayez le code suivant :

```python
import music

music.play(music.BIRTHDAY)
```

Difficile de f aire plus simple ! voici la liste des musiques disponibles. Pour les retrouver, faites `dir(music)`

```
'DADADADUM', 'ENTERTAINER', 'PRELUDE', 'ODE', 'NYAN', 'RINGTONE', 'FUNK', 'BLUES', 'BIRTHDAY', 'WEDDING', 'FUNERAL', 'PUNCHLINE', 'PYTHON', 'BADDY', 'CHASE', 'BA_DING', 'WAWAWAWAA', 'JUMP_UP', 'JUMP_DOWN', 'POWER_UP', 'POWER_DOWN'
```

## 2. Jouer une musique personalisée

Vous pouvez composer votre propre musique comme vous pouvez le voir sur l'exemple suivant :

```python
import music

tune = ["C4:4", "D4:4", "E4:4", "C4:4", "C4:4", "D4:4", "E4:4", "C4:4",
        "E4:4", "F4:4", "G4:8", "E4:4", "F4:4", "G4:8"]
music.play(tune)
```

Le format de chaque note est le nom de la note ("A" pour La, "B" pour Si etc...) suivi du numéro de l'octave suivi de " :" et enfin la longueur de la note.

## 3. Effets sonores

On peut enfin utiliser **music.pitch()** pour spécifier une fréquence et une durée précise. L'exemple ci-dessous reproduit une sirène de police et illustre cette méthode.

```python
import music

while True:
    for freq in range(880, 1760, 16):
        music.pitch(freq, 6)
    for freq in range(1760, 880, -16):
        music.pitch(freq, 6)
```