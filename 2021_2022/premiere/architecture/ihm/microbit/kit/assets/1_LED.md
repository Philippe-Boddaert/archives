---
title : Manipuler la matrice de LED
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Manipuler la matrice de LED

Chaque LED de la carte micro:bit dispose d'une coordonée $`(x,y )`$, dont les valeurs sont les suivantes :

![](https://upload.wikimedia.org/wikipedia/commons/c/cb/Micro_bit_position_des_DEL.png)

## 1. Afficher des messages

### display.show() et display scrool()

Affiche un texte ou un nombre sur l'écran. 

La méthode **show()** montre caractère par caractère alors que la méthode **scroll()** fait défiler plus progressivement.

Testez dans les différentes commandes afin de voir le rôle des paramètres optionnels *wait* et *loop*

1. `display.show(23)`
2. `display.scroll('Hello World!', loop=True)`
3. `display.show('Hello World!', wait=False, loop=True)`

## 2. Afficher / Lire des pixels

### display.get_pixel() et display.set_pixel()

`display.get_pixel(x, y)` renvoie l'illumination du pixel situé à la colonne $`x`$ et la ligne $`y`$ sous forme d'un entier de 0 (éteint) à 9 (complètement allumé)

`display.set_pixel(x, y, value)` allume le pixel situé à la colonne $`x`$ et la ligne $`y`$ avec une illumination *value* de 0 (éteint) à 9 (complètement allumé)

```python
from microbit import *

for x in range(5):
    for y in range(5):
        display.set_pixel(x,y,(x + y)%9)
```

## 3. Afficher des images

### Images prédéfinies

Tester `display.show (Image.HAPPY)`

Il y a d'autres images prédéfinies dans MicroPython sur microbit. On peut les obtenir par la commande `dir(Image)` :

```python
'HEART', 'HEART_SMALL', 'HAPPY', 'SMILE', 'SAD', 'CONFUSED', 'ANGRY', 'ASLEEP', 'SURPRISED', 'SILLY', 'FABULOUS', 'MEH', 'YES', 'NO', 'CLOCK12', 'CLOCK1', 'CLOCK2', 'CLOCK3', 'CLOCK4', 'CLOCK5', 'CLOCK6', 'CLOCK7', 'CLOCK8', 'CLOCK9', 'CLOCK10', 'CLOCK11', 'ARROW_N', 'ARROW_NE', 'ARROW_E', 'ARROW_SE', 'ARROW_S', 'ARROW_SW', 'ARROW_W', 'ARROW_NW', 'TRIANGLE', 'TRIANGLE_LEFT', 'CHESSBOARD', 'DIAMOND', 'DIAMOND_SMALL', 'SQUARE', 'SQUARE_SMALL', 'RABBIT', 'COW', 'MUSIC_CROTCHET', 'MUSIC_QUAVER', 'MUSIC_QUAVERS', 'PITCHFORK', 'XMAS', 'PACMAN', 'TARGET', 'ALL_CLOCKS', 'ALL_ARROWS', 'TSHIRT', 'ROLLERSKATE', 'DUCK', 'HOUSE', 'TORTOISE', 'BUTTERFLY', 'STICKFIGURE', 'GHOST', 'SWORD', 'GIRAFFE', 'SKULL', 'UMBRELLA', 'SNAKE'
```

### Créer sa propre image

Testez l'exemple suivant :

```
monImage = Image("90009:06060:00300:06060:90009")
display.show(monImage)
```

On peut aussi présenter différemment :

```
monImage = Image("90009:"
"06060:"
"00300:"
"06060:"
"06060:")
display.show(monImage)
```

Chaque chiffre représente la valeur du pixel.