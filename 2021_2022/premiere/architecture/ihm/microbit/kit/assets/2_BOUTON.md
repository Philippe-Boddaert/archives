---
title : Utiliser ls boutons
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---

# Utiliser les boutons

### 1. Principe de base

Il y a deux boutons intégrés sur la carte microbit accessibles via les objets **button_a** et **button_b**. 

Ces objets proposent des méthodes très pratiques facilitant grandement l'écriture des programmes qui ont besoin d'interaction avec l'utilisateur.

Chaque pression sur le bouton est détectée par la carte et mémorisée. Il est donc impossible de rater l'événement, même si on interroge pas le bouton au moment précis ou celui-ci est pressé. Pour bien comprendre ce mécanisme, voici une expérience interactive à mener :

```
print(button_a.get_presses())
```

renvoie 0, si le bouton A n'a pas été pressé.

A présent, appuyez plusieurs fois sur le bouton A puis relancez la commande :

```
print(button_a.get_presses())
```

Vous voyez que le nombre de pressions est à présent affiché. Maintenant que le bouton a été interrogé, retapez

```
print(button_a.get_presses())
```

le compteur est remis à 0 à chaque invocation de la commande. Il convient donc de stocker cette information dans une variable pour son usage ultérieur si besoin.

## 2. Méthode is_pressed()

Testez le programme suivant :

```python
from microbit import *

actif = True

while actif:
    if button_a.is_pressed():
        display.show(Image.HAPPY)
    elif button_b.is_pressed():
        actif = False
    else:
        display.show(Image.SAD)

display.clear()
```

Vous voyez ici un exemple d'utilisation de **is_pressed()** qui renvoie **True** si le bouton est présentement pressé au moment ou la méthode est invoquée.

L'exemple montre aussi l'utilisation d'une boucle infinie **while actif** et l'utilisation du bouton B pour mettre fin au programme, ce qui peut souvent s'avérer utile.

### 3. Méthode was_pressed()

Pour finir, il est fréquent d'avoir recours à la méthode **was_pressed()** pour savoir si un bouton a été actionné pendant que le programme était occupé à une autre tâche. En voici un exemple.

```python
from microbit import *

display.scroll("Appuyez sur le bouton A", delay=60, wait=True)
    
if button_a.was_pressed():
    display.show(Image.HAPPY)
else:
    display.show(Image.SAD)
```

On utilise ici la commande **scroll** en mode bloquant (avec **wait=True**). De ce fait, on ne peut pas détecter l'appui du bouton pendant l'affichage du message avec **is_pressed()**. **was_pressed()** permet de savoir si la consigne a bien été appliquée.