---
title : Micro:bit - Kit de survie
author: M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA 
---
# Micro:bit - Kit de survie

Le Kit liste les fonctions suivantes :

1. [Manipuler la matrice de LED](./assets/1_LED.md)
2. [Utiliser les boutons](./assets/2_BOUTON.md)
3. [Utiliser la communication radio](./assets/3_RADIO.md)
4. [Gérer le son](./assets/4_SON.md)
5. [Utiliser le capteur de température](./assets/5_TEMPERATURE.md)
6. [Utiliser l'accéléromètre](./assets/6_ACCELEROMETRE.md)
7. [Utiliser la boussole](./assets/7_BOUSSOLE.md)
