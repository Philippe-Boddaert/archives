# Exercices avec la carte micro:bit

Nous allons voir ici des exercices permettant de se familiariser avec la carte micro:bit. 

Tous ces exercices pourront être faits avec la carte microbit mais aussi avec l['émulateur en ligne](https://create.withcode.uk/)

## Activité 1

Analysez ce code. Que fait-il ?

```python
from microbit import *

while True:
    display.show("1")
    sleep(500)
    display.show(" ")
    sleep(500)
```

Faîtes-le fonctionner sur la carte ou le simulateur en ligne.

#### Exercice 1

Modifiez le programme ci-dessus afin qu'il compte en boucle de 0 jusqu'à 9

*Indication* : on pourra utiliser la commande `str(i)` qui transforme le nombre **i** en texte. Ex : **str(5)** renvoie **"5"**

### Activité 2 : jouer avec les pixels

Chaque LED de la carte micro:bit dispose d'une coordonée $`(x,y )`$, dont les valeurs sont les suivantes :

![](https://upload.wikimedia.org/wikipedia/commons/c/cb/Micro_bit_position_des_DEL.png)

Analysez ce code. Que fait-il ?

```python
from microbit import *

for x in range(5):
    display.set_pixel(x,0,9)
    sleep(500)
```

*Explications* : la fonction `set_pixel` allume un point sur l'écran. Elle prend 3 paramètres :

- les deux premiers sont l'abscisse et l'ordonnée du point (le point de coordonnées 0,0 étant en haut à gauche de l'écran)
- le dernier paramètre est la luminosité du point entre 0 et 9  : 0 signifie que le point est éteint et 9 est la luminosité maximale.

Faîtes-le fonctionner sur la carte ou le simulateur en ligne.

#### Exercice 2

Modifiez ce programme afin qu'il allume la colonne centrale

#### Exercice 3

Modifiez ce programme afin qu'il allume successivement tous les pixels de l'écran.

_Indication_ : On pourra utiliser 2 boucles for imbriquées l'une dans l'autre. Pensez à changer le nom de la variable de la seconde boucle !

#### Exercice 4

Modifiez le programme précédent afin qu'il allume tous les pixels colonne par colonne, donc en 5 étapes.

![](./assets/ex4.gif)

### Activité 3 : jouer avec les boutons

#### Exercice 5

Étudiez le code suivant :

```python
from microbit import *

compteur = 0
while True:
    if button_a.was_pressed():
        display.show(str(compteur))
        sleep(1000)
        display.clear()
```

Que fait-il ? vérifiez votre réponse en le testant sur la carte ou le simulateur en ligne.

#### Exercice 6

Modifiez le programme ci-dessus pour

- que le nombre augmente de 1 à chaque appui sur le bouton A.
- que le nombre revienne à 0 lorsqu'on appuie sur le bouton B.
- que le nombre reste affiché en permanence

#### Exercice 7

Étudiez le code suivant :

```python
from microbit import *

x = 0
y = 0

while True:
    display.set_pixel(x,y,0)
    if button_a.was_pressed():
        x = x - 1
    if button_b.was_pressed():
        x = x + 1
    x = max(0, min(x, 4))
    display.set_pixel(x,y,9)
    sleep(20)
```

Que fait-il ? vérifiez votre réponse en le testant sur la carte ou le simulateur en ligne.

Expliquez le rôle de la ligne `x = max(0, min(x, 4))`

#### Exercice 8

Adaptez le programme ci-dessus afin que le point puisse se déplacer verticalement. On va pour cela utiliser l'accéléromètre de la carte :

- Pour récupérer l'information d'orientation de la carte selon l'axe y, utilisez la ligne `dy = accelerometer.get_y()`
- Si la variable **dy** est plus grande que 600, le point sera sur la 5eme ligne
- Si la variable **dy** est plus grande que 300, le point sera sur la 4eme ligne
- Si la variable **dy** est plus petit que -600, le point sera sur la 1ère ligne
- Si la variable **dy** est plus grande que -300, le point sera sur la 2ème ligne
- sinon, la carte est à peu près horizontale et le point sera su la ligne centrale.

#### Exercice 9

Modifiez le programme précédent afin de déplacer le pixel exclusivement avec l'accéléromètre. Selon que l'on penche la carte de droite à gauche, ou d'avant en arrière, le point se déplace sur la matrice LED sans avoir recours aux boutons A et B qui deviennent obsolètes.

### Activité 4 : Afficher des images

#### Exercice 10

Observez le code suivant. Il réalise une animation.

```python
from microbit import *

imgs = [
    Image('90000:00000:00000:00000:00000:'),
    Image('90000:09000:00000:00000:00000:'),
    Image('90000:09000:00900:00000:00000:'),
    Image('90000:09000:00900:00090:00000:'),
    Image('90000:09000:00900:00090:00009:')
]
display.show(imgs, delay=500,loop=True)
```

Expliquez comment sont codées les images ?

#### Exercice 11

Recréez l'animation ci-contre sur votre carte. Vous remarquerez que les pixels ne sont pas toujours allumés à pleine intensité !

![](./assets/ex14.gif)

#### Exercice 12

Observez le fonctionnement du code suivant :

```python
from microbit import *
from random import choice

mesImages = [Image('00000:00000:00900:00000:00000:'),
        Image('00009:00000:00000:00000:90000:'),
        Image('00009:00000:00900:00000:90000:'),
        Image('90009:00000:00000:00000:90009:'),
        Image('90009:00000:00900:00000:90009:'),
        Image('90009:00000:90009:00000:90009:')]
rolled = choice(mesImages)
display.show(rolled)
```

Remarquez l'emploi de la fonction **choice()** que l'on a importé de la librairie **random**. Elle permet d'extraire une image au hasard depuis la liste *mesImages*.

Modifiez ce programme afin qu'un appui sur le bouton A face rouler le dé.

#### Exercice 13 : Pierre feuille ciseaux

En utilisant la technique de l'exercice précédant, réalisez un jeu de Pierre-Feuille-Ciseaux. 

Un appui sur le bouton A affichera une de ces 3 figures au hasard. 

Vous créerez les images en utilisant la fonction **Image()** comme dans l’exercice précédent.