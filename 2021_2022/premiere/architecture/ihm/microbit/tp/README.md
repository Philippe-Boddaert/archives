# Jeu de Nim

## Description

![](./assets/allumettes.png)

- Le jeu se joue à deux joueurs. Pour nous, ce sera un humain et l'ordinateur
- 25 allumettes sont alignées.
- Chaque joueur, à son tour, doit enlever au minimum 1 et au maximum 3 allumettes.
- Celui qui ramasse la dernière allumette a gagné.


Le déroulement du jeu est le suivant :
```txt
L'Humain est le premier joueur à jouer'
Tant qu'il reste des allumettes
    on affiche la situation en cours (le nbre d'allumettes et qui doit jouer)
    si c'est à l'humain de jouer, on lui demande combien d'allumettes il souhaite enlever, sinon, on demande l'ordinateur.
    on annonce le coup du joueur (humain ou ordinateur)
    on retire ce nombre d'allumette(s) au nombre d'allumette(s) avec lesquelles on joue.
    s'il reste encore des allumettes, on change de joueur (sinon le jeu est terminé...)
On annonce la victoire du dernier joueur à avoir joué.
```

## Modélisation

### Allumette

Les allumettes sont modélisées par les LED de la carte microbit.

![](./assets/complet.png)

_Situation de départ : les 25 LED allumées symbolisent les 25 allumettes du jeu._

La première allumette à prendre est celle située aux coordonnées (4, 4), la dernière aux coordonnées (0, 0).

### Sélection d'allumettes

Pour sélectionner des allumettes, le joueur humain appuie sur le bouton A autant de fois qu'il le souhaite (dans la limite de 3).

Actionner le bouton B permet de mettre fin à son tour, i.e retrait effectif des allumettes et changement de joueur.

### Illustration

|                           | Commentaire                                                  |
| ------------------------- | ------------------------------------------------------------ |
| ![](./assets/complet.png) | Situation de départ, l'ensemble des LEDs sont allumées       |
| ![](./assets/coup_1.png)  | Le joueur Humain décide de prendre 2 allumettes              |
| ![](./assets/coup_2.png)  | Le joueur Ordinateur prend 1 allumette                       |
| ![](./assets/coup_3.png)  | Le joueur Humain décide de prendre 3 allumettes              |
| ...                       | Le jeu continue                                              |
| ![](./assets/coup_4.png)  | C'est au joueur Humain de jouer.<br />il décide de prendre de 2 allumettes... |
| ![](./assets/gagnant.png) | ... Il a gagné !                                             |
| ![](./assets/perdant.png) | Dans le cas où le joueur Humain perd.                        |

## Attendu

L'attendu est un script `nim.py` implémentant le jeu de Nim décrit ci-dessus.

## Pour aller plus loin

Les fonctionnalités suivantes peuvent être développées en plus :

- La sélection des allumettes peut se faire par un autre capteur (secouer x fois, associer le nombre d'allumettes à prendre en fonction de l'orientation de la carte microbit),
- Choisir au départ le joueur qui commence,
- Ajouter une sélection plus "intelligente" que celle aléatoire pour l'ordinateur...Pour cela, il sera possible d'analyser s'il existe une stratégie gagnante...

