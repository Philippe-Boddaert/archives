---
title : Carte Microbit
author : M. BODDAERT, d'après les travaux d'Olivier Lecluse
license: CC-BY-NC-SA
---
# Carte Microbit

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/BBC_Microbit.jpg/713px-BBC_Microbit.jpg)

_Source : Wikipédia_

## 1. Présentation générale

BBC micro:bit est une carte à __microcontroleur__ facile d’accès.

> Un __microcontrôleur__ est un circuit intégré qui rassemble les éléments essentiels d'un ordinateur : processeur, mémoires (mémoire morte et mémoire vive), unités périphériques et interfaces d'entrées-sorties.

![](https://cdn.sanity.io/images/ajwvhvgo/production/4de361b622ac9bf5e8b9c3109a3935dd47b96167-1490x609.png?w=653&q=80&fit=max&auto=format)

La carte micro:bit dispose de :

- __capteurs__ : deux boutons programmables, des capteurs de mouvement, de température, de luminosité, d’orientation etc.
- __actionneurs__ : 25 leds rouges, un haut-parleur, une connexion radio (Bluetooth) permettant de le faire communiquer avec d’autres appareils (d’autres Micro:bit par exemple). 

Une des particularités de ce microcontrôleur est qu'il est programmable en Python !!!

Nous allons pouvoir mettre en pratique nos connaissances dans ce langage pour utiliser les fonctionnalités offertes par la carte micro:bit.

## 2. Origine

Le premier projet était le BBC micro et il remonte au début des années 80. Il avait pour ambition de proposer un ordinateur accessible aux enfants. La BBC a repris ce concept et a produit le __micro:bit__. Il a été distribué à tous les __écoliers britaniques__ et ils ont développé des jeux et applications à l’aide d’un langage par bloc (comme Scratch).

## 3. Prise en main

Pour toute manipulation, la carte micro:bit doit être alimenter, brancher à un ordinateur via le câble micro-usb.

### 3.1. Hello World

Pour écrire un programme en python, nous allons utiliser l'éditeur en ligne, disponible à cette adresse : [https://python.microbit.org/](https://python.microbit.org/)

![](./assets/editeur.png)

Un code par défaut (`hello world`) est proposé.

Notez la première instruction `from microbit * import` obligatoire dans tout programme python à exécuter sur la carte micro:bit.

#### À Faire 1

Cliquer sur `Connect`, une boite de dialogue apparait afin de sélectionner la carte micro:bit.

Cliquer sur `Flash` et observer l'impact sur la carte micro:bit

N.B : Avant de débrancher la carte micro:bit, il est impératif de cliquer sur `Disconnect`

#### À Faire 2

Adapter le programme et modifier le message pour qu'il affiche un texte de votre choix

#### À faire 3

- Dans la boucle ajoutez un `print("bonjour")`
- Appuyez sur le bouton `Open Serial`, vous devriez pouvoir raccorder le micro:bit et accéder à un terminal et voir vos `print("bonjour")` apparaître.

#### Que faire en cas d’erreur ?

Il est plus difficile de programmer sur un microcontroleur car il ne dispose généralement pas d’écran. C’est pourquoi un message d’erreur défile toujours sur la matrice de LEDs en cas d’erreur dans le programme, ainsi que dans la console.

## 4. Aller plus loin

Un [kit de survie](./kit/) de la carte micro bit,  listant les fonctions de base.

À l'aide de ce kit, il est possible de réaliser les [exercices](./exercices/).