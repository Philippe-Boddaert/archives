# Architecture des ordinateurs

__Question 1__ : La figure ci-dessus représente l'architecture de Von Neumann. 
À quoi correspondent les éléments 1, 2, 3, 4 et 5 ? 

![](./qcm_VM.png)

__Question 2__ : Dans l'architecture de Von Neumann, le processeur contient :

__Réponses__ :

- [ ] A. L'unité de commande et l'unité arithmétique et logique
- [ ] B. L'unité de commande et la RAM
- [ ] C. Seulement l'unité arithmétique et logique.
- [ ] D. Seulement l'unité de commande

__Question 3__ : Quel est le rôle de l’unité arithmétique et logique dans un processeur ?

__Réponses__ :

- [ ] A.  réaliser les branchements
- [ ] B.  définir la base des opérations arithmétiques : binaire, octale ou hexadécimale
- [ ] C.  effectuer les calculs
- [ ] D. gérer le contrôle interne du processeur

__Question 3__ : Parmi tous les registres internes que possède une architecture mono-processeur, il en existe un appelé compteur ordinal (program counter). Quel est le rôle de ce registre ?

__Réponses__ :

- [ ] A. il contient l'adresse mémoire de la prochaine instruction à exécuter
- [ ] B. il contient le nombre d'instructions contenues dans le programme
- [ ] C. il contient l'adresse mémoire de l'opérande à récupérer
- [ ] D. il contient le nombre d'opérandes utilisés

__Question 4__ : À quoi sert la RAM dans le fonctionnement d'un ordinateur ?

__Réponses__ : 

- [ ] A. à stocker des données lors de l'exécution de programmes
- [ ] B. à stocker des fichiers
- [ ] C. à relier les périphériques
- [ ] D. à accélérer la connexion à Internet

__Question 5__ : Un ordinateur possède les caractéristiques matérielles suivantes :
```
 mémoire DDR SDRAM : 8 Go
 antémémoire (mémoire cache) : 1 Mo
 disque dur SSD : 1 To
```
Parmi les classements ci-dessous lequel est celui de l’accès mémoire le plus rapide au moins rapide ?

__Réponses__ :

- [ ] A. Antémémoire puis SDRAM puis SSD
- [ ] B. SSD puis Antémémoire puis SDRAM
- [ ] C. SSD puis SDRAM puis Antémémoire
- [ ] D. SDRAM puis SSD puis Antémémoire

__Question 6__ : Quel composant n’est pas considéré comme un __périphérique__ de l’ordinateur?

__Réponses__ :

- [ ] A. Souris
- [ ] B. Clavier
- [ ] C. Moniteur
- [ ] D. CPU
