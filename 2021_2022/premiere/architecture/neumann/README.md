---
title : Architecture de Von Neumann
author : M. BODDAERT, d'après les travaux de David Landry, Germain Becker et Christophe Mieszczak
license : CC-BY-NC-SA
---
# Architecture d'un ordinateur

## 1. Attendus

| Contenus | Capacités attendues |
| :-- | :-- |
|Modèle d’architecture séquentielle (von Neumann) | Distinguer les rôles et les caractéristiques des différents constituants d’une machine.<br />Dérouler l’exécution d’une séquence d’instructions simples du type langage machine.|

## 2. Composants d'un ordinateur

Lorsqu'on ouvre un ordinateur, quelle que soit sa marque, nous retrouvons en général les mêmes éléments (Cf. image ci-dessous)

### 2.1. À Faire

1. Associez le bon élément à chaque numéro du schéma ci-dessous.

![](./assets/materiel_numerote.png)

_Source : bpc-informatique.fr_

2. Associez chaque élément à sa description

| Description | Élément |
| :--  | :--: |
| Cerveau de l'ordinateur, qui permet à l'ordinateur d'effectuer les opérations (calculs) demandés.     |  |
| Stocke les informations des programmes et données en cours de fonctionnement	|  |
| Relie tous les éléments constituant un ordinateur. Sa principale fonction est la mise en relation de ces composants par des bus sous forme de circuits imprimés.	|  |
| Carte d’extension d'ordinateur qui permet de produire une image affichable sur un écran.	|  |
| Assure la mise sous tension de l'ensemble des composants	|  |
| Périphérique d'entrée-sortie qui stocke les données de base de la machine |  |
| Périphérique d'entrée-sortie, assure le stockage et la lecture de données sur support externe non volatile |  |
| Permet de connecter les périphériques (disque dur, lecteur DVD, etc.) à la carte mère.	|  |

## 3. Architecture d'un ordinateur

> __Architecture__ : Règles de conception ou de construction d'une machine. L'architecture d'une machine décrit les liens des différents composants qui la composent.

### 3.1. Un peu d'histoire : L'ENIAC

Contrairement aux ordinateurs d’aujourd’hui, l’__ENIAC__ a été conçu avec un objectif spécifique : __calculer les valeurs des tableaux de portée d’artillerie__. 

Physiquement l'ENIAC est une grosse machine, il contient :

- 17 468 tubes à vide, 
- 7 200 diodes à cristal, 
- 1 500 relais, 
- 70 000 résistances, 
- 10 000 condensateurs et 
- environ 5 millions de soudures faites à la main 

Son poids est de 30 T pour des dimensions de 2,4 × 0,9 × 30,5$`m`$ occupant une surface de 167 $`m^2`$. (Source : Wikipedia)

Des panneaux de bouchons étaient utilisés pour relayer les instructions à la machine, qui pouvait ensuite effectuer les calculs à grande vitesse. 

![](./assets/ENIAC.jpeg)

Malheureusement, l’inconvénient des cartes de connexion était que chaque fois que les programmeurs voulaient que l’ordinateur fonctionne sur un nouveau problème, il devait être __arrêté pendant des jours__ afin que les fils branchés sur le tableau de connexions puissent être reconfigurés.

### 3.2. Architecture Von Neumann

Les grands principes de fonctionnement des ordinateurs actuels résultent de travaux menés au milieu des années 1940. Ces travaux ont défini un schéma d'architecture appelée **architecture de von Neumann**, en référence à [John Von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann) (1903-1957), un mathématicien et physicien (et bien d'autres choses) américano-hongrois qui a participé et publié les travaux en 1945.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/185px-JohnvonNeumann-LosAlamos.jpg)

_Source : Wikimedia_

### 3.3. Généralités

L'idée majeure de l'architecture de von Neumann, était d'utiliser une zone de stockage __unique__, à savoir la mémoire de l'ordinateur, pour conserver à la fois les __programmes__ (instructions) et les __données__ qu'ils devaient manipuler.

Les travaux de von Neumann publiés en 1945 concernaient la conception de l'[EDVAC](https://fr.wikipedia.org/wiki/Electronic_Discrete_Variable_Automatic_Computer), un ordinateur basé sur cette architecture. 

Cet ordinateur était capable d'additionner, soustraire, multiplier et diviser en binaire. Sa capacité mémoire est l'équivalent actuel de 5,5 ko, alors qu'il occupait une surface de 45 m² et pesait presque 8 tonnes. 

Pour le faire fonctionner, trois équipes de trente personnes se succédaient en continu.

### 3.4. Description

Dans l'architecture de von Neumann, un ordinateur est composé de 4 parties :

![](./assets/Von_Neumann.png)

- La **mémoire** qui contient les programmes __ET__ les données, et qui indiquera à l'unité de contrôle quels sont les calculs à faire sur ces données ;
- L'**unité de contrôle** (UC ou *CU* en anglais pour Control Unit) : c'est le chef d'orchestre de l'ordinateur, elle récupère les instructions du programme en mémoire et les données sur lesquelles doivent s'opérer les instructions (via des bus de communication), puis les envoie à l'unité arithmétique et logique ;
- L'**unité arithmétique et logique** (UAL ou *ALU* en anglais) ou unité de traitement : son rôle est d'effectuer les opérations (calculs) de base ;
- Les dispositifs d'**entrée-sortie** pour communiquer avec l'extérieur

## 4. Mémoire

> Une __mémoire__ est un dispositif permettant d’enregistrer, de conserver et de restituer de l’information.

La notion de mémoire pour un ordinateur regroupe différents matériels et ne correspond pas qu’à un seul dispositif permettant de stocker de l’information.

Dans l’ordinateur on trouve donc différents éléments de mémoire. On peut les ordonner suivant les critères suivants : temps d’accès, capacité et coût par bit.

On parle alors de __hiérarchie de mémoire__ :

![](./assets/hierarchie.png)

| Mémoire | Temps d'accès | Débit | Capacité |
| :--: | :--: | :--: | :--: |
| Registre | 1 ns | | Kio |
| Mémoire cache | 2-3 ns | | Mio |
| RAM | 5-60 ns | 1-20 Gio/s | Gio |
| Disque dur | 3-20 ms | 10-320 Mio/s | Tio |


## 5. Processeur

> Le **CPU** (*Central Processing Unit* ou Unité Centrale de Traitement), aussi appelé **processeur**, regroupe à la fois l'unité arithmétique et logique et l'unité de contrôle.

### 5.1. Hardware : Un composant électronique

Un processeur est une puce placée sur une plaque de céramique ou de plastique qui la protège et sert de support aux broches de connexion.

![](./assets/Intel_C4004.jpeg)

_Le premier microprocesseur Intel 4004, Source : Wikipedia_

- La puce est une minuscule pastille de semi-conducteurs en général du silicium sur laquelle un circuit est gravé,
- Ce circuit représente plusieurs millions (voire milliards) de __transistors__ reliés par des sillons conducteurs,
- Lorsque le courant passe, les transistors dirigent les électrons qui servent d'interrupteurs ou de porte pour laisser passer ou non le courant,
- La combinaison de ces portes permet d'effectuer tous les calculs complexes.

### 5.2. Software : un exécuteur d'instructions

Tout programme (écrit en Python, C, Java) est traduit en une suite d'instructions du processeur, qui le traduit en langage machine (suite de 0 et 1).

```mermaid
flowchart TB
A[Programme A]---D
B[Programme B]---D
C[Programme C]---D
D[Jeu d'instructions]---E
D[Jeu d'instructions]---F
D[Jeu d'instructions]---G
E[0100...1101]
F[1000...1001]
G[1001...1011]
subgraph "Langage de Programmation"
A
B
C
end
subgraph "Processeur"
subgraph "Langage assembleur"
D
end
subgraph "Langage machine"
E
F
G
end
end


```

Plus de détails sur la notion de langage machine et langage assembleur : [ici](./LANGAGE.md)


#### 5.2.1. Jeu d'instructions

Tout processeur possède un nombre d'instructions de base limitée, on parle de __jeu d'instructions__.

Exemple : 

- Lire une valeur à une adresse mémoire,
- Ajouter les valeurs de deux registres mémoire,
- Copier la valeur d'un registre à une adresse mémoire.

Ci-dessous les 16 instructions de l'Intel 4004, extrait du [manuel](http://codeabbey.github.io/heavy-data-1/msc4-manual.pdf) :

![](./assets/instructions_4004.png)

#### 5.2.2. Cycle d'exécution

Un processeur possède une **horloge** qui définit le rythme auquel les instructions sont exécutées. Pour exécuter une instruction, le processeur va effectuer ce qu'on appelle un **cycle d'exécution**.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Intel_Core_i7-940_bottom.jpg/320px-Intel_Core_i7-940_bottom.jpg)

_Intel Core i7-940, Source : Wikipedia_

Par exemple, le processeur Intel Core i7-940 a une __fréquence__ de 3,2 GHz. effectue jusqu'à 3,2 milliards de cycles d'horloge par seconde.

Pendant un cycle, trois opérations ont lieu :

1. **Chargement** (*load* en anglais) : l'unité de contrôle récupère le mot binaire (qui contient la prochaine instruction à exécuter) situé en mémoire à l'adresse indiquée par son registre __compteur d'instruction (PC)__ et la stocke dans son registre interne ;
2. **Décodage** : la suite de bits de l'instruction contenue dans le registre interne est décodée pour déduire quelle instruction est à exécuter et sur quelles données. Cette étape peut alors nécessiter de lire d'autres mots binaires depuis la mémoire ou les registres, pour charger les données (les "opérandes") sur lesquelles portent l'opération à effectuer.
3. **Exécution** : l'instruction est exécutée, soit par l'ALU s'il s'agit d'une opération arithmétique ou logique, soit par l'UC s'il s'agit d'une opération de branchement qui va modifier la valeur du registre __PC__.

## 6. Synthèse

- Le **modèle de Von Neumann** est constitué des 4 éléments suivants :
  - d'un processeur contenant :
    - une **Unité Arithmétique et Logique** (UAL ou ALU).
    - une **Unité de Contrôle** (UC).
  - de **mémoire** (contenant les données et les programmes).
  - de **périphériques** (entrées / sorties).
- Il y a plusieurs niveaux de mémoire, leur capacité étant inversement proportionnelle à leur vitesse :
  - la **mémoire de registre** (dans le processeur).
  - la **RAM** (mémoire vive).
  - la **mémoire de masse** (disque-dur, clé USB,...).
- Le processeur (CPU), composé de l'UAL et de l'UC, est chargé d'exécuter des instructions écrites en **langage machine**, c'est-à-dire des mots écrits en binaire et stockés dans la mémoire.
- Tout programme doit être "converti" en langage machine afin d'être exécuté.

