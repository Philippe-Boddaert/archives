# Langage machine

## 1. Contexte

Un __programme__ est une suite de nombres binaires placés en mémoire représentant des instructions exprimées en **langage machine**. C'est le seul langage que peut comprendre le processeur chargé d'exécuter ces différentes instructions.

Ainsi, un programme écrit dans un langage de programmation évolué (on dit de *haut niveau*) comme Python, Java, C, etc. ne peut pas être compris par le processeur. On doit donc utiliser un *compilateur* (pour le C, par exemple) ou un *interpréteur* (pour Python, par exemple) pour transformer le programme en langage machine.

## 2. Instruction machine

Une instruction machine est un mot binaire composé de deux parties :

- le champ *code opération* qui indique au processeur quelle opération il doit effectuer (charger une donnée en mémoire, faire une addition, une comparaison, etc.)
- le champ *opérandes* qui indique au processeurs la (les) donnée(s) sur laquelle (lesquelles) doit s'appliquer l'opération (l'adresse mémoire de la donnée à charger, les deux valeurs à additionner, les deux valeurs à comparer, etc.)

Une instruction machine possède le schéma suivant :

```txt
	champ code opération | champ opérandes |
```

Par exemple :

```
Additionner | Valeur contenue dans le registre R1, Nombre 37
```

Chaque instruction peut occuper 1 ou plusieurs mots dans la mémoire d'un ordinateur. 

**Remarque :** un mot correspond à l'unité de base pouvant être traitée par le processeur. Avec un proceseur 8 bits la taille du mot correspond à 8 bits soit 1 octet. Avec un processeur 64 bits la taille du mots correspond à 64 bits soit 8 octets.

Au début de l'informatique les programmeurs devaient coder leur programme directement en binaire : le **langage machine**. 

Par exemple, le langage machine suivant est une instruction : 

```
01001100 00100101
```

- le premier octet `01001100` correspond au **code de l'opération** à effectuer (opcode) : "ajouter la valeur suivante au registre R1".
- le second octet `00100101` (37 en décimal) est l'**opérande** : la valeur à ajouter à celle contenue dans le registre R1.

## 3. Le langage assembleur

La programmation en binaire étant loin d'être évidente pour un humain, on a inventé le **langage assembleur** qui permet d'écrire les instructions de manière plus compréhensible. Dans notre exemple le code `0100110000100101` est remplacé par :

```
ADD R1,37 
```

Ce qui est tout de même déjà beaucoup plus lisible !

Voici un exemple de **programme assembleur** :

```
INP R0,2
INP R1,2
ADD R2,R1,R0
OUT R2,4
HALT
```

Le langage assembleur est donc une simple traduction brute du langage machine. Pour résumer :

- Le **langage machine est une succession de bits qui est directement interprétable par le processeur** d'un ordinateur.
- Un **langage assembleur** est le langage machine où les combinaisons de bits sont **représentées par des "symboles"** qu'un être humain peut mémoriser.
- Un programme assembleur convertit ces "symboles" en la combinaison de bits correspondante pour que le processeur puisse traiter l'information. Le **programme assembleur traduit donc le langage assembleur en langage machine**.

> **Remarques :** 
>
> - Un langage assembleur est souvent **spécifique à un type de processeur**.
> - Un langage assembleur est appelé "**langage de bas niveau**" car il est très proche du langage machine.

## 4. Les compilateurs / interpréteurs

Le langage assembleur n'est toutefois pas facile à manipuler. C'est pourquoi il a été conçu des langages de programmation plus agréable à utiliser : les **langages de haut niveau (Ex : C, Python, Javascript,...)**.

On parle également de **niveau d'abstraction d'un langage**. Plus celui-ci est proche de notre langage naturel et plus son niveau d'abstraction est élevé. Plus le langage est proche de la machine (binaire) plus celui-ci est de bas niveau.

![](https://gitlab.com/david_landry/nsi/-/raw/master/7%20-%20Architectures%20mat%C3%A9rielles%20et%20syst%C3%A8mes%20d'exploitation/img/abstraction.png)

Mais tout langage de programmation, pour être exécuté par une machine, doit être à un moment où à un autre traduit en langage binaire.

![](https://gitlab.com/david_landry/nsi/-/raw/master/7%20-%20Architectures%20mat%C3%A9rielles%20et%20syst%C3%A8mes%20d'exploitation/img/compilation.png)

Il existe plusieurs manières de procéder :

- La première consiste à **traduire le programme dans son ensemble une fois pour toute et générer un fichier avec le code binaire prêt à être exécuté**. Il s'agit de la méthode dîte de **compilation**, réalisée par un compilateur. Le langage C est un exemple de langage compilé.
- La deuxième méthode consiste à **traduire les instructions en langage binaire au fur et à mesure de la lecture du programme**. Il s'agit de la méthode dîte d'**interprétation**, réalisée par un interpréteur. Le langage Basic est un exemple de langage interpété.
- Enfin il existe des **méthodes mixtes** qui consistent à **traduire le programme en pseudo-code** (bytecode). Ce pseudo-code est **interprété par une machine virtuelle** au moment de l'execution. L'intérêt de cette approche est que l'execution et la traduction du pseudo-code en langage binaire est plus rapide. Mais également, le fait que ce pseudo-code permet une certaine **indépendance vis à vis du processeur** sur lequel il est exécuté. En effet, il suffit juste de disposer d'une machine virtuelle spécifique au processeur en question. Python et Java sont des exemples de langages utilisant cette technique.