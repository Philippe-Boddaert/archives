# Performances

## 1. La mémoire

### 1.1. Faire perdurer la mémoire, alimentation éteinte

La **mémoire vive** (**RAM** Random Access Memory) de l'ordinateur a besoin d'être alimentée en permanence pour garder les données. **A chaque extinction de l'ordinateur, elle est perdue**, on la qualifie de mémoire volatile.

Pour résoudre ce problème on recourt à deux types de **mémoires non volatiles** :

- La **mémoire morte** est une mémoire qui ne peut être que lue (**ROM** Read Only Memory), elle contient en général le **micrologiciel (firmware)** de l'ordinateur (**BIOS** ou UEFI) qui est le programme qui se charge à chaque allumage de l'ordinateur.
- La **mémoire de masse**. Pour stocker les données et les programmes, on ajoute un périphérique appelé mémoire de masse : le **disque dur** de vos ordinateurs, ou une **mémoire flash** dans le cas des tablettes et smartphones. Cette mémoire est capable de **stocker une grande quantité de données**, mais à l'inconvénient d'être **beaucoup moins rapides** que la mémoire vive, c'est pour cela que lors du lancement d'un programme les données nécessaires à son exécution sont généralement transférées vers la RAM pour une exécution plus rapide.

### 1.2. Accélérer les accès à la mémoire

Les performances des processeurs augmentant, l'accès à la RAM peut être un frein à l'exécution des tâches du processeur. 

Pour palier à ce goulot d'étranglement, les processeur contiennent maintenant de la **mémoire cache, une mémoire encore plus rapide que la RAM**.

Dans un ordinateur, il y a donc **plusieurs niveaux de mémoire, leur capacité (liée à leur coût) étant inversement proportionnelle à leur vitesse**.

![](https://gitlab.com/david_landry/nsi/-/raw/master/7%20-%20Architectures%20mat%C3%A9rielles%20et%20syst%C3%A8mes%20d'exploitation/img/Memoires.png)

| Mémoire | Temps d'accès | Débit | Capacité |
| :--: | :--: | :--: | :--: |
| Registre | 1 ns | | Kio |
| Mémoire cache | 2-3 ns | | Mio |
| RAM | 5-60 ns | 1-20 Gio/s | Gio |
| Disque dur | 3-20 ms | 10-320 Mio/s | Tio |

## 2. Le processeur

### 2.1. Réduire la tailles des transistors

La réduction de la taille des transistors, grâce aux progrès technologiques, a permis d'augmenter le nombre de ces composants sur le microprocesseur.

- En 1975, Gordon Moore énonce une prévision basée sur l'évolution du nombre de transistors sur une puce : ce nombre est sensé doubler chaque année. Le constructeurs font en sorte de s'aligner sur ce qu'on appelle la **[loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore)** pendant très longtemps.
- Aujourd'hui on atteint une limite physique : la taille des transistors est de l'ordre de quelques atomes, réduire davantage n'est plus vraiment possible car la miniaturisation est telle que les fabricants commencent à buter sur des effets quantiques. Par conséquent, augmenter leur nombre devient très difficile.

![](./assets/Loi_de_Moore.png)

### 2.2. Augmenter la fréquence d'horloge

L'augmentation de la fréquence d'horloge des microprocesseurs est également un facteur de performance : elle est liée à sa capacité  d'exécuter un nombre plus ou moins important d'instructions machines par seconde. Plus la fréquence d'horloge du CPU est élevée, plus ce CPU est capable d'exécuter un grand nombre d'instructions machines par seconde (en fait, c'est un peu plus compliqué que cela, mais nous nous  contenterons de cette explication). Cependant ...

- L'augmentation des fréquences entraine une augmentation de la consommation électrique : en doublant la fréquence, on multiplis sa consommation par 8. Cela est un important problème à une époque où on cherche à augmenter l'autonomie de materiel embarquant des processeurs (téléphones, objets connectés...)
- L'augmentation des fréquences entraine également une forte augmentation de la température et donc implique de recourir à un refroidissement bruyant et consommant lui aussi de l'énergie !

![](./assets/evolution_frequence.png)

Ainsi, depuis le milieu des années 2000, un plafond semble atteint. La fréquence d'horloge d'un microprocesseur ne dépasse pas les 3,5 Ghz.

### 2.3. Multiplier les processeurs

On utilise aujourd'hui des architectures multiprocesseurs afin d'améliorer la rapidité d'exécution sans augmenter la fréquence d'horloge.

Les microprocesseurs actuels embarquent plusieurs *coeurs*, c'est à dire plusieurs processeurs dans le processeurs.

- Aujourd'hui (en 2020) on trouve sur le marché des CPU possédant jusqu'à 18 coeurs ! Même les smartphones possèdent des microprocesseurs  multicoeurs : le Snapdragon 845 possède 8 coeurs.
- Le gain semble évident : potentiellement, on calcule deux fois plus vite en doublant *seulement* la consommation plutôt qu'en la multipliant par 8.
- En réalité, le gain n'est obtenu que pour les applications spécifiquement programmé pour utilisés cette architecture et travailler en *parallèle* , c'est à dire en effectuant plusieurs tâches simultanément en exploitant simultanément plusieurs coeurs. **C'est donc la qualité des algorithmes qui, en faisant travailler les coeurs en équipe,  feront la différence !**