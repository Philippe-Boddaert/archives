# Exercices - Le Li�vre et la Tortue

__N.B : les scripts suivants sont à exécuter dans le même répertoire que les fichiers [lievre-latin.txt](./assets/lievre-latin.txt) et [lievre-utf8.txt](./assets/lievre-utf8.txt).__

## 1. Quelques fonctions utiles

La fonction `int` renvoie l'entier associé à sa représentation dans une base donnée :

```python
>>> int('1', 16)
1
>>> int('f', 16)
15
>>> int('10', 16)
16
>>> int('a9', 16)
169
```

La fonction `bin` renvoie la représentation binaire d'un entier donné, sous la forme d'une chaine de caractère :

```python
>>> bin(1)
'0b1'
>>> bin(4)
'0b100'
>>> bin(15)
'0b1111'
>>> bin(42)
'0b101010'
```

## 2. Encodages d'un caractère

1. L'encodage ISO-8859-1 (autrement appelé encodage _latin-1_) est définie par la table suivante :

![](./assets/iso-8859-1.png)

2. Il est possible d'obtenir le point de code d'un caractère encodé en utf-8 via la fonction `ord` en python et d'obtenir la représentation binaire du point de code via le site https://unicode-table.com/fr/.

```python
>>> ord('A')
65
>>> ord('♠')
9824
```

Les liens https://unicode-table.com/fr/search/?q=65 et https://unicode-table.com/fr/search/?q=9824 indiquent que :

| Caractère | Point de code | Encodage UTF-8 (Héxa) | Encodage UTF-8 (Binaire) |
| :--: | :--: | :--: | :--: |
| A | 65 | 41 | 01000001 |
| ♠ | 9824 | E2 99 A0 | 11100010 10011001 10100000 |

## 3. À Faire

En vous référant à la table d'encodage iso-8859-1 et utf-8 :
1. Complétez le tableau suivant :

| Caractère | Encodage ISO-8859-1 (Héxa) | Encodage ISO-8859-1 (Binaire) | Encodage UTF-8 (Héxa) | Encodage UTF-8 (Binaire) |
| :--: | :--: | :--: | :--: | :--: |
| L | | | |
| i | | | |
| è | | | |

2. Qu'effectue le script suivant :

```python
with open("lievre-latin.txt","rb") as f:
    byte = f.read(1)
    for i in range(10):
     print(byte.hex(), byte)
     byte = f.read(1)
```

3. Modifier la ligne 4 du script pour qu'il affiche la représentation binaire de l'octet contenu dans la variable `byte`.
4. Dupliquer la version de code obtenu pour que le bloc opère sur le fichier `lievre-utf8.txt` Quelle différence constatez-vous concernant l'encodage de la lettre `è`?
5. Expliquer les résultats obtenus lors de l'exécution de ces 2 blocs d'instructions:

__Bloc 1__ :

```python
with open("lievre-utf8.txt","r", encoding='latin-1') as f:
	ligne = f.readline()
	print(ligne)
```
Résultat obtenu :
```python
Le LiÃ¨vre et la Tortue
```

__Bloc 2__ :

```python
with open("lievre-latin.txt","r", encoding='utf-8') as f:
    ligne = f.readline()
    print(ligne)
```
Résultat obtenu :
```python
Traceback (most recent call last):
...
UnicodeDecodeError: 'utf-8' codec can't decode byte 0xe8 in position 5: invalid continuation byte
```

## 4. Bilan

__Q1. le caractère `à` a toujours la même représentation en mémoire quelque soit l'encodage utilisé ?__

- [ ] A. Vrai
- [ ] B. Faux

__Q2. Dans la table d'encodage Unicode, le caractère `Ô` est le :__

- [ ] A. 212ème
- [ ] B. 213ème
- [ ] C. $`(D4)_{16}`$ème
- [ ] D. Il n'existe dans la table d'encodage Unicode

__Q3. En UTF-8, les caractères sont représentés sur :__

- [ ] A. 1 octet
- [ ] B. 2 octets
- [ ] C. 7 bits
- [ ] D. un nombre variable entre 1 et 4 octets

__Q4. En cas de problème d'affichage visuel d'un texte, quelle(s) est(sont) la(les) solution(s) selon vous ?__

- [ ] A. s'assurer que l'encodage du fichier et le même que celui qui sert à l'affichage
- [ ] B. modifier le fichier pour corriger les erreurs
- [ ] C. appeler son professeur pour qu'il corrige le problème
- [ ] D. modifier l'encodage de la visualisation

__Q5. Soit le point de code U+00D4, cela vous indique que :__

- [ ] A. l'encodage est Unicode et la représentation binaire du caractère est $`(01001101)_2`$
- [ ] B. l'encodage est Unicode et la représentation binaire du caractère est $`(11000011 \,10010100)_2`$
- [ ] C. l'encodage est Unicode et le caractère associé est la lettre Ô
- [ ] D. l'encodage est ISO-8859-1 et la représentation binaire du caractère est $`(01001101)_2`$

__Q6. Quand une application traite un fichier encodé en iso-8859-1 :__

- [ ] A. Elle traite octet par octet
- [ ] B. Elle traite un nombre variable d'octets
- [ ] C. calcule la position en fonction de la valeur de l'octet et affiche le caractère associé
- [ ] D. affiche les caractères en fonction de la table d'encodage Unicode