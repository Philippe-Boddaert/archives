---
title : Représentation des entiers naturels
author : M. BODDAERT, M.MATHIEU
license : CC-BY-NC-SA
---
# Représentation des entiers naturels

## 1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Écriture d'un entier positif dans une base $`b \geq 2`$ | Passer de la représentation d'une base dans une autre |

## 2. Contexte

XLII = 四十二 = 101010 = ?

Ces 3 écritures représentent le même nombre :

- XLII : selon le système de numération romaine,
- 四十二 : selon le système de numération chinoise,
- 101010 : selon le système de numération binaire.

Un nombre s'_écrit_ différemment selon le _système de numération_ utilisé.

### 2.1 Définition

> Un __entier naturel__ est un nombre $`n \in \mathbb{N}`$, c'est-à-dire positif ou nul.

> Un __système de numération__ est un ensemble de règles permettant de représenter les nombres, en utilisant un ensemble de __symboles (aussi appelés chiffres)__ déterminé.

| Système de numération | Symboles |
| :--: | --: |
| Romain | `I, V, X, L, C, D, M` | 
| Chinois | `〇, 一, 二, 三, 四, 五, 六, 七, 八, 九` |
| Binaire | `0, 1` |

> La __base__ de numération est le nombre de symboles utilisés dans un système de numération.

Le système de numération chinois est en base 10 (base décimale), tout comme celui que nous avons appris à l'école.

Dans un ordinateur, le système de numération utilisée est le système binaire, qui utilise exclusivement les symboles 0 et 1.

### 2.2 Problème

1. Comment une machine représente les nombres si elle n'utilise pas le système de numération décimale ?
2. Comment est construit notre système de numération décimale ?
3. Pour répondre à cette question à l'apparence simple, reprenons depuis le début : comment avons-nous appris à compter ?

## 3. La numération décimale (base 10)

Dans la vie courante et dans beaucoup de domaines, nous utilisons la numération décimale. Elle repose à l’origine sur nos dix doigts : les dix symboles – chiffres – permettent de représenter tous les nombres.

- Il y a 10 chiffres : 0, 1, 2, 3, 4, 5, 6, 7, 8, 9. 
- Avec ces derniers, on peut compter jusqu'à 9. 
- Et si l'on veut aller au-delà de 9, il faut changer de _rang_. 

### 3.1 Notion de rang

Le __rang__, c'est-à-dire la position des chiffres, est primordiale dans cette représentation (__numération de position__) : il y a quelques années déjà, vous avez appris ce qu’étaient les unités (colonne de droite), les dizaines, les centaines, etc…

Illustration avec le nombre 181 :

| centaine | dizaine | unité |
| :--: | :--: | :--: |
| 1 | 8 | 1 |

Le nombre 181 se compose d'une centaine, de 8 dizaines et d'une unité, soit $`181 = 1 \times 100 + 8 \times 10 + 1 \times 1`$.

On dit que le nombre 181 occupe 3 rangs, i.e il faut 3 chiffres pour l'écrire. Le chiffre 1 des centaines et le chiffre 1 des unités est le même symbole mais n'a pas la même importance. 

### 3.2 Décomposition d'un nombre en puissance de 10

> Un nombre est égal à la somme des valeurs de ses rangs, et on peut décomposer n'importe quel nombre en puissances de sa base de _manière unique_.

Reprenons l'exemple ci-dessus :

```math
181 = 1 \times 100 + 8 \times 10 + 1 \times 1
```

Une écriture équivalente en __décomposition en puissance de 10__ :

```math
181 = 1 \times 10^2 + 8 \times 10^1 + 1 \times 1^0
```

#### 3.2.1 Exercice

✏ Décomposer le nombre 4138 en utilisant la méthode des divisions successives par 10.

![exercice_1.png](./assets/exercice_1.PNG)

_Rappel de la terminologie de la `division euclidienne`_ :

![Division euclidienne](https://www.jeuxmaths.fr/cours/images/divisioneuclidienne.png)
_Source : jeuxmaths.fr_

## 4. La numération binaire (base 2)

En binaire, pour parler de rang, on utilise le terme bit, qui est la contraction de **_binary digit_**. 

Un bit a deux états stables.

En électronique, il est facile d'obtenir un système présentant deux états stables distincts. Prenons l'exemple d'un interrupteur :

![interrupteur.png](./assets/interrupteur.PNG)

![exemples.png](./assets/exemples.PNG)

Ainsi, pour coder une information qui peut ne prendre que deux états stables, la numération binaire est la plus adaptée.

Comme pour le système décimal, en binaire :

- Il y a 2 chiffres : 0, 1. 
- Avec ces derniers, on peut compter jusqu'à 1. 
- Et si l'on veut aller au-delà de 1, il faut changer de _rang_. 

### 4.1 Exercice

1. ✏ Écrire la représentation binaire des premiers entiers naturels jusque 10 en complétant ce tableau :

| Entier naturel | Représentation en binaire |
| :--: | --: |
| 0 | 0 |
| 1 | 1 |
| 2 | 10 |
| 3 | |
| 4 | |
| 5 | |
| 6 | |
| 7 | |
| 8 | |
| 9 | | 
| 10 | |

2. ✏ Combien de valeurs peut-on coder avec 1 bit ?  avec 2 bits ? avec 3 bits ? avec $`n`$ bits ?

### 4.2 Conversion décimal en binaire

Avec la méthode vue précédement, pour représenter un entier naturel $`n`$, l'opération est fastidieuse, car il faut écrire tous les entiers de 0 à $`n - 1`$.

Il existe plusieurs méthodes (algorithmes) de conversion. La plus connue est la méthode dite de la `division euclidienne` :

- On prend le nombre en base 10,
- On le divise par 2 et on note le reste de la division (soit 1 soit 0),
- On refait la même chose avec le quotient précédent, et on met de nouveau le reste de côté,
- On réitère la division, jusqu'à ce que le quotient soit 0.
- Le nombre en binaire apparaît alors : il suffit de prendre tous les restes de bas en haut.

### 4.3 Exercice

1. ✏ Décomposer le nombre 11 en utilisant la méthode des divisions successives par 2.

![exercice_2.png](./assets/exercice_2.PNG)

2. ✏ Quelle est la représentation binaire de 11 ?

### 4.4 Notation

#### 4.4.1 Levée d'ambiguité
Étant donné que les symboles 0 et 1 sont communs à beaucoup de bases de numération (en l’occurrence 2 et 10), on spécifie la représentation barrée ou parenthésée indicée par la base d'écriture.

Soit l'écriture du nombre $`157`$ : $`(157)_{10} = \overline{157}^{10} = (10011101)_2 = \overline{10011101}^2 `$

Elle diffère de l'écriture du nombre $`10011101`$ : $`(10011101)_{10} = (100110001100000111011101)_2`$.

#### 4.4.2 Vocabulaire : Poids

Le __bit de poids fort__ est le bit, dans une représentation binaire donnée, ayant le plus grand poids ou position (celui de gauche dans la notation positionnelle habituelle).

A l'inverse, le __bit de poids faible__ est celui ayant le plus petit poids ou position.

Soit le nombre $`9`$ dont la représentation binaire est $`(1001)_2`$. Le bit à 1 le plus à gauche est dit de poids fort, tandis que celui le plus à droite est dit de poids faible.

### 4.5 Exercice 

1. ✏ Donner la représentation binaire des nombres suivants, en utilisant la méthode des divisions successives par 2 :
	1. 21
	2. 42
	3. 1023
	4. 1024
2. ✏ Que constate-t-on sur les nombres impairs ?
3. ✏ Que constate-t-on entre les nombres multiples de 2 entre eux ?

### 4.6 Conversion binaire en décimal

Un nombre s'écrit sur $`n`$ bits (rangs), en système binaire. Où chaque bit représente une puissance de 2.

Comme vu précédemment, un nombre est égal à la somme des valeurs de ses rangs.

Pour convertir un nombre représenté en binaire en décimale, on procède de la manière suivante :on multiplie par $`2^0`$ la valeur du rang 0, par $`2^1`$ la valeur du rang 1, par $`2^2`$ la valeur du rang 2, par $`2^3`$ la valeur du rang 3, ... et on somme le tout.

Exemple :

```math
\begin{aligned}
(10011)_2 & = \boldsymbol{1} \times 2^4 + \boldsymbol{0} \times 2^3 + \boldsymbol{0} \times 2^2 + \boldsymbol{1} \times 2^1 + \boldsymbol{1} \times 2^0 \\
& = 16 + 0 + 0 + 2 + 1 \\
& = 19 \\
& = (19)_{10}
\end{aligned}
```
### 4.7 Exercice

✏ Représenter les nombres suivants en base 10 :

1. $`(11111)_2`$
2. $`(101010)_2`$
3. $`(1010101)_2`$
4. $`(101011001)_2`$

### 4.8 Qu'est-ce qu'un octet ?

Un octet (**byte** en anglais) est un regroupement de 8 bits.

On parle aussi de __mot binaire__. Il permet de coder 2<sup>8</sup> = 256 mots différents.

Si nous codons des entiers naturels, nous coderons les nombres 0 à 255.

#### 4.8.1 Unités de mesure

Il est très courant en informatique de mesurer la capacité mémoire d'un disque dur, de la RAM d'un ordinateur ou d'un débit de données Internet avec une unité de mesure exprimée comme un multiple d'octets. 

Ces multiples sont traditionnellement des puissances de 10 et on utilise les préfixes "kilo", " méga", etc. pour les nommer. 

Le tableau ci-dessous donne les principaux multiples utilisés dans la vie courante.

| Nom              | Symbole            | Valeur |
| :--------------- |:---------------:   | -----:|
| Kilooctet  |   ko                     |  10<sup>3</sup> octets |
| Mégaoctet  |   Mo                     |  10<sup>3</sup> ko |
| Gigaoctet  |   Go                     |  10<sup>3</sup> Mo |
| Teraoctet  |   To                     |  10<sup>3</sup> Go |

> Remarque : Historiquement, les multiples utilisés en informatique étaient des puissances de 2. Pour ne pas confondre l'ancienne et la nouvelle notation, on utilise des symboles différents pour représenter ces multiples.

| Nom              | valeur            | Nombre d'octeets |
| :--------------- |:---------------:   | -----:|
| Kio  |   2<sup>10</sup> octets                 |  1024 |
| Mio  |   2<sup>10</sup>Kio                     |1048576|
| Gio  |   2<sup>10</sup>Mio                     |1073741824|
| Tio  |   2<sup>10</sup>Gio                     |1099511627776| 

## 5. La numération hexadécimale (base 16)

La représentation en binaire n'est pas pratique à nous humain pour travailler (longueur de l'information importante, difficile à écrire et à lire sans faire d'erreur...).

Pour cela, nous travaillons avec la base hexadécimale. Le système hexadécimal permet de réduire la longueur des mots et facilite leur manipulation.

Le système se base sur les 16 chifres : `0 1 2 3 4 5 6 7 8 9 A B C D E F`

### 5.1 - Conversion décimale en hexadécimale

Pour convertir un nombre décimal en hexadécimal, la méthode est similaire au binaire, c'est-à-dire par divisions successives, sauf que cette fois on divise par 16.

```math
\begin{aligned}
157 & = 16 \times 9 + 13 \text{ Soit D, en base 16}\\
9 & = 16 \times 0 + 9
\end{aligned}
```

Le nombre 157 s'écrit $`9D`$ en hexadécimal. La notation est : $`(9D)_{16}`$.

### 5.2 - Conversion hexadécimale en décimale

Un nombre s'écrit sur $`n`$ chiffres, en système hexadécimal. Où chaque chiffre représente une puissance de 16 associée à son rang.
Comme vu précédemment, un nombre est égal à la somme des valeurs de ses rangs.

Pour convertir le tout en décimale, on procède de la manière suivante : on multiplie par $`16^0`$ la valeur du rang 0, par $`16^1`$ la valeur du rang 1, par $`16^2`$ la valeur du rang 2, par^$`16^3`$ la valeur du rang 3, ... et on somme le tout.

Exemple :

```math
\begin{aligned}
(12B7)_{16} & = \boldsymbol{1} \times 16^3 + \boldsymbol{2} \times 16^2 + \boldsymbol{B} \times 16^1 + \boldsymbol{7} \times 2^0 \\
& = \boldsymbol{1} \times 16^3 + \boldsymbol{2} \times 16^2 + \boldsymbol{11} \times 16^1 + \boldsymbol{7} \times 2^0 \\
& = \boldsymbol{1} \times 4096 + \boldsymbol{2} \times 256 + \boldsymbol{11} \times 16 + \boldsymbol{7} \times 1 \\
& = 4096 + 512 + 176 + 7 \\
& = 4791
\end{aligned}
```

### 5.3 Exercice

✏ Convertir les nombres suivants en base 10 :

1. $`(10)_{16}`$
2. $`(1B7)_{16}`$
3. $`(ABE)_{16}`$

### 5.4 Conversion binaire en hexadécimale

Pour convertir un nombre binaire en base 16, on regroupe les bits 4 à 4, chaque groupe donnant un chiffre hexadécimal.

À l'inverse, passer d'un nombre hexadécimal à sa représentation binaire se fait en remplaçant chaque chiffre pour son équivalent sur 4 bits.

Ainsi, $`(11011001)_2 = (1101\,1001)_2 = (D9)_{16}`$, tandis que $`(7F)_{16} = (0111\,1111)_2 = (1111111)_2`$.


⚠ ___ATTENTION___ : Si la représentation binaire de départ n'a pas un nombre de bits multiple de 4, il faut ajouter des zéros en bits de poids forts (ce qui ne change pas sa valeur) afin de pouvoir les regrouper 4 par 4.

### 5.4.1 Exercice

✏ Convertir les entiers suivants, représentés en binaire, en hexadécimal et inversement :

1. $`(101111011001)_2`$
2. $`(111011)_2`$
3. $`(42)_{16}`$
4. $`(101)_{16}`$

## 5. Généralisation pour une base $`b`$

Soient :

- $`x`$ la représentation du nombre en base 10,
- $`r`$ la représentation du nombre en base $`b`$,
- $`r`$ étant écrit en utilisant $`n`$ chiffres : $`c_0, c_1...c_{n−1}`$

Nous avons alors :

```math
\begin{aligned}
r = c_{n−1} c_{n−2} \dots c_2 c_1 c_0
\end{aligned}
```

et

```math
\begin{aligned}
 x & = \sum_{i = 0}^{n - 1} c_i \times b_i \\
   & = c_{n−1} \times b_{n−1} + c_{n−2} \times b_{n−2} + ... + c_2 \times b_2 + c_1 \times b_1 + c_0 \times b_0
\end{aligned}
```

## 6. Le boutisme

La représentation des entiers naturels sur des mots de 2, 4 ou 8 octets se heurte au problème de l'ordre dans lequel ces octets sont organisés en mémoire. Ce problème est appelé le __boutisme__ (ou endianness en anglais).

Prenons l'exemple d'un mot de 2 octets (16 bits) comme 5BC9. Il y a deux organisations possibles d'un tel mot en mémoire :

- Le gros boutisme (ou ou « mot de poids fort en tête » ou big endian en anglais), qui consiste à placer l'octet de poids fort en premier, c'est à dire à l'adresse mémoire la plus petite.

![gros_boutisme.png](./assets/gros_boutisme.PNG)

> Quelques architectures respectant cette règle : _les processeurs Motorola 68000, les SPARC (Sun Microsystems) ou encore les System/370 (IBM)_. De plus, tous les protocoles TCP/IP communiquent en gros-boutiste. Il en va de même pour le protocole PCI Express.

- Le petit boutisme (ou little endian en anglais), qui au contraire place l'octet de poids faible en premier.

![petit_boutisme.png](./assets/petit_boutisme.PNG)

> Les processeurs x86 ont une architecture petit-boutiste. Celle-ci, au prix d'une moindre lisibilité du code machine par le programmeur, simplifiait la circuiterie de décodage d'adresses courtes et longues en 1975, quand un 8086 avait 29 000 transistors. Elle est d'influence pratiquement nulle aujourd’hui.

Généralisons pour 4 octets. Ainsi, le mot 5BC96AF sera représenté de la manière suivante :
en **gros boutisme**

![gros_boutisme_2.png](./assets/gros_boutisme_2.PNG)

en **petit boutisme**

![petit_boutisme_2.png](./assets/petit_boutisme_2.PNG)

La représentation petit ou gros boutisme est en principe transparente à l'utilisateur car cela est géré au niveau du système d'exploitation. Cette représentation prend de l'importance quand on accède aux octets soit en mémoire, soit lors d'échanges d'informations sur un réseau.