# Codage des booléens

![](./assets/switch.jpg)
_Source : Pixabay_

## 1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Valeurs booléennes : 0,1. Opérateurs booléens : and, or, not.<br />Expressions booléennes | Dresser la table d’une expression booléenne | 

## 2. Contexte

On rencontre des __booléens__ dans diverses situations quotidiennes :

- un interrupteur (Ouvert / Fermé),
- un jeter de pièce de monnaie (Pile / Face)
- écriture manuelle (Gauche / Droite)
- sens d'une locomotive (Avant / Arrière)

## 3. Définition

> Un ___booléen___ est un type de variable à deux états, destiné à représenter les __valeurs de vérité de la logique__ et l'__algèbre booléenne__.

> Le terme __booléen__ vient de _George Boole_, mathématicien anglais 1815 - 1864. On parle également de variable __binaire__ ou __logique__.

Ce sujet est à la confluence de plusieurs domaines : électronique, logique, mathématique et informatique, d'où des notations diverses.

### 3.1. Notations

|           | Français | Électronique | Logique | Ensembliste | Python |
| :---------: | :--------: | :------------: | :-------: | :-----------: | :------: |
| Etat Haut | Vrai     |   &top;           | 1 | 1 | True |
| Etat Bas | Faux     |   &perp;           | 0 | 0 | False |

__N.B : Par convention, on utilise les valeurs 0 et 1 pour représenter les états d'une variable logique et on note $`\mathcal{B}`$ cet ensemble ($`\mathcal{B} = \{0, 1\}`$)__
`

### 3.2. À Faire

Via cette [application](https://philippe-boddaert.gitlab.io/circuit/?c=0), identifier les objets et leurs valeurs logiques

| Objet | Valeurs logiques |
| :-----: | :----------------: |
| Bouton |                  |
| Ampoule | |

## 4. Fonctions logiques

> Une __fonction logique__ ou __opérateur logique__ $`S`$ défini l'état de sortie d'un système, en fonction des états de ses entrées.

__Exemple pour le [circuit](https://philippe-boddaert.gitlab.io/circuit/?c=0)__ :

- les __entrées__ : le bouton $`a`$
- la __fonction logique__ : $`S`$, l'état de d'ampoule.
- L'état de l'ampoule $`S`$ dépend de l'état du bouton $`a`$ :

```math
\begin{aligned}
S = 1 & \text{ si } a = 1 \\
S = 0 & \text{ si } a = 0
\end{aligned}
```

### 4.1. Table de vérité

On représente l'__ensemble__ des valeurs d'entrées et sorties par une table de vérité : 

- À chaque variable d'entrée correspond une colonne, 
- À chaque ligne, une valeur d'état possible,
- Une colonne de sortie contient la valeur de l'état de l'opération.

__Exemple pour le [circuit](https://philippe-boddaert.gitlab.io/circuit/?c=0)__ :

| $`a`$ | $`S = a`$ |
| :--: | :--: |
| 0 | 0 |
| 1 | 1 |

### 4.2. À Faire

1. Écrire la table de vérité du système suivant :

![](./assets/circuit.png)

2. Pour 2 entrées, combien y-a-t'il de lignes de la table de vérités ? Pour 3 entrées ? Généraliser pour $`n`$ entrées ?

*N.B : La table de vérité doit contenir l'__ensemble__ des combinaisons possibles des valeurs d'entrée. La meilleure manière d'énumérer toutes les combinaisons sans se tromper est de __compter en binaire__*.

### 4.3. Opérateur NON

#### Définition

> L'opérateur **NON** est une fonction logique _unaire_ (1 seule entrée) dont la valeur de sortie est l'inverse de la valeur de son entrée.

> L'opérateur est noté $`\overline{a}`$.

#### Table de vérité

| $`a`$ | $`S = \overline{a}`$ |
| :--: | :--: |
| 0 | 1 |
| 1 | 0 |

__Illustration avec le [circuit](https://philippe-boddaert.gitlab.io/circuit/?c=1)__

### 4.4. Opérateur ET (AND)

#### Définition

> L'opérateur **ET** est une fonction logique _binaire_ (2 entrées) dont la valeur de sortie est 1 si et seulement si les 2 entrées sont simultanément à 1.

> L'opérateur est noté $`\land`$.

#### Table de vérité

| $`a`$ | $`b`$ | $`S = a \land b`$ |
| :--: | :--: | :--: |
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

__Illustration avec le [circuit](https://philippe-boddaert.gitlab.io/circuit/?c=2)__

#### Propriétés

```math
\begin{aligned}
 a \land a & = a \\
 a \land 1 & = a \\
 a \land \overline{a} & = 0 \\
 a \land 0 & = 0
\end{aligned}
```
### 4.5. Opérateur OU (OR)

#### Définition

> L'opérateur **OU** est une fonction logique _binaire_ (2 entrées) dont la valeur de sortie est 1 si l'une des 2 entrées a la valeur 1.

> L'opérateur est noté $`\lor`$.

#### Table de vérité

| $`a`$ | $`b`$ | $`S = a \lor b`$ |
| :--: | :--: | :--: |
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

__Illustration avec le [circuit](https://philippe-boddaert.gitlab.io/circuit/?c=3)__

#### Propriétés

```math
\begin{aligned}
 a \lor a & = a \\
 a \lor 1 & = 1 \\
 a \lor \overline{a} & = 1 \\
 a \lor 0 & = a
\end{aligned}
```

### 4.6. Expressions booléennes

À l’aide de l'ensemble des opérateurs vus ci-dessus, on peut construire des expressions ou fonctions, que l'on évalue en établissant leur table de vérité.

__Exemple : $`S(a, b) = \overline{a} \lor \overline{b}`$__

| $`a`$ | $`b`$ | $`S_1 = \overline{a}`$ | $`S_2 = \overline{b}`$ | $`S = S_1 \lor S_2`$ |
| :--: | :--: | :--: | :--: | :--: |
| 0 | 0 | 1 | 1 | 1 |
| 0 | 1 | 1 | 0 | 1 |
| 1 | 0 | 0 | 1 | 1 |
| 1 | 1 | 0 | 0 | 1 |

#### 4.6.1. Propriétés

Les opérateurs ont plusieurs propriétés communes:

- __Associativité__ : Comme avec les opérations habituelles, certaines parenthèses sont inutiles. Exemple : $`( a \land b ) \land c = a \land (b \land c) = a \land b \land c`$
- __Commutativité__ : L'ordre est sans importance. Exemple : $`a \land b = b \land a`$
- __Distributivité__ : Exemple : $`a \lor ( b \land c ) = ( a \lor b ) \land ( a \lor c )`$
- __Idempotence__ : Exemple : $`a \land a \land a \land \ldots \land a = a`$

#### 4.6.2. À Faire

À partir de ces 3 opérateurs, il est possible d'en décrire d'autres.

Écrire les tables de vérité des opérateurs suivants :

1. __NON ET__ (NAND), défini par $`S(a, b) = \overline{a \land b}`$,
2. __NON OU__ (NOR), défini par $`S(a, b) = \overline{a \lor b}`$,
3. __OU EXCLUSIF__ (XOR), défini par $`S(a, b) = a \oplus b`$ (vaut 1 si uniquement une des deux valeurs de a ou de b vaut 1),
4. __NON OU EXCLUSIF__ (XNOR), défini par $`S(a, b) = \overline{a \oplus b}`$

## 5. Exercices

### 5.1. Établir des tables de vérité

Écrire les tables de vérité des expressions booléennes suivantes :
1. $`S(a, b) = (\overline{a} \land b)`$
2. $`S(a, b) = b \lor (a \land b)`$
3. $`S(a, b) = a \land (a \lor b)`$
4. $`S(a, b, c) = (\overline{a} \land b) \lor (a \land c)`$
5. Communication = Émetteur ET Récepteur
6. Décrocher = (Sonnerie ET Décision de répondre) OU décision d'appeler
7. Bac = Avoir la moyenne OU (NON(Avoir la moyenne) ET rattrapage)

### 5.2. Équivalence d'expressions booléennes

1. Montrer que $`(a \land b) = \overline{(\overline{a} \lor \overline{b})}`$
2. Montrer que $`(a \lor b) = \overline{(\overline{a} \land \overline{b})}`$

_N.B : Deux expressions booléennes sont équivalentes si leurs tables de vérité le sont. Autrement dit, si pour toutes les entrées des tables de vérité, l'ensemble des valeurs de sorties de ces mêmes tables sont équivalentes alors les expressions booléennes sont équivalentes_.

### 5.3. Déterminer une expression booléenne

1. Trouver l'expression booléenne, notée ssi(a, b) à partir de sa table de vérité :
	
| a | b | ssi(a, b) |
| :--: | :--: | :--: |
| 0 | 0 | **1** |
| 0 | 1 | **0** |
| 1 | 0 | **0** |
| 1 | 1 | **1** |

## 6. Lois de De Morgan

Les lois de __De Morgan__ sont des identités entre propositions logiques. Elles ont été formulées par le mathématicien britannique Augustus De Morgan (1806-1871).

1. $`\overline{(a \lor b)} = \overline{a} \land \overline{b}`$
2. $`\overline{(a \land b)} = \overline{a} \lor \overline{b}`$

## 6.1 À Faire

Démontrer ces 2 formules

## Pour aller plus loin

Une [vidéo](https://www.youtube.com/watch?v=68RG57jOF0c) sur la vie et l'impact des travaux de __George Boole__.
