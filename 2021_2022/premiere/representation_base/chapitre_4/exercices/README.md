# Exercices

## Exercice 1. Masques de sous-réseau

Dans un réseau TCP/IP, un ordinateur a une adresse IP qui l’identifie de manière unique (comme un numéro de téléphone), ainsi qu’un masque de sous-réseau.

L’adresse IP et le masque de sous-réseau sont des groupes de 4 entiers positifs, codés tous les deux sur 4 octets.

Le masque identifie à quel sous réseau d’Internet il fait partie, et permet d’obtenir l’adresse IP du sous-réseau à l’aide d’une opération booléenne.

Un ordinateur a pour IP `192.168.0.42`, et son masque de sous-réseau est `255.255.255.0`.

Pour obtenir l’adresse du sous-réseau de l’ordinateur, on effectue alors un AND entre les octets de l’IP et les octets du masque. En représentation binaire, cela donne :

```math
\begin{aligned}
\text{IP} &: 1 1 0 0 0 0 0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0 \\
\text{Masque} &: 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 \\
\text{AND} &: \\
\text{Adresse de sous-réseau} &: 1 1 0 0 0 0 0 0 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
\end{aligned}
```

Sur un processeur 32 bits, le AND est une opération primitive du processeur, donc l’opération de détermination du sous-masque réseau se fait en une instruction.

1. Chercher l'adresse IP de votre ordinateur et de son masque sous-réseau. Sous windows, il faut taper la commande `ipconfig`. Sous linux, il faut taper la commande `ifconfig`.
2. Chercher l'adresse IP de votre téléphone portable. Sous Android, il faut aller dans les Règlages ou Paramètres et aller sur l'icône `À propos` du téléphone.
3. Écrire l'adresse des sous-réseaux que vous avez trouvés.

## Exercice 2. 

$`a, b`$ et $`c`$ sont trois valeurs booléennes.

1. Dans quel(s) cas l’expression booléenne  $`a \lor (b \land c)`$ est-elle vraie ?
2. Même question avec l’expression $`a \lor (b \land \overline{c})`$  ?

## Exercice 3.

Est-il correct d’affirmer que $`\overline{(x \land y)}`$ est identique à $`(\overline{x}) \lor (\overline{y})`$ (conseil : faites une table de vérité).


## Exercice 4. Multiplexeur

On considère la fonction `multiplexeur`, notée `mux` par $`mux(x,y,z)=((\overline{x}) \land y) \lor (x \land z)`$

Complétez la table suivante:

|  x   |  y   |  z   |  $`\overline{x}`$  | $`(\overline{x}) \land y`$ | $`x \land z`$ | $`mux(x,y,z)`$ |
| :--: | :--: | :--: | :--: | :------: | :---: | :--------: |
|  0   |  0   |  0   |      |          |       |            |
|  0   |  0   |  1   |      |          |       |            |
|  0   |  1   |  0   |      |          |       |            |
|  0   |  1   |  1   |      |          |       |            |
|  1   |  0   |  0   |      |          |       |            |
|  1   |  0   |  1   |      |          |       |            |
|  1   |  1   |  0   |      |          |       |            |
|  1   |  1   |  1   |      |          |       |            |


## Exercice 5. Éclairage d‘un escalier 
Le but de cet exercice est de trouver une fonction logique permettant un
éclairage d’un escalier entre trois étages : l’escalier démarre au rez-de-chaussée, arrive au premier étage et aboutit au second étage. Nous voulons un interrupteur à chaque étage et chaque interrupteur doit être capable d’allumer/éteindre l’éclairage de tout l’escalier. Nous notons respectivement $`a, b`$ et $`c`$ les trois variables booléennes associés aux interrupteurs.

1. Écrire la table de vérité de la fonction logique associée à l’éclairage.
2. Écrire une équation logique de cet éclairage.

## Exercice 6. Parking souterrain

Un parking souterrain est géré grâce à un gardien et à partir de capteurs de détection de véhicules.

Un capteur $`p`$ dans le sol détectera la présence d'un véhicule à l'entrée du parking ($`p = 1`$).

Un capteur $`h`$ en hauteur détectera la présence d'un véhicule de plus de 2 mètres ($`h = 1`$). Pour une hauteur supérieure à 2 mètres, l'entrée dans le parking est interdite.

De plus, le gardien du parking aura la possibilité de fermer un contact $`g`$ ($` g = 1`$) si le parking est plein, pour ne pas autoriser l'entrée de véhicules supplémentaires.

L'autorisation de pénétrer sera visualisée sur un feu bicolore :

- Si le feu est vert la barrière s'ouvrira et le véhicule pourra se garer dans le parking
- Si le feu est rouge la barrière restera fermée

1. Donnez la table de vérité du système. Pour les combinaisons matériellement impossibles, le feu rouge restera allumé.
2. En déduire les équations logiques de "Vert" et "Rouge". 
