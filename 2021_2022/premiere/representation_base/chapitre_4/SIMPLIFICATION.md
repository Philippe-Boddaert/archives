# Simplification d'expression booléenne

## 1. Introduction

Soit l'expression booléenne : $`S = (\overline{a} \land \overline{b}) \lor (\overline{a} \land b) \lor (a \land \overline{b}) \lor (a \land b)`$

__Peut-on réduire cette expression, i.e écrire une expression équivalente où le nombre d'opérateurs et / ou de variables est réduit ?__

## 2. Rappel des propriétés de l'Algèbre booléenne

L'algèbre de Boole dispose des propriétés suivantes :

- __Associativité__ : Comme avec les opérations habituelles, certaines parenthèses sont inutiles. Exemple : $`( a \land b ) \land c = a \land (b \land c) = a \land b \land c`$
- __Commutativité__ : L'ordre est sans importance. Exemple : $`a \land b = b \land a`$
- __Distributivité__ : Exemple : $`a \lor ( b \land c ) = ( a \lor b ) \land ( a \lor c )`$
- __Idempotence__ : Exemple : $`a \land a \land a \land \dots \land a = a`$

### 2.1 Simplification grâce aux propriétés algébriques

Étape 1 : Selon la __propriété de distributivité__ de l'algèbre de boole : 

1. $`\overline{a} \land (\overline{b} \lor b) = (\overline{a} \land \overline{b}) \lor (\overline{a} \land b)`$
1. $`a \land (\overline{b} \lor b) = (a \land \overline{b}) \lor (a \land b)`$

Il est possible de réduire l'expression $`S`$ en $`S = (\overline{a} \land (\overline{b} \lor b)) \lor (a \land (\overline{b} \lor b))`$

Étape 2 : Selon la __propriété de l'opérateur OU__ : $`b \lor \overline{b} = 1`$

Il est possible de réduire encore l'expression $`S`$ en $`S = (\overline{a} \land 1) \lor (a \land 1)`$

Étape 3 : Selon la __propriété de l'opérateur ET__ : $`a \land 1 = a`$

Il est possible de réduire encore l'expression $`S`$ en $`S = \overline{a} \lor a`$

Étape 4 : Selon la __propriété de l'opérateur OU__ : $`a \lor \overline{a} = 1`$

Il est possible de réduire encore l'expression $`S`$ en $`S = 1`$

## 2.2. Bilan

- L'expression $`S = (\overline{a} \land \overline{b}) \lor (\overline{a} \land b) \lor (a \land \overline{b}) \lor (a \land b)`$ peut se réduire à l'expression $`S = 1`$.

- On passe d'une expression à 2 variables et 8 opérateurs à ... 1 valeur booléenne !!

- Cette méthode nécessite de connaitre les propriétés de l'algèbre booléenne et de les appliquer successivement jusqu'à trouver une expression non réductible.

- Il existe une autre méthode de simplification d'expression booléenne, la simplification par Tableau de Karnaugh

## 3. Les tableaux de Karnaugh

### 3.1 Construction du tableau

Soit une expression $`S`$ à $`n`$ variables booléennes, dont on connait la table de vérité $`T`$ :

- Le tableau de Karnaugh a $`\lceil \frac{n}{2} \rceil`$ colonnes et $`\lfloor \frac{n}{2} \rfloor`$ lignes
- Les colonnes correspondent au regroupement des  $`\lceil \frac{n}{2} \rceil`$ premières variables,
- Deux colonnes adjacentes diffèrent d'une et une seule valeur des variables,
- Les lignes correspondent au regroupement des  $`\lfloor \frac{n}{2} \rfloor`$ variables suivantes,
- Deux lignes adjacentes diffèrent d'une et une seule valeur des variables,
- Le croisement d'une ligne $`l`$ et une colonne $`c`$ correspond à la valeur de $`S`$ pour les valeurs $`cl`$ dans $`T`$ 

__Illustration__

Soit l'expression booléenne $`S`$ à 3 variables avec la table de vérité $`T`$ :

| $`a`$ | $`b`$ | $`c`$ | $`S = (\overline{a} \land \overline{b} \land c) \lor (\overline{a} \land b \land \overline{c}) \lor (a \land \overline{b} \land c) \lor (a \land b \land \overline{c})`$ |
| :--: | :--: | :--: | :--: |
|  0    |  0    |  0    | 0 |
|  0    |  0    |  1    | 1 |
|  0    |  1    |  0    | 1 |
|  0    |  1    |  1    | 0 |
|  1    |  0    |  0    | 0 |
|  1    |  0    |  1    | 1 |
|  1    |  1    |  0    | 1 |
|  1    |  1    |  1    | 0 |

- Le tableau de Karnaugh a $`\lceil \frac{n}{2} \rceil`$ colonnes et $`\lfloor \frac{n}{2} \rfloor`$ lignes, soit 2 colonnes et 1 ligne.
- Les colonnes correspondent au regroupement des 2 premières variables $`ab`$ ,
- La ligne correspond au regroupement des variables suivantes, soit $`c`$.

| $`S`$ | 00 | 01   | 11   | 10   |
| :--: | :----: | :----: | :----: | :----: |
|  **0**  |  |  |  |  |
| **1** |  |  |  |  |

- La valeur de la case colonne = 00, ligne = 0, correspond à la valeur de $`S`$ pour $`a = 0, b = 0`$ et $`c = 0`$,
- La valeur de la case colonne = 01, ligne = 0, correspond à la valeur de $`S`$ pour $`a = 0, b = 1`$ et $`c = 0`$,
- etc ...

Soit, le tableau de Karnaugh complété suivant :

| $`S`$ | 00   | 01   | 11   | 10   |
| :--: | :----: | :----: | :----: | :----: |
|  **0**  | 0 | 1 | 1 | 0 |
| **1** | 1 | 0 | 0 | 1 |

### 3.2. Simplification

La simplification par **Tableau de Karnaugh** consiste à :

- Rechercher les cases adjacentes qui ont pour valeur 1 
- Regrouper ces cases en paquets de taille égale à une puissance de 2, les plus conséquents possibles.

__Illustration__

Soit le tableau de Karnaugh suivant :

| $`S`$ | 00 | 01   | 11   | 10   |
| :--: | :----: | :----: | :----: | :----: |
|  **0**  | 0 | 1 | 1 | 0 |
| **1** | 1 | 0 | 0 | 1 |

- Est-il possible de faire des paquets de 1 adjacents de taille 8 ($`2^3 = 8`$) ? 
  - Réponse : Non
- Est-il possible de faire des paquets de 1 adjacents de taille 4 ($`2^2 = 4`$) ?
  - Réponse : Non
- Est-il possible de faire des paquets de 1 adjacents de taille 2 ($`2^1 = 2`$) ? 
  - Réponse : Oui, 2 simplifications possibles
    - les colonnes 01 et 11 pour la ligne 0 sont adjacentes et ont la valeur 1. On simplifie en supprimant les variables dont la valeur change, soit $`b \land \overline{c}`$
    - les colonnes 00 et 10 pour la ligne 1 sont adjacentes et ont la valeur 1. On simplifie en supprimant les variables dont la valeur change, soit $`c \land \overline{b}`$
    - On barre les 1 de ces cases pour ne plus les prendre en compte
- Est-il possible de faire des paquets de 1 adjacents de taille 1($`2^0 = 1`$) ?
  - Réponse : Non, Il n'y a plus de cases contenant de 1.
  - L'opération de simplification est terminée.

__Bilan__ : On obtient une expression simplifiée de $`S`$ notée $`S'`$, où $`S' = (b \land \overline{c}) \lor (c \land \overline{b})`$

__À Faire__ : Vérifier l'équivalence de $`S`$ et $`S'`$

## 4. Exercices

### 4.1. Exercice 1

| $`S`$ | 00   | 01   | 11   | 10   |
| :----: | :----: | :----: | :----: | :----: |
| __00__   | 1    | 0    | 0    | 0    |
| __01__   | 1    | 0    | 0    | 0    |
| __11__   | 1    | 0    | 0    | 0    |
| __10__   | 1    | 0    | 0    | 0    |

Donner une expression simplifiée de $`S`$.

### 4.2. Exercice 2

| $`S`$ | 00   | 01   | 11   | 10   |
| :----: | :----: | :----: | :----: | :----: |
| __00__   | 1    | 0    | 0    | 1   |
| __01__   | 1    | 0    | 0    | 1   |
| __11__   | 1    | 0    | 0    | 1   |
| __10__   | 1    | 0    | 0    | 1   |

Donner une expression simplifiée de $`S`$.

### 4.3. Exercice 3

| $`S`$ | 00   | 01   | 11   | 10   |
| :----: | :----: | :----: | :----: | :----: |
| __00__   | 1    | 0    | 0    | 1    |
| __01__   | 0    | 0    | 0    | 0    |
| __11__   | 0    | 0    | 0    | 0    |
| __10__   | 1    | 0    | 0    | 1    |

Donner une expression simplifiée de $`S`$.


### 4.4. Exercice 4

Soit l'expression booléenne $`S`$ exprimé par : $`S = (\overline a \land b \land \overline c \land \overline d) \lor (a \land b \land c \land d) \lor (a \land \overline{b} \land c \land d) \lor (a \land b \land \overline{c} \land \overline{d})`$

| $`S`$ | 00   | 01   | 11   | 10   |
| :----: | :----: | :----: | :----: | :----: |
| __00__   | 0    | 1    | 1    | 0    |
| __01__   | 0    | 0    | 0    | 0    |
| __11__   | 0    | 0    | 1    | 1    |
| __10__   | 0    | 0    | 0    | 0    |

Donner une expression simplifiée de $`S`$.

### 4.5. Exercice 5

#### 1. Contexte

La société de transport `Roue Libre` met en place une nouvelle politique de transport gratuit.

Un usager peut circuler gratuitement si et seulement il remplit au moins une des conditions suivantes :

- il est majeur, sans emploi, habite à moins de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette) 
- il est mineur, sans emploi, habite à moins de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette),
- il est majeur, sans emploi, habite à plus de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette),
- il est mineur, sans emploi, habite à plus de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette),
- il est majeur, travaille, habite à plus de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette),
- il est mineur, travaille, habite à plus de 5km de l'agence de transport et n'a pas de véhicules (vélo, scooter, trotinette).

Le service informatique souhaite mettre à jour son système développé en Python.

Le bloc d'instruction suivant permet de déterminer si le transport est gratuit pour un usager ou non.

```python
mineur = ?
sans_emploi = ?
habite_loin = ?
a_vehicule = ?

if ((not mineur and sans_emploi and not habite_loin and not a_vehicule) or
        (mineur and sans_emploi and not habite_loin and not a_vehicule) or
        (not mineur and sans_emploi and habite_loin and not a_vehicule) or
        (mineur and sans_emploi and habite_loin and not a_vehicule) or
        (not mineur and not sans_emploi and habite_loin and not a_vehicule) or
        (mineur and not sans_emploi and habite_loin and not a_vehicule)
       ):
	print("Le transport est gratuit")
else:
	print("Le transport n'est pas gratuit")
```
#### 2. À Faire

1. Tester avec quelques valeurs de paramètres la correction de cette fonction
2. Combien de tests faudrait-il réaliser pour vérifier l'ensemble des cas possibles ?

#### 3. Simplification

Le directeur informatique souhaite savoir s'il est possible de simplifier cette fonction, tout en garantissant sa correction (son bon résultat)...il ne faudrait pas que la fonction renvoie Vrai pour un usager qui ne remplit pas les critères et inversement !!

Le bloc d'instruction précédent correspond à l'équation logique $`S`$ suivante :

$`S = \overline a b \overline c \overline d + ab \overline c \overline d + \overline a bc \overline d + abc\overline d + \overline a \overline b  c \overline d + a \overline b c \overline d`$

où :

- $`a`$ indique si l'utilisateur est mineur,
- $`b`$ indique si l'usager est sans emploi,
- $`c`$ indique si l'usager habite loin (à plus de 5km de l'agence),
- $`d`$ indique si l'usager dispose d'un véhicule (vélo, scooter, trotinette)

#### 4. À Faire

1. Simplifier à l'aide du tableau de Karnaugh l'équation $`S`$.

| $`S`$ | 00   | 01   | 11   | 10   |
| :----: | :----: | :----: | :----: | :----: |
| __00__   | 0    | 1    | 1    | 0    |
| __01__   | 0    | 0    | 0    | 0    |
| __11__   | 0    | 0    | 0    | 0    |
| __10__   | 1    | 1    | 1    | 1    |

2. Écrire le bloc d'instruction correspondant à l'équation simplifiée de $`S`$
3. Exécuter les 2 blocs d'instructions avec les mêmes valeurs de paramètre. Ont-elles le même comportement ? Que répondre au directeur informatique ?

