# Manipulations des booléens en Python
# 1. Type Booléen

## 1.1. Valeur et Type

En Python, les booléens sont représentés par les mots clés `True` et `False`, ils sont du type `bool`

```python
>>> a = True
>>> b = False
>>> c = "True"
>>> d = "False"
```

__À Faire__ : Dans un notebook Basthon, écrire une instruction permettant d'afficher le type des variables `a`, `b`, `c`et `d`. Que constatez-vous ?

## 1.2. Comparaison

Les opérateurs de comparaison courants sont identiques à ceux des mathématiques mais ATTENTION, il ne faut pas confondre l’égalité et l’affectation/

```python
>>> variable = 5    # une affectation
>>> 5 == 8          # une égalité (qui est fausse)
False
```

**Le résultat d’une comparaison est toujours un booléen**

## 1.3. Comparaisons des nombres

| Comparaison       | Symbole | Exemple       | Résultat |
| ----------------- | ------- | ------------- | -------- |
| Égalité           | `==`    | `1 + 2 == 3`  | `True`   |
| Différence        | `!=`    | `1 + 2 != 3`  | `False`  |
| Supérieur         | `>`     | `4 > 3`       | `True`   |
| Inférieur         | `<`     | `2.2 < 2 * 3` | `True`   |
| Supérieur ou égal | `>=`    | `5 >= 6`      | `False`  |
| Inférieur ou égal | `<=`    | `8 <= 3`      | `False`  |

## 1.4. Appartenance à une structure

On peut tester qu’un élément appartient à une structure avec le mot clé `in`

```python
>>> "a"		in "bonjour"        # False
False
>>> "bon"	in "bonjour"        # True
True
```

# 2. Opérations sur les booléens

Les opérateurs sur les booléens sont de deux types :

- opérateur unaire : prend *un* booléen et en renvoie *un*.
- opérateur binaire : prend *deux* booléens et en renvoie *un*.

## 2.1. Opérateur unaire : la négation

### 2.1.1. La négation: `not`

C’est le seul opérateur *unaire*, il donne le contraire de ce qu’on lui passe.

```python
>>> not True    # s'évalue à False
False
>>> not False   # s'évalue à True
True
```

## 2.2.Opérateur binaire : le OU, noté `or`

**Il est vrai si l’un des deux booléens est vrai.**

```python
>>> True or False
True
>>> True or True
True
```

## 2.3. Opérateur binaire : le ET, noté `and`

**Il est vrai si les deux booléens sont vrais.**

```python
>>> True and False
False
>>> True and True
True
```

## 2.4. Opérateur binaire : le OU Exclusif noté `^`

**Il est vrai si EXACTEMENT un des deux booléens est vrai**

```python
>>> True ^ False
True
>>> True ^ True
False
```

# 3. Conversion avec d'autres types

Python permet de convertir n’importe quoi à un booléen.

Par exemple, une chaîne de caractère vide est évaluée à `False`.

```python
>>> bool(1)
True
>>> bool(0)
False
>>> bool("")
False
>>> bool("abc")
True
```

- 0 est faux, les autres entiers sont vrais,
- une structure vide est fausse, les autres sont vraies.

## 4. À Faire

### Exercice 1

Écrire un bloc d'instructions permettant d'afficher la table de vérité __complète__ de l'opérateur :

1. OU
2. ET 
3. OU EXCLUSIF

__N.B : Au moins une structure boucle doit être utilisée__

__N.B : Il est possible de s'aider de la partie 3 (Conversion avec d'autres types), qui indique que les valeurs 0 et 1 peuvent être converties en booléen, respectivement en False et True.__

__Exemple d'affichage attendu :__

```txt
| a | b | S = a and b |
| - | - | ----------- |
| False | False | ? |
| False | True | ? |
| True | False | ? |
| True | True | ? |
```

### Exercice 2

1. __Sur papier__, déterminer les tables de vérités des expressions suivantes :
   1. $`S = (a \land \overline{b}) `$
   2. $`S = \overline{(a \land b)} \lor c`$
   3. $`S = \overline{a} \lor \overline{b} \lor c`$
   4. $`S = (a \land b \land c) \lor \overline{a} \lor \overline{c}`$

2. Écrire des blocs d'instructions en Python permettant d'afficher les valeurs des tables de vérité et vérifier avec ceux de la question 1.

### Exercice 3

1. Soit le code Python suivant :

```python
pluie_tombe = ?
arroseur_fonctionne = ?

if pluie_tombe or arroseur_fonctionne:
  print("la pelouse est mouillée")
```

Pour quelles valeurs de `pluie_tombe` et `arroseur_fonctionne` le message `la pelouse est mouillée` s'affiche-t-il ?

2. Modifier la condition de la ligne 4 pour que le message  `la pelouse est sèche` s'affiche.

