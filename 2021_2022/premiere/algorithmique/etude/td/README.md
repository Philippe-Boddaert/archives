# Parcours séquentiel d'un tableau

## 1. Attendus

| Contenus                                       | Capacités attendues                                          |
| :--------------------------------------------- | :----------------------------------------------------------- |
| Parcours séquentiel d’un tableau | Écrire un algorithme de recherche d’une occurrence sur des valeurs de type quelconque. <br/>Écrire un algorithme de recherche d’un extremum, de calcul d’une moyenne. |

## 2. Contexte

Dans ce chapitre, nous allons étudier des algorithmes de parcours séquentiel d'un tableau notamment pour rechercher un extremum d'un tableau ou la présence d'un élément donné.

## 3. Recherche d'un maximum

__Principe__ : On initialise la valeur au premier élément du tableau puis on parcours le tableau pour vérifier s'il existe un élément soit plus petit soit plus grand.

Plus formellement, l'algorithme de recherche de maximum d'un tableau peut s'écrire de la manière suivante :

![](./assets/maximum.jpeg)

### 3.1. À Faire

1. Écrire l'implantation en Python de l'algorithme de recherche d'un maximum en utilisant une boucle bornée.
2. Écrire l'implantation en Python de l'algorithme de recherche d'un maximum en utilisant une boucle non bornée.

Tester avec les valeurs d'entrée suivantes : 

1. [4, 5 ,-8, 2, 8, 6, 1]
2. [8, 8, 8, 8, 8, 8]
3. []

## 4. Preuve de programme

1. Comment prouver la __terminaison__ de l'algorithme ?
2. Comment prouver la __correction__ de l'algorithme ?
3. Quel est la __complexité__ de l'algorithme ? Y a-t-il un meilleur des cas ? un pire des cas ?

## 5. Rappel

| Question | Réponse |
| -------- | -------- |
| __Est-ce que l'algorithme se termine ?__ | Trouver un __variant__, i.e un entier positif qui décroit strictement. |
| __Est-ce que l'algorithme est correct ?__ | Trouver un __invariant__, i.e un prédicat vrai à l'initialisation d'algorithme et qui le reste jusqu'à la fin d'algorithme. |
| __Est-ce que l'algorithme donne un résultat en un temps raisonnable ?__ | Calculer la __complexité__, i.e le __nombre d'instructions__ nécessaires à l'exécution de l'algorithme. |

