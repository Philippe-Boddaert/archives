# Exercices - Issus de la BNS (Banque Nationale de Sujet)

## Question 1

On exécute le script suivant :

```python
for i in range(n):
  for j in range(i):
    print('NSI')
```

Combien de fois le mot NSI est-il affiché ?

Réponses :

- [ ] A. $`n^2`$
- [ ] B. $`(n + 1)^2`$
- [ ] C. $`1 + 2 + \dots + (n - 1)`$
- [ ] D. $`1 + 2 + \dots + (n - 1) + n`$


## Question 2

Quel code parmi les quatre proposés ci-dessous s'exécute-t-il en un temps linéaire, i.e en $`\mathcal{O}(n)`$ ?

Réponses :

- [ ] A.
```python
for i in range(n//2):
  for j in range(n):
    print('hello')		
```

- [ ] B.
```python
for i in range(n):
  print('hello')
```

- [ ] C.
```python
t = [ i + j for i in range(n) for j in range(n) ]
for x in t:
  print('hello')
```

- [ ] D.
```python
for i in range(n//2):
  for j in range(n//2):
    print('hello')
```

## Question 3

Quel est le coût d'un algorithme de recherche du maximum d'un tableau de nombres ?

Réponses :

- [ ] A. constant
- [ ] B. logarithmique
- [ ] C. linéaire
- [ ] D. quadratique

## Question 4

Combien d’échanges effectue la fonction Python suivante pour trier un tableau de 10 éléments au pire des cas ?

```python
def tri(tab):
  for i in range (1, len(tab)):
    for j in range (len(tab) - i):
      if tab[j]>tab[j+1]:
        tab[j],tab[j+1] = tab[j+1], tab[j]
```

Réponses :

- [ ] A. 10
- [ ] B. 45
- [ ] C. 55
- [ ] D. 100

## Question 5

On considère le code suivant de recherche d'une valeur dans une liste :

```python
def search(x, y):
  # x est la valeur à chercher
  # y est un tableau de valeurs
  for i in range(len(y)):
    if x == y[i]:
      return i
  return None
```

Quel est le coût de cet algorithme ?

Réponses :

- [ ] A. constant
- [ ] B. logarithmique
- [ ] C. linéaire
- [ ] D. quadratique

## Question 6

Quel est l’ordre de grandeur du coût du __tri par insertion__ (dans le pire des cas) ?

Réponses :

- [ ] A. l'ordre de grandeur du coût dépend de l'ordinateur utilisé
- [ ] B. linéaire en la taille du tableau à trier
- [ ] C. quadratique en la taille du tableau à trier
- [ ] D. indépendant de la taille du tableau à trier

## Question 7

On exécute le script suivant :

```python
tableau = [17, 12, 5, 18, 2, 7, 9, 15, 14, 20]
somme = 0
i = 0
while i < len(tableau):
  somme = somme + tableau[i]
  i = i + 1
resultat = somme / len(tableau)
```

Quelle affirmation est **fausse** parmi les suivantes ?

Réponses :

- [ ] A. à la fin de l'exécution la valeur de i est 9
- [ ] B. le corps de la boucle a été exécuté 10 fois
- [ ] C. resultat contient la moyenne des éléments de liste
- [ ] D. len est une fonction

## Question 8

La fonction `mystere` suivante prend en argument un tableau d'entiers.

```python
def mystere(t):
  for i in range(len(t) - 1):
    if t[i] + 1 != t[i+1]:
      return False
 return True
```

À quelle condition la valeur renvoyée par la fonction est-elle `True` ?

Réponses :

- [ ] A. si le tableau passé en argument est une suite d'entiers consécutifs
- [ ] B. si le tableau passé en argument est trié en ordre croissant
- [ ] C. si le tableau passé en argument est trié en ordre décroissant
- [ ] D. si le tableau passé en argument contient des entiers tous identiques

## Question 9

On définit une fonction de calcul de la moyenne d'une liste de nombres :

```python
def moyenne(L):
  s = 0
  n = len(L)
  for x in L:
    s = s + x
  return s / n
```

Combien cette fonction utilise-t-elle d'additions et de divisions pour calculer la moyenne d'un tableau de 7 nombres ?

Réponses :

- [ ] A. 7
- [ ] B. 8
- [ ] C. 9
- [ ] D. 10

## Question 10

La fonction suivante prend en arguments deux entiers positifs et renvoie leur produit.

```python
def produit(a,b):
  c = 0
  i = 0
  while i < b:
    #
    i = i + 1
    c = c + a
  return c
```

Quelle propriété reste vraie à chaque passage par la ligne marquée d'un # ?

Réponses :

- [ ] A. $`c = a \times (i + 1)`$
- [ ] B. $`c = a \times (i - 1)`$
- [ ] C. $`c = a \times b`$
- [ ] D. $`c = a \times i`$
