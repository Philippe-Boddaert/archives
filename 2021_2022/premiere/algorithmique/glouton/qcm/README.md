# QCM

## Question 1

Un algorithme glouton donne toujours :

- [ ] A. La solution optimale à un problème
- [ ] B. Une solution en un temps relativement court
- [ ] C. Toutes les solutions
- [ ] D. Une solution en un temps relativement long

## Question 2

On remplit un camion de déménagement de 16 mètres cubes avec des palettes de
volumes 10, 8, 5, 3, 1 mètres cubes. En raisonnant suivant un algorithme glouton, choisissez la combinaison que vous obtenez.

- [ ] A. Des palettes de 8 m3, 5 m3, 3 m3
- [ ] B. Des palettes de 10 m3, 5 m3, 1 m3
- [ ] C. Des palettes de 10 m3, 3m3, 3 m3
- [ ] D. Des palettes de 5 m3, 5 m3, 3 m3, 3 m3

## Question 3

Qu’est-ce qu’un algorithme glouton ? (plusieurs réponses possibles)

- [ ] A. C’est un algorithme qui suit le principe de faire, étape par étape, un choix optimum local.
- [ ] B. C’est un algorithme qui tente de faire une optimisation globale.
- [ ] C. C’est un algorithme qui optimise les temps de calcul.
- [ ] D. C’est un algorithme qui prend la meilleure solution parmi toutes les solutions
possibles.

## Question 4

Pour le jeu de pièces de valeurs (6,4,1) et pour une somme S=8 , choisir ce que renvoie l’algorithme glouton parmi les solution suivantes :

- [ ] A. 8 = 6 + 1 + 1
- [ ] B. 8 = 4 + 4
- [ ] C. 8 =1+1+1+1+1+1+1+1
- [ ] D. rien