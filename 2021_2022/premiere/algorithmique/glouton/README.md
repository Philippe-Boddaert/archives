# Algorithmes gloutons

![](./assets/pacman.gif)

_Source : giphy.com_

## 1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Algorithmes gloutons |  Résoudre un problème grâce à un algorithme glouton. |

Extrait du Programme de spécialité de NSI de 1ère (Source : [Eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g))

## 2. Problème du sac à dos

Un cambrioleur possède un sac à dos d'une contenance maximum de 30 Kg. Au cours d'un de ses cambriolages, il a la possibilité de dérober 4 objets A, B, C et D.

Voici un tableau qui résume les caractéristiques de ces objets :

| Objet | A | B | C | D |
| :-- | :--: | :--: | :--: | :--: |
| __Masse__ | 13 kg | 12 kg |  8 kg| 10 kg |
| __Valeur marchande__ | 700 €| 400 €| 300 €| 300 €|

### 2.1. À Faire

Déterminer les objets que le cambrioleur aura intérêt à dérober, sachant que :

- Tous les objets dérobés devront tenir dans le sac à dos (30 Kg maxi),
- Le cambrioleur cherche à obtenir un gain maximum.

__N.B : Une interface graphique, pour visualiser et expérimenter, est disponible à cette adresse [https://philippe-boddaert.gitlab.io/backpack/index.html](https://philippe-boddaert.gitlab.io/backpack/index.html).__

### 2.2. Définition

Ce genre de problème est un grand classique en informatique, on parle de __problème d'optimisation__. 

> Un __problème d’optimisation__ est un problème algorithmique dans lequel l’objectif est de trouver la « meilleure » solution (selon un critère donné) parmi un ensemble de solutions également valides mais potentiellement moins bonnes.

Le contexte d’un problème d’optimisation est donc :

- un __très grand nombre de solutions__ (dans le cas contraire, il n’y aurait pas de difficulté à trouver la meilleure),
- une fonction permettant d’évaluer la qualité de chaque solution,
- l’existence d’une solution optimale, ou suffisamment bonne.

### 2.3. À Faire

1. Lister les combinaisons à tester dans l'illustration du problème du sac à dos avec 4 objets ?
2. Quel est l'algorithme qui permet de trouver une solution optimale au problème du sac à dos ?

### 2.4. Analyse

En généralisant le résultat obtenu, soit $`C_{sac}`$, le nombre de combinaisons à tester pour le problème du sac à objets avec $`n`$ objets est définit par :

```math
\begin{aligned}
C_{sac} & = \sum_{k = 0}^{n} C_{n}^{k} \\
& = \sum_{k = 0}^{n} \dfrac{n!}{k!\,(n - k)!} \\
& \text{Or, d'après la formule du binôme de Newton}\\
& = a^n \text{, où } a \text{ est une constante.}
\end{aligned}
```

Autrement dit, un algorithme qui testerait toutes les combinaisons possibles aurait une complexité en $`\mathcal{O}(a^n)`$, ce qui est inefficace dans la pratique.


À la place de cette méthode "je teste toutes les possibilités", il est possible d'utiliser une méthode dite __gloutonne__ (_greedy_ en anglais).

## 3. Algorithme glouton

### 3.1. Définition

> Le __principe de la méthode gloutonne__ est de, à chaque étape de la résolution du problème, faire le choix qui semble le plus pertinent sur le moment, avec l'espoir qu'au bout du compte, cela nous conduira vers une solution optimale du problème à résoudre.

Autrement dit, l’approche gloutonne repose sur les principes suivants :

- La construction une *solution complète* par une succession de __choix locaux__. 
- La sélection du choix local se fait sur __une des meilleures possibilités de l'ensemble considéré__,
- La __non remise en cause__ des choix précédents.

### 3.2. Application

Appliquons une méthode gloutonne à la résolution du problème du sac à dos, sachant que l'on cherche à maximiser le gain.

1. Commençons par établir un tableau nous donnant la "valeur massique" de chaque objet (pour chaque objet on divise sa valeur par sa masse) :

| Objet | A | B | C | D |
| :-- | :--: | :--: | :--: | :--: |
| Valeur massique | 54 €/kg | 33 €/kg |  38 €/kg| 30 €/kg |

2. On classe ensuite les objets par ordre décroissant de valeur massique,
3. Enfin, on remplit le sac en prenant les objets dans l'ordre et en s'arrêtant dès que la masse limite est atteinte.

&rArr; C'est ici que ce fait "le choix glouton", à chaque étape, on prend l'objet ayant le rapport "valeur-masse" le plus intéressant au vu des objectifs.

### 3.3. À Faire

1. Quel est le résultat obtenu par l'application de l'algorithme sur l'instance ci-dessus ?
2. La solution trouvée ci-dessus est-elle optimale ?
3. Calculer la complexité de l'algorithme en fonction du nombre de tests.

### 3.4. Analyse

Plus généralement , il est important de bien comprendre qu'un algorithme glouton __ne donne pas forcement une solution optimale__. Pour certains types de problèmes, il est possible de démontrer qu'un algorithme glouton donnera toujours une solution optimale, mais cela dépasse largement le cadre de ce cours.

Le problème du sac à dos fait partie des 21 problèmes [NP-complets](https://interstices.info/glossaire/np-complet/) identifiés par Richard Karp en 1972. Ces 21 problèmes sont réputés comme les problèmes les plus difficiles en optimisation combinatoire. Un grand nombre d’autres problèmes NP-complets peuvent se ramener à ces 21 problèmes de base. (Source : interstices.info)

## 4. Problème du rendu de monnaie

Vous êtes des commerçants, vous avez à votre disposition un nombre illimité de pièces de :

![](./assets/monnaie.png)

Vous devez rendre la monnaie à un client à l'aide de ces pièces. La contrainte est d'utiliser le moins de pièces possible.

### 4.1. À Faire

1. Trouver une méthode gloutonne et écrire l'algorithme permettant d'effectuer un rendu de monnaie (en utilisant le moins possible de pièces).
2. Vous devez rendre la somme de 2,63 €, appliquez la méthode que vous venez de mettre au point. Combien de pièces avez-vous utilisées ?

### 4.2. À Faire

1. Vous disposez dorénavant du système de monnaie suivant {1, 2, 5, 10, 20, 50, 100} en pièces d'euro. Combien de pièces sont à rendre pour 49 € ?
2. Vous disposez dorénavant du système de monnaie suivant {1, 3, 6, 12, 24, 30} en pièces d'euro. Combien de pièces sont à rendre pour 49 € ? Est-ce la solution optimale ?
