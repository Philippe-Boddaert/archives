# Recherche dichotomique

## À Faire en guise d'avant-propos

![](./assets/comparatif.jpg)

- Ce [lien](https://philippe-boddaert.gitlab.io/dichotomie/index.html) contient une liste de 350 prénoms. __Combien de temps faut-il pour trouver le vôtre ?__ (Cliquez sur votre prénom pour obtenir le temps mis pour le recherche)
- Ce [lien](https://philippe-boddaert.gitlab.io/dichotomie/index.html?fast=true) contient la même liste de 350 prénoms mais _triée par ordre alphabétique_. __Combien de temps faut-il pour trouver le vôtre ?__ (Cliquez sur votre prénom pour obtenir le temps mis pour le recherche)
- Avez-vous améliorez votre score ? Selon vous, par quel procédé ?

## 1. Recherche dans un tableau : Méthode brute

Voici un algorithme de recherche d’un élément dans un tableau de $`n`$ valeurs.

![](./assets/recherche_brute.jpeg)

1. Expliquer le fonctionnement de cet algorithme.
2. Expliquer pourquoi l'intervalle $`[0;n[`$ ?
3. Formuler le pire des cas de cet algorithme et estimer sa complexité dans ce cas.

### 1.1. À Faire

Écrire la fonction `recherche(tableau, x)` et faites afficher le temps d’exécution avec un tableau trié de 10 000 nombres en ___recherchant le dernier___ :

Pour mesurer le temps, il est possible d'utiliser le code suivant :
```python
# pour remplir un tableau de 10000 nombres et le trier en place
import time
from random import randint

t = [randint(0,100) for i in range(10000)] # pour le créer

t.sort() # pour le trier

# Pour mesurer le temps d’exécution
debut = time.process_time()

#### code à mesurer ici #####

fin = time.process_time()

temps = fin - debut
```

## 3. Recherche dans un tableau : Méthode par dichotomie

### 3.1. Principe 

Imaginons que nous recherchions une personne dans un annuaire classé par ordre alphabétique

- On ouvre l’annuaire au centre
- La personne est soit sur cette page, soit avant soit après
- En supposant qu’elle soit avant ( on la recherche donc dans la première moitié de l’annuaire)
- On ouvre la page du milieu de cette première moitié...
- On recommence jusqu’à obtenir la bonne page ou conclure que la personne n’est pas dans
l’annuaire

### 3.2. À Faire

Si l’annuaire contient les noms des 7 000 000 000 de terriens. Combien de fois faut-il que je divise 7 milliards par 2 pour qu’il n’en reste qu’un ?

### 3.3. L'algorithme

![](./assets/recherche_dichotomie.jpeg)

#### À Faire

Faites fonctionner cet algorithme pour le tableau $`tableau = [1, 1, 3, 10, 16, 18, 19, 22, 35, 41, 46, 52, 55, 59, 60, 65, 67, 80, 91, 100]`$ et l'élément recherché $`x=65`$.

Pour cela, compléter le tableau suivant :

| debut | fin  | m    | t[m] |
| :-----: | :----: | :----: | :----: |
| 0 | 19 |      |      |
|  |  | | |
|  |  | | |
|  |  | | |
|  |  | | |
|  |  | | |

### 3.4. Étude de l'algorithme

#### Terminaison

Pour démontrer la terminaison de cet algorithme il faut construire, comme pour toutes les itérations Tant que, une quantité positive à valeurs dans N, strictement décroissante : le __variant de boucle__.

Notons, $`v_i = fin - debut + 1`$ et étudions son comportement à chaque itération $`i`$ :

- Au départ,  $`v_0 = fin - debut + 1= n - 1 - 0 + 1= n`$
- À la 1ème itération, 3 cas sont à envisager :
  - L'élement $`x`$ se trouve à l'emplacement $`m = \lfloor \frac{ debut + fin}{2} \rfloor = \lfloor \frac{ n - 1}{2} \rfloor`$, dans ce cas l'algorithme s'arrête,
  - L'élément $`x`$ se trouve à un indice > $`m`$, dans cas : 
		- $`debut = m + 1`$
		- $`v_1 = fin - (m + 1) + 1 = n - 1 - \lfloor \frac{ n - 1}{2} \rfloor = \lceil \frac{ n - 1}{2} \rceil \simeq \frac{n}{2}`$,
		- d'où $`v_1 < v_0`$
  - L'élément $`x`$ se trouve à un indice < $`m`$, dans cas : 
    - $`fin = m - 1`$ 
    - $`v_1 = (m - 1) - debut + 1 = \lfloor \frac{ n - 1}{2} \rfloor \simeq \frac{n}{2}`$, 
    - d'où $`v_1 < v_0`$
  - À la fin de l'itération, soit l'algorithme s'arrête, soit $`v_1 < v_0`$, ainsi le variant $`v`$ décroit.
- Ce raisonnement s'applique à toute itération suivante...jusqu'à une itération $`k`$ où :
  - $`debut = fin + 1`$
  - $`v_k = fin - debut + 1 = fin - (fin + 1) + 1 = 0`$


Ainsi, $`v`$ un variant pour la boucle `tant que`, i.e une quantité :

- entière,
- positive,
- strictement décroissante.

En conclusion, l'algorithme `Recherche dichotomique` termine.

#### Correction

__Propriété $`P`$: Si $`x`$ est présent dans $`t`$, alors c'est nécessairement entre les indices $`debut`$ et $`fin`$. $`x \in t \implies x \in t[debut; fin]`$__

Pour prouver qu'il s'agit bien d'un invariant de boucle, on va supposer que notre propriété $`P`$ est vérifiée au début du corps de la boucle, et on va prouver que c'est encore le cas à la fin.

Après avoir défini `m`, examinons les tests successifs. Tout d'abord, si l'on a bien `t[m] = x`, alors on renvoie `Vrai` ce qui est correct, ayant effectivement `m` dans `t`. Dans ce cas, l'invariant ne sert à rien puisque l'on a effectivement trouvé l'élément recherché.

Maintenant, deux cas sont possibles :

- si `t[m] < x`, puisque `t` est trié, la valeur `x` ne peut pas être présent dans `t[debut;m]`. Ainsi, si `x in t[debut;fin]`, alors nécessairement `x in t[m + 1;fin]`. Ainsi, en posant `debut = m + 1`, on a `x in t[debut;fin]`.
- Sinon, sachant de plus que `t[m] != x`, cela signifie que `t[m] > x` et donc que si `x in t[debut;fin]`, alors `x in t[debut;m]`. Ainsi, en posant `fin = m - 1`, on a `x in t[debut;fin]`.

Dans tous les cas, à la fin de l'exécution de la boucle (si l'on y est toujours), la propriété $`P : x \in t \implies x \in t[debut; fin]`$ est encore vérifiée. 

La propriété $`P`$ est donc bien un invariant de la boucle ; l'algorithme `Recherche dichotomique` est correct.

#### Complexité

Pour étudier la complexité, nous allons nous intéresser à la boucle : au niveau de la boucle, combien doit-on effectuer d'itérations pour un tableau de taille $`n`$ dans le cas le plus défavorable ?

Sachant qu'à chaque itération de la boucle on divise le tableau en 2, cela revient donc à se demander :

- combien de fois faut-il diviser le tableau en 2 pour obtenir, à la fin, un tableau comportant un seul entier ? 
- Autrement dit, combien de fois faut-il diviser n par 2 pour obtenir 1 ?

Mathématiquement cela se traduit par l'équation $`\frac{n}{2^p} = 1`$ avec $`p`$ le nombre de fois qu'il faut diviser $`n`$ par 2 pour obtenir 1. Il faut donc trouver $`p`$ !

A ce stade il est nécessaire d'introduire une nouvelle notion mathématique : le "logarithme base 2" noté $`\log_2`$.

Par définition, $`\log_2(2^x) = x`$

Nous avons donc : $`\frac{n}{2^p} = 1 \implies n=2^p \implies \log_2(n)=\log_2(2^p) = p`$, nous avons donc $`p = \log_2(n)`$

Pour un parcours séquentiel, la complexité en temps est en $`\mathcal{O}(n)`$ (complexité linéaire).

Dans la recherche dichotomique, la complexité est en $`\mathcal{O}(\log_2(n))`$ (complexité logarithmique).

### 3.5. À Faire

1. Écrire la fonction `recherche_dichotomie(tableau,x)` dans le même script que la fonction précédente et comparer leur temps d’exécution pour un même tableau trié de 10 000, 100 000, 1 000 000 et 10 000 000 nombres.

2. Ci-dessous un graphique représentant le temps d'exécution de la recherche séquentielle du dernier élément du tableau en fonction de la taille du tableau (10000 et 100000)

![](./assets/recherche_sequentielle.png)

Ce graphique est obtenu grâce à l'extrait de code suivant :

```python
import matplotlib.pyplot as plt

tailles = [10000, 100000]
temps_sequentiel = []
   
for n in tailles:
    t = [randint(0,100) for i in range(n)] # pour créer un tableau de taille n

    t.sort() # pour trier le tableau

    # Calcule le temps d'exécution d'une recherche séquentielle
    debut = time.process_time()
    
    recherche(t, t[-1])

    fin = time.process_time()

    temps_sequentiel.append(fin - debut)

# Affiche la courbe du temps de recherche en fonction de la taille du tableau
plt.plot(tailles, temps_sequentiel, label="Recherche séquentielle", marker=".")
   
plt.title("Comparaison Algorithmes de recherche")
plt.xlabel("Taille du tableau")
plt.ylabel("Temps (sec)")
plt.legend()
   
plt.show()
```

Adapter cet extrait de code pour qu'il affiche, sur le même graphique, la courbe du temps d'exécution de la recherche dichotomique pour un tableau trié de 10 000, 100 000, 1 000 000 et 10 000 000 nombres.

## 4. Conclusion

Nous avons vu sur quelques exemples en quoi la recherche dichotomique était une méthode efficace pour rechercher un élément dans un tableau trié.

Cette méthode n'est qu'un exemple de nombreux autres algorithmes utilisant une méthode générale appelée "___Diviser pour régner___".

En informatique, diviser pour régner (du latin « _Divide ut imperes_ », divide and conquer en anglais) est une technique algorithmique consistant à :

- __Diviser__ : découper un problème initial en sous-problèmes ;
- __Régner__ : résoudre les sous-problèmes (récursivement ou directement s'ils sont assez petits) ;
- __Combiner__ : calculer une solution au problème initial à partir des solutions des sous-problèmes.

Cette technique fournit des algorithmes efficaces pour de nombreux problèmes, comme la **recherche d'un élément dans un tableau trié** (recherche dichotomique), **le tri** (tri fusion, tri rapide), **la multiplication de grands nombres** (algorithme de Karatsuba) ou **la transformation de Fourier discrète** (transformation de Fourier rapide) (_Source : Wikipédia_)

## 5. Pour aller plus loin : Jeu du "Plus petit, Plus grand"

Le jeu du "Plus petit, Plus grand" suit les règles suivantes :

1. Le programme choisit un nombre au hasard entre 1 et 100,
2. L'utilisateur choisit un nombre,

3. L'ordinateur indique si le nombre choisi par l'utilisateur est plus petit, plus-grand ou est le mot à deviner
4. On recommence à partir de l'étape 2. jusqu'à ce que l'utilisateur ait trouvé le nombre.
5. L'ordinateur affiche en combien de coups l'utilisateur a trouvé le nombre.

__À Faire__ :

1. Écrire une implantation en Python du jeu "Plus petit, Plus grand"

2. En combien d'étapes au plus peut-on deviner le nombre:

   - Si on procède au hasard ?

   - Si on applique la méthode de la dichotomie ?
   - Quels nombres représentent le pire des cas (i.e ceux nécessitant le plus de coups) ?
