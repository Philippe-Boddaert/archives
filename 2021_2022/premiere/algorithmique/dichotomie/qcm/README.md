# QCM - Dichotomie

## Question 1

Pour pouvoir utiliser un algorithme de recherche par dichotomie dans un tableau, quelle précondition doit être vraie ?

__Réponses__ :

- [ ] A. le tableau doit être trié
- [ ] B. le tableau ne doit pas comporter de doublons
- [ ] C. le tableau doit comporter uniquement des entiers positifs
- [ ] D. le tableau doit être de longueur inférieure à 1024

## Question 2

La fonction ci-dessous permet d’effectuer une recherche par dichotomie de l’index $`m`$ de l’élément $`x`$ dans un tableau $`t`$ de valeurs distinctes et triées.

```python
def dicho(x,t):
  g = 0
  d = len(t)-1
  while g <= d:
    m = (g+d)//2
    if t[m] == x:
      return m
    elif t[m] < x:
      g = m+1
    else:
      d = m-1
  return None
```

Combien de fois la cinquième ligne du code de la fonction (m = (g+d)//2) sera-t-elle exécutée dans l'appel dicho(32, [4, 5, 7, 25, 32, 50, 51, 60]) ?

__Réponses__ :

- [ ] A. 1 fois
- [ ] B. 2 fois
- [ ] C. 3 fois
- [ ] D. 4 fois

## Question 3

En utilisant une recherche dichotomique, combien faut-il de comparaisons avec l'opérateur == pour trouver une valeur dans un tableau trié de 1000 nombres, dans le pire cas ?

__Réponses__ :

- [ ] A. 3
- [ ] B. 10
- [ ] C. 1000
- [ ] D. 1024

## Question 4

Un algorithme de recherche dichotomique dans un tableau trié de taille $`n`$ nécessite, dans le pire des cas, exactement $`k`$ comparaisons.

Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur un tableau de taille $`2n`$ ?

__Réponses__ :

- [ ] A. $`k`$
- [ ] B. $`k + 1`$
- [ ] C. $`2k`$
- [ ] D. $`2k + 1`$

## Question 5

Avec un algorithme de recherche par dichotomie, combien de comparaisons sont-elles nécessaires pour s’assurer que 22 n’est pas dans le tableau suivant :

```python
[1, 5, 9, 12, 20, 21, 24, 32, 35, 40, 41, 47, 53, 60, 70]	
```

__Réponses__ :

- [ ] A. 2
- [ ] B. 4
- [ ] C. 7
- [ ] D. 13

## Question 6

Un professeur souhaite que ses élèves lui fassent un cadeau le jour de son anniversaire.

Pour connaitre cette date, il propose à ses élèves de la deviner grâce au procédé de recherche dichotomique.

Il y a 365 jours dans une année et les jours peuvent être représentés comme un tableau, dont un extrait est reproduit ici :

<table style="text-align:center;border : 1px solid black;">
  <tr style="border : 1px solid black;"><th>indice</th><td>0</td><td>1</td><td>2</td><td>...</td><td>364</td></tr>
<tr style="border : 1px solid black;"><th>valeur</th><td>1 Janvier</td><td>2 Janvier</td><td>3 Janvier</td><td>...</td><td>31 Décembre</td></tr>
</table>


Pour rappel, l'algorithme de recherche dichotomique est le suivant :

![](../assets/recherche_dichotomie.jpeg)

Trouver la date d'anniversaire du professeur consiste à trouver le paramètre $`x`$ en entrée de l'algorithme.

Les réponses du professeur sont reproduites dans le tableau de valeurs d'exécution de l'algorithme de recherche dichotomique :

| debut | fin  |  m   |  Réponse du professeur   |
| :---: | :--: | :--: | :----------------------: |
|   0   | 364  | 182  |  C'est moins que $`m`$   |
|  |  |  |   C'est moins que $`m`$    |
|  |  |  |    C'est plus que $`m`$    |
|  |  |  |   C'est moins que $`m`$    |
|  |  |  |    C'est plus que $`m`$    |
|  |  |  |   C'est moins que $`m`$    |
|  |  |  |    C'est plus que $`m`$    |
|  |  |  | $`m`$ est le bon indice !! |

1. Compléter le tableau avec les valeurs cohérentes
2. Quel est l'indice $`m`$ correspondant à la date d'anniversaire du professeur ?