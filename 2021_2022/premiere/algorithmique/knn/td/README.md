# Le Choixpeau magique d'Harry Potter

## Description

À l’entrée à l’école de Poudlard, le Choixpeau magique répartit les élèves dans les différentes maisons (Gryffondor, Serpentard, Serdaigle et Poufsouffle) en fonction de leur courage, leur loyauté, leur sagesse et leur malice.

![](https://cdn.pixabay.com/photo/2015/03/16/04/15/sorting-hat-675364_960_720.jpg)

_Source : Pixabay_

Le Choixpeau magique dispose d’un fichier CSV ([choixpeauMagique.csv](./assets/choixpeauMagique.csv)) dans lequel sont répertoriées les données d’un échantillon d’élèves.

|   Nom    | Courage | Loyauté | Sagesse | Malice |     Maison |
| :------: | :-----: | :-----: | :-----: | :----: | ---------: |
|  Adrian  |    9    |    4    |    7    |   10   | Serpentard |
|  Andrew  |    9    |    3    |    4    |   7    | Gryffondor |
| Angelina |   10    |    6    |    5    |   9    | Gryffondor |
| Anthony  |    2    |    8    |    8    |   3    |  Serdaigle |

On souhaite que Choixpeau magique oriente les nouveaux élèves :

|   Nom    | Courage | Loyauté | Sagesse | Malice |
| :------: | :-----: | :-----: | :-----: | :----: |
| Hermione |    8    |    6    |    6    |   6    |
|  Drago   |    6    |    6    |    5    |   8    |
|   Cho    |    7    |    6    |    9    |   6    |
|  Cédric  |    7    |   10    |    5    |   6    |

## 1. Modéliser un élève

On modélise un élève par un dictionnaire.

Exemple pour l'élève Bellatrix :

```python
{'Nom': 'Bellatrix', 'Courage': 10, 'Loyauté': 4, 'Sagesse': 9, 'Malice': 9, 'Maison': 'Serpentard'}
```

1. Donner la modélisation de l'élève Anthony.
2. En utilisant la distance de `Manhattan`, Vérifier que la distance entre Hermione et Adrian est égale à 8.
3. Quelle est la distance entre Arthur et Drago ?
4. Écrire la fonction `distance` qui prend deux élèves en paramètre et renvoie la distance entre ces deux élèves. Ne pas oublier de préciser la documentation et donner au moins un test.

## 2. Charger les données de la table

1. Se reporter au chapitre sur les tables afin d'écrire une fonction qui permet de récupérer les données des élèves d'un fichier CSV pour les stocker dans une liste. On doit obtenir une liste de dictionnaires.

## 3. Trouver la maison majoritaire

1. Écrire une fonction `frequence_des_maisons(table)` qui prend en paramètre une table d'élèves et qui renvoie le dictionnaire des fréquences des maisons. Vous devez obtenir :
```python
{'Serpentard': 12, 'Gryffondor': 17, 'Serdaigle': 11, 'Poufsouffle': 10}
```
2. Écrire une fonction `maison_majoritaire(table)` qui prend en paramètre une table d'élèves et qui renvoie le nom de la maison la plus représentée.

## 4. Algorithme des 7 plus proches voisins

On dispose d'une `table` et d'un `nouveau` qui n'a pas encore de maison. On cherche les 7 plus proches voisins de ce nouveau.

1. Écrire l'algorithme des 7 plus proches voisins.
2. Implémenter cet algorithme en python.

## Partie 5 - Attribuer une maison

On dispose d'une `table` et d'un `nouveau` qui n' a pas encore de maison. On cherche à lui attribuer une maison.

Implémenter en python l'algorithme de l'attribution :

1. Trouver dans la table les 7 plus proches voisins du nouvel élève.
2. Parmi ces proches voisins, trouver la maison majoritaire.
3. Renvoyer la maison majoritaire.

Faire un test avec 'Hermione'. Vous devez obtenir les 7 voisins suivants :

```python
[{'nom': 'Cormac', 'Courage': 9, 'Loyauté': 6, 'sagesse': 5, 'malice': 4, 'maison': 'Gryffondor'}, {'nom': 'Milicent', 'Courage': 9, 'Loyauté': 3, 'sagesse': 5, 'malice': 6, 'maison': 'Serpentar'}, {'nom': 'Neville', 'Courage': 10, 'Loyauté': 5, 'Sagesse': 6, 'Malice': 4, 'Maison': 'Gryffondor'}, {'nom': 'Padma', 'Courage': 6, 'Loyauté': 6, 'Sagesse': 6, 'Malice': 9, 'Maison': 'Serdaigle'}, {'nom': 'Susan', 'Courage': 5, 'loyauté': 6, 'Sagesse': 5, 'malice': 5, 'Maison': 'Poufsouffle'}, {'nom': 'Angelina', 'Courage': 10, 'loyauté': 6, 'Sagesse': 5, 'Malice': 9, 'Maison': 'Gryffondor'}, {'nom': 'Colin', 'Courage': 10, 'Loyauté': 7, 'Sagesse': 4, 'Malice': 7, 'Maison': 'Gryffondor'}]
```

La maison attribuée pour 'Hermione' est 'Gryffondor'.

## 6. Pour aller plus loin

1. Exécuter l'attribution de la classe aux nouveaux élèves avec $`k=3`$, $`k=5`$, $`k=9`$ et comparer les résultats d'attribution obtenus.
2. Implémenter la fonction qui calcule la distance `euclidienne` et déterminer la classe des nouveaux élèves. Comparer les résultats avec ceux obtenus avec la distance de `Manhattan`.
