---
title : Algorithme des k plus proches voisins
author : M. BODDAERT, basé sur les travaux de Germain BECKER, Lycée Mounier, ANGERS
license : CC-BY-NC-SA
---
# Algorithme des _k_ plus proches voisins

## 1. Contexte

- Cet algorithme a été introduit en 1951 par Fix et Hodges dans un rapport de la faculté de médecine aéronautique de la US Air Force.
- Le **machine learning** est un concept utilisé en **intelligence artificielle**. Il s'agit d'entraîner une machine à apprendre à reconnaitre certaines formes. L'algorithme des ___$`k`$ plus proches voisins (knn : k-nearest neighbors)___ donne une méthode qui permet la mise en place de l'entrainement.
- Son principe peut être résumé par cette phrase : ***Dis-moi qui sont tes amis et je te dirai qui tu es***.

## 2. Exemple introductif

Dans cette introduction, nous considérons un jeu de données constitué de la façon suivante :

- les données sont réparties suivant deux types : le type 1 et le type 2,
- les données n'ont que deux caractéristiques : caractéristique 1 et caractéristique 2,

Imaginez la situation suivante dans un jeu :

- Vous avez deux types de personnages : les **fantassins** (type 1 : "fantassin") et les **chevaliers** (type 2 : "chevalier").
- Vous avez deux types de caractéristiques : la **force** (caractéristique 1 : nombre entre 0 et 20) et le **courage** (Caractéristique 2 : nombre entre 0 et 20 ).
- Vous avez une collection de personnages dont vous connaissez les caractéristiques et le type.

Vous introduisez un nouveau personnage dont vous ne connaissez pas le type. Vous possédez les caractéristiques de ce nouveau personnage. 

Le but de l'algorithme *KNN*  est de répondre à la question : ***quel le type de ce nouveau personnage ?***

### 2.1. Visualisation du problème

Les données sont stockées dans un fichier csv téléchargeable : [fichier csv à télécharger](./assets/personnages.csv)

Voici un aperçu des données :

| Nom  | Force | Courage | Type |
| :--: | --: | --: | --: |
| Ario | 20 |  1 | Fantassin |
| Axal | 10 | 10 | Chevalier |
| Cargo | 20 | 11 | Fantassin |
| Clark | 2 | 12 | Chevalier |
| Fancy | 9 | 5 | Fantassin |
| Faq | 15 | 11 | Fantassin |
| ... | ... | ... | ... |

Voici une représentation de ces données :

![](./assets/Figure_1.png)

Nous introduisons une nouvelle donnée (appelée *cible* dans notre exemple) avec ses deux caractéristiques : une force de 12 et un courage de 12,5 . 

![](./assets/Figure_3.png)

Le but de l'algorithme KNN des $`k`$ plus proches voisins est de déterminer le type de la cible.

Dans un premier, il faut fixer le nombre de voisins. Nous allons choisir $`k=7`$. Nous verrons par la suite l'importance de ce choix arbitraire.

Voici une nouvelle représentation avec la cible et la recherche des 7 voisins les plus proches proches, ceux qui se trouvent dans le cercle bleu :

![](./assets/Figure_2.png)

Parmi ses 7 voisins, il y a 2 voisins de type "chevalier" et 5 voisins de type "fantassin", Il est donc probable que notre cible soit de type "fantassin".

### 2.2. Remarques

1. La valeur $`k=7`$ est ici un choix arbitraire. Cette valeur doit néanmoins être choisie judicieusement : trop faible, la qualité de la prédiction diminue ; trop grande, la qualité de la prédiction diminue aussi. Il suffit d'imaginer qu'il existe une classe prédominante en nombre. Avec une grande valeur de $`k`$, cette classe remporterait la prédiction à chaque fois.
2. Nous avons utilisé une distance schématisée par un disque. Ce choix est discutable. Il faut faire attention à la distance Euclidienne qui n'a de sens que dans un repère orthonormé. Nous aurions pu choisir une autre distance.
3. Il est facile de représenter les données avec 1 à 3 caractéristiques. Nous ne pouvons pas représenter des données avec des caractéristiques supérieures à 3 mais l'algorithme reste opérationnel.

## 3. Algorithme

A partir d'un jeu de données  et d'une donnée *cible*, l'algorithme des $`k`$ plus proches voisins déterminer les $`k`$ données les plus proches de la cible.

Voici un algorithme permettant de résoudre ce problème :

**Données et préconditions :**

- une table `donnees` de taille `n` contenant les données et leurs classes
- une donnée cible : `cible`
- un nombre `k` inférieur à `n`
- une règle permettant de calculer la *distance* entre deux données

**Résultat :** un tableau contenant les `k` plus proches voisins de la donnée cible.

**Algorithme :**

1. Créer une table `distances_voisins` contenant les éléments de la table `donnees` et leurs distances avec la donnée `cible`.
2. Trier les données de la table `distances_voisins` selon la distance croissante avec la donnée `cible`
3. Renvoyer les `k` premiers éléments de cette table triée.

## 4. Choix de la valeur $`k`$

### 4.1. Impact du choix de la valeur $`k`$

La valeur de $`k`$ est très importante, elle doit être choisie judicieusement car elle a une influence forte sur la prédiction. Regardons le résultat de la prédiction pour différentes valeurs de $`k`$ sur notre exemple.

**Exemple 1** : le 1 plus proche voisin

Si $`k = 1`$, cela revient à chercher la donnée la plus proche de notre élément cible. Ici, on se rend compte que sa classe est "Chevalier" (point bleu) donc on classerait la cible comme étant de type "Chevalier".

![](./assets/Figure_7.png)

**Exemple 2** : les 3 plus proches voisins

On se rend compte que la classe majoritaire dans les 3 plus proches voisins est "Fantassin" donc on classerait la cible comme étant de type "Fantassin".

![](./assets/Figure_8.png)

**Exemple 3** : les 9 plus proches voisins

On se rend compte que la classe majoritaire dans les 9 plus proches voisins est "Chevalier" donc on classerait la cible comme étant de type "Chevalier".

![](./assets/Figure_9.png)

En poursuivant, si on choisit $`k = n`$ (le nombre total de données), alors la prédiction serait toujours "Fantassin" car c'est la classe majoritaire de l'échantillon. Il est donc incorrect de pense que plus la valeur de $`k`$ augmente meilleure sera la prédiction, c'est plus complexe que cela.

__Mais alors...Quelle valeur de $`k`$ faut-il choisir ?__

### 4.2. Choix de la valeur de $`k`$ par expérimentation

Pour trouver une bonne valeur de $`k`$ il est possible d'appliquer le protocole expérimental suivant :

- Séparer les données en deux paquets : un paquet pour *entraîner le modèle* (90 % par exemple), un second pour *tester le modèle* (les 10% restants)
- Utiliser le premier paquet comme nouveau jeu de données et appliquer l'algorithme knn sur les éléments qui ont été retirés
- Comparer les réponses de l'algorithme avec les réponses attendues (on connaît la classe des éléments retirés donc on peut comparer)

En appliquant ce protocole à différentes valeurs de $`k`$, on peut déterminer quelle valeur fournit les meilleurs résultats.

## 5. Choix de la distance

L'algorithme des plus proches voisins repose presque entièrement sur la distance entre deux données. Dans les exemples vus précédemment, c'est la distance "naturelle" qui a été choisie (celle "à vol d'oiseau" ou distance euclidienne).

D'autres distances existent et le choix de la distance à utiliser  pour l'algorithme des k plus proches voisins peut aboutir à des prédictions différentes.

### 5.1. **La distance Euclidienne (dans un repère orthonormé)**

Soientt deux données $`d_1`$ et $`d_2`$ de coordonnées respectives $`(x_1,y_1)`$ et $`(x_2,y_2)`$.

$`distance(d_1, d_2) = \sqrt{(x_1-x_2)^2 + (y_1 - y_2)^2}`$

### 5.2. La distance de Manhattan

Soient deux données $`d_1`$ et $`d_2`$ de coordonnées respectives $`(x_1,y_1)`$ et $`(x_2,y_2)`$.

$`distance(d_1, d_2) = |x_1-x_2| + |y_1 - y_2|`$

### 5.3. La distance de Tchebychev

Soient deux données $`d_1`$ et $`d_2`$ de coordonnées respectives $`(x_1,y_1)`$ et $`(x_2,y_2)`$.

$`distance(d_1, d_2) = max(|x_1-x_2|, |y_1 - y_2|)`$

## 6. Les données et l'apprentissage automatique

### 6.1. Notion d'apprentissage

La plupart des algorithmes d'apprentissage automatique cherchent à *apprendre* quelque chose du jeu de données qui lui est fourni, c'est-à-dire à remplacer les données par un *modèle* (une sorte de "règle" permettant de classer les données, de prendre une décision, etc.). Autrement dit, un tel algorithme tente de "comprendre" les données pour en déduire un modèle, on peut voir cela comme un apprentissage *intelligent*.

### 6.2. Big data et deep learning

Le **deep learning** (ou *apprentissage profond*) est une famille de méthode d'apprentissage automatique. Cette technique a été rendue possible notamment par l'arrivée de données en volume massifs (**big data**) permettant aux algorithmes d'apprendre à résoudre un problème. Ces données en masse permettent de disposer d'une importante quantité de données sur lesquelles entrainer et affiner les algorithmes.

Les stratégies mises en place par les géants du numérique (GAFAM) tournent entièrement autour de la récolte de données de leurs clients sur n'importe quel sujet pour "nourrir" leurs algorithmes d'apprentissage. C'est ainsi qu'Amazon arrive à nous proposer des "suggestions d'achat". Google quant à lui nous utilise lorsque nous devons prouver que nous ne sommes pas un robot. En effet, qui n'a pas déjà vu un écran de ce genre ?

![](http://info-mounier.fr/premiere_nsi/algorithmique/data/recaptcha.png)

En cliquant aux bons endroits, on assigne une __classe__ aux différentes parties de l'image (feu de circulation ou non) et celles-ci viennent alimenter les bases de données sur lesquelles les algorithmes s'entraînent. Ils serviront ensuite aux voitures autonomes de Google à repérer les feux tricolores sur la route.

## 6.3. Qualité des données et dangers

La qualité des données est primordiale dans l'apprentissage automatique car ce sont elles qui définissent presque entièrement la qualité des résultats des algorithmes. Voici quelques exemples, en vrac :

- Si la base de données utilisée pour entraîner __un algorithme de détourage n'est constituée que de photos d'humains__, il y a des fortes chances que __l'algorithme ne parvienne pas à détourer un chat correctement__. C'était le cas au départ pour l'algorithme du site [remove.bg](http://www.remove.bg/) mais les données d'entraînement sont désormais plus complètes et les détourages fournissent des résultats impressionnants.
- L’algorithme mis en place à partir de 2015 par Amazon ([source](https://www.reuters.com/article/us-amazon-com-jobs-automation-insight/amazon-scraps-secret-ai-recruiting-tool-that-showed-bias-against-women-idUSKCN1MK08G#_blank)) pour __faciliter le recrutement de talents utilisait des données de centaines de milliers de curriculum vitae (CV)__ reçus par Amazon au cours des dix dernières années en vue de noter de nouvelles candidatures. L’algorithme a été rapidement __suspendu car il discriminait grandement les femmes__. En effet, les CV d'entraînement comprenait une __écrasante majorité d'hommes__, l’algorithme ne laissant du coup aucune chance aux nouvelles candidates pourtant qualifiées. On dit dans ce cas que les données sont *biaisées*.
- Aux Etats-unis, ils prédisent __les taux de criminalité dans les quartiers__ et déploient les effectifs policiers en conséquence. Mais les données sur lesquelles __les systèmes sont entrainés sont également biaisés car déséquilibrés avec davantage de personnes de couleurs__ par exemple (voir [source](https://www.technologyreview.com/2019/02/13/137444/predictive-policing-algorithms-ai-crime-dirty-data/)).
