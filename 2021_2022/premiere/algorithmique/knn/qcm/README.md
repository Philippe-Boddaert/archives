# Algorithme KNN

## Question 1. Algorithme KNN

À quelle catégorie appartient l’algorithme des k plus proches voisins ?

### Réponses

- [ ] A. algorithmes de classification et d’apprentissage
- [ ] B. algorithmes de tri
- [ ] C. algorithmes gloutons
- [ ] D. algorithmes de recherche de chemins

## Question 2. Classification

On a représenté sur un quadrillage les éléments de quatre classes (chaque classe est représentée par un carré, un triangle, un losange ou un disque) ainsi qu’un nouvel élément X.

![](https://pixees.fr/informatiquelycee/n_site/img/bns_3.png)

En appliquant l'algorithme des k plus proches voisins pour la distance usuelle dans le plan, avec k=5, à quelle classe est affecté le nouvel élément X ?

### Réponses

- [ ] A. la classe des losanges
- [ ] B. la classe des triangles
- [ ] C. la classe des carrés
- [ ] D. la classe des disques

## Question 3. Algorithme des K plus proches voisins

Une seule des affirmations suivantes est vraie :

### Réponses

- [ ] A. L'algorithme des k plus proches voisins a pour but de déterminer dans un ensemble de données le sous-ensemble à k éléments qui sont les plus proches les uns des autres.
- [ ] B. L'algorithme des k plus proches voisins a pour but de déterminer les k plus proches voisins d'une observation dans un ensemble de données.
- [ ] C. L'algorithme des k plus proches voisins a pour but de déterminer les éléments d'un ensemble de données appartenant à une même classe.
- [ ] D. L'algorithme des k plus proches voisins a pour but de déterminer la classe d'une observation à partir des classes de ses k plus proches voisins.

## Question 4. Prédiction de localisation

On dispose d’une table de données de villes européennes. On utilise ensuite l’algorithme des $k$ plus proches voisins pour compléter automatiquement cette base avec de nouvelles villes.

Ci-dessous, on a extrait les 7 villes connues de la base de données les plus proches de Davos.

| Ville | Pays | Distance jusqu'à Davos |
| :--: | :--: | :--: |
| Berne | Suisse | 180km|
| Innsbruck | Autriche | 130km |
| Milan | Italie | 150km |
| Munich | Allemagne | 200km |
| Stuttgart | Allemagne | 225km |
| Turin | Italie | 250km |
| Zurich | Suisse | 115km|

En appliquant l’algorithme des 4 plus proches voisins, quel sera le pays prédit pour la ville de Davos ?

### Réponses

- [ ] A. Italie
- [ ] B. Autriche
- [ ] C. Suisse
- [ ] D. Allemagne

## Question 5. Calcul de distance

Soient les points de coordonnées suivantes : A(4, 2) B(1, 5) C(3, 2) D(2, 0) E(7, 6) F(6, 3) G(6, 2) H(8, 5)

En utilisant la distance euclidienne, quels sont les deux plus proches voisins du point I(4,5)) ?

### Réponses

- [ ] A. Les points E et F
- [ ] B. Les points C et D
- [ ] C. Les points G et H
- [ ] D. Les points A et B