# Algorithmique

## 1. Contexte

Vous allez vous laver les mains! Vous pratiquer un algorithme sans savoir que cela s'appelle comme cela.

Imaginez que vous rencontrez quelqu'un à qui vous devez tout expliquer, pas à pas, alors vous décrivez l'algorithme de lavage de mains suivant :

![](./assets/lavage.jpeg)

_Source : villemin.gerard.free.fr_

Le lavage de mains est décrit comme un algorithme décomposé en 7 opérations simples.

## 2. Définition

> Un **algorithme** est une __suite finie et non ambiguë d'instructions__ et d’opérations permettant de résoudre un problème donné.

> L'__algorithmique__ est l'étude et la production de règles impliquées dans la définition et la conception d'algorithmes.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Mu%E1%B8%A5ammad_ibn_M%C5%ABs%C4%81_al-Khw%C4%81rizm%C4%AB.png/180px-Mu%E1%B8%A5ammad_ibn_M%C5%ABs%C4%81_al-Khw%C4%81rizm%C4%AB.png)

_Source : Wikipedia_

Le mot algorithme fait référence au nom d’un mathématicien arabe du moyen âge : __Al-Khawarizmi__, il a été le premier a décrire une méthode claire pour la résolution d’équation en 825.

## 3. Intérêts

Un algorithme permet la __transmission d'un savoir faire__.

Pour reprendre l'exemple du lavage des mains, l'avantage est que n'importe qui peut se servir de cette description. Elle est valable pour vous, un membre de votre famille, votre correspondant au Canada …

- l'algorithme est __universel__ : il fonctionne quelle que soit la machine utilisée. Cependant une transcription dans le langage propre de la machine utilisée sera indispensable.
- il est __paramétrable__ : il est possible de choisir des valeurs avant de lancer l'exécution de l'algorithme.
- il est __exigeant__ : il doit décrire clairement, précisément et complétement les étapes à suivre pour réaliser une tâche.
- il peut prendre des __formes diverses__ : texte en langage naturel, algorigramme...Il permet d'expliciter clairement les idées de solution d'un problème indépendamment d'un __langage de programmation__

![](./assets/algorigramme.png)

