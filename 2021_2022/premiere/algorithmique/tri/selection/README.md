# Tri par sélection

![](./assets/podium.jpg)

_Source : Needpix_

## 1. Principe

Quand on considère une collection $`l`$ de $`n`$ éléments, triée dans l'ordre croissant :

- Celui à l'indice 0 est le minimum des $`n`$ éléments, i.e $`l[0:n-1]`$,
- Celui à l'indice 1 est le minimum des $`n - 1`$ éléments suivants, i.e $`l[1:n-1]`$,
- Celui à l'indice 2, le minimum des $`n - 2`$ suivants, i.e $`[2:n-1]`$,
- etc...

## 2. Illustration

Déroulons l'algorithme de tri sur le tableau $`[6, 4, 1, 8, 5, 10, 7, 3, 2, 9]`$ via l'[Illustration animée](https://philippe-boddaert.gitlab.io/tri/tri.html?d=6,%204,%201,%208,%205,%2010,%207,%203,%202,%209&sti=true&a=s&sle=true&slo=false&sco=true)

### 2.1. À Faire

Imaginons que l'on dispose de la fonction `indice_minimum` suivante :

```python
def indice_minimum(tableau, mini):
    '''
    Recherche le mnimum d'un tableau
    :param tableau: (list) Un tableau
    :param mini: (int) indice à partir duquel la recherche est effectuée
    :return: (int) L'indice où se situe le minimum du tableau
    :C.U:
        - len(tableau) > 0
        - 0 <= mini < len(tableau)
    :exemple:
        >>> indice_minimum([5, 4, 9, 1, 3], 0)
        3
        >>> indice_minimum([5, -4, 9, 1, 3], 0)
        1
        >>> indice_minimum([1, 1, 1, 1, 1], 0)
        0
        >>> indice_minimum([5, 4, 9, 1, 3], 2)
        3
    '''
    indice = mini
    for i in range(mini + 1, len(tableau)):
        if tableau[i] < tableau[indice]:
            indice = i
    return indice
```

Compléter la fonction `trier` qui implémente l'algorithme de tri par sélection.

```python
def trier(tableau):
    '''
    Trie un tableau
    :param tableau: (list) Un tableau
    :return: (None)
    :Effet de bord: Le tableau est trié par ordre croissant
    :exemple:
        >>> tableau = [5, 4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [1, 3, 4, 5, 9]
        True
        >>> tableau = [5, -4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [-4, 1, 3, 5, 9]
        True
    '''
    n = len(tableau)
    for i in range(n):
        ... # On recherche l'indice du minimum du tableau [i:n]
        ... # On place la valeur à l'indice i
```

## 3. Terminaison

Pour [rappel](../etude), déterminer si un algorithme termine revient à trouver un __variant__, i.e un entier positif qui décroit strictement au cours de l'algorithme.

### 3.1. Algorithme `indice_minimum`

L'algorithme `indice_minimum` contient une boucle, il s’agit donc de vérifier qu’elle se termine. 

Notons $`n`$ la taille du tableau en entrée, où $`n \in \mathbb{N}^+`$, $`mini`$, l'indice de début de recherche, où $`0 \leq mini \lt n`$.

Notons $`v = n - i - 1`$

Analysons le comportement de $`v`$ au cours de l'exécution :

| $`i`$ | $`v = n - i - 1`$ |
| :-----:  | :-----------------: |
| mini + 1 | $`n - (mini + 1) - 1 = n - mini - 2`$ |
| mini + 2 | $`n - (mini + 2) - 1 = n - mini - 3`$ |
| mini + 3 | $`n - (mini + 3) - 1 = n - mini - 4`$ |
| ... | ... |
| $`n - 1`$ | $`n - (n - 1) - 1 = 0`$ |

$`n - 1 - i`$, où $`i < n`$ est un variant pour la boucle `for`, i.e une quantité :

- entière,
- positive,
- strictement décroissante.

En conclusion, l'algorithme `indice_minimum` se termine.

### 3.2. Algorithme `trier`

L'algorithme `trier` contient une boucle, il s’agit donc de vérifier qu’elle se termine. 

$`n - 1 - i`$, où $`i < n`$ est un variant pour la boucle `for`, i.e une quantité :

- entière,
- positive,
- strictement décroissante.

L'algorithme `trier` fait appel à l'algorithme `indice_minimum`, l'algorithme `trier` termine si et seulement si l'algorithme `indice_minimum` termine. Or, la terminaison de l'algorithme `indice_minimum`a été prouvé dans le 3.1.

En conclusion, l'algorithme `trier` termine.

## 4. Correction

Pour [rappel](../etude), déterminer si un algorithme termine, il faut trouver un __invariant__, i.e un prédicat vrai à l'initialisation de l'algorithme et qui le reste jusqu'à la fin d'algorithme.

### 4.1. Algorithme `indice_minimum`

__Que peut-on dire de la variable `indice` au début et à la fin de chaque itération $`i`$ ?__

Propriété $`P_i`$ : La variable `indice` contient l'indice de la valeur minimale du $`tableau[mini...i]`$.

### 4.2. Algorithme `trier`

__Que peut-on dire du `tableau` au début et à la fin de chaque itération $`i`$ ?__

Propriété $`P_i`$ : Le $`tableau [0...i[`$ ne contient que les éléments du tableau initial et le $`tableau [0...i[`$ est trié.

## 5. Complexité

Pour [rappel](../etude), déterminer la __complexité__ d'un algorithme, il faut calculer le __nombre d'instructions nécessaires__ à l'exécution de l'algorithme.

### 5.1. Illustration

Déroulons les 2 cas suivants :

- [Cas 1](https://philippe-boddaert.gitlab.io/tri/tri.html?d=1,%202,%203,%204,%205,%206,%207,%208,%209,%2010&sti=true&a=s&sle=false&slo=true&sco=true)
- [Cas 2](https://philippe-boddaert.gitlab.io/tri/tri.html?d=10,%209,%208,%207,%206,%205,%204,%203,%202,%201&sti=true&a=s&sle=false&slo=true&sco=true)

Y-a-t-il un meilleur des cas ? pire des cas ?

### 5.2. Nombre de comparaisons

- Au premier passage dans la boucle, il y a $`n-1`$ comparaisons dans la recherche du plus petit élément ; 
- Au deuxième passage, il y en a $`n - 2`$ comparaisons ; 
- Ainsi de suite ; 
- Au dernier passage, il n’y a plus de comparaison car il n’y a plus qu’une valeur à examiner. 
- Il y a donc en tout $`(n − 1)+ (n −2) + \dots +1 + 0`$ comparaisons.

Pour tout entier naturel $`n`$, on a :

```math
(n − 1)+ (n − 2)+ \dots + 1 =\frac{n(n −1)}{2}
```

Ainsi la complexité du tri par sélection, en nombre de comparaisons, est en $`\mathcal{0}(n^2)`$.
