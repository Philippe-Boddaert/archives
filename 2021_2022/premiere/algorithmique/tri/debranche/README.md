# Activité : Tri débranché

## 1. Contexte

Vous disposez d'un lot de 5 pancartes, faces cachées. 

![](./assets/pancarte.jpeg)

Celui-ci doit être ranger par ordre croissant...Pour cela, nous allons tester différents algorithmes.

- Un __coordinateur__ est chargé d'indiquer et d'assurer le bon fonctionnement de l'algorithme,

- Un __compteur__ est chargé de noter le nombre de comparaisons et d'échanges réalisés,
- Les __pancarteurs__ sont chargés de se déplacer et de retourner leur pancarte en suivant les consignes du coordinateur.

## 2. Un premier algorithme de tri

__Pour le coordinateur__ : Après avoir mélangé le lot de pancartes, faces cachées, appliquez l'algorithme suivant :

- Comparer la première pancarte avec la deuxième, si la deuxième est plus petite, échanger. 
- Puis comparer, la première pancarte avec la troisième, échanger si besoin.
- Recommencer jusqu’à comparer la première pancarte avec le reste du lot en échangeant si besoin...À la fin de ce passage, la première pancarte est la plus petite.
- Recommencer maintenant en comparant la seconde pancarte avec les suivantes pour placer correctement le second minimum.

__Pour le compteur__ : Tenir le tableau d'indicateurs suivant :

| Nombres de comparaisons | Nombre d'échanges |
| :-----------------------: | :-----------------: |
| 0 | 0 |

- A chaque fois que 2 __pancarteurs__ ont leur pancarte retournée, le nombre de comparaisons est incrémenté de 1,
- A chaque fois que 2 __pancarteurs__ se déplacent, le nombre d'échanges est incrémenté de 1.

### 2.1. À Faire 

- Qu'obtenez-vous à la fin de l'exécution de cet algorithme ?
- Combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre croissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre décroissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?

## 3. Un deuxième algorithme de tri

__Pour le coordinateur__ : Après avoir mélangé le lot de pancartes, faces cachées, appliquez l'algorithme suivant :

- Retourner la première pancarte face visible,
- Comparer la deuxième pancarte avec la première, si la deuxième est plus petite, échanger. 
- Comparer, la troisième pancarte avec la deuxième, 
  - si la troisième est plus petite, échanger et poursuivre la comparaison avec la première pancarte
  - sinon, passer à la quatrième pancarte.
- Comparer la quatrième pancarte avec la troisième,
  - si la quatrième est plus petite, échanger et poursuivre l'opération de comparaison / échange jusqu'à ce que la pancarte soit plus grande que celle comparée
  - sinon, passer à la cinquième pancarte.
- Recommencer jusqu'à avoir traité toutes les pancartes

__Pour le compteur__ : Tenir le tableau d'indicateurs suivant :

| Nombres de comparaisons | Nombre d'échanges |
| :---------------------: | :---------------: |
|            0            |         0         |

- A chaque fois que 2 __pancarteurs__ ont leur pancarte retournée, le nombre de comparaisons est incrémenté de 1,
- A chaque fois que 2 __pancarteurs__ se déplacent, le nombre d'échanges est incrémenté de 1.

### 3.1. À Faire 

- Qu'obtenez-vous à la fin de l'exécution de cet algorithme ?
- Combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre croissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre décroissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?

## 4. Un troisième algorithme de tri

__Pour le coordinateur__ : Après avoir mélangé le lot de pancartes, faces cachées, appliquez l'algorithme suivant :

- Retourner la première pancarte face visible,
- Comparer la deuxième pancarte avec la première, si la première est plus grande, échanger. 
- Comparer, la troisième pancarte avec la deuxième pancarte, si la deuxième est plus grande, échanger.
- Comparer, la quatrième pancarte avec la troisième pancarte, si la troisième est plus grande, échanger.
- Recommencer  jusqu'à avoir traité toutes les pancartes

__Pour le compteur__ : Tenir le tableau d'indicateurs suivant :

| Nombres de comparaisons | Nombre d'échanges |
| :---------------------: | :---------------: |
|            0            |         0         |

- A chaque fois que 2 __pancarteurs__ ont leur pancarte retournée, le nombre de comparaisons est incrémenté de 1,
- A chaque fois que 2 __pancarteurs__ se déplacent, le nombre d'échanges est incrémenté de 1.

### 4.1. À Faire 

- Qu'obtenez-vous à la fin de l'exécution de cet algorithme ?
- Combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre croissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre décroissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?

## 5. Un quatrième algorithme de tri

__Pour le coordinateur__ : Après avoir mélangé le lot de pancartes, faces cachées, appliquez l'algorithme suivant :

- Retourner la première pancarte face visible,
- Comparer la deuxième pancarte avec la première, si la deuxième est plus grande, le pancarteur lève la main. 
- Comparer, la troisième pancarte avec celle du pancarteur avec la main levée, 
  - si la troisième est plus grande, c'est ce pancarteur qui garde la main levée
  - sinon, passer à la quatrième pancarte.
- Recommencer maintenant avec le reste des pancarteurs
- À la fin de cette phase, 
  - Si le pancarteur à la main levée n'est pas à la dernière position des pancarteurs non triés alors, 
    - On retourne la pile des pancarteurs, de celui à la première position à celui ayant la main levée,
    - On retourne la pile des pancarteurs, de celui à la première position jusqu'au dernier non trié.

__Pour le compteur__ : Tenir le tableau d'indicateurs suivant :

| Nombres de comparaisons | Nombre d'échanges |
| :---------------------: | :---------------: |
|            0            |         0         |

- A chaque fois que 2 __pancarteurs__ ont leur pancarte retournée, le nombre de comparaisons est incrémenté de 1,
- A chaque fois qu'une pile de $`p`$  __pancarteurs__ est retournée, le nombre d'échanges est incrémenté de $`\lfloor \frac{n}{2} \rfloor`$

### 5.1. À Faire 

- Qu'obtenez-vous à la fin de l'exécution de cet algorithme ?
- Combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre croissant, pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?
- À partir d'un lot de pancartes trié dans l'ordre [2, 6, 3, 5, 1, 4], pancartes faces cachées, combien de comparaisons et d'échanges avez-vous effectués ?