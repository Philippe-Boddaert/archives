# Activité : Crêpier psychorigide

## Implantation en Python

Une pile de crêpes peut être modélisée comme un tableau d'entier, représentant le diamètre des crêpes.

![](./assets/representation.png)

Une pile de crêpes est triée, selon le principe du crêpier psychorigide, si les valeurs sont ordonnées de la plus petite à la plus grande.

## Matériel fourni

Le script  [crepier.py](./assets/crepier.py) contient les fonctions `indice_maximum`, `inverser_pile` et `trier`.

## Travail à réaliser

1. Écrire le corps des fonctions suivantes :

```python
def retourner(pile, fin):
    '''
    Retourne la pile entre l'indice 0 et l'indice de fin
    :param pile: (list) Une pile de crêpes
    :param fin: (int) indice de fin de la pile
    :return: (None)
    :Effet de bord: La pile est inversée entre l'indice 0 et de fin
    :Exemple:
        >>> pile = [14, 5, 26, 9, 3]
        >>> retourner(pile, 2)
        >>> pile == [26, 5, 14, 9, 3]
        True
        >>> pile = [26, 5, 14, 9, 3]
        >>> retourner(pile, 3)
        >>> pile == [9, 14, 5, 26, 3]
        True
    '''
    pass
```

```python
def indice_maximum(pile):
    '''
    Récupère l'indice de la valeur maximale de la pile
    :param pile: (list) Une pile de crêpes
    :return: (int) L'indice de la valeur maximale
    :Exemple:
        >>> indice_maximum([5, 6, 2])
        1
        >>> indice_maximum([6, 6, 6])
        0
        >>> indice_maximum([])
        -1
    '''
    pass
```

2. À l'aide des fonctions du 1., écrire le corps de la fonction qui trie selon le principe du crêpier psychorigide :
```python
def trier(pile):
    '''
    Tri une pile de crêpes selon l'algorithme du crêpier psychologique
    :param pile: (list) Une pile de crêpes
    :Effet de bord: La pile est triée par ordre croissant
    :Exemple:
        >>> pile = [26, 14, 9, 5, 3]
        >>> trier(pile)
        >>> pile == [3, 5, 9, 14, 26]
        True
        >>> pile = [9, 14, 3, 5]
        >>> trier(pile)
        >>> pile == [3, 5, 9, 14]
        True
    '''
    pass
```
