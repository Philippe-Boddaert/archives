# Activité : Crêpier psychorigide

## 1. Contexte

Vous disposez devant vous d'un ensemble de diques de tailles différentes, avec un seul côté peint. Ces disques représentent les crêpes de notre crêpier.

![](./assets/pancake.jpeg)

À la fin de sa journée, un crêpier dispose d’une pile désordonnée de crêpes. Le crêpier étant un peu psychorigide, il décide de ranger sa pile de crêpes, de la plus grande (en bas) à la plus petite (en haut).

Pour cette tâche, le crêpier peut faire une seule action : glisser sa spatule entre deux crêpes et retourner le haut de la pile.

Attention, le plan de travail de notre crêpier est totalement occupé, aussi le crêpier n'a pas le droit de déposer des crêpes en dehors de la pile initiale.

## 2. Algorithme de résolution

__Question : Comment doit-il procéder pour trier toute la pile ?__

Si au bout de 10 minutes vous n'avez toujours pas trouver la solution, demandez la « fiche indice ».

## 3. Formalisation et Analyse

1. Écrire en langage naturel l'algorithme qui permet de résoudre le problème du crêpier. Cet algorithme doit être :
   1. __général__, résoudre le problème quelque soit la configuration de départ,
   2. __sans ambiguÏté__, toute personne résout le problème du crêpier en suivant _stricto sensu_ les instructions de votre algorithme.
2. Calculer le nombre de coups nécessaires pour ranger la pile de crêpes. 
   1. Pour ranger une crêpe, combien de coups sont nécessaires au minimum et maximum ? 
   2. Généraliser pour $`n`$ crêpes ? 

3. __Prolongement__ : On complique la tâche en imaginant que chaque crêpe a deux faces différentes : l'une est brûlée (face colorée), l'autre non. Le crêpier veut trier ses crêpes en ayant systématiquement la face brûlée en dessous.
   1. Comment doit-il procéder pour trier toute la pile ?
   2. Quels changements apporter à l'algorithme du 1. ?
   3. Quel est le nombre de coups nécessaires pour ranger la pile de crêpes ?
