# Exercices

## 1. Terminaison

Justifier la **terminaison** des algorithmes en identifiant pour chacun d’entre eux un **variant de boucle**.

Pour rappel, un **variant de boucle** est une expression :

- entière
- positive
- qui décroît strictement à chaque itération

### Algorithme 1

```python
a = int(input("a : "))
b = int(input("b : "))
m = 0
while b > 0:
   m += a
   b -= 1
print("a*b = ", m)
```

### Algorithme 2

```python
a = int(input("a : "))
b = int(input("b : "))
i = 0
m = 0
while i < b:
   m += a
   i += 1
print("a*b = ", m)
```

### Algorithme 3

```python
n = int(input("n : "))
q = 0
while n != 0:
   n -= 3
   q += 1
print(q)
```

## 2. Complexité

Calculer la __complexité__ des fonctions ci-dessous.

### Fonction 1

```python
def conversion(n):
    h = n // 3600
    m = (n - 3600*h) // 60
    s = n % 60
    return h,m,s
```

### Fonction 2

```python
def puissanceMoinsUn(n):
   if n%2==0:
      res = 1
   else:
      res = -1
   return res
```

### Fonction 3

```python
def fonction_2(n):
   for i in range(n):
      print("Bonjour!")
```

### Fonction 4

```python
def somme_entiers(n):
    somme = 0
    for i in range(n + 1):
        somme += i
    return somme
```

### Fonction 5

```python
def factorielle(n):
   fact = 1 
   i = 2
   while i <= n:
      fact = fact * i
      i = i + 1
   return fact
```

### Fonction 6

```python
n = 100
s = 0
for i in range(n):
   a = n
   while a > 1:
      a = a/2
   s += 1
```

