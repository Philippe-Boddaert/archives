# Étude d'un algorithme

## 1. Contexte

Exécuter l'algorithme sur quelques cas ne permet pas de valider qu'il effectue correctement ce qui est attendu de lui.

Il faut donc pouvoir utiliser un système de preuve _formel_ de programme.

Face à un algorithme, il faut se poser et répondre à 3 questions :

1. Est-ce-que l'algorithme s'arrête ?
2. Est-ce-que le résultat obtenu est conforme au résultat attendu ?
3. Est-ce-qu’il donne le résultat en un temps raisonnable ?

## 2. Définition

Chaque question soulevée est relative à une notion et une démarche à adopter.

| Question | Notion |
| :-- | :-- |
| 1. Est-ce-que l'algorithme s'arrête ? |  __Problème de la Terminaison__ 
| 2. Est-ce-que le résultat obtenu est conforme au résultat attendu ? | __Problème de la Correction__ | 
| 3. Est-ce-qu’il donne le résultat en un temps raisonnable ? | __Problème du calcul de la Complexité__ |

## 3. Terminaison d'un algorithme

### 3.1. À Faire

1. Est-ce que ces 2 programmes se terminent ?
2. Que réalisent ces 2 programmes ?

__Programme 1__

```python
def mystere(tableau):
    m = tableau[0]
    for i in range(len(tableau)):
        if tableau[i] < m:
            m = tableau[i]
    return m
```

__Programme 2__

```python
def mystere(tableau):
    i = 0
    m = tableau[0]
    while i < len(tableau):
    	if tableau[i] < m:
    		m = tableau[i]
    return m
```

On peut voir, par le biais des exemples ci-dessus, qu'en lisant la suite des instructions, nous avons l'intuition que les algorithmes terminent ou non.

Pour des algorithmes avec plus d'instructions, il nous faut un moyen formel de prouver la terminaison d'un algorithme.

### 3.1. Théorème

> Toute suite d’entiers naturels strictement décroissante est finie.
>
> *Axiome fondamental de l'arithmétique*

Ce théorème indique qu'il n’existe pas de suite infinie d'entiers naturels strictement décroissante.

Pour prouver la terminaison d'un algorithme, il suffit de trouver un ***entier naturel strictement décroissant pendant l'exécution de l'algorithme***.

Cet entier est appelé ***variant de boucle***.

Pour identifier si une boucle se termine effectivement, il faut :

1. identifier le variant de boucle et vérifier qu’il est positif,
2. vérifier qu’il décroît effectivement et strictement à chaque itération,
3. Si c'est le cas, la boucle se termine, en vertu du 3ème axiome fondamental de l'arithmétique. Dans le cas contraire, la boucle est infinie.

### 3.2. Application

#### Pour le programme 1

```python
def mystere(tableau):
    m = tableau[0]
    for i in range(len(tableau)):
        if tableau[i] < m:
            m = tableau[i]
    return m
```

Notons $`n`$ la taille du tableau en entrée, où $`n \in \mathbb{N}^+`$.

Notons $`v = n - i - 1`$

Analysons le comportement de $`v`$ au cours de l'exécution :

| $`i`$ | $`v = n - i - 1`$ |
| :-----:  | :-----------------: |
| 0 | $`n - 1`$ |
| 1 | $`n - 2 `$ |
| 3 | $`n - 3`$ |
| ... | ... |
| $`n - 1`$ | 0 |

  $`v`$ est un bon candidat pour être un __variant de boucle__ car c'est une quantité:

- positive
- décroissante strictement à chaque itération

En vertu de *l'axiome fondamental de l'arithmétique*, l'existence de $`v`$ prouve la terminaison de l'algorithme.

#### Pour le programme 2

Existe-il un variant de boucle prouvant sa terminaison ? Si oui, lequel ? Si non, quelle conclusion peut-on faire ?

## 4. Correction d'un algorithme

Multiplier les exemples qui "fonctionnent" ne veut pas dire que l'algorithme donnera le "bon résultat" dans toutes les circonstances. 

C'est un peu comme en mathématiques, vérifier qu'une propriété est vraie sur un exemple n'a pas valeur de démonstration. Il se pourrait très bien que dans une situation donnée notre algorithme ne donne pas le résultat attendu.

### 4.1. Notion d'invariant de boucle

Il existe un moyen de démontrer (au sens mathématique du terme) la correction d'un algorithme, nous allons utiliser un __invariant de boucle__

On appelle __invariant de boucle__ une propriété qui est vraie avant et après chaque itération.

### 4.2. Preuve de l'invariant de boucle

La démonstration peut se faire par un raisonnement par récurrence et doit se faire en 3 étapes :

- __INITIALISATION__ : on doit montrer que l'invariant de boucle est vrai avant la première itération de la boucle
- __CONSERVATION__ : on doit montrer que si l'invariant de boucle est vrai avant une itération de la boucle, il le reste avant l'itération suivante.
- __TERMINAISON__ : une fois la boucle terminée, l'invariant fournit une propriété utile qui aide à montrer la correction de l'algorithme.

### 4.3. Application

```python
def mystere(tableau):
    m = tableau[0]
    for i in range(len(tableau)):
        if tableau[i] < m:
            m = tableau[i]
    return m
```

Prenons le tableau $`t =  [10, 12, 8, 27, 11]`$.

Analysons le comportement de $`m`$ et $`t[0;i]`$ :

|   $`i`$   | $`t[0;i]`$ | $`m`$ |
| :-------: | :---------------: | :--: |
|     -     | - |10|
|     0     | [10] |10|
|     1     | [10, 12] |10|
|     3     | [10, 12, 8] |8|
|    4    | [10, 12, 8, 27] |8|
| 5 | [10, 12, 8, 27, 11] |8|

Il est possible de constater que la variable $`m`$ contient la valeur minimale du tableau $`t[0; i]`$ à chaque début et fin d'itération.

La proposition $`\mathcal{P}_i`$ :  la variable $`m`$ contient la valeur minimale du tableau $`t[0; i]`$ est un bon candidat pour être un invariant de boucle.

- __INITIALISATION__ : Avant la première itération, $`\mathcal{P}_0`$ est vraie car $`m`$ est initialisé au premier élément du tableau,
- __CONSERVATION__ : Supposons $`\mathcal{P}_k`$ vraie pour un certain rang $`k`$,  $`k \gt 0`$. Montrons que $`\mathcal{P}_{k + 1}`$ reste vraie au rang $`k + 1`$. 
  - Si l'élément $`t[k + 1] \lt m`$ alors $`m`$ prend cette valeur. ainsi à la fin de l'itération $`k + 1`$, la variable $`m`$ contient la valeur minimale du tableau $`t[0; k + 1]`$ et $`\mathcal{P}_{K + 1}`$ est vraie,
  - Si l'élément $`t[k + 1] \geq m`$ alors $`m`$ n'est pas mis à jour. Ainsi à la fin de l'itération $`k + 1`$, la variable $`m`$ contient la valeur minimale du tableau $`t[0; k + 1]`$, puisque $`\mathcal{P}_k`$ est vraie par hypothèse et l'élément $`t[k + 1]`$ plus grand que $`m`$. $`\mathcal{P}_{K + 1}`$ est vraie, si $`\mathcal{P}_K`$ est vraie.
  - Dans tous les cas, nous venons de prouver la conservation de la propriété $`\mathcal{P}_k`$ : la variable $`m`$ contient la valeur minimale du tableau $`t[0; k]`$.

- __TERMINAISON__ : Puisque $`\mathcal{P}_i`$ est vraie à l'initialisation et qu'elle reste vraie à chaque itération, à la fin de l'algorithme, $`i`$ valant $`n - 1`$, la propriété $`\mathcal{P}_{n - 1}`$ : la variable $`m`$ contient la valeur minimale du tableau $`t[0; n - 1]`$ est vraie. Notre algorithme est donc correct.

## 5. Complexité d'un algorithme

Une mesure naturelle de la complexité d'un algorithme serait d'exécuter l'algorithme et calculer le temps d'exécution réel. Il s'agit d'une très mauvaise mesure. En effet, elle dépend de beaucoup de paramètres : le compilateur, le langage de programmation, la machine, ...

C'est pourquoi la mesure de complexité utilisée est le ***nombre d'opérations*** de l'algorithme. Cette mesure a l'avantage d'être indépendante de la machine et du langage, et d'être évaluable sur le papier.

### 5.1. Application

Prenons l'exemple du programme 1 et calculons le nombre d'opérations en fonction de la taille $`n`$ du tableau en paramètre

```python
def mystere(tableau):
    m = tableau[0]
    for i in range(len(tableau)):
        if tableau[i] < m:
            m = tableau[i]
    return m
```

2 cas sont à envisager :

- Cas 1 : Un tableau de $`n`$ valeurs distinctes,
- Cas 2 : Un tableau de $`n`$ valeurs égales.

#### Cas 1

```python
def mystere(tableau):
    m = tableau[0]									# 2 opérations
    for i in range(len(tableau)):   # n opérations
        if tableau[i] < m:					# 2n opérations
            m = tableau[i]					# 2n opérations
    return m												# 1 opération
```

Soit $`\mathcal{C}_1`$ le nombre d'opérations réalisées. Il s'exprime par : 

```math
\begin{aligned}
	\mathcal{C}_1 = & 2 + n + 2n + 2n + 1 \\
	& 5n + 3
\end{aligned}
```

#### Cas 2

```python
def mystere(tableau):
    m = tableau[0]									# 2 opérations
    for i in range(len(tableau)):   # n opérations
        if tableau[i] < m:					# 2n opérations
            m = tableau[i]					# 0 opération
    return m												# 1 opération
```

Soit $`\mathcal{C}_2`$ le nombre d'opérations réalisées. Il s'exprime par : 

```math
\begin{aligned}
	\mathcal{C}_2 = & 2 + n + 2n + 0 + 1 \\
	& 3n + 3
\end{aligned}
```

### 5.2. Notion d'ordre de grandeur asymptotique

C'est une échelle de référence, qui considère les cas où l'ensemble des données en jeu est très grand.

On note cet ordre de grandeur avec un $`\mathcal{O}`$ majuscule (Notation de Landau)

Pour obtenir l'ordre de grandeur à partir d'un polynôme :

1. Supprimer les constantes
2. Garder uniquement le terme qui possède l'exposant le plus grand
3. Supprimer le coefficient devant ce terme

Exemples :

```math
\begin{aligned}
2n + 1 \Rightarrow & {\color{red}\cancel{2}} n + {\color{red}\cancel{1}} \Rightarrow \mathcal{O}(n) \\
6n^2 + 3n + 5 \Rightarrow & {\color{red}\cancel{6}} n^2 + {\color{red}\cancel{3n}} + {\color{red}\cancel{5}} \Rightarrow \mathcal{O}(n^2)
\end{aligned}
```

### 5.3. Comparaisons des ordres de grandeur

Voici un tableau dans lequel le temps de calcul de différentes complexités algorithmiques est présenté, en fonction de la taille des données.

|                           | $`n = 1`$  |  $`n = 10`$  |       $`n = 100`$       | $`n = 1000`$ | $`n = 1000000`$ |         Problème exemple         |
| :------------------------ | :--------: | :----------: | :---------------------: | :----------: | :-------------: | :------------------------------: |
| $`\mathcal{O}(1)`$        | 1 &micro;s |  1 &micro;s  |       1 &micro;s        |  1 &micro;s  |   1 &micro;s    | Accès à une cellule d'un tableau |
| $`\mathcal{O}(\log{}n)`$  | 1 &micro;s |  4 &micro;s  |       7 &micro;s        | 10 &micro;s  |   20 &micro;s   | Calcul de la taille d'un entier  |
| $`\mathcal{O}(n)`$        | 1 &micro;s | 10 &micro;s  |      100 &micro;s       |    10 ms     |       1 s       |      Parcours d'un tableau       |
| $`\mathcal{O}(n\log{}n)`$ | 1 &micro;s |  4 &micro;s  |      700 &micro;s       |    10 ms     |      20 s       |            Tri fusion            |
| $`\mathcal{O}(n^2)`$      | 1 &micro;s | 100 &micro;s |          10 ms          |     1 s      |   11.57 jours   |      Parcours d'une matrice      |
| $`\mathcal{O}(n^3)`$      | 1 &micro;s |     1 ms     |           1 s           |  16.67 min   |    31709 ans    |    Multiplication matricielle    |
| $`\mathcal{O}(2^n)`$      | 2 &micro;s |   1.024 ms   | 401969368413314 siècles |      -       |        -        |      Problème du sac à dos       |
| $`\mathcal{O}(n!)`$       | 1 &micro;s |    3.62 s    |    2.9e+142 siècles     |      -       |        -        | Problème du voyageur de commerce |

On dit d'une complexité d'un algorithme :

- en $`\mathcal{O}(1)`$  qu'elle est __constante__ par rapport à la taille de la donnée,
- en $`\mathcal{O}(\log n)`$  qu'elle est __logarithmique__ par rapport à la taille de la donnée,
- en $`\mathcal{O}(n)`$  qu'elle est __linéaire__ par rapport à la taille de la donnée,
- en $`\mathcal{O}(n^2)`$  qu'elle est __quadratique__ par rapport à la taille de la donnée,
- en $`\mathcal{O}(2^n)`$  qu'elle est __exponentielle__ par rapport à la taille de la donnée.

## 6. Synthèse

| Question                                                     | Réponse                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| __Est-ce que l'algorithme se termine ?__                     | Trouver un __variant__, i.e un entier positif qui décroit strictement. |
| __Est-ce que l'algorithme est correct ?__                    | Trouver un __invariant__, i.e un prédicat vrai à l'initialisation d'algorithme et qui le reste jusqu'à la fin d'algorithme. |
| __Est-ce que l'algorithme donne un résultat en un temps raisonnable ?__ | Calculer la __complexité__, i.e le __nombre d'instructions__ nécessaires à l'exécution de l'algorithme. |