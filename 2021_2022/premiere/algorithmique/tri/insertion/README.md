# Tri par insertion

![](./assets/cards.jpg)

_Source : Pixabay_

Le tri par insertion est le tri le plus connu.

C'est celui que nous utilisons intuitivement quand nous devont trier une liste d'objets, par exemple quand nous jouons aux cartes.

## 1. Principe

Quand on considère une collection $`l`$ de $`n`$ éléments, dont les $`i - 1`$ premiers sont triés.

Placer le $`i^{ème}`$ élément, consiste à le déplacer dans la sous-collection triée jusqu'à sa "bonne" place.

La "bonne" place est l'indice $`j`$ où :

- tous les éléments de $`l[0:j]`$ sont $`\lt`$ à l'élément $`i`$ et,
- tous les éléments de $`l[j + 1:i - 1]`$ sont $`\gt`$ à l'élément $`i`$.

## 2. Illustration

Déroulons l'algorithme de tri sur le tableau $`[6, 4, 1, 8, 5, 10, 7, 3, 2, 9]`$ via l'[Illustration animée](https://philippe-boddaert.gitlab.io/tri/tri.html?d=6,%204,%201,%208,%205,%2010,%207,%203,%202,%209&sti=true&a=i&sle=true&slo=false&sco=true)

### 2.1. À Faire

1. Imaginons que l'on dispose de la fonction `deplacer` suivante :

```python
def deplacer(tableau, indice):
    '''
    Déplace l'élément à l'indice à sa "bonne place" dans le tableau
    :param tableau: (list) Un tableau
    :param indice: (int) un indice 
    :return: None
    :C.U: le tableau[0:indice] est trié
    :Effet de Bord: l'élément à l'indice est déplacé jusqu'à sa "bonne place"
    '''
    position = indice
    valeur = tableau[position]
    
    while position > 0 and tableau[position - 1] > valeur:
        tableau[position] = tableau[position - 1]
        position -= 1
    tableau[position] = valeur
```

Dérouler l'exécution de cette fonction sur le tableau $`t = [1, 3, 7, 2, 0, 4]`$ 

- Soit $`indice = 3`$, que peut-on dire de $`t[0:indice]`$ ? Quel est le résultat de l'appel à distance(t, 3) ?
- Soit $`indice = 4`$, que peut-on dire de $`t[0:indice]`$ ? Quel est le résultat de l'appel à distance(t, 4) ?

2. Compléter la fonction `trier` qui implémente l'algorithme de tri par insertion.

```python
def trier(tableau):
    '''
    Trie un tableau
    :param tableau: (list) Un tableau
    :return: (None)
    :Effet de bord: Le tableau est trié par ordre croissant
    :exemple:
        >>> tableau = [5, 4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [1, 3, 4, 5, 9]
        True
        >>> tableau = [5, -4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [-4, 1, 3, 5, 9]
        True
    '''
    n = len(tableau)
    for i in range(1, n):
        deplacer(...) # on déplace l'élément à sa bonne place dans le tableau
```

## 3. Terminaison

Pour [rappel](../etude), déterminer si un algorithme termine revient à trouver un __variant__, i.e un entier positif qui décroit strictement au cours de l'algorithme.

### 3.1. Algorithme `deplacer`

L'algorithme `deplacer` contient une boucle non bornée, il s’agit donc de vérifier qu’elle se termine. _position_ est un variant pour la boucle `while`, i.e une quantité :

- entière,
- positive,
- strictement décroissante.

En conclusion, l'algorithme `deplacer` se termine.

### 3.2. Algorithme `trier`

L'algorithme `trier` contient une boucle, il s’agit donc de vérifier qu’elle se termine. $`n - 1 - i`$, où $`i < n`$ est un variant pour la boucle `for`, i.e une quantité :

- entière,
- positive,
- strictement décroissante.

L'algorithme `trier` fait appel à l'algorithme `deplacer`, l'algorithme `trier` termine si et seulement si l'algorithme `deplacer` termine. Or, la terminaison de l'algorithme deplacer été prouvé dans le 3.1.

En conclusion, l'algorithme `trier` termine.

## 4. Correction

Pour [rappel](../etude), déterminer si un algorithme termine, il faut trouver un __invariant__, i.e un prédicat vrai à l'initialisation de l'algorithme et qui le reste jusqu'à la fin d'algorithme.

### 4.1. Algorithme `deplacer`

__Que peut-on dire des éléments `tableau[0;position[` et `tableau[position + 1;indice]` par rapport à l'élément à la position?__

Propriété $`P_i`$ : Les éléments $`tableau[0;i[`$ sont inférieurs à $`tableau[i]`$ et les éléments $`tableau[i + 1; indice]`$ sont supérieurs à $`tableau[i]`$.

### 4.2. Algorithme `trier`

__Que peut-on dire du `tableau` au début et à la fin de chaque itération $`i`$ ?__

Propriété $`P_i`$ : Le $`tableau [0...i[`$ ne contient que les éléments du tableau initial et le $`tableau [0...i[`$ est trié.

## 5. Complexité

Pour [rappel](../etude), déterminer la __complexité__ d'un algorithme, il faut calculer le __nombre d'instructions nécessaires__ à l'exécution de l'algorithme.

### 5.1. Illustration

Déroulons les 2 cas suivants :

- [Cas 1](https://philippe-boddaert.gitlab.io/tri/tri.html?d=1,%202,%203,%204,%205,%206,%207,%208,%209,%2010&sti=true&a=i&sle=false&slo=true&sco=true)
- [Cas 2](https://philippe-boddaert.gitlab.io/tri/tri.html?d=10,%209,%208,%207,%206,%205,%204,%203,%202,%201&sti=true&a=i&sle=false&slo=true&sco=true)

Y-a-t-il un meilleur des cas ? pire des cas ?

## 5.2. Nombre de comparaisons

### Meilleur des cas

- À chaque tour on ne fait qu'une comparaison pour s'apercevoir que l'élément est déjà à sa place.
- L'algorithme effectue $`n - 1`$ comparaisons

Ainsi la complexité du tri par insertion, dans le meilleur des cas, en nombre de comparaisons, est en $`\mathcal{0}(n)`$.

### Pire des cas

Le tableau est trié dans le mauvais sens (sens décroissant)

- il faut d'abord 1 comparaison pour ramener par 1 permutation l'élément d'index 1 (=le plus faible) en position 0.
- Puis 2 comparaisons pour ramener par 2 permutations l'élément d'index 2 (=le plus faible) en position 0.
- Puis 3 comparaisons pour ramener par 3 permutations l'élément d'index 3 (=le plus faible) en position 0. 
- L'avant-dernier élément demande $`n-2`$ comparaisons pour ramener par $`n-2`$ permutations cet élément en position 0. 
- Le dernier demande $`n-1`$ comparaisons pour ramener par $`n-1`$ permutations cet élément en position 0.

- Il y a donc en tout $`(n − 1)+ (n −2) + \dots +1 + 0`$ comparaisons.

Pour tout entier naturel $`n`$, on a :

```math
(n − 1)+ (n − 2)+ \dots + 1 =\frac{n(n −1)}{2}
```

Ainsi la complexité du tri par insertion, dans le pire des cas, en nombre de comparaisons, est en $`\mathcal{0}(n^2)`$.
