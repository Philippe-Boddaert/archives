# Algorithmes de Tri

## 1. Contexte

Plusieurs domaines en informatique nécessitent un tri des données avant de les utiliser (gestion, annuaires, sciences sociales, ...). 

- Le __moteur de recherche Google__ _trie_ les résultats selon leur pertinence,
- À l'ouverture d'__Instagram__, __TikTok__ ou __Youtube__, le contenu proposé est _trié_ selon l'ordre antichronologique et vos préférences,
- La feuille d'appel sur __Pronote__ est _triée_ selon l'ordre alphabétique de nom et prénom des élèves.

Rappelons que le mot même d'__ordinateur__ est un néologisme proposé par le français Jacques Perret en 1955, qui vient à l'origine du latin, et qui signifierait : « Dieu qui met de l’ordre dans le monde » (Cf [Correspondance de Jacques Perret](https://journals.openedition.org/bibnum/534#:~:text=4Le%20professeur%20Jacques%20Perret,1%20la%20lettre%20autographe%20suivante.&text=16%20IV%2055-,%C2%AB%20Cher%20monsieur%2C,l'ordre%20dans%20le%20monde.))

## 1.1. En python...

Deux méthodes sont connues

- `sort`

```python
>>> l = [5, 1, 9, 4, 7]
>>> l.sort()
>>> print(l)
[1, 4, 5, 7, 9]
```

- `sorted`

```python
>>> l = [5, 1, 9, 4, 7]
>>> l_triee = sorted(l)
>>> print(l_triee)
[1, 4, 5, 7, 9]
>>> print(l)
[5, 1, 9, 4, 7]
```

La méthode `sort` est dite __tri en place__ car elle modifie directement la structure sur laquelle elle s'applique.

La méthode `sorted` renvoie une nouvelle structure triée et ne modifie pas la structure sur laquelle elle s'applique.

__Question : Il est possible de se demander comment sont programmées ces méthodes ? Y a-t-il différents algorithmes de tri__ ?

## 2. Quelques considérations

Pour la suite du cours sur les tris, nous considérerons :

1. Le tri se fait systématiquement par __ordre croissant__ (il n'est pas difficile d'adapter les algorithmes pour un tri par ordre décroissant)
2. La __notion d'ordre__ est sensée être intuitive :

   - sur les entiers et les flottants, on choisit "<" comme relation d'ordre

   - sur les caractères, on choisit l'ordre alphabétique

   - sur les chaines de caractères, on choisit l'ordre lexicographique (celui du dictionnaire)

3. Le tri se fait __en place__.

## 3. Spécification

Voici donc la **spécification du problème de tri** que nous nous fixons :

![](./assets/specifications.jpeg)

## 4. Attendus

| Contenus | Capacités attendues |
| :-- | :-- |
| Tris par insertion, par sélection | Écrire un algorithme de tri.<br />Décrire un invariant de boucle qui prouve la correction des tris par insertion, par sélection. |