# TD - Étude comparative des algorithmes de tri

## 1. Contexte

Les séances précédentes nous ont permis d'étudier le principe de plusieurs algorithmes de tri.

La mesure de comparaison s'effectue via le __calcul de la complexité__ des algorithmes.

L'objectif de cette activité est d'étudier ces algorithmes en conditions réelles, i.e par exécution sur machine.

![](./assets/comparatif.jpg)

La mesure est le temps d'exécution et d'analyser l'évolution de cette mesure selon la taille du tableau à trier et l'algorithme exécuté.

## 2. Matériel fourni

Le script [etude.py](./assets/etude.py) contient les fonctions de base à l'étude comparative.

Les scripts [tri_selection](./assets/tri_selection.py), [tri_insertion](./assets/tri_insertion.py), [tri_bulle](./assets/tri_bulle.py) et [tri_rapide](./assets/tri_rapide.py) contiennent chacun une fonction `trier` prenant en entrée un tableau et effectuant le tri selon la méthode nommée.

## 3. Premier pas

Le programme principal du script `etude` et le suivant :

```python
tailles = [10, 100, 500, 1000, 5000, 10000]
temps_selection = []

for n in tailles:
    tableau = generer_tableau(n, 0, n)
    temps_selection.append(calculer_temps(copie(tableau), tri_selection.trier))

plt.plot(tailles, temps_selection, label="Tri par sélection")

plt.title("Comparaison Algorithmes de tri")
plt.xlabel("Taille du tableau")
plt.ylabel("Temps (sec)")
plt.legend()

plt.show()
```

Ce bloc d'instructions :

- Génère successivement des tableaux aléatoires de taille différente,
- Calcule le temps d'exécution du tri par sélection sur ces tableaux,
- Les temps d'exécutions sont stockés dans un tableau
- Un graphique est affiché où 
  - l'abscisse est la taille du tableau étudié
  - l'ordonnée est le temps d'exécution de la fonction de tri sur le tableau

Voici le résultat obtenu après exécution :

![](./assets/execution.png)

On souhaite enrichir ce graphique avec les courbes d'exécution des autres algorithmes de tri (tri par insertion, tri à bulle, tri rapide).

## 4. Questions préparatoires

1. Pour une taille de tableau donnée, faut-il utiliser la variable `tableau` en entrée des différents algorithmes ? Si oui, pourquoi ? Si non, pourquoi ?
2. À quoi sert la fonction copie dans ce programme ? Quel effet serait constaté si cette fonction n'était pas utilisée ?
3. Quel moyen utilisé pour stocker les temps d'exécutions des différents algorithmes `tri_insertion`, `tri_bulle` et `tri_rapide` ?

## 5. Mise en place

Modifier le programme principal pour : 

- calculer les temps d'exécutions de chaque algorithme de tri 
- enrichir le graphique avec les courbes représentatives de ces calculs.

## 6. Analyse

Quelle analyse faites-vous de ce graphique ?

- Comment se comportent les algorithmes en fonction de la taille du tableau ?
- Y a-t-il un algorithme plus "rapide" que les autres ?
- Comment faire le lien entre les courbes obtenues et la complexité calculée théoriquement ?
- Est-ce que la notion de meilleur/pire des cas des algorithmes est prise en compte dans cette étude ? Si non, comment la prendre en compte ? Quel est l'effet attendu ? constaté ?
- Comment comparer les algorithmes proposés et la fonction `sort` de Python ? Mettez en place les éléments nécessaires, que constatez-vous ?
