def echanger(tableau, i, j):
    '''
    Echange les 2 éléments aux indices i et j
    :param tableau: (list) Un tableau
    :param i: (int) un indice
    :param j: (int) un indice
    :return: (None)
    '''
    tableau[i], tableau[j] = tableau[j], tableau[i]

def remonter(tableau, taille):
    '''
    Remonte l'élément de valeur maximal du tableau à l'indice taille - 1
    :param tableau: (list) Un tableau
    :param taille: (int) L'indice maximal à prendre en compte
    :return: (None)
    :Effet de bord: l'élément maximal du tableau est à l'indice taille - 1
    :exemple:
        >>> tableau = [5, 3, 9, 4, 1]
        >>> remonter(tableau, len(tableau) - 1)
        >>> tableau == [3, 5, 4, 1, 9]
        True
        >>> remonter(tableau, len(tableau) - 2)
        >>> tableau == [3, 4, 1, 5, 9]
        True
    '''
    for i in range(taille):
        if tableau[i] > tableau[i + 1]:
            echanger(tableau, i, i + 1)
            
def trier(tableau):
    '''
    Trie un tableau
    :param tableau: (list) Un tableau
    :return: (None)
    :Effet de bord: Le tableau est trié par ordre croissant
    :exemple:
        >>> tableau = [5, 4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [1, 3, 4, 5, 9]
        True
        >>> tableau = [5, -4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [-4, 1, 3, 5, 9]
        True
    '''
    n = len(tableau)
    for i in range(n - 1, -1, -1):
        remonter(tableau, i)
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()