def indice_minimum(tableau, mini):
    '''
    Recherche le mnimum d'une tableau
    :param tableau: (list) Un tableau
    :param mini: (int) indice minimum à prendre en considération dans la recherche
    :return: (int) L'indice où se situe le minimum du tableau
    :C.U:
        - len(tableau) > 0
        - maxi < len(tableau)
    :exemple:
        >>> indice_minimum([5, 4, 9, 1, 3], 0)
        3
        >>> indice_minimum([5, -4, 9, 1, 3], 0)
        1
        >>> indice_minimum([1, 1, 1, 1, 1], 0)
        0
        >>> indice_minimum([5, 4, 9, 1, 3], 2)
        3
    '''
    indice = mini
    for i in range(mini + 1, len(tableau)):
        if tableau[i] < tableau[indice]:
            indice = i
    return indice

def echanger(tableau, i, j):
    '''
    Echange les 2 éléments aux indices i et j
    :param tableau: (list) Un tableau
    :param i: (int) un indice
    :param j: (int) un indice
    :return: (None)
    '''
    tableau[i], tableau[j] = tableau[j], tableau[i]

def trier(tableau):
    '''
    Trie un tableau
    :param tableau: (list) Un tableau
    :return: (None)
    :Effet de bord: Le tableau est trié par ordre croissant
    :exemple:
        >>> tableau = [5, 4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [1, 3, 4, 5, 9]
        True
        >>> tableau = [5, -4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [-4, 1, 3, 5, 9]
        True
    '''
    n = len(tableau)
    for i in range(n):
        mini = indice_minimum(tableau, i)
        echanger(tableau, i, mini)

if __name__ == "__main__":
    import doctest
    doctest.testmod()