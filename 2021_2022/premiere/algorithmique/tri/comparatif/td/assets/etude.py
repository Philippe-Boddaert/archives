import random, time
import matplotlib.pyplot as plt
import tri_rapide, tri_insertion, tri_selection, tri_bulle

def generer_tableau(n, mini, maxi):
    '''
    Génère un tableau de n éléments aléatoires
    :param n: (int) Nombre d'éléments du tableau
    :param mini: (int) Valeur minimale
    :param maxi: (int) Valeur maximale
    :return: (list) Un tableau de n éléments
    '''
    return [ random.randint(mini, maxi) for _ in range(n) ]

def copie(tableau):
    '''
    Renvoie une copie du tableau
    :param tableau: (list) Un tableau
    :return: (list) Une copie du tableau
    '''
    return [ element for element in tableau]

def calculer_temps(tableau, fonction):
    '''
    Calcule le temps d'exécution d'une fonction sur un tableau
    :param tableau: (list) Un tableau
    :param fonction: (function) Une fonction à exécuter sur le tableau
    :return: (float) Le temps d'exécution en secondes
    '''
    start = time.process_time()

    fonction(tableau)

    end = time.process_time()
    
    return end - start


tailles = [10, 100, 500, 1000, 5000, 10000]
temps_selection = []

for n in tailles:
    tableau = generer_tableau(n, 0, n)
    temps_selection.append(calculer_temps(copie(tableau), tri_selection.trier))

plt.plot(tailles, temps_selection, label="Tri par sélection")

plt.title("Comparaison Algorithmes de tri")
plt.xlabel("Taille du tableau")
plt.ylabel("Temps (sec)")
plt.legend()

plt.show()
