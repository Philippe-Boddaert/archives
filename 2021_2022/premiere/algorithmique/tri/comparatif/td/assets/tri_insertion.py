def deplacer(tableau, indice):
    '''
    Déplace l'élément à l'indice à sa "bonne place" dans le tableau
    :param tableau: (list) Un tableau
    :param indice: (int) un indice 
    :return: None
    :C.U: le tableau[0:indice] est trié
    :Effet de Bord: l'élément à l'indice est déplacé jusqu'à sa "bonne place"
    '''
    position = indice
    valeur = tableau[position]
    
    while position > 0 and tableau[position - 1] > valeur:
        tableau[position] = tableau[position - 1]
        position -= 1
    tableau[position] = valeur

def trier(tableau):
    '''
    Trie un tableau
    :param tableau: (list) Un tableau
    :return: (None)
    :Effet de bord: Le tableau est trié par ordre croissant
    :exemple:
        >>> tableau = [5, 4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [1, 3, 4, 5, 9]
        True
        >>> tableau = [5, -4, 9, 1, 3]
        >>> trier(tableau)
        >>> tableau == [-4, 1, 3, 5, 9]
        True
    '''
    n = len(tableau)
    for i in range(1, n):
        deplacer(tableau, i)


if __name__ == "__main__":
    import doctest
    doctest.testmod()