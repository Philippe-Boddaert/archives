# Autres tris

## 1. Tri à bulle

![](./assets/bulles.jpg)

### 1.1. Principe

Quand on considère une collection $`l`$ de $`n`$ éléments, non triée :

- Si on compare répétitivement les éléments consécutifs de $`l`$,
- En les permutant lorsqu'ils sont mal triés,
- A la fin du processus, l'élément en dernière position est celui ayant la valeur maximale de $`l`$.

Cet algorithme doit son nom au fait qu'il déplace rapidement les plus grands éléments en fin de tableau, comme des `bulles` d'air qui remonteraient rapidement à la surface d'un liquide.

### 1.2. Illustration

Déroulons l'algorithme de tri sur le tableau $`[6, 4, 1, 8, 5, 10, 7, 3, 2, 9]`$ via l'[Illustration animée](https://philippe-boddaert.gitlab.io/tri/tri.html?d=6,%204,%201,%208,%205,%2010,%207,%203,%202,%209&sti=true&a=b&sle=false&slo=false&sco=true)

### 1.3. Complexité

Quel est le meilleur et le pire des cas pour cet algorithme, matéralisés par les animations suivantes :

- [Cas 1](https://philippe-boddaert.gitlab.io/tri/tri.html?d=10,%209,%208,%207,%206,%205,%204,%203,%202,%201&sti=true&a=b&sle=false&slo=true&sco=true)
- [Cas 2](https://philippe-boddaert.gitlab.io/tri/tri.html?d=1,%202,%203,%204,%205,%206,%207,%208,%209,%2010&sti=true&a=b&sle=false&slo=true&sco=true)

## 2. Tri rapide

![](./assets/rapide.jpg)

### 2.1. Principe

Quand on considère une collection $`l`$ de $`n`$ éléments, non triée :

- Si on partitionne $`l`$ en fonction d'un pivot (le premier élément par exemple),
- En plaçant le pivot à la "bonne" place,
- Il ne reste qu'à trier récursivement chaque collection à la gauche et à la droite du pivot.

La "bonne" place du pivot est l'indice $`j`$ où :

- tous les éléments de $`l[0:j]`$ sont $`\lt`$ au pivot,
- tous les éléments de $`l[j + 1:i - 1]`$ sont $`\gt`$ au pivot.

### 2.2. Illustration

Déroulons l'algorithme de tri sur le tableau $`[6, 4, 1, 8, 5, 10, 7, 3, 2, 9]`$ via l'[Illustration animée](https://philippe-boddaert.gitlab.io/tri/tri.html?d=6,%204,%201,%208,%205,%2010,%207,%203,%202,%209&sti=true&a=q&sle=false&slo=false&sco=true)

### 2.3. Complexité

Quel est le meilleur et le pire des cas pour cet algorithme, matéralisés par les animations suivantes :

- [Cas 1](https://philippe-boddaert.gitlab.io/tri/tri.html?d=10,%209,%208,%207,%206,%205,%204,%203,%202,%201&sti=true&a=q&sle=false&slo=true&sco=true)
- [Cas 2](https://philippe-boddaert.gitlab.io/tri/tri.html?d=5,%203,%204,%201,%202,%209,%208,%2010,%207,%206&sti=true&a=q&sle=false&slo=true&sco=true)

## 3. Comparatif

![](./assets/comparatif.jpg)

Un comparatif des différents tris vus en cours est disponible [ici](https://philippe-boddaert.gitlab.io/tri/comparatif.html)

Le [TD](./td) a vocation à étudier par simulation les différents tris.