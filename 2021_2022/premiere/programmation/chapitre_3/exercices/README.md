# Exercices - Les boucles

L'activité consiste en une série d'exercices relatifs à la notion de _boucles_ en Python.

## Pré-requis

Avoir effectué l'activité sur les [boucles](../chapitre_3/)

## Consignes

Trois fiches exercices sont proposées :

- Niveau [facile](./TD_0.ipynb)
- Niveau [avancé](./TD.ipynb)
- Niveau [expert](./TD_2.ipynb)

Pour effectuer les exercices :

1. Télécharger la fiche exercice de votre choix (fichier avec l'extension .ipynb),
2. Se rendre sur [Basthon](https://notebook.basthon.fr/),
3. Ouvrir le fichier .ipynb téléchargé.

