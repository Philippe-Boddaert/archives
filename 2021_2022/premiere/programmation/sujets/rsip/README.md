## Activité : RSIP

## 1. Contexte

Pour cette veille de vacances, je souhaite vous communiquer un message personnel, de manière confidentielle.

![](./assets/top_secret.jpg)

_Source : Pixabay_

Sur votre papier, se trouvent : 

- un _message_ qui semble illisible pour le moment,
- une _clé_ qui se présente sous une forme binaire. 

Questions :

1. Quel message ai-je voulu vous transmettre ?
2. Comment _déchiffrer_ le _message_ ? À quoi sert la _clé_ ?
3. Plus globalement : Comment communiquer secrètement et de manière sécurisée ?

## 2. Définitions

Beaucoup de termes sont utilisés sur le sujet de la sécurisation des communications.

Listons les termes et identifions ceux adéquats et inadéquats :

| Terme | Définition | Adéquat (&#128077;) / Inadéquat (&#128078;) |
| :-----: | :---------- | :-------------------: |
| ___Cryptographie___ | Discipline s’attachant à protéger des messages en s’aidant souvent de secrets ou clés |  &#128077; |
| ___Chiffrement___ | Procédé de cryptographie grâce auquel on souhaite rendre la compréhension d’un document impossible à toute personne qui n’a pas la clé de (dé)chiffrement | &#128077; |
| ___Chiffrer___ | Consiste à procéder à un chiffrement. À partir d'un message original dit _clair_, appliquer un algorithme de chiffrement pour obtenir un message chiffré, i.e. incompréhensible pour celui qui n'a pas la clé ayant servi à chiffrer.  | &#128077; |
| ___Déchiffrer___ | consiste à retrouver le message original (le _clair_) d’un message _chiffré_ dont on possède la clé de (dé)chiffrement. | &#128077; |
|  ___Crypter / Cryptage___ | Le Référentiel Général de Sécurité de l’ANSSI qualifie d’incorrect « cryptage ». En effet, la terminologie de cryptage reviendrait à chiffrer un fichier sans en connaître la clé et donc sans pouvoir le déchiffrer ensuite. Le terme n’est par ailleurs pas reconnu par le dictionnaire de l’Académie française. | &#128078; |
| ___Encrypter___ | Le terme « encrypter » et ses dérivés (déencrypter, désencrypter) sont des anglicismes. Donc, on ne les utilise pas non plus. | &#128078; |
| ___Décrypter___ | Consiste à retrouver le texte original (le _clair_) à partir d’un message _chiffré_ sans posséder la clé de (dé)chiffrement. | &#128077; |
|  ___Chiffrage___  | Évaluer le coût de quelque chose. ABSOLUMENT RIEN à voir avec le chiffrement. | &#128078; |
| ___Coder / Encoder / Décoder___ | Façon d’écrire les mêmes données, mais de manière différente (ex. en binaire, en hexadécimal,...). Il n’y a aucune vocation à assurer la confidentialité, ce n’est donc pas du chiffrement. | &#128078; |

Bilan :

1. Le message que j'ai voulu vous transmettre est le __clair__,
2. J'ai défini une __clé__ de chiffrement, i.e un entier, représenté en binaire,
3. J'ai utilisé un algorithme de __chiffrement__ pour obtenir le __chiffré__, écrit sur votre papier.

Le problème formalisé est : __Comment déchiffrer le chiffré à partir de la clé pour obtenir le clair ?__

## 3. Technique de Chiffrement

Le chiffrement utilisé est un __chiffrement par décalage__.

Il utilise l'alphabet suivant : `ABCDEFGHIJKLMNOPQRSTUVWXYZ .,'`

### 3.1. Déchiffrer

#### 3.1.1. Principe

Déchiffrer consiste à décaler les lettres de l’alphabet d’un certain rang (la « clé »). Connaître la valeur de la clé permet de déchiffrer très facilement le message en appliquant un __décalage aux lettres dans le sens inverse de l'alphabet.__

#### 3.1.2. À Faire

1. Soit le chiffré `RSIP`, quel est le mot clair obtenu pour une clé égale à 1 ? clé égale à 4 ? 34 ?
2. Soit le chiffré `UZR 'YMBU ZA`, quel est le mot clair obtenu pour une clé égale à 30 ? clé égale à 12 ?
3. Convertir la clé binaire fournie sur votre carte en décimale. Quels sont les 5 premières lettres du message clair que je vous ai transmis ?

#### 3.1.3. Formalisation

![](./assets/dechiffrer.jpeg)

### 3.2. Chiffrer

#### 3.2.1. Principe

Comme l'opération de déchiffrement, Chiffrer consiste à décaler les lettres de l’alphabet d’un certain rang (la « clé »). 

Chiffrer consiste à appliquer un __décalage aux lettres dans le sens de lecture de l'alphabet__.

#### 3.2.2. À Faire

1. Soit le clair `MASSENA`, quel est le chiffré obtenu pour une clé égale à au nombre d'élèves présents dans la salle ?
2. Convenez d'une clé commune $`k`$  avec votre voisin direct. Pensez à un mot (de 5 à 10 lettres) et chiffrez le avec la clé $`k`$. Transmettez sur papier le chiffré obtenu à votre voisin. Échangez vos feuilles et déchiffrez le mot de votre voisin.
3. Comment effectuer la même opération avec un autre voisin que le vôtre ?

#### 3.2.3. Formalisation

![](./assets/chiffrer.jpeg)

## 4. Pour aller plus loin

1. Histoire des codes secrets, Simon Singh, Livre de Poche, ISBN : 978-2-253-15097-8
2. Les codes secrets, Chaîne ScienceEtonnante, [Youtube](https://www.youtube.com/watch?v=8BM9LPDjOw0)
3. À quel point le chiffrement 256 bits est-il sûr ?, Chaîne 3blue1Brown, [Youtube](
