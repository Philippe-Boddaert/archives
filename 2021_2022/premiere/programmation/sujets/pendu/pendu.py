# Author : 
# Date :
# Description : Implantation du jeu Pendu

import random

def dessiner(nb):
    '''
    Renvoi le dessin du pendu en fonction du nombre d'erreurs du joueur
    :param nb: (int) Nombre d'erreurs
    :return: (str) La chaine de caractères correspondant au dessin du pendu
    '''
    tab=[
    """
     +-------+
     |
     |
     |
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |
     |
     |
    ==============
    """
      ,
    """
     +-------+
     |       |
     |       O
     |       |
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |      |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |      | |
     |
    ==============
    """
    ]
    return tab[nb]

# Tant que le jeu n'est pas fini
#   la proposition courante est affichée
#   On demande une lettre au joueur
#   Si cette lettre n'est dans le mot alors
#      on incrémente le nombre d'erreurs
#   Sinon
#      la lettre est revélée dans la proposition courante
#   On affiche le dessin du pendu en fonction du nombre courant d'erreurs
# Si le joueur a gagné alors
#   On affiche le message 'Le joueur a gagné !'
# Sinon
#   On affiche le message 'Le joueur a perdu ! Le mot était :' avec le mot à trouver
