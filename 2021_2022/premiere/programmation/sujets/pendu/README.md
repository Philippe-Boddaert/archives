# JEU DU PENDU

## 1. Règles du jeu

Le Pendu est un jeu consistant à trouver un mot en devinant quelles sont les lettres qui le composent.

Le jeu se joue traditionnellement à deux, avec un papier et un crayon, selon un déroulement bien particulier. Dans notre cas, un joueur sera l'ordinateur.

```
1. L'ordinateur choisit au hasard un mot et dessine une rangée de tirets, chacun correspondant à une lettre de ce mot.
2. Le joueur humain annonce une lettre.
3. La lettre fait-elle partie du mot ?
   4. Oui : l'ordinateur l'inscrit à sa place autant de fois qu'elle se trouve dans le mot.
   5. Non : l'ordinateur dessine le premier trait du pendu.
6. Le jeu se poursuit jusqu'à ce que :
7. le joueur humain gagne la partie en trouvant toutes les lettres du mot et/ou en le devinant correctement.
8. l'ordinateur gagne la partie en complétant le dessin du pendu.
```

## 2. Exemple

Partie perdue par le joueur : 
![](./gagne.gif)

Partie gagnée par le joueur : 
![](./perdu.gif)

## 3. Mode opératoire

Vous avez à votre disposition :

- un fichier `words.txt` qui contient 800 mots,
- un script `pendu.py` contenant une fonction qui permet de dessiner le pendu.

```python
 def dessiner(nb):
    '''
    Renvoi le dessin du pendu en fonction du nombre d'erreurs du joueur
    :param nb: (int) Nombre d'erreurs
    :return: (str) La chaine de caractères correspondant au dessin du pendu
    '''
    tab=[
    """
     +-------+
     |
     |
     |
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |
     |
     |
    ==============
    """
      ,
    """
     +-------+
     |       |
     |       O
     |       |
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |      |
     |
    ==============
    """,
    """
     +-------+
     |       |
     |       O
     |      -|-
     |      | |
     |
    ==============
    """
    ]
    return tab[nb]
```

- Vous allez compléter le fichier `pendu.py` avec le code python des fonctions utiles à la mise en place du jeu. Vous devez être vigilant au découpage et à la documentation de vos fonctions,

## 4. Aide

- `charger_mots`, qui prend un nom de fichier en paramètre et renvoie la liste de mots qu'il contient sous la forme d'un tableau,
- `choisir_mot`, qui prend un ensemble de mots en paramètre et renvoie un mot, choisi au hasard,
- `reveler_lettre`, qui prend la lettre proposée par l'humain, la proposition en cours de construction, et le mot choisi par l'ordinateur, et renvoie la proposition dont les `-` sont échangés avec la lettre à tous les emplacements où elle apparait dans le mot.
```python
>>> reveler_lettre('e', '-------', 'beignet')
'-e---e-'
>>> reveler_lettre('a', '-e---e-', 'beignet')
'-e---e-'
>>> reveler_lettre('i', '-e---e-', 'beignet')
'-ei--e-'
```

## 5. Allez plus loin

- Afficher le nombre d'essais utilisés par le joueur pour trouver le mot,
- Afficher un message particulier si la lettre proposée par le joueur a déjà était proposée,
- Demander en début de partie le nombre de lettres minimales et maximales du mot à trouver, le jeu propose un mot de la liste répondant à ces critères,
- Modifier le jeu pour qu'au lieu de proposer une lettre, le joueur doit faire une proposition de mot. Le jeu indique le nombre de lettres faisant partie du mot ainsi que le nombre de lettres à la bonne place.
