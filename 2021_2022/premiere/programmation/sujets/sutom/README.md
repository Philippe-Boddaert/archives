# SUTOM

## 1. Règles du jeu

Le jeu repose sur la recherche de mots d'un nombre fixé de lettres.

Un candidat propose un mot qui doit contenir le bon nombre de lettres et être correctement orthographié.

Le mot apparaît alors sur une grille : 

- les lettres présentes et bien placées sont coloriées en rouge, 
- les lettres présentes mais mal placées sont cerclées de jaune,
- les lettres non présentes sont coloriées en bleu.

![](./assets/exemple.png)

Une démonstration du jeu est disponible à l'adresse : [https://philippe-boddaert.gitlab.io/sutom/](https://philippe-boddaert.gitlab.io/sutom/).

## 2. Matériel fourni

Vous avez à votre disposition :

- [index.html](./assets/index.html) : fichier qui contient la structure HTML de la page du jeu
- [style.css](./assets/style.css) : fichier qui contient le style de la page du jeu
- [sutom.js](./sutom.js) : fichier qui contient la logique d'implémentation du jeu

Le fichier `sutom.js` est le seul élément à modifier.

__N.B : Il contient une liste prédéfinie de 29 mots, que vous pouvez modifier à votre guise.__

## 3. Travail à réaliser

L'objectif est d'implémenter en JavaScript (dans le fichier `sutom.js`) la logique suivante : 

```txt
1. solution <- choisir un mot parmi une liste prédéfinie de mots
2. générer une grille de n colonnes et 6 lignes, où n est la taille du mots.
3. l <- 0 # ligne en cours
4. Révéler la première lettre de solution dans la case (l, 0), i.e première colonne de la l-ième ligne.
5. A la saisie d'une lettre par l'utilisateur, afficher la lettre dans la case adéquate.
6. Lorsque un mot de n lettres est saisi, vérifier le mot.
7. Pour chaque lettre du mot,
8.    Si la lettre est bien placée par rapport à la solution, l'encadrer en rouge,
9.    Sinon si la lettre est présente mais mal placée par rapport à la solution, l'encadrer en jaune,
10.   Sinon, l'encadrer en bleu.
11. Incrémenter l
12. Recommencer à la ligne 4 tant que le mot n'a pas été trouvé et que le nombre d'essais est non atteint.
```

## 4. Aide

Quelques instructions utiles à l'implémentation du jeu.

### 4.1. Obtenir au hasard un entier entre $`[0; n[`$

```javascript
Math.floor(Math.random()*n)
```

### 4.2. Obtenir un élément HTML via un sélecteur

```javascript
document.querySelector("#identifiant")
// ou
document.querySelector(".classe")
// ou
document.querySelectorAll(".classe")
```

### 4.3. Attacher un événement à un gestionnaire d'événements

```javascript
document.querySelector('#identifiant').addEvenListener('click', function(){
  alert('Ce message est très important !');
});
// ou
function afficherMessage(){
  alert('Ce message est très important !');
}
document.querySelector('#identifiant').addEvenListener('click', afficherMessage);
```

