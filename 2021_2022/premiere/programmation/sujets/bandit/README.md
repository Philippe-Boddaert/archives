---
title : Jeu du Bandit Manchot
author : Philippe BODDAERT
license : CC-BY-NC-SA
---

# Bandit Manchot

## 1. Contexte

Différents modèles de machine à sous, aussi appelés *bandits manchots*, existent dans les établissements de jeu.

![](./assets/machine.png)

_Source : Pixabay_

Un modèle simple consiste en trois rouleaux comportant quatre symboles, représentés ici par les symboles 🍏 💖 👑 💎.

Les combinaisons gagnantes et les gains correspondants sont indiqués ci-dessous :

|            Combinaison |       Gain |
| ---------------------: | ---------: |
|                trois 💎 | mise × 250 |
|                trois 👑 | mise × 150 |
|                trois 💖 | mise × 50 |
|               deux 💎 |   mise × 5 |
|            un seul 💎 |   mise × 2 |
| tout autre combinaison |          0 |

La distribution des symboles n'est pas _équiprobable_ sur les différents rouleaux. Voici la distribution utilisée :

![](./assets/rouleau.png)

On souhaite réaliser un jeu consistant en une utilisation de cette machine à sous : le joueur pourra miser une somme de son choix et simuler un lancer du bandit manchot.

## 2. Matériel fourni

- Le lien [https://www.online-python.com/p94FmQhxSU](https://www.online-python.com/p94FmQhxSU) est une __version de démonstration__ du jeu.
- L'algorithme principal du jeu est le suivant :

![](./assets/algorithme.jpeg)

\* Les éléments en bleu sont des fonctions à implanter en python 

- Le script [bandit.py](./bandit.py) contient :
  - une variable `rouleaux` , de type matrice, qui contient les rouleaux de symboles du jeu. Un élément d'un rouleau est un entier représentant le point de code du symbole UTF-8, parmi 🍏 💖 👑 💎.
  - les prototypes des fonctions `basculer`, `afficher_tirage`, `calculer_gain`.

## 3. Attendu

Le script `bandit.py` complété avec l'implantation des fonctions (avec la documentation associée) et l'algorithme principal du jeu du Bandit Manchot.

## 4. Aide

Les spécifications des fonctions sont les suivantes :

- `basculer`, prend en paramètre un ensemble de _rouleaux_ sous forme d'une matrice et renvoie un tableau où chaque élément est un point de code d'un rouleau,
- `afficher_tirage`, prend en paramètre un _tirage_ sous la forme d'un tableau de points de code et a pour effet de bord d'afficher le tirage de symboles du bandit manchot,
- `calculer_gain`, prend en paramètre un _tirage_, sous la forme d'un tableau de points de code et renvoie le gain associé à la combinaison sous la forme d'un entier.

La fonction `chr` prend un point de code d'un caractère et renvoie le caractère UTF-8 associé, sous la forme d'une chaine de caractères.

```python
>>> chr(128142)
>>> '💎'
>>> chr(128081)
>>> '👑'
```

## 5. Pour aller plus loin

Nous allons truquer la machine du Bandit manchot....grâce à une combinaison secrète.

La combinaison secrète est modélisée comme un tableau de point de code UTF-8 :

```python
SECRET = [8593, 8593, 8595, 8595, 8592, 8594, 8592, 8594, 66, 65]
```

Lorsque le jeu invite l'utilisateur à renseigner sa mise, s'il renseigne la combinaison secrète, alors la mise est de 100 et le tirage est toujours gagnant (parmi les combinaisons gagnantes).

1. Quelle est la combinaison secrète ?
2. Comment, à partir d'une séquence renseignée par l'utilisateur, est-il possible de vérifier s'il s'agit de la combinaison secrète ?
3. Implanter dans le jeu, la logique de trucage du jeu.
