---
title : Le Vingt-et-un
author : M. BODDAERT
License : CC-BY-NC-SA
---
# &spades; Le Vingt-et-un &spades;

## 0. Préambule

Le module `dé` est implémenté.

## 1. Contexte

On souhaite programmer un jeu qui s’apparente au BlackJack mais se joue avec deux dés, le Vingt-et-un.

Le __joueur__ est opposé à __la banque__, le but est de s’approcher de la valeur 21 sans dépasser ce total, en lançant un nombre de dés choisi.

![Backjack](./assets/blackjack.png)

_Source : Pixabay_

## 2. Règles du jeu

Une partie se déroule de la manière suivante :

```txt
1. Tant que la partie n'est pas finie
2. 	On demande au joueur de choisir de lancer 0, 1 ou 2 dés (noté n)
3. 	S’il choisit 0, la partie s’arrête
4. 	Sinon, 
5. 		n dés sont lancés aléatoirement et la somme de leurs valeurs est ajouté au score du joueur. 
6. 		Le banquier lance alors le même nombre n de dés que le joueur. La somme des valeurs est ajouté au score de la banque.
```

La partie s'arrête quand l'une de ses conditions est satisfaite :

- Le score d'un des participants dépasse 21. Il a perdu et l’autre a gagné,
- Le score d'un des participants est égale 21, il a gagné et l'autre a perdu,
- Le joueur a choisi de lancer aucun dé. Le gagnant est celui dont le score est maximal.

## 3. Exemple

Un exemple de __partie perdue__ par le joueur :
```bash
Combien de dés ? 2
Au tour du joueur !
+-----+
| * * |
|     |
|  *  |
|     |
| * * |
+-----+
+-----+
| * * |
|     |
|  *  |
|     |
| * * |
+-----+
Total joueur 10
Au tour de la banque !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
+-----+
| * * |
|     |
|  *  |
|     |
| * * |
+-----+
Total banque 7

Combien de dés ? 2
Au tour du joueur !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
+-----+
| * * |
|     |
|     |
|     |
| * * |
+-----+
Total joueur 16
Au tour de la banque !
+-----+
|     |
|     |
|  *  |
|     |
|     |
+-----+
+-----+
| * * |
|     |
| * * |
|     |
| * * |
+-----+
Total banque 14

Combien de dés ? 2
Au tour du joueur !
+-----+
| * * |
|     |
|  *  |
|     |
| * * |
+-----+
+-----+
| * * |
|     |
|  *  |
|     |
| * * |
+-----+
Total joueur 26
Au tour de la banque !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
+-----+
|     |
|     |
|  *  |
|     |
|     |
+-----+
Total banque 17
Vous avez perdu, vous avez dépassé 21.
```

Un exemple de __partie gagnée__ par le joueur :
```bash
Combien de dés ? 1
Au tour du joueur !
+-----+
|     |
|     |
|  *  |
|     |
|     |
+-----+
Total joueur 1
Au tour de la banque !
+-----+
| * * |
|     |
| * * |
|     |
| * * |
+-----+
Total banque 6

Combien de dés ? 2
Au tour du joueur !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
+-----+
|   * |
|     |
|  *  |
|     |
| *   |
+-----+
Total joueur 6
Au tour de la banque !
+-----+
|   * |
|     |
|  *  |
|     |
| *   |
+-----+
+-----+
| * * |
|     |
| * * |
|     |
| * * |
+-----+
Total banque 15

Combien de dés ? 1
Au tour du joueur !
+-----+
|   * |
|     |
|  *  |
|     |
| *   |
+-----+
Total joueur 9
Au tour de la banque !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
Total banque 17

Combien de dés ? 1
Au tour du joueur !
+-----+
|     |
|     |
|  *  |
|     |
|     |
+-----+
Total joueur 10
Au tour de la banque !
+-----+
|   * |
|     |
|  *  |
|     |
| *   |
+-----+
Total banque 20

Combien de dés ? 1
Au tour du joueur !
+-----+
|   * |
|     |
|     |
|     |
| *   |
+-----+
Total joueur 12
Au tour de la banque !
+-----+
| * * |
|     |
| * * |
|     |
| * * |
+-----+
Total banque 26
Vous avez gagné, la banque a dépassé 21.
```

## 4. Mode opératoire

Pour construire l'implantation du jeu, il est possible de d'abord construire une implantation qui :

1. Demande un nombre de dés, simule les lancers et calcule du score pour un seul joueur,
2. Effectue le 1. tant que le joueur 1 indique un nombre de dés à lancer et que le score est inférieur à 21,
3. Effectue le 2. avec la gestion des 2 joueurs.

## 5. Aller plus loin 

Cette partie est à effectuer seulement si la partie précédente est validée.

Deux évolutions du jeu sont envisageables :

- __Esthétique__ : Afficher les valeurs de dés sous la forme d'une chaine de caractères __en ligne__ :
```txt
+-----+ +-----+ +-----+ +-----+ +-----+ +-----+
|     | | *   | | *   | | * * | | * * | | * * |
|  *  | |     | |  *  | |     | |  *  | | * * |
|     | |   * | |   * | | * * | | * * | | * * |
+-----+ +-----+ +-----+ +-----+ +-----+ +-----+
```

- __Mécanique__ : Gérer un système de mises où le joueur détient une somme de départ. Le joueur place une mise, à chaque manche, qu'il perd ou double selon s'il gagne ou perd la manche. Le jeu s'arrête lorsque le joueur le décide ou qu'il n'a plus d'argent.