---
title : Le Yams
author : M. BODDAERT
License : CC-BY-NC-SA
---
# &#9861; Le Yams &#9861;

## 0. Préambule

Le module `dé` est implémenté.

## 1. Contexte

On souhaite programmer un jeu qui s’apparente au Yams.

Deux joueurs s'affrontent, tour à tour. Le but est de s’approcher de la valeur 390, en établissant des combinaisons de dés.

![Backjack](./assets/yams.png)

_Source : Pixabay_

## 2. Règles du jeu

Une partie se déroule de la manière suivante :

```txt
1. Tant que la partie n'est pas finie
2.  Le joueur courant lance 5 dès
2. 	On demande au joueur de choisir de relancer les dés qu'il souhaite parmis les 5 dés, ce jusqu'à 3 reprises
3. 	À la fin du tour, le score du joueur est mis jour avec le score de la combinaison des 5 dés
4. 	On passe au joueur suivant
```

La partie s'arrête quand l'une de ses conditions est satisfaite :

- Le score d'un des participants atteint 390,
- Le gagnant est celui dont le score est maximal.

## 3. Exemple

## 4. Mode opératoire

Pour construire l'implantation du jeu, il est possible de d'abord construire une implantation qui :

1. Lance 5 dés, demande les dés à relancer et calcule le score pour un seul joueur,
2. Tant que que le score est inférieur à 390, effectue le 1. 
3. Effectue le 2. avec la gestion des 2 joueurs.

## 5. Aller plus loin 

Cette partie est à effectuer seulement si la partie précédente est validée.

Deux évolutions du jeu sont envisageables :

- __Esthétique__ : Afficher les valeurs de dés sous la forme d'une chaine de caractères __en ligne__ :
```txt
+-----+ +-----+ +-----+ +-----+ +-----+ +-----+
|     | | *   | | *   | | * * | | * * | | * * |
|  *  | |     | |  *  | |     | |  *  | | * * |
|     | |   * | |   * | | * * | | * * | | * * |
+-----+ +-----+ +-----+ +-----+ +-----+ +-----+
```

- __Mécanique__ : Gérer un système de mises où le joueur, avant que son premier lancer soit effectué, détermine la combinaison qu'il souhaite faire (brelan, full, ...). Si à la fin de son tour, il a réalisé la combinaison, il gagne les points équivalents, sinon il ne gagne aucun point. __Toute combinaison misée ne peut plus être choisie à nouveau de la partie__.

## Annexe - Valeur des combinaisons

| Combinaison  |                Description                 |                Points                 |
| :----------: | :----------------------------------------: | :-----------------------------------: |
|    Brelan    |            Trois dés identiques            | 3 x valeur de la face du dé identique |
| Petite suite |        1,2,3,4 ; 2,3,4,5 ou 3,4,5,6        |               30 points               |
| Grande suite |           1,2,3,4,5 ou 2,3,4,5,6           |               40 points               |
|     Full     | Trois dés identiques + deux dés identiques |               25 points               |
|    Carré     |           Quatre dés identiques            | 4 * valeur de la face du dé identique |
|   Yahtzee    |            Cinq dés identiques             |               50 points               |
|      -       |          Toute autre combinaison           |                0 point                |
