import random

des = {
    1 : ["+-----+",
         "|     |",
         "|     |",
         "|  *  |",
         "|     |",
         "|     |",
         "+-----+"],
    2 : ["+-----+",
         "|   * |",
         "|     |",
         "|     |",
         "|     |",
         "| *   |",
         "+-----+"],
    3 : ["+-----+",
         "|   * |",
         "|     |",
         "|  *  |",
         "|     |",
         "| *   |",
         "+-----+"],
    4 : ["+-----+",
         "| * * |",
         "|     |",
         "|     |",
         "|     |",
         "| * * |",
         "+-----+"],
    5 : ["+-----+",
         "| * * |",
         "|     |",
         "|  *  |",
         "|     |",
         "| * * |",
         "+-----+"],
    6 : ["+-----+",
         "| * * |",
         "|     |",
         "| * * |",
         "|     |",
         "| * * |",
         "+-----+"]
    }
    
def lancer(n):
    '''
    Lancer n dés, aléatoire
    :param n: (int) nombre de dés à lancer
    :return: (list) Tableau de valeur de dés
    '''
    des = []
    
    for i in range(n):
        des.append(random.randint(1, 6))
    
    return des

def somme(des):
    '''
    Calcule la somme des valeurs des dés
    :param des: (list) Tableau de valeurs de dés
    :return: (int) somme des valaurs
    '''
    return sum(des)

def afficher(valeur):
    '''
    Affiche la valeur des dés
    :param valeur: (int) Valeur du dé
    :return: None
    :Effet de bord: La valeur du dé est affiché
    :CU: 1 <= de <= 6
    '''
    print("\n".join(des[valeur]))
    
def afficher_des(valeurs):
    '''
    Affiche la valeur des dés
    :param valeurs: (list) tableau de valeurs de dés
    :return: None
    :Effet de bord: La valeur du dé est affiché
    :CU: 1 <= de <= 6
    '''
    if len(valeurs) == 1:
        print("\n".join(des[valeurs[0]]))
    else:
        chaine = ""
        
        for ligne in range(7):
            for de in valeurs:
                chaine = chaine + des[de][ligne] + " "
            chaine = chaine + "\n"
        
        print(chaine)
            