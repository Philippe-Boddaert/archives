# Projet - 421

## 1. Contexte

Le **421** est un **jeu de dés** très populaire en France. En effet, ses règles simples et sa convivialité en font un des classiques des jeux de convivialité.

Pour jouer au 421, il vous faut :

- Être minimum deux joueurs.
- 3 dés
- Un « pot » de 21 jetons.

## 2. Règles du Jeu

- Le premier joueur lance les trois dés. 
- Pour chacun des trois dés, il peut choisir de relancer ou non. 
- S’il ne relance pas il aura effectué un seul lancer. 
- Le joueur peut, s'il le désire, relancer une deuxième et une troisième fois tout ou partie des trois dés.
- Ceci va influer sur le reste de la manche car les autres joueurs devront effectuer le même nombre de lancers que le premier joueur.
- A la fin du tour, son score est mis à jour avec la valeur en jetons de la combinaison de ses 3 dès (Cf. Annexe - Valeur des combinaisons) et on passe au joueur suivant
- Le gagnant est celui qui atteint le premier le score de 21.

## 3. Objectifs

L'objectif du projet est d'implanter le jeu du 421, en version 2 joueurs.

Pour cela, il est possible de le programmer par __version itérative__.

- __Version du jeu 1.__ Écrire un programme, ne prenant en compte qu'un joueur, lui demande un nombre $`n`$ de dés à lancer ($`n <= 3`$) et effectue un tirage. Le jeu s'arrête si le joueur indique un nombre de dés à lancer nul.
- __Version du jeu 2.__ Reprendre la version 1 et ajouter la gestion de la relance, i.e le joueur choisit les dés qu'ils souhaitent garder et seuls ceux non sélectionnés sont rejoués.
- __Version du jeu 3.__ Reprendre la version 2 et ajouter le calcul du score, i.e à la fin d'un tour (lorsque 3 tirages ont été effectués ou le joueur a indiqué un nombre de dés à lancer nul), le score du joueur est incrémenté de la valeur de la combinaisons des dés.
- __Version du jeu 4.__ Reprendre la version 3 et ajouter la gestion du jeu à 2 joueurs
- __Version du jeu 5.__ Reprendre la version 4 et y apporter toutes évolutions de votre choix (graphiques, nouvelles règles ....)

## 4. Modalités d'évaluation

Ce projet vise à développer et évaluer les __compétences transversales__ suivantes :

- Faire preuve d’autonomie, d’initiative et de créativité ;
- Présenter un problème ou sa solution, développer une argumentation dans le cadre d’un débat ;
- Coopérer au sein d’une équipe dans le cadre d’un projet ;
- Rechercher de l’information, partager des ressources.


## 5. Attendus

- Le projet est à réaliser par __binôme ou trinôme__,
- Un script .py est à fournir par version du jeu contenant :
  - les fonctions et procédures de la version,
  - les documentations et jeu de tests afférents.

- Un __cahier de bord__ (au format markdown) est à maintenir et à fournir contenant :
  - 1 argumentation sur les choix de __structures de données__ utilisées,
  - 1 rubrique sur les __questions et problèmes rencontrés__ d'ordre technique (sur l'utilisation d'une bibliothèque, implantation d'une fonction...).
  - 1 rubrique sur l'__organisation de l'équipe__ : répartition des tâches, état de la coopération, sujet ayant fait débat, moyens de médiation et résolution employés.

## Annexe - Valeur des combinaisons

| **Combinaisons**                             | **Valeurs en jetons** |
| :--------------------------------------------: | :---------------------: |
| 421                                          | 10 jetons             |
| 3 As                                         | 7 jetons              |
| 2 As – Six /ou/ 3 Six                        | 6 jetons              |
| 2 As – Cinq /ou/ 3 Cinq                      | 5 jetons              |
| 2 As – Quatre /ou/ 3 Quatre                  | 4 jetons              |
| 2 As – Trois /ou/ 3 Trois                    | 3 jetons              |
| 2 as – Deux /ou/ 3 deux                     | 2 jetons              |
| Une suite *(1,2,3 ou 6,5,4 par exemple)*     | 2 jetons              |
| La combinaison 2,2,1 est appelée « nénette ». | -1 jeton |
| Toutes les autres combinaisons  | 0 jeton               |