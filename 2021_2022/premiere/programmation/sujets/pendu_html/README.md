# PENDU

## 1. Règles du jeu

Le Pendu est un jeu consistant à trouver un mot en devinant quelles sont les lettres qui le composent.

Le jeu se joue traditionnellement à deux, avec un papier et un crayon, selon un déroulement bien particulier. Dans notre cas, un joueur sera l'ordinateur.

```txt
1. L'ordinateur choisit au hasard un mot et dessine une rangée de tirets, chacun correspondant à une lettre de ce mot.
2. Le joueur humain annonce une lettre.
3. La lettre fait-elle partie du mot ?
   4. Oui : l'ordinateur l'inscrit à sa place autant de fois qu'elle se trouve dans le mot.
   5. Non : l'ordinateur dessine le premier trait du pendu.
6. Le jeu se poursuit jusqu'à ce que :
7. le joueur humain gagne la partie en trouvant toutes les lettres du mot et/ou en le devinant correctement.
8. l'ordinateur gagne la partie en complétant le dessin du pendu.
```

![](./assets/exemple.png)

Une démonstration du jeu est disponible à l'adresse : [https://philippe-boddaert.gitlab.io/pendu/](https://philippe-boddaert.gitlab.io/pendu/).

## 2. Matériel fourni

Vous avez à votre disposition :

- [index.html](./assets/index.html) : fichier qui contient la structure HTML de la page du jeu
- [style.css](./assets/style.css) : fichier qui contient le style de la page du jeu
- [pendu.js](./pendu.js) : fichier qui contient la logique d'implémentation du jeu

Le fichier `pendu.js` est le seul élément à modifier.

__N.B : Il contient une liste prédéfinie de 29 mots ainsi que les dessins du pendu sous la forme d'un tableau de chaines de caractères, que vous pouvez modifier à votre guise.__

## 3. Travail à réaliser

L'objectif est d'implémenter en JavaScript (dans le fichier `pendu.js`) la logique du jeu décrite dans le 1.

## 4. Aide

Quelques instructions utiles à l'implémentation du jeu.

### 4.1. Obtenir au hasard un entier entre $`[0; n[`$

```javascript
Math.floor(Math.random()*n)
```

### 4.2. Obtenir un élément HTML via un sélecteur

```javascript
document.querySelector("#identifiant")
// ou
document.querySelector(".classe")
// ou
document.querySelectorAll(".classe")
```

### 4.3. Attacher un événement à un gestionnaire d'événements

```javascript
document.querySelector('#identifiant').addEvenListener('click', function(){
  alert('Ce message est très important !');
});
// ou
function afficherMessage(){
  alert('Ce message est très important !');
}
document.querySelector('#identifiant').addEvenListener('click', afficherMessage);
```

