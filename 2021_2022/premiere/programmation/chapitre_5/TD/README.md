---
title: TD - Gestion de dé
author: M. BODDAERT
license: CC-BY-NC-SA
---

# TD - Gestion de dé

## 1. Objectifs

L'objectif de ce TD est de créer une bibliothèque de gestion de dé, pouvant être utilisé ultérieurement dans d'autres programmes.

La bibliothèque sera implementée sous la forme d'un script `de.py`.

## 2. Tirage aléatoire

Écrire une fonction `lancer`, qui prend en paramètre un nombre $`n`$ de dés à lancer, sous la forme d'un entier et renvoie la simulation le lancer aléatoire de $`n`$ dés, sous la forme d'un tableau de valeurs.

Le tirage aléatoire d'un dé peut être implanté à l'aide de la méthode `randint` du module `random`.

Des exemples d'utilisation :

```python
>>> import random
>>> random.randint(0, 10)
8
>>> random.randint(0, 10)
4
>>> help(random.randint)
Help on method randint in module random:

randint(a, b) method of random.Random instance
    Return random integer in range [a, b], including both end points.
```

## 3. Affichage

Écrire une procédure `afficher`, qui prend en paramètre un dé, sous la forme d'une valeur entière (entre 1 et 6) et a pour effet de bord d'afficher la valeur du dé sous la forme d'une face (Exemple pour 1 et 6) :

```txt
+-----+
|     |
|     |
|  *  |
|     |
|     |
+-----+
```

ou 

```txt
+-----+
| * * |
|     |
| * * |
|     |
| * * |
+-----+
```

__N.B : Il est possible de d'abord s'interroger sur la structure de données adéquate pour stocker les différentes forme des faces d'un dé.__

## 4. Somme d'un tirage

Écrire une fonction `somme` qui prend en paramètre des dés, sous la forme d'un tableau de valeurs et renvoie la somme des valeurs du tirage.

## 5. Analyse de l'aléatoire

Nous utilisons la bibliothèque `random` mais garantit-elle l'équiprobabilité des tirages ?

Autrement dit, pour un "grand" nombre de lancers, la probabilité d'avoir 1, 2, 3, 4, 5 ou 6 est-elle la même ? Dans le cas où la réponse est négative, nous aurions...un dé biaisé !

Écrire une fonction `occurrence` qui prend en entrée le nombre de tirages $`n`$ voulu et renvoie le nombre d'occurrences de chaque face du dé, sous la forme d'un dictionnaire.

## 6. Exécution du module

En vous reportant au 2.2 du [Compléments du cours](../COMPLEMENTS.md), ajouter un lancement d'1 dé au lancement du script.
