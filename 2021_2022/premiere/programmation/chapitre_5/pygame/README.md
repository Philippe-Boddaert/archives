# Pygame

## Préambule

Cette activité a pour but d'apprendre à utiliser le module pygame. Il nous sera possible de créer des interfaces graphiques (affichage de formes, gestion des entrées clavier, souris...)

## Définition

Pygame est un module pour Python pour le contrôler les fenêtres, vidéos et sons.
Ce module fournit des méthodes pour faciliter la création d'application multimédias, qui s'exécuteront sans modification sur toutes les plateformes (windows, Linux, Mac...)

La documentation officielle est disponible à cette url : [https://www.pygame.org/docs/](https://www.pygame.org/docs/)

## Installation

Comme tout module python, Pygame doit être installé par l'intermédiaire de pip.

```bash
$ python3 -m pip install pygame==1.9.6
```

## Importer

Pour utiliser le module, il faut l'importer dans un script .py.

```python
import pygame
pygame.init()
```

La fonction `pygame.init()` doit être appelée pour que tous les modules importés dans `pygame` soient correctement initialisés. Si nous oublions cela, certains modules ne fonctionneront pas.

## Exercice 1 - Une fenêtre blanche

Pour comprendre les mécanismes du module, copiez le code suivant dans un script .py et exécutez le.

```python
import pygame
pygame.init()

screen = pygame.display.set_mode((720, 480))

while True:
	screen.fill((255, 255, 255))
	pygame.display.update()
```

La ligne 4 permet de créer et afficher une fenêtre au dimension (largeur, hauteur)

La ligne 7 permet de mettre la couleur de fond à blanc.

### À Faire

1. Commenter la ligne 6 et exécuter le code. Que constatez-vous ?
2. Décommenter le ligne 6 et commenter la ligne 8 et exécuter le code. Que constatez-vous ?

On déduit que :

- une fenêtre gérée par Pygame reste affichée le temps de l'exécution du script. Il est donc nécessaire de mettre en place une boucle `while`.
- le remplissage de la fenêtre n'est visible à l'écran qu'après l'appel à la méthode `pygame.display.update`.

### À Faire 

1. En regardant la documentation de pygame, modifiez le script pour afficher la fenêtre en plein écran avec un fond de couleur jaune.

## Exercice 2 - Un segment vert, un carré bleu, un cercle rouge

Copiez le code suivant dans un script .py et exécutez le.

````python
import pygame
pygame.init()

screen = pygame.display.set_mode((720, 480))
rectangle = pygame.Rect((360, 240), (32, 32))

while True:
	screen.fill((255, 255, 255))
	pygame.draw.rect(screen, (0, 0, 255), rectangle)
	pygame.display.update()
````

La ligne 5 permet de créer un rectangle de dimension (largeur, hauteur), positionnée aux coordonnées (x, y).

La ligne 9 permet d'afficher le carré en rouge à la position définie dans la fenêtre.

### À Faire

1. Placez l'instruction de la ligne 9 avant la ligne 8 et exécutez le code. Que constatez-vous ?
2. En regardant la documentation de pygame, modifiez le script pour afficher un segment bleu qui passe par les points de coordonnées (32,32) et (100, 50) et de largeur 5.
3. Modifiez le script pour afficher un cercle rouge de rayon 16 dont le centre est au centre de la fenêtre.

On en déduit que l'ordre d'affichage dépend de l'ordre de leur manipulation.

## Exercice 3 - Les événements

Copiez le code suivant dans un script .py et exécutez le.

```python
import pygame
pygame.init()

screen = pygame.display.set_mode((720, 480))
rectangle = pygame.Rect((360, 240), (32, 32))

COLORS = ((255, 0, 0), (0, 255, 0), (0, 0, 255))
color = 0

while True:
    
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            color = (color + 1) % len(COLORS)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                quit()
            elif event.key == pygame.K_RIGHT:
                rectangle.move_ip((10, 0))
    
    screen.fill((255, 255, 255))
    pygame.draw.rect(screen, COLORS[color], rectangle)
    pygame.display.update()
```

### À Faire

1. Cliquez sur le bouton gauche de votre souris, que constatez-vous ?
2. Appuyez plusieurs fois sur la flèche &rarr; , que constatez-vous ?
3. Appuyez sur la touche Esc, que constatez-vous ?

On déduit qu'il est possible de capturer les saisies du clavier, ou de la souris et d'y associer des actions.

4. En regardant la documentation de pygame, modifiez le script pour déplacer le carré selon les directions &larr;, &uarr; et &darr;.
5. En regardant la documentation de pygame, modifiez le script pour réduire la taille du carré sur la touche $`-`$ du pavé numérique et agrandir la taille du carré sur un la touche $`+`$ du pavé numérique.
6. En regardant la documentation de pygame, modifiez le script pour que le carré rouge suive les déplacements de la souris.

## Exercice 4 - Les collisions

Copiez le code suivant dans un script .py et exécutez le.

```python
import pygame
pygame.init()

screen = pygame.display.set_mode((720, 480))
rectangle = pygame.Rect((360, 240), (32, 32))
ground = pygame.Rect((0, 460), (720, 20))

COLORS = ((255, 0, 0), (0, 255, 0), (0, 0, 255))
color = 0

while True:
    
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                quit()
            elif event.key == pygame.K_RIGHT:
                rectangle.move_ip((10, 0))
            elif event.key == pygame.K_LEFT:
                rectangle.move_ip((-10, 0))
            elif event.key == pygame.K_UP:
                rectangle.move_ip((0, -10))
            elif event.key == pygame.K_DOWN:
                rectangle.move_ip((0, 10))

    color = 1 if rectangle.colliderect(ground) else 0
    
    screen.fill((255, 255, 255))
    
    pygame.draw.rect(screen, COLORS[color], rectangle)
    pygame.draw.rect(screen, COLORS[1], ground)
    pygame.display.update()
```

### À Faire

1. Appuyez plusieurs fois sur la flèche &darr;, que constatez-vous lorsque le rectangle touche la barre verte ?
2. En consultant la documentation de pygame, modifiez le script précédent pour qu'un cercle rouge suive le mouvement de la souris et que le carré change de couleur lorsque vous passez la souris au-dessus de sa position (Cf vidéo ci-dessous)

<figure class="video_container">
  <video controls
    src="assets/exercice-4-2.mp4"
    >
</video>
<figcaption style='text-align:center;'>Résultat attendu pour la question 2 (Source : <a href="assets/exercice-4-2.mp4">exercice-4-2.mp4</a>)</figcaption>
</figure>

3. En consultant la documentation de pygame, modifiez le script de la question 2 pour qu'à chaque fois que la souris passe au-dessus du carré, le compteur de collision est mise à jour

<figure class="video_container">
  <video controls
    src="assets/exercice-4-3.mp4"
    >
</video>
<figcaption style="text-align : center;">Résultat attendu pour la question 3 (Source : <a href="assets/exercice-4-3.mp4">exercice-4-3.mp4</a>)</figcaption>
</figure>