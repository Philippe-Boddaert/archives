import math

def retirer_cle(numero_secu):
    """
    Description de la fonction : Retire la cle d'un numéro de sécurité sociale
    parametre : numero_secu (int) : numéro de sécurité sociale complet (avec la clé)
    Préconditions sur les paramètres : numero_secu doit posséder 15 chiffres
    return (int) : numéro de sécurité sociale sans la clé
    Postcondition sur le résultat : numéro de 13 chiffres
    Effet de bord : Néant
    """
    return int(str(numero_secu)[0:13])

def extraire_cle(numero_secu):
    """
    Description de la fonction : Extrait la cle d'un numéro de sécurité sociale
    parametre : numero_secu (int) : numéro de sécurité sociale complet (avec la clé)
    Préconditions sur les paramètres : numero_secu doit posséder 15 chiffres
    return (int) : clé de numero_secu
    Postcondition sur le résultat : numéro de 2 chiffres
    Effet de bord : Néant
    """
    return int(str(numero_secu)[13:])


def verifier_numero_secu(numero_secu):
    """
    Description de la fonction ; Indique si le numéro de sécurité sociale est valide ou non.
    Un numéro de sécurité sociale est valide si :
        - il contient 15 chiffres
        - la clé (2 derniers chiffres) correspond à l'application de l'algorithme de Luhn sur les 13 premiers chiffres
    parametre : numero_secu (int) : numéro de sécurité sociale complet (avec la clé)
    Préconditions sur les paramètres : Néant
    return (bool) : True si le numéro est valide, False sinon. 
    Postcondition sur le résultat : Néant
    Effet de bord : Néant
    """
    numero_sans_cle = retirer_cle(numero_secu)
    cle = extraire_cle(numero_secu)
    return math.ceil(math.log10(numero_secu)) == 15 and cle == (97 - numero_sans_cle % 97)
    #return True