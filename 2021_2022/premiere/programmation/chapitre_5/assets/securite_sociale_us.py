import math


def verifier_numero_secu(numero_secu):
    """
    Description de la fonction ; Indique si le numéro de sécurité sociale est valide ou non.
    Un numéro de sécurité sociale est valide si :
        - il contient 11 caractères
        - il est constitué de 3 groupes de chiffres, séparés par un '-' :
            - le premier ensemble de 3 chiffres est appelé Area Number
            - le second ensemble de 2 chiffres est appelé Group Number
            - le troisième ensemble de 4 chiffres est appelé Serial Number
    parametre : numero_secu (str) : numéro de sécurité sociale complet
    Préconditions sur les paramètres : Néant
    return (bool) : True si le numéro est valide, False sinon. 
    Postcondition sur le résultat : Néant
    Effet de bord : Néant
    """
    ensemble = numero_secu.split("-")
    return len(ensemble) == 3 and math.log10(int(ensemble[0])) == 3 and math.log10(int(ensemble[1])) == 2 and math.log10(int(ensemble[2])) == 4


