---
title : Les Bibliothèques
author: M. BODDAERT
license : CC-BY-NC-SA
---
# Les bibliothèques

## 1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Utilisation de bibliothèques | Utiliser la documentation d’une bibliothèque. |

## 2. Contexte

### 2.1. Rappel

Aujourd'hui, en Python, nous savons :

- créer et affecter une **valeur** à une **variable** 
```python
x = 12
```
- créer et appeler une **fonction** 
```python
def est_pair(entier):
  """
  Indique si l'entier est pair, ou non
  """
  return entier % 2 == 0
est_pair(3)
```
- tester une **expression booléenne** 
```python
x = 3
if est_pair(x):
    print(x, "est pair")
else:
    print(x, "est impair")
```
- effectuer des **boucles**
```python
somme = 0
for i in range(10):
    somme = somme + i
```

Mais Python est un langage qui permet de faire d'autres choses.

### 2.2. Exemples


```python
# calculer la racine carrée d'un nombre. Exemple : racine carrée de 9 ?
import math

math.sqrt(9)
```


```python
# afficher un graphique. Exemple : 4 points aux coordonnées (1,2), (3,3), (4,5), (6,1)
from matplotlib import pyplot

x = [1, 3, 4, 6]
y = [2, 3, 5, 1]

pyplot.plot(x, y)
pyplot.show()
```


```python
# afficher une carte. Exemple : une carte, dont le centre, est le lycée Masséna, à Nice.
import folium

m = folium.Map(location=[43.699478, 7.275392], zoom_start=22)

folium.Marker([43.699478, 7.275392], tooltip="Nous sommes au Lycée Masséna !").add_to(m)

m
```

## 3. Les bibliothèques

### 3.1. Définition

> Une __bibliothèque__ est :
> - un ensemble de fonctions existantes, utilisables dans un script Python,
> - une "**boîte noire**", dont on utilise les fonctions, sans connaitre leurs implémentations,
> - équivalent aux termes ```module```, ```paquet``` ou ```script```.

![Boite noire, Source : Pixabay](https://cdn.pixabay.com/photo/2015/03/22/18/18/faq-685060_960_720.jpg)

Nous n'avons pas besoin de réécrire ses fonctions, ni de les maintenir dans notre programme.

Met en oeuvre le principe de la programmation **modulaire** :

- Réutilisation de code existant,
- Facilitation de maintenance (il suffit de modifier une fois le code).

Il existe des centaines de milliers de bibliothèques. Le site [PyPI](https://pypi.org/) en recense 207000.

### 3.2. Comment utiliser une bibliothèque ?

1. Installer la bibliothèque, grâce à la commande shell suivante :
```bash
pip3 install <nom de la bibliothèque>
```
2. Importer la bibliothèque dans votre programme :
```python
import <nom de la bibliothèque>
```
3. Pour utiliser les fonctions d'une bibliothèque, il faut les préfixer du nom de la bibliothèque avec un point au milieu.
```python
import math

math.ceil(3.14)
```

### 3.4. Quelles sont les bibliothèques disponibles sur ma machine ?

Pour connaitre les bibliothèques installés et disponibles, il faut saisir dans l'interpréteur Python, l'instruction : 

```python
help('modules')
```

Quelques bibliothèques natives disponibles :

| nom      | Descriptif                                                   |
| :------- | :----------------------------------------------------------- |
| random   | fonctions permettant de travailler avec des valeurs aléatoires |
| math     | toutes les fonctions utiles pour les opérations mathématiques (cosinus,sinus,exp,etc.) |
| sys      | fonctions systèmes                                           |
| os       | fonctions permettant d'interagir avec le système d'exploitation |
| time     | fonctions permettant de travailler avec le temps             |
| calendar | fonctions de calendrier                                      |
| doctest  | fonctions permettant de manipuler le gestionnaire de tests   |

### 3.5. Quelles sont les fonctions disponibles d'une bibliothèque ?

Pour connaitre les bibliothèques installées et disponibles, il faut saisir dans l'interpréteur python, l'instruction : 

```python
dir(math)
```

Rappel : la commande ```help``` peut nous aider quant à la spécification et utilisation d'une fonction.

```python
help(math.sqrt)
```

## 4. Comment créer une bibliothèque ?

Une bibliothèque est un fichier, contenant du code python, suivi du suffixe "```.py```". 

Pour créer, gérer ces fichiers, un éditeur (autre que Basthon) est nécessaire :

- Pizo, Edupython, Spyder sont les logiciels disponibles sur les environnements,
- https://trinket.io/ est un éditeur en ligne permettant de créer des modules.

### 4.1. Exemple

Dans un programme, dans le même répertoire que la bibliothèque ```securite_sociale_fr```, exécutons les instructions suivantes :

```python
import securite_sociale_fr

print(securite_sociale_fr.verifier_numero_secu(123456789012311))
print(securite_sociale_fr.verifier_numero_secu(123456789012312))
```

Dans un autre programme, dans un autre répertoire que la bibliothèque ```securite_sociale_fr```, exécutons les mêmes instructions.

Constate-t-on le même comportement ?

### 4.2. Bilan

- ```import``` nécessite le nom de la bibliothèque sans les guillemets et sans l'extension ```.py```,
- Pour utiliser les fonctions, il faut les préfixer du nom de la bibliothèque avec un point au milieu,
- Nos bibliothèques doivent être dans le même répertoire que le programme principal.

## 5. La primitive import

### 5.1. À Faire

Importons la bibliothèque ```securite_sociale_fr``` dans le Shell Python de l'éditeur et regardons certains éléments :

1. Que se passe-t-il dans la vue ```variables``` avant / après un import ?
2. Que se passe-t-il dans la vue ```variables``` avant / après affectation de la valeur 112345678910111 à `mon_ine` dans la bibliothèque ```securite_sociale_fr``` ?
3. Que se passe-t-il dans la vue ```variables``` avant / après modification de la bibliothèque (la fonction est_valide renvoie True)?

### 5.2. Bilan

- Après un import, Python créé une variable ayant pour nom, le nom de la bibliothèque et de type ```module```,
- Comme toute variable, sa valeur peut être modifiée. Donc, **attention à la manipulation des variables !**
- Si la bibliothèque est modifiée, les changements sont pris en compte au prochain ```import <nom du module>```.

### 5.3. À Faire

Importons, de différentes manières, la bibliothèque ```math```dans l'éditeur et regardons la vue ```variables``` :

1. ```import math```. Quelles sont les variables créées ? De quelle type ?
2. ```from math import cos, sqrt```. Quelles sont les variables créées ? De quelle type ?
3. ```from math import *```. Quelles sont les variables créées ? De quelle type ?

### 5.4. Bilan

Il faut voir l'import d'une bibliothèque comme l'accès à une arborescence :

![](./assets/arborescence.jpg)

| instruction                                                 | Commentaire                                                  |
| :---------------------------------------------------------- | :----------------------------------------------------------- |
| <span style='color:red'>import math</span>                  | On a accès à la racine de l'arborescence (ici math). Pour accéder aux fonctions, il faut descendre dans l'arborescence (en utilisant '.'). (Rouge dans l'illustration) |
| <span style='color:blue'>from math import *</span>          | On est dans l'arborescence de math. Pour accéder aux fonctions, on utilise directement le nom des fonctions. (Bleu dans l'illustration) |
| <span style='color:green'>from math import cos, sqrt</span> | On est dans l'arboresence de math, mais filtrée. On a accès, qu'aux fonctions listées, directement par leurs noms. (Vert dans l'illustration) |

## 6. Sécurité

### 6.1. À Faire

Regardons le contenu de la bibliothèque ```inconnu``` dans l'éditeur :

1. Que fait la fonction ```obtenir_message``` ?
2. Que fait la fonction ```obtenir_autre_message``` ?

Dans un programme, importons la bibliothèque ```inconnu```.

3. Que se passe-t-il quand on exécute l'instruction ```inconnu.obtenir_autre_message()``` ? 

### 6.2. Bilan

- Un module contient des instructions que nous n'avons pas forcément écrites,
- Il est dangeureux d'importer un module inconnu,
- Dans la mesure du possible :
	- on importe que les fonctions identifiées et utiles (grâce à l'instruction ```from <nom du module> import <liste de fonctions, séparées par des virgules >```),
	- on importe que des modules avec de la documentation,
	- on consulte le contenu de la bibliothèque.

# 7. Synthèse

| Quoi                                                | Où                         | Comment                                                      |
| :-------------------------------------------------- | :------------------------- | :----------------------------------------------------------- |
| Installer une bibliothèque                          | dans un shell              | ```pip3 install <nom de la bibliothèque>```                  |
| Lister les bibliothèques disponibles                | dans l'interpréteur Python | ```help('modules')```                                        |
| Charger une bibliothèque                            | dans un programme          | - ```import <nom de la bibliothèque>```<br />- ```from <nom de la bibliothèque> import <noms des fonctions séparés par une virgule>```<br />- ```from <nom de la bibliothèque> import *``` |
| Lister les fonctions disponibles d'une bibliothèque | dans l'interpréteur Python | ```dir(<nom de la bibliothèque>)```                          |
| Utiliser une fonction d'une bibliothèque            | dans un programme          | ```<nom de la bibliothèque>.<nom de la fonction>```          |