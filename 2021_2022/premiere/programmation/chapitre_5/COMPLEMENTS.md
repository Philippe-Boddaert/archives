---
title : Les Bibliothèques
author: M. BODDAERT
license : CC-BY-NC-SA
---
# Compléments sur les bibliothèques

## 1. Priorité des bibliothèques

### 1.1. À Faire

Importons le module [math](./assets/math.py), qui contient une fonction ajoute_un()

```python
import math

math.ajoute_un(10)
```

Pourquoi un tel comportement ?
le module math que j'ai écrit est bien dans le répertoire courant...

```python
math.sqrt(2)
```

Le module contenu dans le fichier math.py de mon répertoire courant ne contient pas la fonction sqrt.
Pourquoi j'obtiens ce comportement ?
```
AttributeError: module 'math' has no attribute 'sqrt'
```

### 1.2. Explication

Si 2 modules comportent le même nom, il n'est pas garanti que celui que vous importez soit celui que vous souhaitez utiliser.

Quand 2 bibliothèques ont le même nom, l'interpréteur Python choisit par priorité, en fonction de l'installation.

Pour trouver le module, Python regarde successivement dans le répertoire d'installation des modules natifs et dans le répertoire courant. 

## 2. Exécution d'un module

### 2.1. À Faire

Ajoutons à la fin de la bibliothèque ```securite_sociale_fr``` l'instruction suivante :
```
print(est_valide(189035918301290))
```
Exécutons le script comme programme principal.

1. Que constate-t-on ?

Dans un autre programme, importons la bibliothèque ```securite_sociale_fr``` et exécutons l'instruction suivante :
```
import securite_sociale_fr

print(securite_sociale_fr.est_valide(est_valide(179035918301290)))
```
2. Que constate-t-on ?

### 2.2. Explication

Quand on lance un script python, tout le script est exécuté séquentiellement.
Une bibliothèque est un script et elle ne déroge pas à cette règle : tout le script est exécuté séquentiellement au moment de l'import.

Il y a donc un problème quand on a un script qui contient du code que l’on souhaite exécuter quand on lance le script directement, mais pas quand on l’importe dans un autre script, en tant que bibliothèque.

Pour remédier à ce problème, il y a un moyen de spécifier à Python d'exécuter du code **seulement si** le script est un programme principal, en utilisant l'instruction suivante :

```
if __name__ == '__main__':
```

Dans ce cas, tout le code inclus dans le bloc ```if``` sera executé si et seulement si le script est un programme principal. Dans le cas où le script est importé, le code inclus dans le bloc ```if``` ne sera pas executé.

Ajoutons l'instruction ```if __name__ == '__main__':``` et relançons le programme principal.

3. Que constate-t-on ?

## 3. Alias

Lorque le nom des bibliothèques est long, il peut être judicieux d'utiliser l'```alias```, un nom symbolique représentant le module.

Si le nom de la bibliothèque est suivi par ```as```, alors le nom suivant as est directement lié à la bibliothèque importée. 

Pour utiliser les fonctions, on n'utilise plus le nom du module mais le nom symbolique associé.

```python
import securite_sociale_fr as secu

secu.est_valide(123456789012311)
```
