# Les fonctions

L'activité consiste en un __cours__ et un __TD__ relatifs à la notion de _fonctions_ en Python.

## Pré-requis

Avoir effectué l'activité sur les [boucles](../chapitre_3/)

## Consignes

### Cours

1. Télécharger le fichier [Cours.ipynb](./Cours.ipynb),
2. Se rendre sur [Basthon](https://notebook.basthon.fr/),
3. Ouvrir le fichier Cours.ipynb téléchargé,
4. Suivre le déroulé du Cours, réaliser les `À Faire` les réponses sont à mettre directement dans votre fichier.

### TD

1. Télécharger le fichier [TD.ipynb](./TD/TD.ipynb),
2. Se rendre sur [Basthon](https://notebook.basthon.fr/),
3. Ouvrir le fichier TD.ipynb téléchargé,
4. Suivre le déroulé du TD, les réponses sont à mettre directement dans votre fichier,
5. À la fin de la séance, n'oubliez pas de sauvegarder votre fichier sur votre clé USB ou espace personnel.
