---
title : TP Code Barre
author : M. BODDAERT
license : CC-BY-NC-SA
---
# Un premier TP

## Le code ISBN d'un livre

L'__ISBN__ est un numéro international normalisé permettant l'identification d'un livre dans une édition donnée. ISBN est l'acronyme d'__I__nternational __S__tandard __B__ook __N__umber.

__L'ISBN a été conçu pour simplifier le traitement informatisé des livres__

- Les libraires peuvent passer des commandes standardisées, les distributeurs ont le même code pour traiter les commandes et les retours, les différentes opérations de gestion dans les bibliothèques et centres de documentation sont également facilitées.
- Par ailleurs, le caractère international de cette numérotation constitue, à l'étranger également, une référence unique pour tous les professionnels du livre.

__L'ISBN identifie donc de façon unique un livre quel que soit son support de publication,imprimé ou numérique.__

![ISBN du Livre Ada ou la beauté des nombres, Catherine Dufour](./assets/isbn.png)

ISBN du livre de Catherine Dufour, _Ada ou la beauté des nombres_.

### Le TP

Ce [premier TP](./isbn.md) vous conduira à étudier le format de données de l'ISBN et la méthode qui vérifie la validité d'un code ISBN.

#### Les codes à vérifier

<table>
<tr>
<td><img src="./assets/9788175257665.png"/></td>
<td><img src="./assets/9780733426094.png"/></td>
<td><img src="./assets/9783165484100.png"/></td>
<td><img src="./assets/9780747595823.png"/></td>
<td><img src="./assets/9781234567897.png"/></td>
</tr>
</table>

## Pour les plus rapides

[Voici un second TP](./vitale.md) qui vous conduira à étudier et écrire la méthode qui vérifie la validité d'une carte vitale.
