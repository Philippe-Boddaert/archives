---
title : TD Découverte Notebook
author : M. BODDAERT
license : CC-BY-NC-SA
---
# TD - Notebook

## Objectifs

L'activité a pour but d'appréhender un cas d'utilisation du Markdown pour créer des présentations via le logiciel en ligne [Basthon](https://notebook.basthon.fr).

## Consignes

| Étapes | Actions | Résultats attendus et constatés |
| :------: | :------- | :------------------------------- |
|    1    | Se rendre à l'adresse suivante : [Cliquer ici](https://notebook.basthon.fr/?ipynb=eJy1Vdtq20AQ_ZWNAkkLjuz4UqeGENqkpSlJMbShhNiIlXZkrb0XeXflSA7-l77mvV_Q_FhHcuy4kNBSMAajOTNzZuZod3TnpdQlXs-7Uo47AcznaaFCr-YpKuE5PNLKgXJe786LQAjr9W7uPAmOMupoiVrBGdhE366NwBVpyVUZ3mJRqzJXqKRmwvStQm6rMxOV2C65oOTL13NvUdsC-S7pC6oG-Dv0SR_haaY5ORmo5tIk06wyWz451VLitORka51slB-oA3KTD0kfDFYiDEikZWpAMQNEgCWgxpDlhGVEZfLh3vBpBoRRZYmgxOqIP9y7h_sVzZlWCioPddzGNHJcqyWrcubhpwBTsUoaJVzhQ5X468eQXO5TazMsyvbpTPMyjLx_d7pNDVaar5q_wH6SUny7icSwiXzTmStbk1oxqCEDiFIQq7nDP0VGoAxsq-n_OhlZ-O81dm6GrxLnUtur1yUwTv0RT5PCxzOxtOvXHz6fyvNr1rn-fjqpL70jHp9EnB1DFDc60O5O8nmRtEM-O-o2Dpvto0Y0b3TjfPxmPle20wrziTLiNoE9g0lrir3IHY9eD1TQp5bQGUQkAqL4DGhWnglXbgA8TL7vB9saf6D-LkCW99O0_1EfXbEJz9rjT_CCCvZINMbRBLS5tbNkRJVpCWl4Zx4352nYaYdjaDansXpBhUvK1zLg7IThPcM7yhWtrtTOzg7KMKz9IUM5n9NahNTgNGecptpQSXHECRgFwqYQlXGM21TQInjct_3CJUjZwjjcUaOMjko0rdCnrby0W6j9OirgKtZVYc1AcmO0CSQ-ltBjFl_TzMBY7NzrtZAh5gICyPGdLjHPTwuMkRzHWb4fh956fvB8F6Ud4kpBToc0OCZqs-lNi1F5U2wgIK88fNX-RiNey3_rd6uTocJYG0nxE9N-MgJUW2Nyc7H4DbcnWZs) ou Ouvrir ce [fichier](./assets/DIAPORAMA.ipynb) avec Basthon |   Cela a pour effet d'ouvrir le logiciel en ligne [Basthon](https://notebook.basthon.fr) et de charger du contenu généré pour ce TD, qu'on appelle un __notebook__.   |
| 2 | Cliquer sur le bouton "Enter/Exit RISE Slideshow" | Le __notebook__ passe en mode présentation. |
| 3 | Naviguer avec les flèches droite et gauche du clavier | On constate qu'il est possible de naviguer dans la présentation dont chaque slide contient une partie du contenu chargé. |

___Comment ça marche ?___

| Étapes | Actions | Résultats attendus et constatés |
| :------: | :------- | :------------------------------- |
| 1 | Cliquer sur le bouton x | Le mode présentation est arrété |
| 2 | Double-cliquer sur le titre `LA NSI` | On constatee que la cellule passe en mode édition et affiche du code Markdown ! |
| 3 | Double-cliquer sur l'ensemble des autres cellules | Chaque cellule du fichier contient du code Markdown, langage que vous avez manipulé et maitrisé lors de la 1ère partie du TD. $`\rarr`$ Il est donc possible de faire des présentations qualitatives, simplement à partie de texte contenant du markdown. |
| 4 | Cliquer sur Affichage > Barre d'outils de cellule > Slideshow | Une barre de configuration apparait dans chaque cellule. |
| 5 | Tester les différentes valeurs dans chaque cellule et lancer le mode présentation pour comprendre l'impact de chaque valeur. | |

## Méthodologie de travail

Maintenant, c'est à vous de créer votre premier slideshow.
1. Cliquer sur Fichier > Nouveau, l'interface apparait
2. Ajouter des cellules, et le contenu de votre fichier `.md` généré dans le TD Markdown. 
3. L'objectif est de créer une présentation sur Guido Van Rossum, rien qu'avec le contenu du fichier et la configuration des cellules du __notebook__.
4. Votre présentation devra être généré en utilisant la fonction "Exporter sous..." de Basthon. 
5. L'extension du fichier généré est `.ipynb` et peut être ouverte avec Basthon...et bien d'autres éditeurs.