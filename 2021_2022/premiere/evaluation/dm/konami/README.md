# DM : Code Konami

## Contexte

__Konami__ est une société japonaise de développement et d'édition de jeux vidéo.

Depuis les années 1970, Konami a développé des licences de jeux ayant connues des succès internationaux.

Il est possible de citer :

- Castlevania,
- eFootball( anciennement connu sous le nom ISS ou Pro Evolution Soccer),
- Metal Gear Solid,
- Silent Hill,
- L'adaptation des jeux Yu-Gi-Oh!,
- Gradius...

C'est sur un des aspects du jeu Gradius que le DM porte...En effet, il s'agit du premier jeu implémentant un __code de triche__ !

## Modalités

- Cette activité est un DM **individuel**,
- Le [notebook basthon](./sujet.ipynb) contient des questions théoriques et des questions de programmation. Il est à ouvrir sur basthon
- Des zones de réponses sont prévues et doivent être __complétées directement__ dans ce document.
- Il vaut mieux traiter les questions dans l'ordre (certaines questions sont liées entre elles)

## Rendu

- Vous devez rendre le fichier complété pour le __31/01/2022 16:00__,
- Il est à déposer dans le casier dans Pronote,
- __Tout travail rendu en retard ne sera pas évalué__.

## Objectifs

L'objectif de ce DM est de développer votre compétence de la spécialité NSI "__Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions__".

Pour atteindre cet objectif, vous aurez besoin d'utiliser vos connaissances acquises depuis le début de l'année :
- Langage de programmation :  
    - Affectation  
    - Structures conditionnelles
    - Boucles bornées
    - Fonctions
- Réprésentation des données, types et valeurs de bases :
    - Conversion de base (décimale...)
