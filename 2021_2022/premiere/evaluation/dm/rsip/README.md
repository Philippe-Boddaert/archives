# DM : RSIP

## Contexte

L'activité [RSIP](https://gitlab.com/Philippe-Boddaert/premiere/-/tree/main/programmation/sujets/rsip) consistait à déchiffrer à la main un message chiffré, sur un alphabet donné, à l'aide d'une clé fournie.

![](https://media.giphy.com/media/SSM6HdOicCahnOZ5hM/giphy.gif)

Ce DM a pour but d'implanter en python les fonctions permettant de déchiffrer et chiffrer des messages textuels.

## Modalités

- Cette activité est un DM **individuel**,
- Le [notebook basthon](./sujet.ipynb) contient des questions théoriques et des questions de programmation. Il est à ouvrir sur basthon 
- Des zones de réponses sont prévues et doivent être __complétées directement__ dans ce document.
- Il vaux mieux traiter les questions dans l'ordre (certaines questions sont liées entre elles)

## Rendu

- Vous devez rendre le fichier complété pour le __03/01/2022 23:59__,
- Il est à déposer dans le casier dans Pronote,
- __Tout travail rendu en retard ne sera pas évalué__.

## Objectifs

L'objectif de ce DM est de développer votre compétence de la spécialité NSI "__Décomposer un problème en sous-problèmes, reconnaître des situations déjà analysées et réutiliser des solutions__".

Pour atteindre cet objectif, vous aurez besoin d'utiliser vos connaissances acquises depuis le début de l'année :
- Langage de programmation :  
    - Affectation  
    - Structures conditionnelles 
    - Boucles bornées
    - Fonctions
- Réprésentation des données, types et valeurs de bases :
    - Conversion de base (décimale...)
