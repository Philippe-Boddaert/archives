---
title : Pokemon
author : M. BODDAERT, d'après les travaux de Christophe Mieszczak
license : CC-BY-NC-SA
---
# Pokemon

## 1. Contexte

Le format __CSV__ est un format texte utilisé pour sauvegarder des données en utilisant une structure simple.

Ouvrez, __avec Notepad++__, le fichier [pokemon_stats_1.csv](./assets/pokemon_stats_1.csv)

Voici un extrait des données de ce fichier :

```plaintext
name;classification;attack;defense
Swablu;Cotton Bird Pokémon;40;60
Budew;Bud Pokémon;30;35
Minun;Cheering Pokémon;40;50
Metapod;Cocoon Pokémon;20;55
Chikorita;Leaf Pokémon;49;65
Pinsir;Stagbeetle Pokémon;155;120
Cinccino;Scarf Pokémon;95;60
Elgyem;Cerebral Pokémon;55;55
Foongus;Mushroom Pokémon;55;45
Ariados;Long Leg Pokémon;90;70
Aipom;Long Tail Pokémon;70;55
Accelgor;Shell Out Pokémon;70;40
Dialga;Temporal Pokémon;120;120
Starly;Starling Pokémon;55;30
Ledyba;Five Star Pokémon;20;30
Politoed;Frog Pokémon;75;75
Dodrio;Triple Bird Pokémon;110;70
Aggron;Iron Armor Pokémon;140;230
Mankey;Pig Monkey Pokémon;80;35
```

Vous l'aviez deviné, il s'agit d'une base de données au sujet des Pokémons !

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/269px-International_Pok%C3%A9mon_logo.svg.png)

_Source : Wikipedia_

## 2. Attendus

Ce TP a pour objectif d'analyser les données de combats et morphologiques des Pokemon, via la manipulation de fichiers CSV.

Vous devez créer un module `pokemon.py` contenant les blocs d'instructions, les fonctions adéquates en Python documentées permettant de répondre aux questions ci-dessous.

## 2.1. Statistiques de combats

La base nous donne des informations sur les Pokemon selon plusieurs critères appelés `attributs`, ou `champs` présents dans la première ligne et séparés par des `;`

- `name ` désigne bien sûr le nom des Pokemon.
- `classification` nous précise la classe du Pokémon.
- `attack` sa force d'attaque. Plus elle est élevée, plus le Pokémon est dangereux.
- `defense` son niveau de défense. Plus il est élevé, plus le Pokémon est résistant.

Sous les `attributs` , on trouve leurs `valeurs` elles aussi séparées par un `;`.

__En observant ces données__, pourriez-vous me dire :

1. Combien y-a-t-il de Pokémons ?
2. Quelles sont les caractéristiques de *Comfey* ?
3. Qui a la meilleure défense ?
4. Combien de Pokémons ont une attaque supérieure à 70 ?
5. Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ?
6. Existe-t-il des Pokémons dont l'attaque et la défense son supérieur à 100 ? Si oui, combien ?

Pas facile ... Heureusement, il y a d'autres moyens de visualiser et de traiter ces données.

## 2.2. Statistiques morphologiques

Les statistiques morphologiques se situent dans le fichier [pokemon_stats_2.csv](./assets/pokemon_stats_2.csv).

Complétez votre module `pokemon.py`avec les fonctions et méthodes adéquates pour répondre aux questions suivantes :

1. Quels sont les attributs de cette base ?
2. Déterminer les 5 plus gros Pokémons.
3. Quel est le poids moyen d'un Pokémon ?
4. Combien y-a-t-il de types de Pokémon différents ?

## 2.3. Joindre les 2 bases

On se pose maintenant quelques questions :

1. Les Pokémons les plus gros sont-ils les plus forts ?

2. Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ?

Le problème est que les données nécessaires sont réparties dans deux bases différentes : on a besoin de les joindre pour répondre à nos questions !

#### 2.3.1. Indications

Considérons la première base : Existe-t-il un attribut capable d'identifier de manière unique une ligne de notre base de donnée ? Un tel attribut est appelé __identifiant__, ou __clé primaire__.

Pour joindre nos deux bases, il faudrait qu'elles aient un attribut commun qui soit un identifiant dans au moins une des deux bases. Y-en-a-t-il un ?

Vous allez :

1. Commencer par ouvrir les deux fichiers csv sur lesquels on travaille,
2. Faire correspondre les attributs. Pour cela, triez séparément, les deux tableaux selon l'attribut name : les colonnes correspondent dorénavant !
3. Supprimez les colonnes inutiles pour plus de lisibilité.
4. Répondez maintenant aux questions en utilisant les tris adéquats :
   1. Les Pokémons les plus gros sont-ils les plus forts ?
   2. Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ?