---
title : Les données en table
author : M. BODDAERT
license : CC-BY-NC-SA
---

# Les données en tables

## 1. Contexte

Une des utilisations principales de l’informatique de nos jours est le traitement de quantités importantes de données dans des domaines très variés : 

- un site de commerce en ligne peut avoir à gérer des bases données pour des dizaines de milliers (voire plus) d’articles en vente, de clients, de commandes,
- un hôpital doit pouvoir accéder efficacement à tous les détails de traitements de ses patients, etc. 

Mais si les logiciels de traitement de base de données sont des programmes hautement spécialisés pour effectuer ce genre de tâches le plus efficacement possible, il est facile de mettre en œuvre les opérations de base dans un langage de programmation comme Python.

## 2. Attendus

| Contenus | Capacités attendues |
| :--:|  :-- |
| Indexation de tables | Importer une table depuis un fichier texte tabulé ou un fichier CSV. |
| Recherche dans une table | Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle.|
| Tri d’une table | Trier une table suivant une colonne. |
| Fusion de tables | Construire une nouvelle table en combinant les données de deux tables. |

## 3. Organisation

Ce chapitre se décompose en 3 sections :

- [Section 1 : Gestion de fichiers](./section_1/)
- [Section 2 : Tables en CSV](./section_2/)
- [Section 3 : Étude du Pokédex](./section_3/)