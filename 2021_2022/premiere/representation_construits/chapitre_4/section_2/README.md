---
title : Les tables CSV
author : M. BODDAERT, d'après les travaux d'Éric Rougier et Paul Godard
license : CC-BY-NC-SA
---
# Les tables CSV

## 1. Contexte

L'informatique permet de traiter des quantités d'information très importantes dans des domaines très variés.

Pour s'en convaincre on peut regarder le site [www.data.gouv.fr](https://www.data.gouv.fr).

Encore faut-il que ces données soient organisées pour pouvoir les exploiter.

Plusieurs formats de fichier existent dont le format en tables de type CSV, objet de ce cours.

En terminale NSI, on découvrira les base de données qui sont destinés aussi à traiter un grand nombre de données.

## 2. Définition

CSV signifie **Comma-Separated Values** autrement dit : *Valeurs séparées par des virgules*.

Ce format permet d'organiser les données en table dans un fichier :

- Chaque ligne du fichier correspond à un **enregistrement**,
- Pour chaque ligne, les **descripteurs** (valeurs) sont séparées par des virgules.

![](./assets/table_csv.png)

En réalité les anglo-saxons ont choisi la virgule comme séparateur car leur nombre décimaux sont décrits avec un point (*dot* en anglais).

En France il n'est pas rare d'opter pour le point-virgule, mais d'autres caractères peuvent être utilisés.

La table ci-dessus est donc décrite de la manière suivante :

```txt
Prénom,Nom,Age
Marla,Adrienne,13
Kandy,Meah,23
Nellie,Bresner,32
```

On peut ouvrir les fichiers CSV avec un tableur et traiter les données avec celui-ci.

Exemple avec le fichier [personnes.csv](./assets/personnes.csv)

![](./assets/personnes_csv.png)

Mais pour plus de souplesse dans le traitement des données, on va programmer en Python.

Pour la suite, on utilise une bibliothèques `csv` qui est spécialisée dans le traitement des fichier CSV.

## 3. Utilisation de la bibliothèque CSV

La bibliothèque `csv` permet d'importer et d'exporter des données dans des fichiers `csv`.

### 3.1. Importer des données

```python
import csv

with open('personnes.csv', newline='') as fichier:
    population = csv.reader(fichier, delimiter=',')
    for personne in population:
        print(personne)
```

Dans ce cas, les résultats sont stockés sous forme d’un tableau de tableau. On a par exemple :

- La lecture de la 1ère ligne qui correspond à l'entête
  `>>> population[0]`
  `['Prénom', 'Nom', 'Age']`
- La lecture de la 2ème ligne qui correspond au 1er enregistrement.
  `>>> population[1]`
  `['Marla', 'Adrienne', '13']`

Ce mode de lecture des données oblige à connaître l'ordre des champs dans les enregistrements. De même que tout champs d'un fichier CSV est de type `str`.

#### 3.1.1. À Faire

1. Écrire une fonction `convertir` qui prend en paramètre les données d'une personne, sous la forme d'un tableau, et renvoie un dictionnaire où les clés sont les descripteurs des champs et les données sont du type adéquat.
2. Écrire une fonction `importer` qui prend en paramètre un nom de fichier csv et renvoie un tableau de personnes, où chaque personne est représentée sous la forme d'un dictionnaire.

### 3.2. Exploitation des données

#### 3.2.1 Tri des données

Pour exploiter les données, il peut être intéressant de les trier. Une utilisation possible est l’obtention du classement des entrées selon tel ou tel critère.

Le langage python définit 2 méthodes de tri : 

- `sort` qui trie en place (qui modifie la liste d'origine),
- `sorted` qui renvoie une nouvelle liste sans modifier la liste d'origine`

Il faut définir dans les 2 cas les critères de tris à l'aide de l'argument `key`.

Par exemple, si l'on veut trier les personnes par age, on doit spécifier la clé àge`. Pur cela, on définit une fonction appropriée:

```python
def cle_age(personne):
  '''
  Renvoie l'age d'une personne
  :param personne: (dict) Une personne
  :return: (int) l'age de la personne
  '''
  return personne['age']
```

Ainsi, pour classer les personnes de la plus âgée à la plus jeune, on effectue :

```python
population.sort(key=cle_age, reverse=True)
```

##### 3.2.1.1. Généralités

La spécification de la méthode `sort` est la suivante :

- Entrées :
  - __key__ : (function) Une fonction qui renvoie une valeur correspondant à une propriété d'un élément de la collection,
  - __reverse__ : (bool) Indique si la collection doit être triée de amnière croissante ou décroissante (Par défaut, reverse = True)
- Sortie : Aucune
- Effet de bord : La collection est triée selon la fonction de comparaison et dans l'ordre indiqué

##### 3.2.1.2. À Faire

1. Écrire un bloc d'instructions permettant de trier les personnes par nom dans l'ordre alphabétique,
2. Écrire un bloc d'instructions permettant de trier les personnes par nom dans l'ordre inverse de la question 1,
3. Écrire un bloc d'instructions permettant de trier les personnes par taille du prénom,
4. Écrire un bloc d'instructions permettant d'obtenir les 5 personnes les plus jeunes.

#### 3.2.2 Filtre des données

Le langage python définit la méthode `filter` .

Il faut définir le critère de sélection à l'aide d'un prédicat.

Par exemple, pour obtenir les personnes de moins d'un certain age :

```python
def est_majeur(personne):
  '''
  Indique si la personne est majeure ou non
  :param personne: (dict) Une personne
  :return: (bool) True si la personne est majeure, False sinon
  '''
  return cle_age(personne) >= 18
```

Ainsi, pour obtenir un tableau des personnes majeures, on effectue :

```python
list(filter(est_majeur, population))
```

La méthode `filter` applique le prédicat sur chaque élément du tableau.

Si cet élément satisfait la condition du prédicat, il est ajouté au nouveau tableau renvoyé par la méthode. 

##### 3.2.2.1. Généralités

La spécification de la méthode `filter` est la suivante :

- Entrées :
  - __predicat__ : (function) Une fonction qui renvoie True si l'élément satisfait une condition,
  - __collection__ : (iterable) La collection à filtrer
- Sortie : (list) Un nouveau tableau d'éléments filtrés de la collection, i.e ceux pour lesquels le prédicat est True

##### 3.2.2.2. À Faire

1. Écrire un bloc d'instructions permettant de filtrer les personnes ayant pour prénom Linnie,
2. Écrire un bloc d'instructions permettant de filtrer les personnes ayant entre 25 et 36 ans exclu,
3. Écrire un bloc d'instructions permettant de filtrer les personnes ayant pour initiales MA.
