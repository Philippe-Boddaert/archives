---
title : Gestion des fichiers
author : M. BODDAERT, d'après les travaux d'Éric Rougier et Paul Godard
license : CC-BY-NC-SA
---

# Gestion des fichiers

## 1. Contexte

Jusqu'à présent nous avons manipulé des données volatiles, i.e la durée de vie des variables et leur valeur est circonscrite à la durée d'exécution d'un programme.

```python
compteur = 0

for i in range(5):
  compteur += 1

print(compteur)
```

À la fin de l'exécution de ce programme, la variable compteur est "détruite" par l'interpréteur Python.

Question : __Comment garder en mémoire _persistante_ des valeurs pour une utilisation future ?__

## 2. Définition

> Un __fichier informatique__ est au sens commun, une collection, un ensemble de données numériques réunies sous un même nom, enregistrées sur un support de stockage permanent, appelé mémoire de masse (disque dur, clé usb, un CD-ROM ou le cloud)

> Techniquement, tout fichier numérique est constitué d'une séquence d'octets codant des caractères.

## 3. Ouvrir un fichier

La fonction `open(nom_fichier, mode, encodage)` permet d'ouvrir un fichier en lecture ou en écriture.

| Valeur | Mode d'accès possibles                                       |
| ------ | ------------------------------------------------------------ |
| **r**  | Ouverture en lecture seulement                               |
| **w**  | Ouverture en écriture seulement (la fonction crée le fichier s'il n'existe pas et l’écrase s’il existe...) |
| **a**  | Ouverture en écriture seulement avec ajout du contenu à la fin du fichier (la fonction crée le fichier s'il n'existe pas) |
| **+**  | Lorsque l'on rajoute **+ derrière r, w** ou **a** on rajoute la lecture **et** l'écriture à leur mode d'ouverture |

__À Faire__ : Après avoir téléchargé le fichier [cigale.txt](./assets/cigale.txt), tester le bloc d'instructions suivant :

```python
# Open a file
fichier = open("cigale.txt", "r", encoding="utf-8")     # fo : file_object
print ("Nom du fichier : ", fichier.name)           		# affiche "Name of the file: foo.csv"
print ("Fermé ou Ouvert : ", fichier.closed)           	# affiche "Closed or not : False"
print ("Mode d'ouverture : ", fichier.mode)             # affiche "Opening mode : w"
fichier.close()                                      		# fermeture du fichier
print ("Fermé ou Ouvert : ", fichier.closed)           	# affiche "Closed or not : True"
```

L'instruction `fichier.close()` est obligatoire car elle permet de spécifier au système d'exploitation que le fichier est "libre", i.e utilisable par un autre programme.

Il n'est pas rare d'oublier cette instruction, Python propose et privilègie une autre écriture :

```python
with open("cigale.txt", "r", encoding="utf-8") as fichier:
	print ("Nom du fichier : ", fichier.name)           		# affiche "Name of the file: foo.csv"
	print ("Fermé ou Ouvert : ", fichier.closed)           	# affiche "Closed or not : False"
	print ("Mode d'ouverture : ", fichier.mode)             # affiche "Opening mode : w"
```

## 4. Lire un fichier

La méthode `readline()` permet de lire le fichier __séquentiellement__.

### 4.1. Lire une ligne

__À Faire__ : Tester le bloc d'instructions suivant

```python
with open("cigale.txt", "r", encoding="utf-8") as fichier:
	premiere_ligne = fichier.readline()
	print(premiere_ligne)
  
	deuxieme_ligne = fichier.readline()
	print(deuxieme_ligne)
  
	troisieme_ligne = fichier.readline()
	print(troisieme_ligne)
```

### 4.2. Lire ligne par ligne

__À Faire__ : Tester le bloc d'instructions suivant

```python
with open("cigale.txt", "r", encoding="utf-8") as fichier:
	ligne = fichier.readline()
	while ligne != '':
		print(ligne)
		ligne = fichier.readline()
```

### 4.3. Lire l'ensemble des lignes

__À Faire__ : Tester le bloc d'instructions suivant

```python
with open("cigale.txt", "r", encoding="utf-8") as fichier:
	lignes = fichier.readlines()
	print(lignes)
```

### 4.4. Bilan

1. Quelles différences constatez-vous entre les modes de lecture ?
2. Quelles limites / contraintes identifiez-vous ?

## 5. Écrire dans un fichier

La méthode `write(str)` permet d’écrire une chaine de caractère dans un fichier ouvert.

### 5.1. Écrire une ligne

__À Faire__ : Tester le bloc d'instructions suivant

```python
with open("bonjour.txt", "w", encoding="utf-8") as fichier:
	fichier.write("Hello World !")
```

### 5.2. À Faire

1. Soit la fable _Le Corbeau et le Renard_, représenté sous la forme d'un tuple de chaine de caractères, écrire un bloc d'instructions permettant de sauvegarder le texte dans un fichier nommé `corbeau.txt` :

```python
fable = ("Maître corbeau, sur un arbre perché",
"Tenait en son bec un fromage.",
"Maître renard, par l’odeur alléché,",
"Lui tint à peu près ce langage :",
"« Hé ! bonjour, Monsieur du Corbeau.",
"Que vous êtes joli ! que vous me semblez beau !",
"Sans mentir, si votre ramage",
"Se rapporte à votre plumage,",
"Vous êtes le phénix des hôtes de ces bois. »",
"À ces mots, le corbeau ne se sent pas de joie ;",
"Et pour montrer sa belle voix,",
"Il ouvre un large bec, laisse tomber sa proie.",
"Le renard s’en saisit, et dit : « Mon bon monsieur,",
"Apprenez que tout flatteur",
"Vit aux dépens de celui qui l’écoute.",
"Cette leçon vaut bien un fromage sans doute. »",
"Le corbeau honteux et confus,",
"Jura, mais un peu tard, qu’on ne l’y prendrait plus.",
"",
"Extrait du Livre 1 des fables de Jean de La Fontaine")
```

2. La fable _Le loup et l'Agneau_ est sauvegardé dans le fichier [loup.txt](./assets/loup.txt), j'ai cependant oublié la mention `Extrait du Livre 1 des fables de Jean de La Fontaine`. Écrire un bloc d'instructions permettant d'ajouter cette mention à la fin du fichier.

3. _L'hiver approchant, le rhume prenant, les N en D, de même pour le T, les M en B, idem pour le P_. Écrire une procédure `texte_enrhume` qui prend en paramètre un `nom` de fichier en paramètre et a pour effet de bord de créer un fichier dans lequel l'ensemble des N et T sont remplacés par D et l'ensemble des M et P sont remplacés par B.
