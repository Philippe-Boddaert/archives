# Représentation de types construits

À partir des __types de base__ se constituent des __types construits__ permettant de mémoriser et manipuler des données plus nombreuses : les tableaux, les tuples et les dictionnaires.

![](https://cdn.pixabay.com/photo/2017/01/15/14/45/duplo-1981724_960_720.jpg)

_Source : Pixabay_

Les types __conteneurs__ (ou types construits) de ce chapitre ont pour nature de stocker des __collections__ d'objets, i.e un ensemble de valeurs d'un type de base.

Cette séquence se décompose en 3 chapitres :

- [Chapitre 1 : Les Tableaux](./chapitre_1/)
- [Chapitre 2 : Les P-uplets (ou tuples)](./chapitre_2/)
- [Chapitre 3 : Les Dictionnaires](./chapitre_3/)
