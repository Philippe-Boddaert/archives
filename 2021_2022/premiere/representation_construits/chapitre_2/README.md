# Représentation des données : Les P-uplets (ou Tuples)

## 1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| p-uplets.<br />p-uplets nommés | Écrire une fonction renvoyant un p-uplet de valeurs. |

## 2. Définition

> Les __tuples__ sont des séquences qui permettent de regrouper plusieurs informations sous un seul nom.
> 
> Les termes __tuple__ ou __p-uplet__ sont utilisés indifféremment pour désigner cette généralisation du _couple, triplet, quadruplet_ ...

## 3. Création d'un tuple

> Les valeurs de la collection sont entourées par des parenthèses `()`, leurs éléments sont séparés par des virgules.

Le tuple suivant contient les coordonnées GPS du centre de la ville de Nice :

```python
>>> coordonnees = (43.7101728, 7.2619532)
```

__À Faire__

1. Quel est le type de la variable `coordonnees`?
2. Comment afficher les valeurs du tuple ?

## 4. Longueur d'un tuple

Il est possible d'accéder facilement à la longueur du tuple gràce à la fonction `len`.

```python
>>> prenoms = ("Thomas", "Églantine", "Omar", "Kelly", "Vittorio")
>>> len(prenoms)
5
```

## 5. Accès aux éléments d'un tuple

- Chaque élément a une position précise dans le conteneur, comme nous l'avons vu pour les chaînes de caractères (*str*).
- On appelle cette position ___indice___, c'est cet indice qui nous permet d'accéder à un élément en particulier.
- Le premier élément d'un tuple a pour indice **0**.

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> personne = ('Guido', 'Van Rossum', '31/01/1956')
>>> personne[0]
?
>>> personne[2]
?
>>> personne[4]
?
```

### 5.1. Accès aux tranches

On peut désigner une **tranche** en indiquant l'indice du premier élèment, ainsi que celui du dernier ***non inclus*** :

```py
>>> personne = ('Guido', 'Van Rossum', '31/01/1956')
>>> personne[0:2]
[42, 37, 41]
```

## 6. Ajout, modification et suppression d'éléments

Les tuples sont __immutables__, i.e il est impossible d'ajouter, de modifier ou de supprimer ses éléments. 

Les valeurs des éléments et la longueur du tuple sont __statiques__, i.e défini une et une seule fois à la création du tuple.

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> pointures =  (42, 37, 41, 42, 43)
>>> pointures[2] = 44
>>> print(pointures) 
?
>>> pointures.append(39)
>>> print(pointures) 
?
>>> del pointures[2]
>>> print(pointures) 
?
```

## 7. Parcours d'un tuple

Lorsque l'on veut répéter une opération pour chaque élément d'un tuple $`t`$, il est possible de parcourir le tuple avec une boucle bornée. Cette boucle peut itérer sur les __indices__ des éléments ou directement sur les __valeurs__ des éléments.

### 7.1. Parcours par indice

La boucle suivante énumère les indices `i` du tuple `pointures`, les éléments correspondants étant récupérés via l'expression `pointures[i]`.

```python
pointures = (42, 37, 41, 42, 43)
for i in range(len(pointures)): # on utilise la fonction len
    print("pointure de l'élève à l'indice", i, ":", pointures[i])
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%2842,%2037,%2041,%2042,%2043%29%0Afor%20i%20in%20range%28len%28pointures%29%29%3A%20%23%20on%20utilise%20la%20fonction%20len%0A%20%20%20%20print%28%22pointure%20de%20l'%C3%A9l%C3%A8ve%20%C3%A0%20l'indice%22,%20i,%20%22%3A%22,%20pointures%5Bi%5D%29&cumulative=true&curInstr=7&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 7.2. Parcours par valeur

La boucle suivante énumère quant à elle, les différents éléments `e` du tuple : elle effectue un tour de boucle pour chaque élément de `pointures` et l'élément inspecté à un tour donné est associé à la variable `e`.

```python
pointures = (43, 42, 41, 42, 37)
for e in pointures: 
    print("pointure de l'élève :", e)
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%2843,%2042,%2041,%2042,%2037%29%0Afor%20e%20in%20pointures%3A%20%0A%20%20%20%20print%28%22pointure%20de%20l'%C3%A9l%C3%A8ve%20%3A%22,%20e%29&cumulative=true&curInstr=7&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 7.3. Remarques

- On utilisera l'un ou l'autre des parcours selon le besoin. Si on a besoin de connaître l'indice auquel on se trouve à chaque tour de boucle il faudra opter pour le parcours par indice, sinon le parcours par valeur est à privilégier.
- Il est possible de ne parcourir qu'une partie du tableau. Pour cela, il faut utiliser le parcours par indice en indiquant dans la fonction `range` les indices souhaités. Par exemple, pour parcourir seulement les éléments compris entre l'index 1 et l'index 3 (inclus), on écrit :

```python
pointures = (15, 13, 8, 18, 10)
for i in range(1, 4):
    print(pointures[i])
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%2815,%2013,%208,%2018,%2010%29%0Afor%20i%20in%20range%281,%204%29%3A%0A%20%20%20%20print%28pointures%5Bi%5D%29&cumulative=true&curInstr=8&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

## 8. Utilisation courante

### 8.1. Déstructuration

Il peut être *pratique* de récupérer les valeurs stockées dans le tuple dans des variables de type simple : on parle de **déstructuration**.

```python
t = (1, 2, 5)
a, b, c = t
print("t=", t)
print("a=", a)
print("b=", b)
print("c=", c)
```

### 8.2. Permutation de variables

Habituellement, lorsque l'on souhaite pemuter deux variables, il est nécessaire de passer par une troisième variable auxiliaire. Grâce aux tuples, la permutation peut se faire en une instruction.

```python
a = 3
b = 2
print("a=",a, "b=", b)
print("Permutation")
a, b = (b, a)
print("a=",a, "b=", b)
```