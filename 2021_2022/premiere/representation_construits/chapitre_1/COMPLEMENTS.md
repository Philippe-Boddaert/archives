# Les Tableaux - Compléments

## 1. Tableau donné en compréhension

Plutôt que de remplir un tableau par énumération de ces éléments dans une boucle, on peut définir des tableaux en compréhension, c'est-à-dire des tableaux dont le contenu est défini par une expression.

### 1.1. Création par une boucle classique

On commence par créer un tableau vide, puis, on ajoute grâce à une boucle les éléments un à un grâce à la méthode append().

Voici par exemple comment créer un tableau les 10 premiers entiers naturels.

```python
entiers = []
for n in range(10):
	entiers.append(n)
print(entiers)
```

### 1.2. Création par compréhension

Cette construction syntaxique offre des avantages de lisibilité et de concision et se rapproche de la notation utilisée en mathématiques : $`S=\{n | n\in {\mathbb{N}}, n<10\}`$

```python
entiers = [ n for n in range(10) ]
```

Il est possible d'ajouter des conditions, exemple ici on crée un tableau des 10 premiers entiers naturels pairs.

```python
entiers = [ n for n in range(10) if n % 2 == 0]
```

### 1.3. Exercices

#### Exercice 1

1. Tester le script suivant :

```python
#création de tableau vide
carre=[]
#ajout de 3 valeurs
carre.append(1*1)
carre.append(2*2)
carre.append(3*3)
#affichage du tableau
print(carre)
```

2. Écrire un bloc d'instructions équivalent en utilisant la construction par compréhension

#### Exercice 2

1. Tester le script suivant :

```python
tableau = [x*4 for x in range(2,6)]
print (tableau)
```

2. Écrire un bloc d'instructions équivalent en utilisant la construction par une boucle bornée.

#### Exercice 3

1. Quel est le contenu du tableau associé à mon_tab après l'exécution du programme ci-dessous ?

```python
l = [1, 7, 9, 15, 5, 20, 10, 8]
mon_tab = []
for p in l:
	if p > 10:
		mon_tab = mon_tab + [p]
```

2. Écrire un bloc d'instructions équivalent en utilisant la construction par compréhension

#### Exercice 4

Soit $`S`$ l'ensemble des 13 premières puissances de 2, défini par : $`S=\{2^n ∣ n\in \mathbb{N},n<13\}`$.

1. Écrire la création par compréhension du tableau $`S`$ ,
2. Écrire la création par compréhension du tableau $`S'`$ correspondant à l'ensemble des puissances 2 de $`S`$ impairs.

## 2. Tableau à 2 dimensions : les Matrices

```math
\begin{pmatrix}
2 & 4 & 3 \\
1 & 8 & 6 \\
5 & 7 & 9
\end{pmatrix}
```

En python, une matrice est un *tableau de tableaux*.

```python
matrice = [[2,4,3],[1,8,6],[5,7,9]]
```

Il est possible de visualiser la structure créée en mémoire avec [Python Tutor](https://pythontutor.com/visualize.html#code=matrice%20%3D%20%5B%5B2,4,3%5D,%5B1,8,6%5D,%5B5,7,9%5D%5D&cumulative=true&curInstr=1&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 2.1. Accès aux éléments par indice

Pour accéder à un élément d'une matrice, s'agissant d'un tableau de tableaux, on utilisera deux __indices__ :

- L'indice du tableau contenant l'élément (Par convention `i`)
- L'indice de l'élément dans le tableau sélectionné précédemment (Par convention `j`)

On accède donc à l'élément situé en ligne **i** et colonne **j** par :

```pyth
 matrice[i][j]
```

Par exemple si l'on reprend le tableau ci dessus, on accède au deuxième élément de la troisième ligne :

```python
matrice[2][1]
7
```

__À Faire__

Soit la matrice `m = [["a","b"],["c","d"],["e","f"],["g","h"]]`

1. Comment accéder à l'élément "e" ?
2. Quelle est la valeur de `m[1][1]` ? 
3. Quelle est la valeur de `m[0][2]` ?
4. Quelle est la valeur de `len(m)`? `len(m[0])`?

### 2.2. Accès aux éléments par valeur

Comme il s'agit d'une structure imbriquée, nous devons utiliser deux boucles imbriquées.

On peut par exemple itérer sur les valeurs des lignes et des colonnes de chaque ligne.

```python
matrice = [[2,4,3],[1,8,6],[5,7,9]]
for ligne in matrice:
  for valeur in ligne:
    print(valeur)
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=matrice%20%3D%20%5B%5B2,4,3%5D,%5B1,8,6%5D,%5B5,7,9%5D%5D%0Afor%20ligne%20in%20matrice%3A%0A%20%20for%20valeur%20in%20ligne%3A%0A%20%20%20%20print%28valeur%29&cumulative=true&curInstr=20&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 2.3. Création par compréhension

Une grille de jeu est souvent représenté sous la forme d'une matrice.

Exemple pour la grille de morpion

![Tic tac Toe - source wikipedia - GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Tic-tac-toe-game-1.svg/479px-Tic-tac-toe-game-1.svg.png)

_Source : Wikipedia_

La grille est une matrice 3 * 3, au départ vide, dont les valeurs vont successivement prendre la valeur 'x' ou 'o' selon le joueur.

Une implémentation en python possible de création de la grille vide :

```python
grille = [
  [ ' ', ' ', ' '],
	[ ' ', ' ', ' '],
	[ ' ', ' ', ' '],  
]
```

__À Faire__

1. En fonction du _1. Tableau par compréhension_, écrire un instruction permettant de créer une ligne de la grille par compréhension,
2. En déduire l'écriture d" une instruction permettant de créer la grille vide,
3. Que se passe-t-il lorsque l'on simule le coup du joueur 1, à savoir placer un pion en (0, 0) ?

## 3. Gestion de la mémoire

### 3.1. État des lieux

Il est possible de visualiser un tableau comme une suite de cases mémoires contiguës. Cependant, lorsque l'on écrit

```python
t = [1, 2, 3]
```

Cela n'affecte pas 3 cases mémoires à la variable `t` mais l'___adresse mémoire___ du tableau `t`. Cela a beaucoup d'importance en Python, il faut se méfier car il y a des différences avec l'utilisation des types de base.

Par exemple avec des entiers, si on écrit :

```python
a = 5
b = a
a = 2
print(a, b)
```

L'instruction `b = a` va créer une autre case mémoire (que celle de `a`) pour la variable `b` dont la valeur vaut celle de `a` c'est-à-dire 5. Ainsi, lorsque l'on modifie la valeur de `a` à la ligne suivante, celle de `b` n'est pas modifiée puisqu'il s'agit d'une case mémoire différente.

On peut voir cela avec [Python Tutor](https://pythontutor.com/visualize.html#code=a%20%3D%205%0Ab%20%3D%20a%0Aa%20%3D%202%0Aprint%28a,%20b%29&cumulative=true&curInstr=4&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false).

### 3.2. Impacts sur les tableaux

Si on effectue le même genre d'opérations avec des tableaux, on a un comportement différent.

```python
t = [1, 2, 3]
u = t
t[0] = 5
print(t, u)
```

__À Faire__

1. Visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=t%20%3D%20%5B1,%202,%203%5D%0Au%20%3D%20t%0At%5B0%5D%20%3D%205%0Aprint%28t,%20u%29&cumulative=true&curInstr=4&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
2. Que constatez-vous ?

### 3.3. Bilan

L'instruction `u = t` va créer un nouveau tableau `u` qui vaut `t` c'est-à-dire l'adresse mémoire du tableau `t`. 

Autrement dit, les deux tableaux partagent la même adresse mémoire, ils désignent le *même tableau*. 

Ainsi, une modification sur le tableau `t` (ligne 3) sera visible sur le tableau `u` et réciproquement.

En particulier, lorsque l'on passe un tableau en paramètre d'une fonction, celui-ci peut être modifiée par la fonction, ce qui n'est pas le cas pour des variables de base. Cela sera particulièrement intéressant lorsque l'on triera les valeurs d'un tableau (plus tard dans l'année) mais peut aussi être la source d'erreurs pas évidente à détecter.
