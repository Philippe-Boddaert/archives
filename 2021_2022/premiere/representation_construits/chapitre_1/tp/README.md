---
title : Les Drapeaux
author: M. BODDAERT
license: CC-BY-NC-SA
---
# TP : Les Drapeaux

<img src='./assets/france.png'/> 

Fig 1. Drapeau français

## 1. Contexte

Un drapeau peut être vu comme une matrice, i.e un tableau de tableaux, de dimension $`n \times m`$.

Soient les codes couleurs suivants :

| Code | Couleur associée |
| :--: | :----------------: |
| 0    | NOIR             |
| 1    | ROUGE             |
| 2    | VERT             |
| 3    | JAUNE             |
| 4    | BLEU             |
| 5    | MAGENTA             |
| 6    | CYAN             |
| 7    | BLANC             |

Il est dès lors possible de représenter le drapeau français de la Fig.1 , en python, par la matrice $`15 \times 7`$ suivante :

```python
>>> drapeau = [
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1],
  [4, 4, 4, 4, 4, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1]
]
```

En utilisant le formalisme par compréhension :

```python
>>> drapeau = [ [4] * 5 + [7] * 5 + [1] * 5 for _ in range(7)]
```

ou

```python
>>> drapeau = [ [4] * 5 + [7] * 5 + [1] * 5] * 7
```

Ce formalisme compact traduit le fait que le drapeau français est constitué de ***5 cases bleues, 5 cases blanches et 5 cases rouges, et cela sur 7 lignes***.

## 2. Matériel fourni

Le module [paint](./paint.py) dispose de :

- 8 variables : `NOIR, ROUGE, VERT, JAUNE, BLEU, MAGENTA, CYAN, BLANC`, qui permettent de matérialiser les couleurs,
- d'une méthode `dessiner` qui permet d'afficher un drapeau, à partir d'une matrice passée en paramètre, dans la console de l'interpréteur python.

<figure>
  <center><img src='./assets/utilisation_paint.png'/></center>
  <figcaption style='text-align:center;'>Fig 2. Exemple d'utilisation du module paint</figcaption>
</figure>

## 3. Consignes

Vous devez créer un module `drapeau`.

Pour chaque drapeau de la liste fournie en Annexe 1, vous devez :

- Formaliser la matrice modélisant le drapeau,

- Identifier la structure algorithmique,

- Écrire une fonction, ayant pour nom `creer_drapeau_xxx` où `xxx` est le nom du pays, qui permet de créer la matrice modélisant le drapeau.

***N.B : Vous devez utiliser le formalisme par compréhension ou l'utilisation de boucle mais il est interdit d'utiliser l'affectation par valeur, case par case.***

## 4. Annexe 1 : Liste des pays et drapeaux

|  Pays    | Drapeau     | Consigne |
| :--: | :--: | :-- |
| Monaco | <img src='./assets/monaco.png'/>| Le drapeau est de dimension $`15 \times 6`$ |
| Pologne | <img src='./assets/pologne.png'/>| Vous devez utiliser la fonction de création du drapeau de Monaco. |
| Belgique |  <img src='./assets/belgique.png'/> | Le drapeau est de dimension $`15 \times 7`$ |
| Italie | <img src='./assets/irlande.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Guinée | <img src='./assets/guinee.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Mali |<img src='./assets/mali.png'/> | Vous devez utiliser la fonction de création du drapeau de Guinée. |
| Pays-Bas |<img src='./assets/pays_bas.png'/> | Le drapeau est de dimension $`15 \times 6`$ |
| Hongrie | <img src='./assets/hongrie.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Iran | <img src='./assets/iran.png'/>| Vous devez utiliser la fonction de création du drapeau de Hongrie. |
| Suède | <img src='./assets/suede.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Finlande | <img src='./assets/finlande.png'/> | Le drapeau est de dimension $`15 \times 7`$ |
| Suisse | <img src='./assets/suisse.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Botswana | <img src='./assets/botswana.png'/> | Le drapeau est de dimension $`15 \times 7`$ |
| République du Congo | <img src='./assets/republique_congo.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Trinidad et Tobago |<img src='./assets/trinidad.png'/> | Le drapeau est de dimension $`15 \times 7`$ |
| Jamaique |<img src='./assets/jamaique.png'/> | Le drapeau est de dimension $`15 \times 7`$ |
| Japon | <img src='./assets/japon.png'/>| Le drapeau est de dimension $`15 \times 7`$ |
| Palaos | <img src='./assets/palaos.png'/>| Le drapeau est de dimension $`15 \times 7`$ |

## 5. Annexe 2 : Fonctions utiles

### 5.1. Copier un tableau

```python
def copie(tableau):
    '''
    Renvoie une copie du tableau
    :param tableau: (list) un tableau
    :return: (list) une copie du tableau
    :doctest:
        >>> copie([1, 1, 1, 1])
        [1, 1, 1, 1]
        >>> copie([1, 2, 3, 4])
        [1, 2, 3, 4]
        >>> copie([])
        []
    '''
    resultat = []
    for element in tableau:
        resultat.append(element)
    return resultat
```

### 5.2. Inverser un tableau

```python
def inverser(tableau):
    '''
    Inverse les éléments du tableau
    :param tableau: (list) un tableau
    :doctest:
        >>> t = [1, 2, 3]
        >>> inverser(t)
        >>> t
        [3, 2, 1]
        >>> t = ['b', 'o', 'n', 'j', 'o', 'u', 'r']
        >>> inverser(t)
        >>> t
        ['r', 'u', 'o', 'j', 'n', 'o', 'b']
    '''
    debut = 0
    fin = len(tableau) - 1
    
    while debut < fin:
        tableau[debut], tableau[fin] = tableau[fin], tableau[debut]
        debut += 1
        fin -= 1
```
