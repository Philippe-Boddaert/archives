# Représentation des données : Les Tableaux

## 1. Généralités

### 1.1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Tableau indexé, tableau donné en compréhension | - Lire et modifier les éléments d’un tableau grâce à leurs index.<br />- Construire un tableau par compréhension.<br />- Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j].<br />- Itérer sur les éléments d’un tableau. |

### 1.2. Contexte

Je dispose d'un contact chez Nike pour obtenir des nouvelles paires de Sneakers à un prix défiant toute concurrence pour mes élèves.

Pour cela, j'ai besoin de vos pointures... 

__À Faire__ : Quel bloc d'instructions permet de vous demander et stocker l'ensemble de vos pointures ?

## 2. Définition

> Un __tableau__ est une __séquence ordonnée__ qui permet de contenir d'autres objets de n'importe quel type. On peut donc avoir des tableaux de nombres entiers, des tableaux de flottants, des tableaux de chaînes de caractères, etc. 
> 
> Autrement dit, un __tableau__ permet de stocker __plusieurs variables__ dans une seule variable et d'y accéder ensuite facilement.

## 3. Création d'un tableau

> Les valeurs de la collection sont entourées par des crochets `[]`, leurs éléments sont séparés par des virgules.

```python
>>> pointures = [42, 37, 41, 42, 43]
```

Il est possible de créer un tableau vide. Dans ce cas, aucune valeur du tableau n'est définie :

```python
>>> pointures = []
```

__À Faire__

1. Quel est le type de la variable `pointures`?
2. Comment afficher les valeurs du tableau ?

## 4. Longueur d'un tableau

Il est possible d'accéder facilement à la longueur du tableau grâce à la fonction `len`.

```python
>>> prenoms = ["Thomas", "Églantine", "Omar", "Kelly", "Vittorio"]
>>> len(prenoms)
5
```

## 5. Accès aux éléments d'un tableau

- Chaque élément a une position précise dans le conteneur, comme nous l'avons vu pour les chaînes de caractères (*str*).
- On appelle cette position ___indice___, c'est cet indice qui nous permet d'accéder à un élément en particulier.
- Le premier élément d'un tableau a pour indice **0**.

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> pointures = [42, 37, 41, 42, 43]
>>> pointures[0]
?
>>> pointures[3]
?
>>> pointures[4]
?
>>> pointures[37]
?
```

### 5.1. Accès aux tranches

On peut désigner une **tranche** en indiquant l'indice du premier élèment, ainsi que celui du dernier ***non inclus*** :

```py
>>> pointures = [42, 37, 41, 42, 43]
>>> pointures[0:3]
[42, 37, 41]
```

## 6. Ajout, modification et suppression d'éléments

Les tableaux sont dits __mutables__, i.e il est possible d'ajouter, de modifier ou de supprimer ses éléments.

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> pointures =  [42, 37, 41, 42, 43]
>>> pointures[2] = 44
>>> print(pointures) 
?
>>> pointures.append(39)
>>> print(pointures) 
?
>>> pointures.pop(2)
>>> print(pointures) 
?
>>> del pointures[2]
>>> print(pointures) 
?
```

## 7. Parcours d'un tableau

Lorsque l'on veut répéter une opération pour chaque élément d'un tableau $`t`$, il est possible de parcourir le tableau avec une boucle bornée. Cette boucle peut itérer sur les __indices__ des éléments ou directement sur les __valeurs__ des éléments.

### 7.1. Parcours par indice

La boucle suivante énumère les indices `i` du tableau `pointures`, les éléments correspondants étant récupérés via l'expression `pointures[i]`.

```python
pointures = [42, 37, 41, 42, 43]
for i in range(len(pointures)): # on utilise la fonction len
    print("pointure de l'élève à l'indice", i, ":", pointures[i])
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%5B42,%2037,%2041,%2042,%2043%5D%0Afor%20i%20in%20range%28len%28pointures%29%29%3A%20%23%20on%20utilise%20la%20fonction%20len%0A%20%20%20%20print%28%22pointure%20de%20l'%C3%A9l%C3%A8ve%20%C3%A0%20l'indice%22,%20i,%20%22%3A%22,%20pointures%5Bi%5D%29&cumulative=true&curInstr=0&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 7.2. Parcours par valeur

La boucle suivante énumère quant à elle, les différents éléments `e` du tableau : elle effectue un tour de boucle pour chaque élément de `pointures` et l'élément inspecté à un tour donné est associé à la variable `e`.

```python
pointures = [43, 42, 41, 42, 37]
for e in pointures: 
    print("pointure de l'élève :", e)
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%5B43,%2042,%2041,%2042,%2037%5D%0Afor%20e%20in%20pointures%3A%20%0A%20%20%20%20print%28%22pointure%20de%20l'%C3%A9l%C3%A8ve%20%3A%22,%20e%29&cumulative=true&curInstr=12&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

### 7.3. Remarques

- On utilisera l'un ou l'autre des parcours selon le besoin. Si on a besoin de connaître l'indice auquel on se trouve à chaque tour de boucle il faudra opter pour le parcours par indice, sinon le parcours par valeur est à privilégier.
- Il est possible de ne parcourir qu'une partie du tableau. Pour cela, il faut utiliser le parcours par indice en indiquant dans la fonction `range` les indices souhaités. Par exemple, pour parcourir seulement les éléments compris entre l'index 1 et l'index 3 (inclus), on écrit :

```python
pointures = [15, 13, 8, 18, 10]
for i in range(1, 4):
    print(pointures[i])
```

Il est possible de visualiser l'exécution avec [Python Tutor](https://pythontutor.com/visualize.html#code=pointures%20%3D%20%5B15,%2013,%208,%2018,%2010%5D%0Afor%20i%20in%20range%281,%204%29%3A%0A%20%20%20%20print%28pointures%5Bi%5D%29&cumulative=true&curInstr=8&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
