# Représentation des données : Les Dictionnaires 

## 1. Généralités

### 1.1. Attendus

| Contenus | Capacités attendues |
| :--: | :-- |
| Dictionnaires par clés et valeurs | Construire une entrée de dictionnaire.<br />Itérer sur les éléments d’un dictionnaire. |

### 1.2. Contexte

Prenons l'exemple d'un répertoire téléhonique. Nous pouvons le mémoriser simplement comme un tableau de tableaux [nom,numéro]

```python
repertoire = [["Paul", 5234],
             ["Emile", 5345],
             ["Victor", 5186],
             ["Rose", 5678],
             ["Hélène", 5432]]
```

Si nous voulons appeler Rose, nous avons deux possibilités avec un tel tableau :

1. Soit il faut savoir que les informations la concernant sont dans le quatrième élément de la liste (ce qui ne semble pas très pratique et réaliste)
```python
print(repertoire[3][1]) # il faut savoir que l'index de Rose est 3
```
2. Soit nous cherchons dans le tableau en partant du premier élément de la liste jusqu'à ce que nous trouvions Rose (ce qui revient à feuilleter son répertoire) : cela nécessite d'utiliser une boucle pour parcourir le tableau.
```python
for personne in repertoire:
    if personne[0] == 'Rose':
        print(personne[1])
```

Vous conviendrez que ce n'est pas pratique pour accéder à son numéro de téléphone. De même, la modification ou l'ajout d'un information nécessiterait de devoir feuilleter tout le répertoire. Il semblerait plus pratique d'associer un nom à un numéro, autrement dit d'associer à une __information__ à une __clé__.

C'est ce que les dictionnaires permettent !

## 2. Définition

> Un dictionnaire est un ensemble **non ordonné** de paires (clé, valeur). À partir d'une clé, on peut alors accéder directement à la valeur qui lui est associée.
>
> Par contre, contrairement aux types de conteneurs précédemment étudiés, on ne peut pas accéder au contenu d'un dictionnaire à l'aide d'un indice : un dictionnaire __n'est pas une séquence__.

## 3. Création d'un dictionnaire

> Les éléments de la collection sont entourées par des accolades `{}`, séparés par une virgule.
>
> La clé et la valeur d'un élément sont séparées par `:`.

```python
repertoire = {"Paul": 5234,
             "Emile": 5345,
             "Victor": 5186,
             "Rose": 5678,
             "Hélène": 5432}
```

Il est possible de créer un dictionnaire vide. Dans ce cas, aucune valeur du dictionnaire n'est définie :

```python
repertoire = {}
```

__À Faire__

1. Quel est le type de la variable `repertoire`?
2. Comment afficher les valeurs du dictionnaire ?

## 4. Taille d'un dictionnaire

Il est possible d'accéder facilement à la taille, i.e le nombre d'éléments du dictionnaire, grâce à la fonction `len`.

```python
>>> repertoire = {"Paul": 5234,
             "Emile": 5345,
             "Victor": 5186,
             "Rose": 5678,
             "Hélène": 5432}
>>> print(len(repertoire))
4
```

## 5. Accès aux éléments d'un dictionnaire

L'**accès** à une valeur d'un dictionnaire se fait par sa clé.

```python
>>> repertoire = {"Paul": 5234,
             "Emile": 5345,
             "Victor": 5186,
             "Rose": 5678,
             "Hélène": 5432}
>>> print(repertoire['Rose'])
5678
```

__À Faire__ :

1. Quel est le résultat de l'instruction `repertoire['ROSE']` ?
2. Quel est le résultat de l'instruction `repertoire['Jacques']`?

## 6. Ajout, modification et suppression d'éléments

Les dictionnaires sont dits __mutables__, i.e il est possible d'ajouter, de modifier ou de supprimer ses éléments.

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> repertoire = {"Paul": 5234,
             "Emile": 5345,
             "Victor": 5186,
             "Rose": 5678,
             "Hélène": 5432}
>>> repertoire["Rose"] = 5142
>>> print(repertoire) 
?
>>> repertoire["Kévin"] = 4231
>>> print(repertoire) 
?
>>> del repertoire["Hélène"]
>>> print(repertoire) 
?
```

N.B : Une __clé__ d'un dictionnaire est unique. Il ne peut y avoir 2 éléments avec la même clé et des valeurs différentes. 

## 7. Parcours d'un dictionnaire

Il est possible de parcourir un dictionnaire de trois manières :

1. parcourir l'ensemble des __clés__ avec la méthode `keys()`,
2. parcourir l'ensemble des __valeurs__ avec la méthode `values()`,
3. parcourir l'ensemble des __paires clés-valeurs__ avec la méthode `items()`.

### 7.1. Parcours par clé

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> for element in repertoire.keys():
    print(element)
```

La variable de boucle correspond successivement aux clés du dictionnaire.

### 7.2. Parcours par valeur

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> for element in repertoire.values():
    print(element)
```

La variable de boucle correspond successivement aux valeurs du dictionnaire.

### 7.3. Parcours par paires clés-valeurs

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> for prenom, numéro in repertoire.items():
    print(prenom, '->', numero)
```

La variable de boucle correspond à un tuple (clé, valeur) des éléments du dictionnaire.

### 7.4 Bilan

Par défaut, l'itération s'effectue sur les __clés__.

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> for element in repertoire:
    print(element)
```

Il est possible d'effectuer un calcul ou une opération sur les valeurs en accédant à l'élément via la clé.

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> for element in repertoire:
    print(repertoire[element] % 2 == 0)
```


## 8 Existence d'un élément dans un dictionnaire

Il est possible d'interroger l'appartenance d'une clé ou d'une valeur dans un dictionnaire grâce à l'opérateur `in`

__À Faire__ : Remplacer les `?` par les valeurs attendues

```python
>>> repertoire = {'Paul': 5234, 'Emile': 5345, 'Victor': 5186, 'Rose': 5678, 'Hélène': 5432}
>>> print('Paul' in repertoire)
?
>>> print('Jacques' in repertoire)
?
>>> print('Paul' in repertoire.keys())
?
>>> print(5432 in repertoire.values())
?
```
